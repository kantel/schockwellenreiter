#title "Google-Flickentag: Android-Update – 20190702"
#maintitle "Google schließt am »Juli-Patchday« wieder Sicherheitslücken in Android"
#prevp "2019070201"
#nextp "2019070203"

<%= html.getLink(imageref("lindakaffee-b"), "Sommerkaffee auf dem Wochenmarkt") %>

## Google schließt am »Juli-Patchday« wieder Sicherheitslücken in Android

Google hat mit seinem monatlichen Sicherheitsupdate für Android (und damit auch auf seinen [Pixel-Geräten](https://source.android.com/security/bulletin/pixel/2019-07-01)) wieder [Sicherheitslücken](https://source.android.com/security/bulletin/2019-07-01) geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen: 01.07.2019, 05.07.2019.

Die Updates werden so nach und nach per OTA *(over the air)* auf Pixel 3, Pixel 3 XL, Pixel 2, Pixel 2 XL, Pixel, Pixel XL verteilt.

Die anderen Hersteller werden wie üblich in Bälde nachziehen, sofern sie überhaupt noch entsprechenden Support leisten. *([[cert]])*

*[[photojoerg]]*