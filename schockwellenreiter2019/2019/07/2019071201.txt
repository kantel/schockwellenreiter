#title "Schau Mama, ein Hundebild! – 20190712"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019071001"
#nextp "2019071301"

<%= html.getLink(imageref("joeyjonyjuuhu-b"), "Joey und Jonny bei JuuHu") %>

**Schau Mama, ein Hundebild!** Denn es ist wieder Freitag und wie jeden Freitag soll Euch ein Hundebild darüber hinwegtrösten, daß es vermutlich die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Diesmal ist es ein Photo, das den kleinen Sheltie mit seinem großen Freund Jonny zeigt, aufgenommen beim [Rally-Obedience](cp^RO)-Turnier bei Jugend und Hund e.V. vor vierzehn Tagen.

Für den zweitägigen Hiatus gibt es eigentlich keine Erklärung, außer, daß mir danach ist und daß Tradition eben Tradition bleiben muß.

Daher: Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*