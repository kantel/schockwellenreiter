#title "Eine Sicherheitslücke im VLC? Oder doch nicht? – 20190724"
#maintitle "Eine schwere Sicherheitslücke im VLC Media Player? Oder doch nicht?"
#prevp "2019072301"
#nextp "2019072402"

<%= html.getLink(imageref("mobilecomputing-b"), "Mobile Computing") %>

## Eine schwere Sicherheitslücke im VLC Media Player? Oder doch nicht?

Vor [zwei Tagen berichtete ich](Security Alert: VLC Media Player – 20190722), daß das *Computer Emergency Response Team* (CERT Bund) des Bundesamtes für Sicherheit in der Informationstechnik eine gefährliche [Sicherheitslücke](https://www.cert-bund.de/advisoryshort/CB-K19-0634) im [VLC Media Player](cp^VLC Media Player) entdeckt haben will. Nun berichtet Golem.de, daß diese Lücke nur ein eher [unbedeutender Fehler sei](https://www.golem.de/news/videolan-eine-vlc-luecke-die-keine-ist-1907-142758.html), der von der Behörde fälschlich als schwere Sicherheitslücke eingestuft wurde.

Mir fehlt das *Know How*, um zu entscheiden, wer von den Kontrahenten Recht hat. Für die Darstellung von Golem.de spricht allerdings, daß der CERT Bund das Gefahrenpotential des Fehlers mittlerweile als »niedrig« eingestuft hat. Ich will keine Empfehlung aussprechen, aber es scheint so, als ob Ihr den VLC Media Player wieder gefahrlos benutzen könnt (auf eigene Gefahr). *[[photogabi]]*