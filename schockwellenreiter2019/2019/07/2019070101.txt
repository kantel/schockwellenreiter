#title "Die Zahlen – 20190701"
#maintitle "Die Zahlen"
#prevp "2019063001"
#nextp "2019070102"

## Die Zahlen

Jeder Monatserste beginnt mit ein paar Zahlen, die hochtrabend manchmal auch *Mediadaten* genannt werden. Im Juni 2019 hatte der *Schockwellenreiter* laut seinem nicht immer zuverlässigen, aber hoffentlich datenschutzkonformen [Neugiertool](cp^Piwik) exakt **4.806 Besucher** mit **9.662 Seitenansichten**. Auch wenn die Exaktheit der Ziffern eine Genauigkeit der Zahlen nur vortäuscht, sind das für solch einen heißen Monat doch recht ansehnliche Werte. Natürlich freue ich mich über jede Besucherin und jeden Besucher und bedanke mich bei allen meinen Leserinnen und Lesern.

😎 &nbsp; *Bleibt mir gewogen!*
