#title "Ach Matomo! – 20190726"
#maintitle "Ach Matomo!"
#prevp "2019072501"
#nextp "2019072602"

<%= html.getLink(imageref("weihnachtensommer2019-b"), "In fünf Monaten ist Weihnachten (schon fast vorbei)") %>

## Ach Matomo!

Vor [drei Wochen](Es ist angerichtet – 20190702) teilte mir mein (hoffentlich!) datenschutzkonformes [Neugiertool](cp^Piwik), das früher Piwik hieß und nun Matomo genannt werden will, mit, daß die Version 3.10.0 zur Installation bereitstehe. Da ich jedoch ein gebranntes Kind bin, was fehlgeschlagene Matomo- (respektive Piwik-) Updates angeht und da keine sicherheitsrelevanten Neuerungen im Changelog aufgeführt wurden, beschloß ich, das Nuller-Update zu ignorieren und auf eine fehlerbereinigte Version zu warten (die so sicher kommt, wie das »Amen« in der Kirche).

Gestern meldete sich der Hersteller wieder und kündigte eine [Version 3.11.0](https://matomo.org/changelog/matomo-3-11-0/) an. Scheiße, schon wieder ein Nuller-Update, war meine erste Reaktion. Aber da in diesem Falle aus Sicherheitsgründen ein Update dringend empfohlen wurde, blieb mir ja nichts anderes übrig, als es einzufahren.

Und was soll ich sagen? Das automatische Update lief schief, mein Matomo-Server brachte nach einem 500er Error (der ist bei Matomo eigentlich normal -- er kommt nach jedem Update) und einem darauf folgenden Neustart nur noch Fehlermeldungen. Also biß ich in den sauren Apfel und entschloß mich zu einem manuellen Update. Das dauert bei meinem [Spielzeugprovider](http://www.strato.de/) leider immer etwa eine Stunde, aber danach lief das Neugiertool wieder wie gewohnt.

Nur befürchte ich, daß in wenigen Tagen eine fehlerbereinigte Einser-Version nachgeschoben wird. Ich habe ja nichts besseres vor, als Software-Updates einzufahren, die dann auch noch fast immer schiefgehen. *[[photogabi]]*

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Vielleicht würden die Updates nicht schief gehen wenn man nicht gleich zwei Versionen überspringt?   
BTW, ich habe Matomo schon entsorgt als es noch Piwik hieß. Es mag zwar (hoffentlich) datenschutzkonform sein, aber ich kam zu dem Schluss, dass kein Tracking datenschutzfreundlicher ist und ich eigentlich nur Zeit verschwende, wenn ich schaue wer wann, von wo und mit welcher Software auf meine Seite gestolpert ist. 😉

*– Martin D.* <%= p("k01") %>

