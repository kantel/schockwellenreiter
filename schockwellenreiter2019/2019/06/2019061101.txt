#title "Security Alert: Multimedia-Software – 20190611"
#maintitle "Security Alert: Multimedia-Software"
#prevp "2019060703"
#nextp "2019061301"


## Security Alert: Multimedia-Software

[[dr]]<%= imageref("richierich") %>[[dd]]

Aufgrund mehrerer kritischer Sicherheitslücken im [Mediaplayer VLC](https://www.videolan.org/) bietet der Hersteller die [korrigierte Version 3.0.7](http://www.jbkempf.com/blog/post/2019/VLC-3.0.7-and-security) an.

Und natürlich dürft Ihr auch wieder Eurer Lieblingsbeschäftigung nachgehen und den Adobe Flash Player aktualisieren. Denn die neuen Versionen des Players

- 32.0.0.207 für Windows und Mac
- 32.0.0.207 für Linux
- 32.0.0.207 für Windows 10 (Microsoft Edge) und
- 32.0.0.207 für Internet Explorer 10 und 11

beheben wieder kritische [Sicherheitslücken](https://helpx.adobe.com/security/products/flash-player/apsb19-30.html). Die neue Version kann entweder über die automatische Updatefunktion oder von [dieser Adresse](http://www.adobe.com/go/getflash) bezogen werden. *([[cert]])*