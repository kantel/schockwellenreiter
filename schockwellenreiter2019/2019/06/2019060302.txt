#title "Security Alert: Airport – 20190603"
#maintitle "Apple liefert Sicherheitsupdate für eingestellte AirPort Basisstationen"
#prevp "2019060301"
#nextp "2019060401"

<%= html.getLink(imageref("schrippenfeuerwehr-b"), "Schrippenfeuerwehr") %>

## Apple liefert Sicherheitsupdate für eingestellte AirPort Basisstationen

Apple hat mit dem [AirPort Base Station Firmware Update 7.9.1](https://support.apple.com/de-de/HT210090) für die nicht mehr hergestellten AirPort Extreme 802.11ac und AirPort Time Capsule 802.11ac mehrere kritische Sicherheitslücken behoben.

Die Installation erfolgt über das AirPort-Dienstprogramm auf dem Mac (im Programme-Ordner unter »Dienstprogramme«). *([[cert]])*