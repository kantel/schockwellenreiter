#title "Google veröffentlicht Version 75 von Chrome – 20190614"
#maintitle "Google veröffentlicht Version 75 seines Browsers Chrome"
#prevp "2019061401"
#nextp "2019061403"

<%= html.getLink(imageref("kastenaufsofa-b"), "Kasten auf Sofa") %>

## Google veröffentlicht Version 75 seines Browsers Chrome

Google schließt mit dem Update auf die Version 75 (75.0.3770.90) eine [Sicherheitslücke](https://chromereleases.googleblog.com/2019/06/stable-channel-update-for-desktop_13.html), die Angreifer unter bestimmten Vorraussetzungen zur Systemkompromittierung mißbrauchen könnten.

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden. *([[cert]])*

*[[photojoerg]]*