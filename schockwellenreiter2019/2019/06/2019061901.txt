#title "Firefox korrigiert Sicherheitslücken – 20190619"
#maintitle "Mozilla veröffentlicht Firefox 67.0.3 und korrigiert Sicherheitslücken"
#prevp "2019061601"
#nextp "2019061902"

<%= html.getLink(imageref("schwarzgelb-b"), "Schwarz-Gelb") %>

## Mozilla veröffentlicht Firefox 67.0.3 und korrigiert Sicherheitslücken

Die Entwickler des [Mozilla Firefox](http://www.mozilla.com/de/firefox/) haben mit dem Update auf 67.0.3 und 60.7.1 ESR [kritische Sicherheitslücken](https://www.mozilla.org/en-US/security/advisories/mfsa2019-18/) behoben.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*

*[[photogabi]]*
