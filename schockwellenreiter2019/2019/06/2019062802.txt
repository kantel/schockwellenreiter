#title "Schau Mama, ein Hundebild! – 20190628"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019062801"
#nextp "2019063001"

<%= html.getLink(imageref("joeyjuuhu2018-b"), "Joey bei Jugend und Hund (2018)") %>

**Schau Mama, ein Hundebild!** Daß das Hundebild diese Woche die kleine Fellkugel auf dem letztjährigen [Rally-Obedience](cp^ro)-Turnier bei Jugend und Hund e.V. zeigt, hat seinen Grund. Denn für das [diesjährige Turnier](http://jugendundhund.de/), das morgen stattfindet, standen wir lange auf der Warteliste und haben dann am Mittwoch doch noch einen Startplatz zugesagt bekommen. Darüber freue ich mich wie Bolle.

Natürlich ist der Startplatz auch wieder ein Grund, daß es die nächsten zwei Tage vermutlich keine oder nur spärliche Updates hier im *Schockwellenreiter* geben wird, denn statt das Internet vollzuschreiben, werden der Sheltie und ich um Punkte kämpfen. Denn wie immer wollen wir, wenn nicht gewinnen, dann wenigstens gut aussehen. Daher drückt uns bitte die Daumen.

Zum Sonntag hin soll es noch einmal richtig heiß werden. Sucht Euch also besser ein schattiges Plätzchen im Biergarten. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *(Photo &copy; 2018: Sascha Harting)*