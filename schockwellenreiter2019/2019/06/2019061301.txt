#title "Microsoft veröffentlicht Update für Office for Mac – 20190613"
#maintitle "Microsoft veröffentlicht Update 16.26.0 für Office for Mac"
#prevp "2019061101"
#nextp "2019061302"

<%= html.getLink(imageref("einsup-b"), "1up") %>

## Microsoft veröffentlicht Update 16.26.0 für Office for Mac

Microsoft liefert mit dem [Update auf 16.26.0 für Office for Mac](https://docs.microsoft.com/en-us/officeupdates/release-notes-office-for-mac) natürlich auch aktuelle Sicherheitskorrekturen.

Sofern Office nicht schon von sich aus darauf hinweist, erhält man die Korrekturen am einfachsten über Microsofts AutoUpdate - in einem beliebigen Office-Programm über Menü: `Hilfe > Auf Updates überprüfen`. *([[cert]])*

*[[photojoerg]]*