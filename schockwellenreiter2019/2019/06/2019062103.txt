#title "Update für eingestellte AirPort Basisstationen - 20190621"
#maintitle "Apple liefert Sicherheitsupdate für eingestellte AirPort Basisstationen"
#prevp "2019062102"
#nextp "2019062104"

## Apple liefert Sicherheitsupdate für eingestellte AirPort Basisstationen

[[dr]]<%= imageref("oldford") %>[[dd]]

Nachdem Apple im Mai mit dem [AirPort Base Station Firmware Update 7.9.1](https://support.apple.com/de-de/HT210090) für die nicht mehr hergestellten AirPort Extreme 802.11ac und AirPort Time Capsule 802.11ac mehrere kritische Sicherheitslücken behoben hatte, folgt nun für die Geräte aus den Baureihen AirPort Express, AirPort Extreme und AirPort Time Capsule mit 802.11n WLAN das [Airport Base Station Firmware Update 7.8.1](https://support.apple.com/kb/DL2008?locale=de_DE).

Die Installation erfolgt über das AirPort-Dienstprogramm auf dem Mac (im Programme-Ordner unter `Dienstprogramme`). *([[cert]])*