#title "Bug im Python-Script? – 20191025"
#maintitle "Bug in Python oder leichtsinnige Programmierung durch die Nutzer?"
#prevp "2019102403"
#nextp "2019102502"

<%= html.getLink(imageref("spreeuferkoepenick-b"), "Spreeufer in Köpenick") %>

## Bug in Python oder leichtsinnige Programmierung?

Da machte in den letzten Tagen die Nachricht von einem angeblichen Bug im Python-Interpreter die Runde, [der unter verschiedenen Betriebssystemen unterschiedliche Ergebnisse präsentierte](https://arstechnica.com/information-technology/2019/10/chemists-discover-cross-platform-python-scripts-not-so-cross-platform/). Unter macOS Mavericks und Windows 10 seien »korrekte« Ergebnisse berechnet worden, aber unter macOS Mojave und Ubuntu gäbe es Abweichungen von bis zu einem Prozent. Dadurch sollen hunderte von wissenschaftlichen Arbeiten mit fehlerhaften Ergebnissen behaftet sein.

[Doch schaut man sich das Problem mal genauer an](https://pubs.acs.org/doi/full/10.1021/acs.orglett.9b03216), ist eher eine nachlässige Programmierung die Ursache dieses »Fehlers«: Denn es geht um das `glob`-Modul, mit dem eine Liste von Dateien erzeugt wird, die einem bestimmten Muster folgen. Doch die [Dokumentation zu diesem Modul](https://docs.python.org/3/library/glob.html) warnt ausdrücklich:

>The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell, although results are returned in arbitrary order. […] Whether or not the results are sorted depends on the file system.

Daher kann man nur konstatieren, daß die Programmierer dieser Studien nicht aufgepaßt haben. Denn eigentlich lernt man im Grundkurs Numerik (zumindest habe ich das so noch im Grundkurs Numerik gelernt), daß es bei komplizierten Berechnungen sehr wohl auf die Reihenfolge der Eingaben ankommen kann. Das `glob`-Modul hätte also gar nicht verwendet werden dürfen.

In die gleiche Kerbe schlägt auch *Philip Williams*, einer der Entdecker dieses angeblichen Bugs:

>The hope is that this paper gets us to talk a bit more about how we treat and view software that we exchange back and forth, […] We somehow naively assume this stuff will work, being experimentalists who don't have a lot of background in computer science.

Dem kann ich mich nur anschließen. *[[photojoerg]]*