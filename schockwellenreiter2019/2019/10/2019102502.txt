#title "Pygame-Tutorial für Anfänger – 20191025"
#maintitle "Pygame-Tutorial für Anfänger"
#prevp "2019102501"
#nextp "2019102503"

## Pygame-Tutorial für Anfänger

<iframe width="852" height="480" src="https://www.youtube.com/embed/FfWpgLFMI7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ich versuche ja immer, zum Freitag ein Video-Tutorial zu finden, mit dem ich mich und Ihr Euch über das Wochenende beschäftigen könnt, falls dieses verregnet wird. Heute habe ich ein über zweistündiges [Pygame](cp^Pygame)-Tutorial im Gepäck, das Euch diese Spielebibliothek beibringt, indem Ihr einen [Space Invaders](https://de.wikipedia.org/wiki/Space_Invaders)-Klon programmiert.

Das Video ist ein Zusammenschnitt einer [siebzehnteiligen Playlist](https://www.youtube.com/watch?v=8GF6O6vNXCc&list=PLhTjy8cBISEo3SzET7Fc3-b4miKWp41yX), die der YouTube-Kanal *buildwithpython* erstellt hat. Ihr könnt Euch das Tutorial also auch häppchenweise reinziehen.