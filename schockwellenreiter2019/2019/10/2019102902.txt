#title "Weiter mit dem mobilen Update-Chaos? – 20191029"
#maintitle "Weiter mit dem mobilen Update-Chaos bei Apple?"
#prevp "2019102901"
#nextp "2019102903"

<%= html.getLink(imageref("buerosesselbuerger-b"), "Bürosessel Bürgerstraße (an Schaltkasten)") %>

## Weiter mit dem mobilen Update-Chaos bei Apple?

Apple versucht weiterhin, das selbstverschuldete, mobile Update-Chaos für iOS und iPadOS zu fixen. So hat die Firma heute das lange erwartete [iOS 13.2](https://support.apple.com/de-de/HT210393#132) und [iPadOS 13.2](https://support.apple.com/de-de/HT210394#132) veröffentlicht und bietet damit die noch fehlenden Funktionalitäten aber auch Fehlerbehebungen und Verbesserungen. Wie üblich wurden auch [Sicherheitskorrekturen](https://support.apple.com/en-us/HT201222) vorgenommen.

Das Update auf iOS/iPadOS 13.2 kann über OTA (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung`) erfolgen. Ihr solltet dabei an ausreichend Akku-Ladung und freien Speicherplatz denken. Eine vorherige Sicherung des Geräts ist wie immer sehr zu empfehlen.

Außerdem hat Apple für die Modelle iPhone 5s, iPhone 6, iPhone 6 Plus, iPad Air, iPad mini 2, iPad mini 3 und den iPod touch der 6. Generation, die kein Update auf iOS/iPadOS 13 mehr erhalten, ein [Sicherheitsupdate auf iOS 12.4.3](https://support.apple.com/en-us/HT209084#1243) verteilt.

Auch dieses Update kann über OTA (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung`) geladen werden. *([[cert]])*

Ich selber bin ja, seitdem ich zum Rentenbeginn mein Diensthandy abgeben mußte, nicht mehr mit mobilen Apple-Geräten unterwegs, aber ich hoffe für Euch da draußen, daß mit diesen Updates Apples mobiles Update-Chaos endlich beseitigt wurde.

*[[photojoerg]]*