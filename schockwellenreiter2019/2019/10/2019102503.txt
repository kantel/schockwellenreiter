#title "Schau Mama, ein Hundebild! – 20191025"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019102502"
#nextp "2019102701"

<%= html.getLink(imageref("frauentogsheltie-b"), "Joey am Frauentog in Köpenick") %>

**Schau Mama, ein Hundebild!** Es gibt ein Hundebild im *Schockwellenreiter* und das bedeutet, daß heute mit hoher Wahrscheinlichkeit Freitag ist und das Wochenende naht. Zwar hat mich [dieses kürzlich vorgestellte Video](Processing (Java) und Sprites – 20191023) inspiriert, auch mal wieder was mit [Processing.py](cp^processingpy) und der Spieleprogrammierung anzustellen, aber auf der anderen Seite versprechen uns die Wetterfrösche für die nächsten Tage das letzte, sonnige Oktoberwochenende.

Daher werde ich wohl eher statt hier unten Programmierversuche anzustellen, meinem Rentnerkeller entfliehen und mit der kleinen Fellkugel durch die Stadt oder das Umland streifen und die letzten Sonnenstrahlen einsammeln. Der Sheltie und ich waren in den letzten Wochen schon viel draußen und haben den Herbst genossen. Das hat sehr viel Spaß gemacht und ich habe auch sehr viele Photos nach Hause gebracht. Erwartet daher für die nächsten zwei Tage entweder keine oder nur sehr wenige Updates hier im <del>Blog</del> Kritzelheft.

Macht es einfach so wie wir: Geht raus und genießt den Herbst. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*