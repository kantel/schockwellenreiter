#title "RubyFrontierDocs – es geht voran – 20191218"
#maintitle "RubyFrontierDocs – es geht voran"
#prevp "2019121701"
#nextp "2019121901"

<%= html.getLink(imageref("rubyfrontierdovsv02-b"), "RubyFrontierDocs v02") %>

## RubyFrontierDocs – es geht voran

Inspiriert durch das gestrige »[Fahrrad für den Geist](HyperCard: Fahrrad für den Geist – 20191217)«-Video habe ich noch ein wenig an meiner Implementierung der [RubyFrontierDocs](RubyFrontierDocs, warum nicht? – 20191215) gebastelt und erste Ergebnisse erzielt.

RubyFrontierDocs soll ja so etwas wie [MkDocs](cp^MkDocs) werden, ein Werkzeug, mit dem man relativ einfach Handbücher und Dokumentationen, die in Markdown geschrieben sind, ins Netz stellen (und eventuell via [Pandoc](cp^Pandoc) auch als Druckerzeugnisse oder Ebooks vertreiben) kann. MkDocs ist mir an einigen Stellen zu unflexibel und darum möchte ich [RubyFrontier](cp^RubyFrontier) aufpeppen, um dieses Ziel zu erreichen.

RubyFrontierDocs soll explizit keine Blog-Software werden (es gibt schon genügend Generatoren für statische Seiten, die Weblogs können), daher habe ich -- auch um das Projekt möglichst einfach zu halten -- auf einen RSS-Feed verzichtet.

Was habe ich bisher angestellt? Da ich RubyFrontier ja schon sehr lange nutze, habe ich natürlich ein Ruby auf meiner Maschine installiert mit sämtlichen Bibliotheken, die RubyFrontier benötigt (zwingend notwendig für dieses Projekt ist eigentlich nur `kramdown`). Dann habe ich mit `:kramdown: true` in der `prefs.yaml` der Software bekanntgemacht, daß sie alle Seiten als Markdown mit `kramdown` übersetzen soll. Da `kramdown` (wie auch John Grubers Markdown) mit den ERB seltsame Dinge anstellt, mußte ich im `pageFilter` das wieder korrigieren. Die Änderungen könnt Ihr in meinem [GitHub-Repo zu diesem Projekt](https://github.com/kantel/rubyfrontierdoc/tree/master/rubyfrontierdoc) in der Datei `pageFilter.rb` nachlesen.

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3955614336&asins=3955614336&linkId=9a98edbb2bed78320427a3d599ff5040&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Dann habe ich viel an einem CSS gebastelt, denn ich möchte natürlich, daß diese Seiten ein *responsives Layout* haben. Ich bin noch nicht ganz durch, aber es sieht schon recht erfolgversprechend aus. Für das responsive Layout habe ich mich bei »[Praxiswissen Responsive Webdesign][a1]« von *Tim Kadlec* bedient (und werde mich auch weiterhin bedienen). Es hat zwar schon einige Jahre auf dem Buckel, die Rezepte darin sollten aber immer noch gültig sein. Außerdem hat es so schöne Hundebilder auf dem Cover und gebraucht kriegt Ihr es bestimmt auch supergünstig.

[a1]: https://www.amazon.de/Praxiswissen-Responsive-Webdesign-oreilly-basics/dp/3955614336/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3KZDDRNKZ4ZJ0&keywords=praxiswissen+responsive+webdesign&qid=1576677007&sprefix=praxiswissen+res,aps,154&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=4d0a5fa2f4af24277e3597c3e13ff33e&language=de_DE

Ein Problem, das ich auch schon seit Anfang dieses Jahres mit meinem <del>Weblog</del> Kritzelheft habe, sind die Links auf die vorherige wie auch auf die nächste Seite. Da die jüngsten Inkarnationen von macOS, die Dateien aus einem Verzeichnis nicht mehr sortiert zurückliefern, funktionieren die von RubyFrontier mitgelieferten Skripte nicht mehr. Ich habe mich hier -- wie auch schon im *Schockwellenreiter* -- mit Direktiven beholfen, die auf die vorherige und nächste Seite verweisen. Diese werden im Template von folgendem ERB-Makro abgearbeitet:

~~~ruby
def nextprevlinks()
  title, path = html.getTitleAndPath(@adrPageTable[:title])
  s = ""
  if  @adrPageTable[:prevp]
    pname = "#{prevp}"
    s << html.getLink(imageref("arrow-left"), pname)
  else
    s << imageref("arrow-left-grey")
  end
  s << "&nbsp;"
  if @adrPageTable[:nextp]
    nname = "#{nextp}"
    s << html.getLink(imageref("arrow-right"), nname)
  else
    s << imageref("arrow-right-grey")
  end
  "<p>#{s}</p>\n"
end
~~~

Die Methode ist *brute force*, aber sie funktioniert.

Das Template habe ich mit derzeit 38 Zeilen relativ einfach gehalten und kann sicher ebenfalls noch ein wenig Aufhübschen vertragen. Ihr findet es [im GitHub-Repo](https://github.com/kantel/rubyfrontierdoc/blob/master/rubyfrontierdoc/%23template.txt) unter dem Dateinamen `#template.txt`.

Im Prinzip kann man damit schon loslegen. Einfach die vollständige GitHub-Repo herunterladen, die `#ftpsite.yaml` an die eigene Installation anpassen, ein paar Seiten erstellen (und nicht vergessen, das Inhaltsverzeichnis in der `#prefs.yaml` zu aktualisieren) und ab geht die Post.

Ich habe während der Arbeit an diesem Projekt übrigens festgestellt, daß so etwas dringend nötig ist. Denn der RubyFrontier-Befehl `New Site` scheint eine GUI-Bibliothek zu benutzen, die nicht 64-Bit-fähig ist. Da ich meine neuen RubyFrontier-Projekte immer manuell zusammengestellt hatte, hatte ich diesen Befehl nie genutzt und daher war mir dies auch nie aufgefallen. Wie dem auch sei, mit *RubyFrontierDocs* hat man eine Vorlage, die immer (und besser) funktioniert, als es der Befehl `New Site` je getan hat.

Das ist erst der Anfang. Ich werde einmal weiter an dem Projekt basteln, bis ich damit zufrieden bin. Außerdem schwebt mir ein Nachfolgeprojekt vor, in dem ich RubyFrontier mit dem [Tufte CSS](https://edwardtufte.github.io/tufte-css/) verheiraten möchte. Ich hatte damit schon [einmal begonnen](http://blog.schockwellenreiter.de/2016/10/2016102801.html), es dann aber aus den Augen verloren. Und wer weiß – wenn ich dann richtig fit in CSS bin, entwickle ich vielleicht auch endlich einen *responsiven Schockwellenreiter*. 🤓 Denn momentan macht mir das Wühlen in CSS und HTML5 richtig Spaß. *Still digging!*