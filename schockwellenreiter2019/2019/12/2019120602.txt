#title "Schau Mama, ein Hundebild! – 201921206"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019120601"
#nextp "2019120603"

<%= html.getLink(imageref("umherum-b"), "Um den Hundeführer herum") %>

**Schau Mama, ein Hundebild!** Dieses traditionelle, freitägliche Hundebild soll Euch nicht nur darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates im *Schockwellenreiter* geben wird, sondern auch darauf vorbereiten, daß in der nächsten Woche die Updates ebenfalls nur sehr spärlich zu erwarten sind. Denn in Ausübung meines kleinen Nebenjobs werde ich zu einem Meeting nach Freiburg (genauer zum [Max-Planck-Institut für ausländisches und internationales Strafrecht](https://www.mpicc.de/de/) -- nein, ich habe nichts verbrochen (zumindest weiß ich (noch) nichts davon)) reisen und es werden vermutlich sehr arbeitsreiche Tage werden. Daher tröstet Euch mit dem Photo der kleinen Fellkugel, das auf dem [Rally-Obedience](cp^ro)-Turnier des VSB Lankwitz im Septemeber dieses Jahres aufgenommen wurde, über die Update-freie Zeit hinweg und lest stattdessen die alten Nachrichten -- es sind schließlich genung davon da.

Leider kann ich Euch nicht mit guten Nachrichten der Wetterfrösche trösten, aber ich werde versuchen, noch ein Video-Tutorial aufzutreiben, das Euch während der Schockwellenreiter-freien Zeit beschäftigen könnte.

Ein schönes Wochenende und eine schöne nächste Woche Euch allen da draußen. Wir lesen uns spätestens am Montag, den 16. Dezember, wieder. *(Photo &copy; 2019: Gerlinde Borger)*