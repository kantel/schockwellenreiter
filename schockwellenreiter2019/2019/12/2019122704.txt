#title "Schau Mama, ein Hundebild! – 20191227"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019122703"
#nextp "2019122901"

<%= html.getLink(imageref("weltkugelsheltie-b"), "Der Sheltie und die Weltkugel") %>

**Schau Mama, ein Hundebild!** Nein, trotz der angestrebten Ruhepause zwischen den Jahren habe ich nicht vergessen, daß heute Freitag und somit ein Hundebild fällig ist. Heute mit einem Photo der kleinen Fellkugel auf dem [Fliegeberg](https://de.wikipedia.org/wiki/Fliegeberg) im Lilienthalpark Berlin-Lichterfelde. Daß ich es eigentlich zwischen den Jahren ruhiger angehen wollte, wißt Ihr ja schon, doch irgendwie halte ich mich nicht wirklich daran. Egal, morgen ist zumindest [Wochenmarkt](https://www.dicke-linda-markt.de/) und am Sonntag ein verspätetes Festtagsbier in der »Runden Ecke« angesagt. Erwartet daher wenig bis gar keine Beiträge hier im *Schockwellenreiter*. Sollte es dennoch welche geben, freut Euch.

Die nächsten zwei Tage soll es einen Kälteeinbruch geben, doch schon am Montag soll die Temperatur wieder nach oben schnellen. Aber »kalt« heißt ja noch lange nicht, daß man keine Hunderunde drehen kann. Nur wegen der zu erwartenden Feuerwerksknallerei in den nächsten Tagen werden wir diese wohl eher nach *jotwede* verlegen, der Sheltie ist nämlich sensibel.

Egal, ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*