#title "TextMate Update – 20191231"
#maintitle "TextMate Update"
#prevp "2019123002"
#nextp "http://blog.schockwellenreiter.de/2020/01/2020010101.html"

<%= html.getLink(imageref("busteriscoming-b"), "Buster is Coming") %>

## TextMate Update

Am letzten Tag des Jahres überraschte mich mein Leib- und Magen-Editor [TextMate](cp^TextMate), mit dem ich nahezu alle meine Arbeiten am Rechner erledige *(isch 'abe nämlisch kein Woeerrrd, dafür aber Markdown und LaTeX)*, mit der Mitteilung, daß seit vorgestern ein neues Update (v2.0.5 und v2.0.6) zum Download bereitsteht. Es ist ein reines Bugfix-Update, das einige der Unzulänglichkeiten von macOS 10.15 (Catalina) umschifft.

Aber das ist nicht das Wesentliche: Ich freue mich besonders, daß das von mir so geliebte Teil trotz der Konkurrenz von [Atom](cp^Atom), Sublime Text und [Visual Studio Code](cp^Visual Studio Code) immer noch weiterentwickelt wird. Denn ich habe es versucht, aber Sublime Text ist kommerziell und mit den anderen beiden Genannten bin ich bisher nicht warmgeworden.

Alle Änderungen seit der Version 2.0 könnt Ihr [hier nachschauen](https://github.com/textmate/textmate/compare/v2.0...v2.0.5). Es sind nicht wenige …

