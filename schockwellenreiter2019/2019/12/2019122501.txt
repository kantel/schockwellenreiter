#title "GitLab Pages statt Amazons S3? – 20191225"
#maintitle "GitLab Pages, eine Alternative zu Amazons S3?"
#prevp "2019122201"
#nextp "2019122502"

## Worknote: GitLab Pages, eine Alternative zu Amazons S3?

<iframe width="852" height="480" src="https://www.youtube.com/embed/cERdbQ-GgOo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Heute früh hat es -- wie mir berichtet wurde -- anscheinend einen Ausfall des *Schockwellenreiters* gegeben. Der Browser meldete einen Proxy Error mit folgender Fehlermeldung:

~~~
Reason: DNS lookup failure for: www.schockwellenreiter.de.s3-website-us-east-1.amazonaws.com
~~~

Das sieht arg nach meinen [Problemen mit der Erstellung einer neuen statischen Seite](Braintrust Query: AWS mit eigenen Domains – 20191219) auf [Amazon S3](cp^Amazon S3) aus, die ich immer noch nicht gelöst bekommen habe. Außerdem sind meine [Processing.py-Seiten](http://py.kantel-chaos-team.de/) aus irgendwelchen Gründen auf Amazon S3 auch nicht mehr erreichbar -- alle anderen Seiten, wie zum Beispiel mein [Wiki](cp^Startseite) funktionieren aber (noch)!

Mich macht es jedoch mißtrauisch. Alle meine Websites liegen auf Amazon S3 und wenn der Dienst ausfällt, bin ich aufgeschmissen. Also Zeit, sich nach Alternativen umzuschauen.

[Meine erste Idee](Digitaler Nachlaß, statische Seiten … 20191107) war [GitHub Pages](https://pages.github.com/), jedoch scheint der Dienst ein paar Restriktionen zu kennen, die ihn für eine Site in der Größe des *Schockwellenreiters* unbrauchbar machen.

Also habe ich mir nun [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) angeschaut. Hier sind die Bedingungen für eine freie Site wesentlich liberaler. Ich werde daher in den nächsten Tagen mal mit diesem Dienst herumspielen und Euch über das Ergebnis informieren.

Dieser informative Beitrag zum [Hosten statischer Seiten auf GitLab Pages](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/) mit einigen Beispielen der notwendigen Konfigurationsdatei wurde im Video erwähnt. *Still digging!*

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Also … wenn es dir zumindest auch darum geht, eine Langzeitarchivierung
deiner Texte sicherzustellen, wäre doch der richtige Weg, die Websites
als Netzpublikationen bei der DNB abzuliefern bzw. anzumelden. Die DNB
wird mit Sicherheit länger bestehen als z.B. archive.org oder
kommerzielle Portale wie GitLab oder wie sie alle heißen. Dein
<a href="http://d-nb.info/gnd/123952654">Namenseintrag</a>
in der GND wird demnächst wahrscheinlich in einen Personeneintrag
umgewandelt. Ich habe mein TeX-Blog schon lange dort angemeldet, es wird
seit 2015 regelmäßig
<a href="http://d-nb.info/1078191190">archiviert</a>.

*– Juergen F.* <%= p("k01") %>

