#title "Asteroids programmieren – 20190814"
#maintitle "Asteroids programmieren in P5.js"
#prevp "2019081401"
#nextp "2019081501"

## Coding Challenge: Asteroids programmieren in P5.js

<iframe width="852" height="480" src="https://www.youtube.com/embed/hacZU523FyM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In dieser zweiteiligen *Coding Challenge*, die *Daniel Shiffman* schon vor knapp drei Jahren veröffentlicht hatte, programmiert er das Konsolenspiel [Asteroids](https://de.wikipedia.org/wiki/Asteroids) von 1979 in [P5.js](cp^p5js), der JavaScript-Version von [Processing](cp^Processing) nach. Im ersten Teil baut er das Schiff und die Asteroiden, während im zweiten Teil die Laserkanone bestückt und eine Kollisionserkennung programmiert wird:

<iframe width="560" height="315" src="https://www.youtube.com/embed/xTTuih7P0c0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1558604200&asins=1558604200&linkId=48a77440cafe6d352ae9b13fa4ddc0e3&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Natürlich hat es einen Grund, warum ich diese alte *Coding Challenge* ausgegraben habe. Denn ich denke, daß sich dieser [Arcade](https://de.wikipedia.org/wiki/Arcade-Spiel)-Klassiker hervorragend dazu eignet, auch in [Pygame Zero](cp^Pygame Zero) implementiert zu werden -- natürlich leicht modernisiert mit ansprechender Graphik (vielleicht [dieser](https://www.kenney.nl/assets/space-shooter-redux) oder [dieser](https://gamedevelopment.tutsplus.com/articles/enjoy-these-totally-free-space-based-shoot-em-up-sprites--gamedev-2368)?) versehen. Und irgendwo liegt in meiner Bibliothek auch noch ein [uraltes Java-KI-Lehrbuch][a1] herum, in dem der Computer mit Hilfe eines [genetischen Algorithmusses](https://de.wikipedia.org/wiki/Evolution%C3%A4rer_Algorithmus) lernt, Asteroids zu spielen. Da müßte doch was zu machen sein …<br clear="all" />


[a1]: https://www.amazon.de/Intelligent-Java-Applications-Internet-Intranets/dp/1558604200/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=Intelligent+Java+Applications&qid=1565789461&s=books-intl-de&sr=1-2&linkCode=ll1&tag=derschockwell-21&linkId=5680fe3d9fda218dcb48bd57d4f61ff4&language=de_DE