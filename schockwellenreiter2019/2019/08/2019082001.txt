#title "Vier kurze (politische) Links – 20190820"
#maintitle "Vier kurze (mehr oder weniger politische) Links"
#prevp "2019081901"
#nextp "2019082002"

<%= html.getLink(imageref("radwegkms-b"), "Radwegzuparker in der Karl-Marx-Straße") %>

## Vier kurze (mehr oder weniger politische) Links

Mit diesem Photo des chronisch zugeparkten Radstreifens in der Neuköllner Karl-Marx-Straße leite ich eine kleine Linkschleuder ein, die sich mehr oder weniger mit politischen Themen beschäftigt:

- Da gehen sie hin, Eure Daten: Durch ein Datenleck sind rund [90.000 Daten](https://futurezone.at/digital-life/90000-mastercard-kunden-von-massivem-datenleck-betroffen/400582475) von Nutzern des Mastercard-Bonusprogramms *Priceless Specials* aus Deutschland im Internet aufgetaucht. Der Zahlungsdienstleister spricht von einem »Problem« und [stoppt das Programm](https://www.golem.de/news/zahlungsdienstleister-mastercard-schliesst-bonusprogramm-nach-datenleck-1908-143310.html).

- (Dr.) Franziska Giffey: [Doktorspiele](https://www.tagesschau.de/inland/giffey-139.html) können der Karriere [schaden](https://www.tagesschau.de/inland/giffey-spd-101.html).

- Gibt es [vegane Nazis](https://hpd.de/artikel/vegane-nazis-17116)? Ja, die gibt es: Die amerikanischen Neu-Arier zum Beispiel stellen Veganismus als »Kennzeichen jedes authentischen Nationalsozialisten« dar. Der wahre, sprich: ethisch motivierte Veganer sei der »Archetyp des noblen arischen Kriegers«, dessen vornehmste Aufgabe darin bestehe, die »Welt von jüdischer Kontrolle zu befreien«. Aber andersherum wird auch ein Schuh draus: »Arschlöcher bleiben Arschlöcher, auch wenn sie sich vegan ernähren.«

- Die gute Nachricht des Tages: Das Bundesverfassungsgericht hält die [Mietpreisbremse für mit dem Grundgesetz vereinbar](https://www.tagesschau.de/inland/verfassungsgericht-mietpreisbremse-101.html) und wies die Klage einer Vermieterin ab. Freßt das, Deutsche Wohnen & Co.

**War sonst noch was?** Ach ja, *Thomas Knüwer* schreibt über die [Zerstörung der Zeitung durch den Youtuber Rezo](https://www.indiskretionehrensache.de/2019/08/rezo-zeitung/): »Die meisten Zeitungen sind Billo-Shit-Unterhaltung.« *D'accord!* Aber ich färbe mir jetzt trotzdem nicht die Haare blau.

*[[photojoerg]]*

