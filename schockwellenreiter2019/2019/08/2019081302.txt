#title "Runway: ML für Kreative (2) – 20190813"
#maintitle "Runway ML: Maschinelles Lernen für Kreative: Die Reise geht weiter"
#prevp "2019081301"
#nextp "2019081303"

## Runway: Maschinelles Lernen für Kreative: Die Reise geht weiter

<iframe width="852" height="480" src="https://www.youtube.com/embed/7btNir5L8Jc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Am Wochenende hatte *Daniel Shiffman* sein zweites Video zu [Runway ML](https://runwayml.com/) veröffentlicht, der Bibliothek, die »Maschinelles Lernen« Kreativen zugänglich machen will. In diesem zeigte er, wie man Runway ML mit [Processing (Java)](cp^Processing) verkoppelt.

Als nächstes soll ein Video folgen, daß Runway ML mit [P5.js](cp^P5.js), der JavaScript-Version von Processing verbandelt. Ich werde die Angelegenheit beobachten und berichten. Zwar bin ich noch nicht vollständig von Runway ML überzeugt (das Geschäftsmodelll bereitet mir leichte Kopfschmerzen), aber dennoch sieht das ganze nach einer spannenden und vielversprechenden Angelgenheit aus. Außerdem scheint es auch ein [Python SDK zu Runway ML](https://github.com/runwayml/model-sdk) zu geben. *Still digging!*