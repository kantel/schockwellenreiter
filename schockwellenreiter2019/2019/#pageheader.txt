<!DOCTYPE html>
<html lang="de">
<head>
	<%= metatags() %>
	<meta name="viewport" content="width=device-width">
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://blog.schockwellenreiter.de/feed/rss.xml" />
	<link rel="icon" href="http://blog.schockwellenreiter.de/favicon.ico" type="image/x-icon" />
	<%= linkstylesheets() %>
	<%= linkjavascripts() %>
	<title><%= maintitle %> – <%= sitetitle %></title>

	<!-- Le styles -->
	<style>
		body {
		padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}
	</style>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
 	<![endif]-->

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  // tracker methods like "setCustomDimension" should be called before "trackPageView"
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.kantel-chaos-team.de/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->

</head>
<%= bodytag() %>