#title "Schau Mama, ein Garten-Sheltie! – 20190426"
#maintitle "Schau Mama, ein Garten-Sheltie!"
#prevp "2019042601"
#nextp "2019042603"

<%= html.getLink(imageref("gartensheltie2019-01-b"), "Garten-Sheltie Frühjahr 2019") %>

**Schau Mama, ein Garten-Sheltie!** Es ist Freitag, die Sonne scheint und das Wochenende naht. Daher soll Euch das Photo der kleinen Fellkugel, wie sie in unserem Gärtchen die Sonne genießt, darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Denn ich möchte wieder viel lesen und schreiben, ein wenig programmieren und natürlich den kleinen Sheltie bespaßen.

Auch wenn die Wetterfrösche einen leichten Temperaturrückgang und weniger Sonne für die nächsten Tage vorhersagen: Laßt Euch nicht verdrießen, der Frühling kommt und niemand kann ihn aufhalten.

Ein schönes Wochenende Euch allen da draueßn. Wir lesen uns spätestens am Montag wieder! *[[photojoerg]]*