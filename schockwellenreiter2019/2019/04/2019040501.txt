#title "Schau Mama, ein Hundebild! – 20190405"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019040402"
#nextp "2019040502"

<%= html.getLink(imageref("rostock2019joey-b"), "Sieger bim RO-Turnier in Rostock") %>

**Schau Mama, ein Hundebild!** Und was für ein Hundebild -- es zeigt die kleine Fellkugel am letzten Sonnabend auf dem [Rally-Obedience](cp^ro)-Turnier in Rostock (wir [berichteten](Schau Mama, ein Hundebild am Donnerstag! – 20190328)), nachdem er trotz unseres Trainingsrückstandes und einer fast halbjährigen Turnierabstinenz auf Anhieb nicht nur eine Qualifikation, sondern auch den dritten Platz in der RO 2 erkämpft hatte. Dafür wurde er mit einer riesigen Schleife belohnt, die er mit <del>Stolz</del> Fassung trägt.

Ja, und wie immer soll Euch das Hundebild über die nächsten zwei Tage hinwegtrösten, in denen es vermutlich keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Ich habe anderes vor, als das Internet vollzuschreiben und der kleine Sheltie will auch bespaßt werden. Denn es ist endlich Frühling und da geht man raus an die frische Luft.

Macht es uns doch einfach nach. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*