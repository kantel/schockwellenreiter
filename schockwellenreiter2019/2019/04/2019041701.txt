#title "Brython und Pyodide: Python im Browser – 20190417"
#maintitle "Brython und Pyodide: Python im Browser"
#prevp "2019041602"
#nextp "2019041801"

<%= html.getLink(imageref("hwgbendastrasse-b"), "HWG Bendastraße") %>

## Brython und Pyodide: Die Schlange im Browser – und das doppelt

Gestern machte mich ein Leser des *Schockwellenreiters* auf [Brython](http://www.brython.info/) (**Br**owser P**ython**) aufmerksam, eine Python-3-Implementierung, die im Browser läuft. Wenn ich die [Installationsanleitung](http://www.brython.info/static_doc/en/install.html) richtig interpretiere, übersetzt Brython die Python-Skripte nach JavaScript und führt diese dann aus. Dazu muß man diese beiden Skripte, die man sich unter anderem [hier herunterladen](https://github.com/brython-dev/brython/releases) kann mit

~~~html
<script type="text/javascript" src="brython.js"></script>
<script type="text/javascript" src="brython_stdlib.js"></script>
~~~

im Browser laden und zum Beispiel mit

~~~html
<body onload="brython()">
~~~

aktivieren, um dann Python-Skripte des Typs

~~~html
<script type="text/python3">
~~~

ausführen zu können. Neben der oben geladenen Standardbibliothek können einmal auch andere (Pure Python) Module geladen werden, allerdings keine Module, die zum Beispiel C-basierte Teile enthalten. Das schließt Module wie [Numpy](cp^Numpy), [SciPy](cp^SciPy), [Matplotlib](cp^Matplotlib) oder [Pandas](cp^pandas) aus (doch dazu unten mehr). Und dann gibt es noch ein »eingebautes« Modul [Browser](http://www.brython.info/static_doc/en/dom_api.html), das den Python-Skripten Zugriffe auf das DOM-API erlaubt.

Brython ist als kompletter JavaScript-Ersatz konzipiert, kann aber auch JavaScript-Objekte nutzen. Über die Geschwindigkeit kann ich noch nichts sagen, aber naturgemäß wird sie etwas langsamer als bei reinen JavaScript-Anwendugen sein. Ich hoffe, daß ich in der nächsten Zeit dazu komme, ein paar Tests zu fahren. Dann werde ich hier weiter berichten.

Brython scheint etwa seit vier bis fünf Jahren (die aktuellen Versionsnummer ist 3.7.x) entwickelt zu werden. Es gibt ein [GitHub-Repo](https://github.com/brython-dev/brython) und neben der [Dokumentation auf der Projektseite](http://www.brython.info/static_doc/en/intro.html?lang=en) eine [Entwickler-Doku bei ReadTheDocs](https://brython.readthedocs.io/en/master/), bei der allerdings momentan das Inhaltsverzeichnis zerschossen ist. Und hier könnt Ihr ein paar [Videos und Vorträge zu Brython](https://github.com/brython-dev/brython/wiki/Brython-videos-and-talks) finden.

Wer jedoch NumPy, Matplotlib, Scipy und Pandas benötigt, der muß auf [Pyodide](https://github.com/iodide-project/pyodide), ein Mozilla-Projekt, zurückgreifen. Dieser Python-Interpreter ist in erster Linie darauf ausgelegt, Data-Science-Anwendungen direkt im Browser auszuführen. Pyodide setzt auf emscripten und WebAssembly und arbeitet mit verbreiteten Python-Libraries wie NumPy und Matplotlib zusammen. Die Installation ist allerdings nicht trivial und das Projekt ist auch noch sehr jung.

Weitere Details zum Pyodide-Projekt und der Entstehungsphase lassen sich dem [Blogbeitrag auf Mozilla Hacks](https://hacks.mozilla.org/2019/03/standardizing-wasi-a-webassembly-system-interface/) entnehmen. Ein Blick auf die auf GitHub geführten [Packages für Pyodide](https://github.com/iodide-project/pyodide/tree/master/packages) zeigt, welche Libraries für Pyodide verfügbar sind. Zum Ausprobieren bietet sich das in Iodide umgesetzte [Demo-Notebook](https://alpha.iodide.io/notebooks/300/) an. Weitere Details [gibt es bei Tante Heise](https://www.heise.de/developer/meldung/Mozilla-bringt-Python-in-den-Browser-4401345.html).

Ich sehe momentan für mich keinen Anwendungsfall für Pyodide, aber Brython werde ich definitiv mal testen. *Still digging!*

<%= imageref("python-verwirrt") %>
