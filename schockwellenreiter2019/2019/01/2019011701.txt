#title "Update für Office for Mac – 20190117"
#maintitle "Microsoft veröffentlicht Update 16.21.0 für Office for Mac"
#prevp "2019011601"
#nextp "2019011702"

<%= html.getLink(imageref("rainyday-b"), "201901bild15") %>

## Microsoft veröffentlicht Update 16.21.0 für Office for Mac

Microsoft liefert mit dem [Update auf 16.21.0 für Office for Mac](https://docs.microsoft.com/en-us/officeupdates/release-notes-office-for-mac#january-2019-release) natürlich auch aktuelle Sicherheitskorrekturen.

Sofern Office nicht schon von sich aus darauf hinweist, erhält man die Korrekturen am einfachsten über Microsofts AutoUpdate - in einem beliebigen Office-Programm über das Menü: `Hilfe > Auf Updates überprüfen`. *([[cert]])*

*[[photojoerg]]*