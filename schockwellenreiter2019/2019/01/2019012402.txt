#title "Neu in meiner Bibliothek: Codierte Kunst – 20190124"
#maintitle "Neu in meiner Bibliothek: Codierte Kunst"
#prevp "2019012401"
#nextp "2019012501"


## Neu in meiner Bibliothek: Codierte Kunst

[[dr]]<%= html.getLink(imageref("codiertekunst"), "http://codiertekunst.joachim-wedekind.de/") %>[[dd]]

[Hier](Neu in meinem Wiki: Snap! (BYOB) – 20190115) hatte ich berichtet, daß ich mir das Buch »[Codierte Kunst – Programmieren mit Snap](http://codiertekunst.joachim-wedekind.de/)« von *Joachim Wedekind* bestellt hatte. Nun ist es zwischenzeitlich bei mir eingetroffen und ich muß sagen, daß meine Vorfreude berechtigt war. Das Buch ist 370&nbsp;Seiten stark (DIN A 4) und nutzt diese Seiten, um einmal eine Einführung in die Computerkunst und ihre Geschichte zu geben und dann zu zeigen, wie man sie programmiert. Dabei werden die Originale nicht einfach nur nachempfunden (viele der Original-Algorithmen sind nicht dokumentiert oder überliefert), sondern auch neu kombiniert und abgewandelt. Der Autor nennt dies *Recoding & Remixing*.

Die so entstandenen Werke sind fast durchgehend in Farbe und auch großflächig im Buch abgedruckt. Sie sind nicht nur eine Hommage an die jeweiligen Original-Künstler, sondern üben auch einen faszinierenden Reiz aus und können durchaus als eigenständige Kunstwerke durchgehen.

Programmiert wird durchgängig in der freien (AGPL 3.0), visuellen Programmiersprache [Snap!](cp^Snap! (BYOB)), einem [Scratch](cp^Scratch)-Ableger, der über eine, auf [Logo](cp^Logo) basierende Turtle-Graphik verfügt -- schließlich will dieses Buch auch gleichzeitig eine Einführung in Snap! sein (was ebenfalls durchaus gelungen ist). Ich bin kein Freund visueller Programmiersprachen, ich liebe [meinen Texteditor](cp^TextMate), daher bin ich auch nie mit [Nodebox 3](cp^Nodebox 3) warmgeworden. Aber die Knotenschachtel ist ja für hochwertige, graphische 2D-Ausgaben (statische Bilder, aber auch Filme) konzipiert, daher überlege ich, mich mit dem *Recoding & Remixing* mal in der [Nodebox 1](cp^Nodebox) in der Fork von *Karsten Wolf* zu versuchen. Aber natürlich wäre auch [Pythons Schildkröte](https://docs.python.org/3/library/turtle.html) ein Kandidat. Aber da sie aufgrund des Alters der darunterliegenden Tkinter-Bibliothek nur eingeschränkte Farbmöglichkeiten besitzt (und keinen Alpha-Kanal), müßte man die Schildkröte mit [PIL/Pillow](http://blog.schockwellenreiter.de/2016/08/2016081505.html) kombinieren, um ansprechende Ergebnisse zu erzielen. Außerdem gäbe es dann ja auch noch [Processing.py](cp^processingpy), den Python-Mode von [Processing](cp^Processing) und das wäre vermutlich der einfachste Weg. Aber ich liebe nun mal Herausforderungen, daher schaun wir mal …

Das Buch kann ich jedenfalls allen meinen Leserinnen und Lesern empfehlen. Es ist im Selbstverlag erschienen und Ihr könnt es über diese Webseite von *Joachim Wedekind* [bestellen](http://codiertekunst.joachim-wedekind.de/buchbestellung/).

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Vielen Dank für die freundlichen Anmerkungen. Als Freund der Texteditoren kannst du ja mal [GP](https://gpblocks.org/about/) probieren. In dessen *developer mode* kann der Code angezeigt und meines Wissens auch ediert werden. Damit wollten die Macher u.a. zeigen, dass hinter der Fassade der visuellen Programmierung eben ganz normales, echtes Programmieren steckt ;-)

*– Joachim Wedekind* <%= p("k01") %>

