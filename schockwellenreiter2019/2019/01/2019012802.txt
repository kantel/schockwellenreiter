#title "Eine neue Version des Neugiertools – 20190128"
#maintitle "Matomo erfolgreich auf 3.8.1 aktualisiert"
#prevp "2019012801"
#nextp "2019012901"

<%= html.getLink(imageref("mercedes280-b"), "Mercedes 280 SE L") %>

## Matomo erfolgreich auf 3.8.1 aktualisiert

Vor wenigen Tagen meldete das von mir eingesetzte und hoffentlich datenschutzkonforme [Neugiertool](cp^piwik), das früher Piwik hieß und nun Matomo genannt werden will, daß eine neue Version 3.8.0 draußen sei. Bekanntlich aktualisiere ich ja nur in [Notfällen](http://blog.schockwellenreiter.de/2018/11/2018111903.html) auf Nuller-Versionen und so ignorierte ich die Meldung auch dieses Mal. Und ich tat gut daran, denn heute -- nur ein paar Tage später -- gab Matomo ein Update auf 3.8.1 bekannt. Auch wenn ich bei automatischen Updates immer ein ungutes Gefühl habe, ich habe es gewagt.

Es gab den nach einem Update üblichen `Internal Server Error`, der aber nach einem Reload der Seite seltsamerweise immer verschwindet, dieses Mal zweimal, einmal nach dem Einspielen der Software und einmal nach dem Update der Datenbank -- aber nun scheint alles wieder zu funktionieren.

Ach ja, was neu ist, steht in den [Release Notes zur Version 3.8.0](https://matomo.org/changelog/matomo-3-8-0/), Release Notes für 3.8.1 existieren (noch?) nicht, vermutlich wurde aber nur ein übersehener Fehler der Nuller-Version gefixt. *[[photojoerg]]*