#title "Schau Mama, ein Hundebild! – 20190125"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019012502"
#nextp "2019012504"

<%= html.getLink(imageref("platzro-b"), "Rally Obedience: Platz aus der Distanz") %>

**Schau Mama, ein Hundebild!** Denn es ist wieder Freitag und Freitags gibt es ein Hundebild, das Euch darüber hinwegtrösten soll, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Die Fellkugel und ich haben nach einer langen Krankheitspause des Shelties unser [Rally Obedience](cp^ro)-Training wieder aufgenommen, aber leider fällt es witterungsbedingt morgen aus. Das [Photo von uns beiden](Rally Obedience: Platz aus der Distanz) vom RO-Turnier beim HSV Kremmen/Schwante soll uns und Euch daher an bessere Zeiten und schöneres Wetter erinnern.

Ich werde den kleinen Hund natürlich dennoch irgendwie bespaßen und natürlich viel lesen (zum Beispiel über [Codierte Kunst](Neu in meiner Bibliothek: Codierte Kunst – 20190124) und die [Geschichte der Computergraphik](History of Computer Graphics and Animation – 20190123) 🤓) und außerdem wollte ich nicht nur über Programmieren lesen, sondern mich auch selber mal wieder darin versuchen. Das ist genug Grund für eine vorübergehende Funkstille hier im <del>Blog</del> Kritzelheft.

Daher wünsche ich Euch allen da draußen ein schönes Wochenende und wir lesen uns spätestens am Montag wieder. *(Photo &copy; 2018: Sascha Harting)*