#title "Schau Mama, ein Hundebild! – 20191122"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019112102"
#nextp "2019112202"

<%= html.getLink(imageref("sheltietreptow-b"), "Der Sheltie am Bahnhof Treptower Park") %>

**Schau Mama, ein Hundebild!** Das Photo der kleinen Fellkugel auf dem Rückweg von einem unserer letzten Ausflüge soll Euch daran erinnern, daß heute Freitag ist. Und da die Wetterfrösche für die nächsten zwei Tage eher trockenes und sonniges Wetter für Berlin ankündigen, soll es Euch darüber hinwegtrösten, daß es vermutlich vor Montag nur wenige oder keine neuen Updates hier im *Schockwellenreiter* geben wird. Denn der Sheltie und ich werden dann wieder unterwegs sein und Berlin und sein Umland erkunden. Das sind die Freuden eines Rentnerdaseins.

Ich arbeite übrigens an einem Konzept, unsere Ausflüge mit Photos hier im <del>Blog</del> Kritzelheft zu dokumentieren, darum habe ich mich schließlich mit [Leaflet](cp^Leaflet) und [OpenStreetMap](cp^OpenStreetMap) als Kartengrundlage [beschäftigt](Getting Started with Leaflet – 20191119). Ein erster Prototyp sieht schon recht vielversprechend aus. Laßt Euch also überraschen.

Ein schönes Wochenende Euch allen da draußen. Genießt die beiden Tage. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*