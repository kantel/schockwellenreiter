#title "Security Alert: RDP – 20191104"
#maintitle "Sicherheitslücke in der Fernwartungsfunktion RDP wird offenbar zunehmend ausgenutzt"
#prevp "2019110104"
#nextp "2019110501"

<%= html.getLink(imageref("wohnlamdschaftgermania-b"), "Wohnlandschaft Germaniapromenade (1)") %>

## Sicherheitslücke in der Fernwartungsfunktion RDP wird offenbar zunehmend ausgenutzt

Die als »Bluekeep« bezeichnete Sicherheitslücke in der Fernwartungsfunktion *Remote Desktop Services* (RDP) hat das Potential zu ähnlich massiven Angriffen wie seinerzeit 2017 durch *WannaCry*. Und jetzt wurden Sicherheitsforscher offenbar auf mehrere Exploits aufmerksam, die darauf ausgelegt sind, BlueKeep in größerem Stil auszunutzen.

**Zur Erinnerung**: Zum Mai-Patchday [warnte Microsoft vor der als »kritisch« eingestuften Sicherheitslücke](https://support.microsoft.com/de-de/help/4500705/customer-guidance-for-cve-2019-0708) (CVE-2019-0708) in der Fernwartungsfunktion *Remote Desktop Services* (RDP). Angreifer könnten diese Lücke aus der Ferne und ohne Authentifizierung ausnutzen. Windows 8.1 und 10 sind davon nicht betroffen.

Alle anderen Windows Versionen erhielten Patches dafür, sogar die eigentlich nicht mehr unterstützten Produkte Windows XP und Server 2003 (die Patches für diese beiden Versionen müssen manuell heruntergeladen und installiert werden). *([[cert]])*

*[[photojoerg]]*