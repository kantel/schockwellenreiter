#title "How to Setup GitHub Pages – 20191111"
#maintitle "How to Setup GitHub Pages - Website on Github"
#prevp "2019111101"
#nextp "2019111103"

## Tutorial: How to Setup GitHub Pages - Website on Github

<iframe width="852" height="480" src="https://www.youtube.com/embed/FiOgz3nKpgk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Im Nachtrag zu [diesem Beitrag über statische Seiten auf GitHub Pages](Digitaler Nachlaß, statische Seiten … 20191107) flatterte mir obiges Video-Tutorial von *MicrowaveSam* in meinen Feedreader. Es ist zwar schon etwas älter (von Dezember 2014), aber ich hoffe, daß es mir dennoch nützlich sein kann.