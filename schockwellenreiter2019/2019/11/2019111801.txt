#title "Für umme lesen: Raspy-Bücher – 20191118"
#maintitle "Für umme lesen: Raspberry Pi und Pygame Zero Bücher und Magazine"
#prevp "2019111701"
#nextp "2019111901"

## Für umme lesen: Raspberry Pi und Pygame Zero Bücher und Magazine

[[dr]]<%= html.getLink(imageref("retrogamingcover-s"), "Retro Gaming with Raspberry Pi") %>[[dd]]

Die heutigen Empfehlungen sind für die Freunde der [Spieleprogrammierung](cp^Spieleprogrammierung) und für die Liebhaber des Mini-Computerchens [Raspberry Pi](cp^Raspberry Pi) ein absolutes *Must Have*! Das beginnt mit diesem Knüller: Das Buch »[Retro Gaming with Raspberry Pi](https://magpi.raspberrypi.org/books/retro-gaming)« von den Machern des Raspberry Pi Magazins *[MagPi](https://magpi.raspberrypi.org/)* bietet neben Artikeln, wie Ihr den Raspberry Pi in einen klassischen (Spiele-) Computer oder eine Arcade-Automaten oder eine Konsole verwandeln könnt, sechs Nachbauten klassischer Computerspiele, alle in [Pygame Zero](cp^Pygame Zero) programmiert. Und die Projekte, zum Beispiel ein Space-Invaders- oder ein Pacman-Klon sind von *Mark Vanstone* recht anspruchsvoll implementiert und zeigen, daß Pygame Zero über den Status einer Anfänger- und Lernsprache hinausgewachsen ist.

Wem das Herumgehample mit mit dem PDF zu umständlich ist und wer das Buch lieber als Paperback neben seinem Rechner liegen hat -- was sehr zu empfehlen ist --, kann es auch für £ 10 versandkostenfrei [hier bestellen](https://store.rpipress.cc/products/retro-gaming-with-raspberry-pi).

Wer nun auf den Geschmack gekommen ist, sich mit dem Mini-Computerchen intensiver zu befassen, kann sich die dritte Auflage des »[offiziellen Rapsberry Pi Beginners Guide](https://magpi.raspberrypi.org/books/beginners-guide-3rd-ed)« als 252-seitiges PDF [hier herunterladen](https://magpi.raspberrypi.org/books/beginners-guide-3rd-ed). Aber auch dieses Buch kann man für für £ 10 versandkostenfrei [hier bestellen](https://store.rpipress.cc/products/the-official-raspberry-pi-beginners-guide-3rd-edition).

Und wer noch keinen Raspberry Pi sein eigen nennt, für den gibt es das »[Raspberry Pi Starter Kit](https://www.raspberrypi.org/blog/new-book-get-started-with-raspberry-pi/)«, einen Raspberry Pi 3A+ mit »offiziellem« Gehäuse und einer 16GB microSD-Karte mit dem Raspbian Betriebssystem vorgeladen. Begleitet wird es von dem 116-seitigem Buch »[Get Started With Raspberry Pi](https://magpi.raspberrypi.org/books/get-started)«. Das alles gibt es natürlich nicht für umme, sondern Ihr könnt es für £ 35 [hier bestellen](https://store.rpipress.cc/products/get-started-with-raspberry-pi). Wer sich aber nur für das Begleitbuch interessert, das gibt es als PDF ebenfalls [zum kostenlosen Download](https://magpi.raspberrypi.org/books/get-started).

[[dr]]<%= html.getLink(imageref("pygamezero"), "cp^pygamezero") %>[[dd]]

Von [Wireframe](https://wireframe.raspberrypi.org/), dem britischen Magazin für Computerspiel-Enthusiasten, sind die Ausgaben 25 und 26 draußen. Wie immer könnt Ihr sie ebenfalls als [kostenloses PDF herunterladen](https://wireframe.raspberrypi.org/issues) und wie gewohnt gibt es in jeder Ausgabe auch ein klassisches Computerspiel zum Nachprogrammieren mit Pygame Zero:

- In der Ausgabe 25 zeigt Euch *Rik Cross* wie Ihr ein »[Tile Matching Game](https://www.raspberrypi.org/blog/make-a-columns-style-tile-matching-game-wireframe-25/)« programmieren könnt.
- Und in der aktuellen Ausgabe 26 gibt es von dem oben schon erwähnten *Mark Vanstone* einen [Nachbau eines der ersten Boss-Kämpfe](https://www.raspberrypi.org/blog/code-a-phoenix-style-mothership-battle-wireframe-26/) aus dem klassischen Arcade-Spiel »Phoenix«.

**War sonst noch was?** Ach ja, ich sollte nicht vergessen zu erwähnen, das es alle Ausgaben des Raspberry Pi Magazins »MagPi« ebenfalls [zum kostenlosen Download](https://magpi.raspberrypi.org/issues) als PDF gibt. Bisher sind 87 Ausgaben erschienen, da habt Ihr schon eine Menge zu lesen.

<%= html.getLink(imageref("pygamebunt"), "cp^Pygame Zero") %>
