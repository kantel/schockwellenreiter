#title "Security Alert: Google Chrome – 20191119"
#maintitle "Google veröffentlicht wichtige Sicherheitsupdates für seinen Browser Chrome"
#prevp "2019111902"
#nextp "2019112001"

<%= html.getLink(imageref("letzterose-b"), "Letzte Herbst-Rose") %>

## Google veröffentlicht wichtige Sicherheitsupdates für seinen Browser Chrome

Google schließt mit dem Update auf die Version 78 (78.0.3904.108) wieder kritische [Sicherheitslücken](https://chromereleases.googleblog.com/2019/11/stable-channel-update-for-desktop_18.html).

Chrome aktualisiert sich (außer bei Linux) über die integrierte  Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden. *([[cert]])*

*[[photojoerg]]*