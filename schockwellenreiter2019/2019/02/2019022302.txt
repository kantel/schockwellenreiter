#title "Math Adventures with Python – 20190223"
#maintitle "Neu in meiner Bibliothek: Math Adventures with Python"
#prevp "2019022301"
#nextp "2019022401"

## Neu in meiner Bibliothek: Math Adventures with Python

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1593278675&asins=1593278675&linkId=d29b3294a5c8cd10d3fdf82d2a6bd528&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Weil heute [Processing.py](cp^processingpy)-Tag im *Schockwellenreiter* ist: Gerade brachte mir der freundliche Bote von DHL das Buch »[Math Adventures with Python – Fractals, Automata, 3D Graphics, and More!][a1]« von *Peter Farrell*. Der Autor ist/war langjähriger Mathematik- und Informatiklehrer (unter anderem als Entwicklungshelfer in Kenia) und ist ein Anhänger der Lehrmethodik [Seymor Paperts](https://de.wikipedia.org/wiki/Seymour_Papert). Und so wundert es sicher niemanden, daß die ersten drei Kapitel des Buches Mathematik und Python mtihilfe des [Turtle-Moduls](https://docs.python.org/3.7/library/turtle.html) verbinden -- schließlich war Papert der Erfinder der Programmiersprache [Logo](cp^Logo) und der Turtle-Graphik --, aber dann überrascht der Autor mich: Ab Kapitel vier (von zwölf Kapiteln) setzt er fast durchgehend Processing.py ein. Und ich dachte schon, ich wäre der einzige, der von [Processings](cp^Processing) Python-Mode so begeistert ist.

Auch inhaltlich deckt das Buch viele Themen ab, die mich interessieren. Für Funktionsgraphen und das Auffinden von Nullstellen wird eben nicht die [Matplotlib](cp^Matplotlib), sondern Processing.py bemüht, es geht um Fraktale, Matrizen für 3D-Graphiken mal ohne [Numpy](cp^numpy), Rekursionen, zelluläre Automaten und ein (kleiner) Ausflug in die KI mit genetischen Algorithmen wird auch unternommen.

Es ist sicher nicht zu überlesen: Schon nach einem kurzen Durchblättern mit gelegentlichem Festlesen bin ich begeistert von diesem Buch. Schlagt zu, es ist zur Zeit beim Buchriesen unseres (Un-) Vertrauens auch noch sehr günstig zu bekommen (das war der Grund, warum ich es blind bestellt hatte) und was Besseres auf diesem Gebiet ist mir nicht bekannt. Ihr werdet sicher nicht dümmer davon. *Processing.py rulez!*

[a1]: https://www.amazon.de/Math-Adventures-Python-Fractals-Automata/dp/1593278675/ref=as_li_ss_tl?ie=UTF8&qid=1550943742&sr=8-1&keywords=math+adventures+with+python&linkCode=ll1&tag=derschockwell-21&linkId=5d2cf639931c81a8efe1746e6e922925&language=de_DE