#title "Pandas Cookbook – 20190204"
#maintitle "Der Ebook-Download am Montag: Pandas Cookbook"
#prevp "2019020301"
#nextp "2019020402"


## Der Ebook-Download am Montag: Pandas Cookbook

[[dr]]<%= html.getLink(imageref("B03869_cover"), "https://www.packtpub.com//packt/offers/free-learning") %>[[dd]]

Heute meint es *Packt Publishing* im Rahmen der freien (frei wie 🍺&nbsp;Freibier) und täglich wechselnden Ebook-Download-Aktion mal wieder gut mit uns 🐍&nbsp;Pythonistas und offeriert uns das »[Pandas Cookbook](https://www.packtpub.com//packt/offers/free-learning)« von *Theodore Petrou*. Auf über 500 Seiten will uns die Schwarte in die Programmierung der Python-Bibliothek für die Verwaltung und Analyse von Daten einführen. Das Buch beginnt mit einer Einführung in die Grundlagen von 🐼&nbsp;[Pandas](cp^Pandas), behandelt dann sehr ausführlich *Data Frames*, die Datenstruktur, auf die Pandas beruht, um dann Rezepte für die Datenanalyse und die Visualisierung von Daten mit Pandas Bordmitteln, mit der [Matplotlib](cp^Matplotlib) und mit Seaborn vorzustellen.

Das Teil ist 2017 erschienen, dürfte damit also noch hinreichend aktuell sein.

**Die Spielregeln**: Wer noch keinen Account bei *Packt* besitzt, muß sich einmalig kostenlos registrieren, alle anderen können – nach einem eventuell notwendigen Login – den Titel bis heute Nacht um 24:00 Uhr (GMT) als Epub, Mobi und/oder PDF sofort herunterladen.

