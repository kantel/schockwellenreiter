#title "Coding Challenge: DFT mit komplexen Zahlen – 20190203"
#maintitle "Coding Challenge: DFT mit komplexen Zahlen"
#prevp "2019020201"
#nextp "2019020401"


## Diskrete Fourier-Transformation: Zeichnen mit komplexen Zahlen

<iframe width="842" height="480" src="https://www.youtube.com/embed/7_vKzcgpfvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In dieser Coding-Challenge setzt *Daniel Shiffman* sein Experiment [Zeichnen mit einer Fourier-Transformation](Zeichnen mit DFT – 20190125) fort und ersetzt die jeweiligen x- und y-Koordinaten durch je eine [komplexe Zahl](https://de.wikipedia.org/wiki/Komplexe_Zahl). Da er dies in [P5.js](cp^p5js), dem JavaScript-Mode von [Processing](cp^Processing), programmiert, muß er dazu eine Klasse `Complex` einführen und dort eine Methode für die Multiplikation komplexer Zahlen definieren. Wir Pythonistas haben es da einfacher, wir bekommen die komplexen Zahlen mit allen ihren Operationen frei Haus mitgeliefert – auch in 🐍&nbsp;[Processing.py](cp^processingpy), dem Python-Mode von Processing. So ist das in Python eigentlich keine *Challenge*, aber vielleicht setze ich mich dennoch einmal daran. *Still digging!*