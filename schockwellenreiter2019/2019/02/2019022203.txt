#title "Schau Mama, ein Hundebild! – 20190222"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019022202"
#nextp "2019022301"

<%= html.getLink(imageref("hundekekse-b"), "Her mit den Hundekeksen!") %>

**Schau Mama, ein Hundebild!** Denn es ist wieder Freitag und zum Freitag gehört ein Hundebild, das Euch darüber hinwegtrösten soll, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Denn die Wetterfrösche haben uns ein kaltes, aber sonniges Vorfrühlingswetter versprochen und das möchte ich mit der kleinen Fellkugel ausnutzen -- auch wenn sie zur Zeit noch etwas unrund läuft. Doch am Dienstag geht es erneut zur [Tierärztin](https://www.tierarzt-watson.de/) und dann hoffe ich, daß der Fremdkörper in der rechten Pfote entgültig aus dem Hund entfernt wird. Schließlich wollen wir Ende März auf unserem ersten [Rally Obedience](cp^ro)-Turnier in diesem Jahr in Rostock starten und da sollte der Sheltie natürlich wieder fit sein.

Ansonsten habe ich ja auch noch mein [gestern vorgestelltes](Winterzeit ist Lesezeit – 20190221) Lektüreprogramm, in das ich auch heute schon wieder vertieft war. Ein weiterer Grund, keine Updates zu erwarten.

Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *(Photo &copy; 2018: Sascha Harting)*