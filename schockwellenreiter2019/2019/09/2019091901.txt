#title "Zwei Browser Updates – 20190919"
#maintitle "Zwei Browser Updates"
#prevp "2019091801"
#nextp "2019091902"

<%= html.getLink(imageref("wohnsitzbritzerwaeldchen-b"), "Wohnsitz Britzer Wäldchen") %>

## Zwei Browser Updates

Mozilla wie auch Google haben heute Sicherheits-Updates ihrer Browser Firefox und Chrome unters Volk gebracht:

Die Entwickler des [Mozilla Firefox](http://www.mozilla.com/de/firefox/) haben mit dem [Wartungsupdate auf 69.0.1](https://www.mozilla.org/en-US/firefox/69.0.1/releasenotes/) mehrere Fehler behoben. Firefox weist selbst auf dieses Update hin. Die Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden.

Und Google schließt mit dem [Update auf die Version 76](https://chromereleases.googleblog.com/2019/09/stable-channel-update-for-desktop_18.html) (77.0.3865.90) auch wieder Sicherheitslücken. 

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden. *([[cert]])*

*[[photojoerg]]*