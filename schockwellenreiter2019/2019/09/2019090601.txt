#title "Kritische Sicherheitslücke in Samba – 20190906"
#maintitle "Kritische Sicherheitslücke in Samba"
#prevp "2019090503"
#nextp "2019090602"

<%= html.getLink(imageref("minimalistischerspielplatz-b"), "Minimalistischer Spielplatz") %>

## Kritische Sicherheitslücke in Samba

Eine [kritische Sicherheitslücke](https://www.samba.org/samba/security/CVE-2019-10197.html) in der freien Server-Software für Windows-Clients [Samba](https://de.wikipedia.org/wiki/Samba_(Software)) ermöglicht Angreifern unter bestimmten Umständen unbefugte Verzeichniszugriffe.

Abgesicherte Samba-Versionen sowie Packages für einige Linux-Distributionen stehen bereit. Näheres hierzu findet sich auch bei [Tante Heise](https://www.heise.de/security/meldung/Samba-Patch-verhindert-Ausbruch-aus-freigegebenen-Ordnern-4514500.html), *([[cert]])*

*[[photogabi]]*