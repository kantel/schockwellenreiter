#title "No touchStarted() – 20190912"
#maintitle "P5.js: Kein touchStarted() unter Android?"
#prevp "2019091102"
#nextp "2019091301"

<%= html.getLink(imageref("schaltkastenpanda-b"), "Schaltkastenpanda in Treptow") %>

## P5.js: Kein touchStarted() unter Android?

Denn erstens kommt es anders und zweitens als man denkt: Das [gestern](Basic Side Scroller mit P5 (Update) – 20190911) in meinem *Basic Side Scroller* mit P5.js [implementierte](https://p5js.org/reference/#/p5/touchStarted) `touchStarted()` ließ zumindest auf meinem Nokia 2 mit Android das kleine rosa Alien kalt. Es wollte einfach nicht hüpfen. Ich werde mich daher nach [Alternativen](https://p5js.org/reference/#/p5/mousePressed) umschauen müssen, `mousePressed()` soll möglicherweise laut der Dokumentation auf *Touch Devices* funktionieren.

Ich wäre sehr dankbar, wenn mir jemand von Euch da draußen in meinen (Email-) Kommentaren mitteilen könnte, ob und wie sie oder er Events auf Smartphones oder Tablets in [P5.js](cp^P5js) verarbeiten konnte. *Still digging!* *[[photojoerg]]*