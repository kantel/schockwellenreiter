#title "So werde ich zur Umweltsau gemacht – 20190903"
#maintitle "So werde ich – dank des Berliner Senats – zur Umweltsau"
#prevp "2019090301"
#nextp "2019090303"

<%= html.getLink(imageref("glascontainer-b"), "Glascontainer am Kranoldplatz") %>

## So werde ich – dank des Berliner Senats – zur Umweltsau

Wie ich heute einem Aushang der Berliner Stadtreinigung (BSR) in unserem Hausflur entnehmen konnte, hat sich unser Berliner Rot-Rot-Grüner Senat etwas neues ausgedacht, um die Bürger zu quälen und der Umwelt zu schaden. Die Altglassammlung wird nämlich <del>aufgegeben</del> »umgestellt«. Die Sammeltonnen im Hof werden abgeschafft, stattdessen soll der Bürger sein Altglas mehrmals die Woche selber zu (in unserem Falle) diesen angegebenen Altglascontainern schleppen (in Klammern der Weg hin und zurück und die etwa benötigte Zeit für Fußgänger (laut Google Maps)): 

- Britzer Damm 30: 280 Meter (560 Meter, 7 - 8 Minuten)
- Bendastraße 6: 400 Meter (800 Meter , 12 Minuten)
- Britzer Damm 56: 650 Meter (1,3 Kilometer, 23 Minuten)
- Gradestraße 81: 2,2 Kilometer (4,4 Kilometer, 1 Stunde)

Abgesehen davon, daß die öffentlich aufgestellten Altglascontainer -- wie jeder Berliner (außer den Politikern) weiß -- ein [Hotspot der Illegalen Müllentsorgung](Glascontainer am Kranoldplatz) sind, warum soll ich mich auf den beschwerlichen Weg machen und mein Altglas mehrmals die Woche zu den Containern schleppen? Wer bezahlt mir die Mühe und die Zeit (selbst bei den naheliegendsten Containern komme ich auf mindestens eine Stunde pro Monat)? Ich bin schließlich auch nicht mehr der Jüngste und ich habe bisher über die Betriebskosten zusätzlich zur Miete für die Altglascontainer im Hof gezahlt (und das habe ich ohne zu Murren getan).

Daher wird mein Altglas in Zukunft eben zusätzlich im normalen Hausmüll landen. Ich laß mich doch von der Politik nicht quälen und zu sinnlosen Symbol-Handlungen zwingen. Leckt mich doch einfach am Arsch. *[[photojoerg]]*

---

**2 (Email-) Kommentar**

---

<%= a("k01") %>

>Willkommen in der Realität. Der ganze Rest der Republik bringt sein Altglas schon immer zu meist fussläufig erreichbaren Containern. Die dabei aufgewendete Zeit und Energie ersetzt uns auch niemand. Mir jedenfalls ist keine einzige Stadt bekannt, in der Altglas "direkt beim Bürger" eingesammelt wird.

*– Jens T.* <%= p("k01") %>

---

<%= a("k0101") %>

>>Nur weil Berlin ausnahmsweise mal etwas besser gelöst hatte als der Rest der Republik, muß doch nicht ausgerechnet dieses abgeschafft werden.

*– Jörg Kantel* <%= p("k0101") %>

