#title "Google Flickentag (Android) – 20190904"
#maintitle "Google Flickentag mit neuer Android-Version"
#prevp "2019090304"
#nextp "2019090501"

<%= html.getLink(imageref("technikstandortbritz-b"), "Technikstandort Britz") %>

## Google Flickentag mit neuer Android-Version

Am gestrigen Google-Flickentag hat die Datenkrake nicht nur Sicherheitslücken in Android geschlossen, sondern auch mit Android 10 eine neue Version unters Volk gebracht. Beginnen wir mit Android 10:

Google hat gestern Abend (3.9.) wie angekündigt die neue [Version 10](https://www.android.com/android-10/) seines Betriebssystems Android veröffentlicht. Die wesentliche Neuerungen sind unter anderem der neue »Dark mode«, neue Gestensteuerung mit  einer komplett auf Wischgesten basierender Bedienungsoberfläche,  verbesserter Privatsphäre, Unterstützung für 5G, Freigabeverknüpfungen, optimierte Benachrichtigungen und natürlich auch behobene [Sicherheitsprobleme](https://source.android.com/security/bulletin/android-q.html).

Das Update wird zunächst für alle Pixel-Geräte per OTA verteilt und sollte im Laufe der Zeit dann auch mit der üblichen Verzögerung bei weiteren Geräten ankommen.

Außerdem hat Google mit seinem monatlichen Sicherheitsupdate für Android (und damit auch auf seinen [Pixel-Geräten](https://source.android.com/security/bulletin/pixel/2019-09-01)) wieder [Sicherheitslücken](https://source.android.com/security/bulletin/2019-09-01) geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen: 01.09.2019, 05.09.2019.

Die Updates werden so nach und nach per OTA *(over the air)* auf Pixel 3, Pixel 3 XL, Pixel 2, Pixel 2 XL, Pixel, Pixel XL verteilt.

Die anderen Hersteller werden wie üblich in Bälde nachziehen, sofern sie überhaupt noch entsprechenden Support leisten. *([[cert]])*

*[[photogabi]]*