#title "Schau Mama, ein Hundebild! – 20190906"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019090602"
#nextp "2019090701"

<%= html.getLink(imageref("schattensheltie2-b"), "Ein Sheltie im Schatten (2)") %>

**Schau Mama, ein Hundebild!** Auch wenn die Posting-Frequenz in den letzten Tagen etwas zugenommen hat, gibt es keinen echten Anlaß für dieses Photo der Fellkugel in unserem Garten -- außer der Tradition dieses <del>Blogs</del> Kritzelheftes: Es ist Freitag und Freitags gibt es ein Hundebild im *Schockwellenreiter*!

Dieses Wochenende stehen auch keine hundesportlichen Aktivitäten an, aber ich will mich in den nächsten Tagen mal wieder mit seltsamen Programmiersprachen und exotischen Frameworks herumschlagen. Ich weiß aber selber noch nicht, ob und was dabei herauskommt. Laßt Euch also überraschen -- ich bin genau so gespannt wie ihr.

Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*