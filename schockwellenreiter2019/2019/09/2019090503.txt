#title "Das Einhornrennen auf der Eyeo 2019 – 20190905"
#maintitle "Live Coding: Das Einhornrennen auf der Eyeo 2019"
#prevp "2019090502"
#nextp "2019090601"

## Live Coding: Das Einhornrennen auf der Eyeo 2019

<iframe src="https://player.vimeo.com/video/354276216?color=FF6E4F" width="852" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

Die Einrichtung von *Daniel Shiffmans* neuem Studio verzögert sich ein wenig (hoffen wir, daß er nicht einen BER bauen will). In der Zwischenzeit hat er sich auf der [Eyeo 2019](http://eyeofestival.com/) herumgetrieben und gezeigt, wie man in nur 45&nbsp;Minuten mit [P5.js](cp^p5js), dem JavaScript-Mode von [Processing](cp^Processing), [ml5.js](https://ml5js.org/), Googles [Teachable Machine](https://teachablemachine.withgoogle.com/), [tensorflow.js](https://www.tensorflow.org/js) und [Runway ML](https://runwayml.com/) ein Spiel bauen kann, das das gesamte Publikum mit einbezieht.

Das alles ist auch noch sehr vergnüglich anzuschauen. Vergesst all die langweiligen Comedians. Schaut *Daniel Shiffman*. Da werdet Ihr nicht dümmer von (im Gegensatz zu den Programmen von *Mario Barth* oder *Dieter Nuhr* verblödet Ihr beim Zuschauen nicht) und Spaß macht es auch noch.