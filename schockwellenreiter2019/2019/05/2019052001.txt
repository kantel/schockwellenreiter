#title "Video-Tutorial: What the Hell is Ren'Py? – 20190520"
#maintitle "Video-Tutorial: What the Hell is Ren'Py?"
#prevp "2019051801"
#nextp "2019052101"


## Video-Tutorial: What the Hell is Ren'Py?

<iframe width="852" height="480" src="https://www.youtube.com/embed/CjJ2R22U11Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Elaine M. Podosek* (aka *Emp(ish)*) möchte Menschen überzeugen, ihre eigenen Computerspiele (und Webseiten) selber zu programmieren. Dafür hat sie unter anderem eine [25-teilige Playlist zu Ren'Py](https://www.youtube.com/watch?v=CjJ2R22U11Q&list=PLJgbnTkC4bkCj2_8ZUg1o4CafLcRH0rAX) auf YouTube online gestellt, der kostenlosen und frei verwendbaren (MIT-Lizenz), in Python geschriebenen und erweiterbaren [Engine](cp^renpy) für die Erstellung von *Visual Novels*, aber auch von anderer Software, wie beispielsweise von Textadventures, Geschichten, Präsentationen und animierten Illustrationen.

Und sie macht das sehr gut, ich bin heute bis Video Nummer 12 gekommen und begeistert. Sie geht vor allem auf fortgeschrittenere Features ein und zeigt, wie man Ren'Py mit eigenen, selbstgeschriebenen Python-Skripts erweitert und aufbohrt. Wenn Ihr also in der nächsten Zeit mehr über Ren'Py hier im <del>Blog</del> Kritzelheft lesen werdet, dann habt Ihr das *Elaine* zu verdanken. *Still digging!*

<%= html.getLink(imageref("renpy-logo"), "cp^renpy") %>