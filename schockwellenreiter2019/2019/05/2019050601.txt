#title "Security Alerts (und ihre Auflösung) – 20190506"
#maintitle "Security Alerts (und ihre Auflösung)"
#prevp "2019050501"
#nextp "2019050701"

<%= html.getLink(imageref("katzensicheresvogelhaus-b"), "Katzensicheres Vogelhaus") %>

## Security Alerts (und ihre Auflösung)

Heute gibt es einmal eine dringende Sicherheitswarnung von Dell und ein Update von Mozilla wegen der [abgelaufenen Zertifikate im Firefox](Security Alert: Firefox – 20190504). Ich fange mit Dell an:

**Dell warnt vor Sicherheitslücken im Dell Support Assist Client**: Der auf vielen Dell-Systemen vorinstallierte Dell Support Assist Client hat offenbar [schwere Sicherheitslücken](https://www.dell.com/support/article/de/de/dedhs1/sln316857/dsa-2019-051-dell-supportassist-client-multiple-vulnerabilities?lang=en), die Angreifern einen Zugriff von der Ferne ermöglichen. Dell rät dazu, den Dell Support Assist Client auf die Version 3.2.0.90 zu aktualisieren. Der Download der fehlerbereinigten Version findet sich [hier](https://downloads.dell.com/serviceability/Catalog/SupportAssistInstaller.exe).

**Mozilla veröffentlicht Firefox 66.0.4 und behebt das Zertifikatsproblem**: 

Die Entwickler des [Mozilla Firefox](http://www.mozilla.com/de/firefox/) haben mit dem [Update auf 66.0.4 und 60.6.2 ESR](https://www.mozilla.org/en-US/firefox/66.0.4/releasenotes/) das Problem mit den deaktivierten Addons aufgrund eines abgelaufenen Zertifikats behoben. Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*

*[[photogabi]]*