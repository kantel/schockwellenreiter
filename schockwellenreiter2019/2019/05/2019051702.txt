#title "Schau Mama, ein Hundebild! – 20190517"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019051701"
#nextp "2019051801"

<%= html.getLink(imageref("rojuuhu2018-b"), "Rally Obedience bei JuuHu 2018") %>

**Schau Mama, ein Hundebild!** Denn die Woche verging wie im Fluge und schon wieder ist es Freitag. Daher soll Euch das Photo des kleinen Shelties vom Juni letzten Jahres (aufgenommen beim [Rally Obedience](cp^RO) Turnier von [JuuHu](http://jugendundhund.de/)) darüber hinwegtrösten, daß es morgen und am Sonntag vermutlich keine oder nur sehr wenige Updates hier im *Schockwellenreiter* geben wird. Denn ich habe mir für die beiden Tage ein großes Lesepensum vorgenommen, programmieren möchte ich auch wieder ein wenig und natürlich darf und soll die Fellkugel auch nicht zu kurz kommen.

Das Wetter für die nächsten zwei Tage soll sehr vielversprechend werden. Genießt es daher. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *(Photo © 2018: Sascha Harting)*