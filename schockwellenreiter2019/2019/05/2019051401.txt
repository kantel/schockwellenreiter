#title "Apfel-Flickentag – 20190514"
#maintitle "Apfel-Flickentag"
#prevp "2019051101"
#nextp "2019051402"

<%= html.getLink(imageref("sarrelundspeiche-b"), "Sattel und Speiche") %>

## Apfel-Flickentag: iOS und macOS

Gestern brachte die Firma mit dem angebissenen Apfel Updates für seine Hauptbetriebssysteme iOS und macOS heraus. Als erstes hat sie das [Update auf iOS 12.3](https://support.apple.com/de-de/HT209084#123) veröffentlicht und brachte damit folgendes:

- neue TV-App,
- AirPlay 2 unterstützt nun das Teilen von Videos, Fotos, Musik und mehr jetzt auch auf ein AirPlay 2-kompatibles Smart-TV.

Schließlich wurden natürlich auch wieder [Sicherheitskorrekturen](https://support.apple.com/de-de/HT210120) vorgenommen. Das Update kann über OTA (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung`) geladen werden.

Dann hat Apple auch die [Version 10.14.5 des Mac-Betriebssystems macOS Mojave](https://support.apple.com/en-us/HT209149#macos10145) mit folgenden Änderungen zum Download freigegeben:

- fügt AirPlay 2-Unterstützung hinzu, um Videos, Fotos, Musik und mehr direkt von deinem Mac auf einem AirPlay 2-kompatiblen Smart-TV teilen zu können,
- verbessert die Audiolatenz auf den MacBook Pro-Modellen von 2018,
- behebt ein Problem, bei dem sehr große OmniOutliner- und OmniPlan-Dokumente nicht korrekt dargestellt wurden.

Und natürlich wurden auch wieder [Sicherheitslücken](https://support.apple.com/de-de/HT210119) gestopft.

*Last but not least* hat Apple mit dem [Sicherheitsupdate 2019-03 für High Sierra und dem Sicherheitsupdate 2019-03 für Sierra](https://support.apple.com/de-de/HT210119) auch jene Sicherheitslücken geschlossen, die mit macOS Mojave 10.14.5 behoben wurden.

Das Update steht wie üblich über die Softwareaktualisierung beziehungsweise dem Mac App Store bereit. *([[cert]])*

*[[photojoerg]]*