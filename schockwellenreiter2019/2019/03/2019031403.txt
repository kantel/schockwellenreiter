#title "Spiele mit Python supereasy programmieren – 20190314"
#maintitle "Spiele mit Python supereasy programmieren"
#prevp "2019031402"
#nextp "2019031501"

## Neu: Spiele mit Python supereasy programmieren

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3831036756&asins=3831036756&linkId=74ac850d14599b01a0fbc1a08ea374be&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Heute trudelte auf meinem Schreibtisch das Buch »[Spiele mit Python supereasy programmieren][a1]« von *Carol Voderman* und anderen ein. Laßt Euch von dem dämlichen und anbiedernden Titel (für den Verlag und Übersetzerin geschlagen hören) nicht abschrecken, im englischen Original heißt das Buch »Computer Coding Python Games for Kids« und den Inhalt, den dieser eher nüchterne Titel verspricht, den liefert das Buch auch: Nach einer Einführung in Python werden auf etwas über 220 Seiten neun nette Spiele mit Python und [Pygame Zero](cp^Pygame Zero) vorgestellt und Stück für Stück programmiert. Dabei wird alles einfach und kindgerecht erklärt.

Das Buch ist ganz schön bunt, mit vielen Illustrationen im Retro-Stil und macht auch kindischen, alten Männern wie mir Spaß. Und die Assets kann man sich von der [Seite des Verlags](https://www.dk.com/uk/information/the-python-games-resource-pack/) herunterladen, wobei die Bilder auch hier im gleichen 8-Bit-Retro-Stil gehalten sind.

Die Autoren scheinen alle mehr oder weniger mit der schottischen Sektion der Organisation [CoderDojo](https://coderdojo.com/de-DE), die Programmierclubs für Kinder betreibt, verbandelt zu sein und diese Erfahrungen merkt man dem Buch auch an und die Fröhlichkeit ist ansteckend.

Ich habe bisher noch nichts mit dem Buch angestellt, sondern es nur kurz durchgeblättert. Aber ich habe den Eindruck, daß es mir zum einen bei meiner Erkundung von Pygame Zero helfen kann und zum anderen mir sicher sehr viel Spaß bereiten wird. *Still digging!*

<%= html.getLink(imageref("pygamebunt"), "cp^Pygame Zero") %>


[a1]: https://www.amazon.de/Spiele-mit-Python%C2%AE-supereasy-programmieren/dp/3831036756/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=VHHK5SE8NQ3F&keywords=spiele+mit+python+supereasy+programmieren&qid=1552581996&s=gateway&sprefix=spiele+mit+python,aps,156&sr=8-1-fkmrnull&linkCode=ll1&tag=derschockwell-21&linkId=a6e739bb5506f2a42def348103be8cce&language=de_DE