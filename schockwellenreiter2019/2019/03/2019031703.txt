#title "Pygame Zero Intro (1) – 20190317"
#maintitle "Pygame Zero Intro (1)"
#prevp "2019031702"
#nextp "2019031801"

## Pygame Zero Intro (1)

In einer Reihe von Beiträgen möchte ich [Pygame Zero](cp^Pygame Zero) erkunden und die Bibliothek kennenlernen. Es soll also so etwas wie *»learning by doing«* sein, es ist also nicht auszuschließen, daß ich mich gelegentlich in Sackgassen verrenne, die ich dann hoffentlich in einem folgenden Beitrag wieder verlassen kann. Aber nach ersten Experimenten halte ich diese auf [Pygame](cp^Pygame) aufsetzende [Python](cp^Python)-Bibliothek für ähnlich spannend wie [Processing](cp^Processing) und denke daher, daß sich die Mühe lohnen wird.

Die erste Überraschung: Pygame Zero benötigt tatsächlich null und nichts. Der Befehl `pgzrun` frißt sogar eine leere Datei.

<%= html.getLink(imageref("pgzintro01-b"), "Intro to Pygame Zero 1") %>

Auch das hat Pygame Zero mit Processing gemein. Denn wenn man eine leere Processing-Datei laufen läßt, erhält man ein kleines, graues Fenster. Okay, es gibt einen Unterschied: Das Pygame-Zero-Fenster ist bei Default 800 x 600 Pixel groß und besitzt einen schwarzen Hintergrund.

Natürlich muß man Pygame-Zero-Scripte nicht unbedingt im Terminal mit

~~~bash
pgzrun dateiname.py
~~~

aufrufen, kommandozeilenscheue Menschen wie ich können ein Script auch aus einem Editor oder einer IDE ihrer Wahl starten (solange die IDE oder der Editor so etwas wie einen `Run`-Button für Python bietet). Dazu sind allerdings zwei Zeilen notwendig. Das gleiche Ergebnis des obigen Screenshots erhält man nämlich, wenn man die Datei `empty.py` mit diesen beiden Zeilen aufpeppt:

~~~python
import pgzrun

pgzrun.go()
~~~

Das Kommando `pgzrun.go()` muß immer als letztes Kommando eines Skripts aufgerufen werden, denn alles, was nach diesem Kommando noch im Script steht, wird von Pygame Zero ignoriert.

Beenden kann man das Script, indem man entweder auf den *Schließen-Knopf* des Fensters klickt oder mit `CTRL-Q` (Windows, Linux) oder `CMD-Q` (Mac) die betriebsystem-üblichen Tastatur-Befehle zum Beenden eines Programmes nutzt.

So liegt es natürlich nahe, eine Template-Datei für alle Pygame-Zero-Scripte zu erstellen, damit man bei jedem neuen Projekt nicht immer völlig bei Null anfangen muß. Sie könnte in ihrer einfachsten Form beispielsweise so aussehen:

~~~python
import pgzrun

WIDTH = 400
HEIGHT = 400
TITLE = "🐍🐍 Pygäme Zero Template 🐍🐍"

def draw():
    pass

def update():
    pass

pgzrun.go()
~~~

Das Ergebnis sieht dann so aus:

<%= html.getLink(imageref("pgzintro02-b"), "Intro to Pygame Zero 2") %>

`WIDTH`. `HEIGHT` und `TITLE` sind Direktiven, die an Pygame Zero weitergegeben werden. Die ersten beiden geben die Fensterbreite und -höhe an und der Inhalt von `TITLE` wird in die Titelzeile des Fensters geschrieben. Da bei mir Pygame Zero mit Python 3.7 läuft, habe ich dort großzügig Umlaute und Emojis eingebaut, Pygame Zero läuft aber auch mit Python 2.7 und das ist bekanntlich nicht ohne Umwege so wirklich Unicode-fest.

### Hooks

Die Hauptschleife eines Spiels in Python sind [typischerweise](http://blog.schockwellenreiter.de/2018/04/2018041701.html) so oder ähnlich aus:

~~~python
keep_going = True
while keep_going:
    process_input()
    update()
    draw()
~~~

Auch hier nimmt Pygame Zero dem Programmierer viel Arbeit ab. Die Bibliothek besitzt mit `update()` und `draw()` zwei vordefinierte *[Hooks](https://pygame-zero.readthedocs.io/en/stable/hooks.html)*, die nur überschrieben werden müssen. `draw()` wird von Pygame Zero jedesmal dann aufgerufen, wenn das Fenster neu gezeichnet werden muß. Der Methode dürfen keine Argumente übergeben werden. Pygame Zero kümmert sich selber darum, wann das Fenster neu gezeichnet werden muß, um unnötiges Neuzeichnen zu vermeiden. Typischerweise geschieht dies in folgenden Situationen:

- Die `update()`-Funktion (siehe unten) löst ein Neuzeichnen aus.
- Eine `clock()`-Event wurde ausgelöst (dazu in einem späteren Beitrag mehr).
- Ein Eingabe-Event löst ein Neuzeichnen aus.

Die `update()`- oder `update(dt)`-Methode wird von Pygame Zero 60 mal in der Sekunde aufgerufen. Mit dem optionalen Parameter `dt` *(delta time)* können Ereignisse zeitbasiert statt framebasiert gesteuert werden.

### Die Klasse Actor()

Pygame Zero scheint sich völlig von der Sprite-Klasse aus Pygame verabschiedet zu haben. Stattdessen gibt es eine Klasse `Actor()`, die fast alle Eigenschaften von Pygames [`Rect()`-Klasse](https://www.pygame.org/docs/ref/rect.html) übernommen hat (insbesondere auch die Methoden zur Kollisionserkennung). Die Klasse `Actor()` bekommt als Parameter den Namen einer Bilddatei (ohne Suffix) übergeben. Diese Bilddatei **muß** im Ordner `images` unterhalb des aufrufenden Skriptes liegen, damit Pygame Zero sie findet. Üblicherweise kann Pygame Zero `.png`-,  `.gif`- und `.jpg`-Formate lesen, wobei das `.png`-Format empfohlen wird.

Mit diesem Wissen möchte ich ein erstes, kleines Pygame-Zero-Script (aus der [Dokumentation](https://pygame-zero.readthedocs.io/en/stable/introduction.html) vorstellen: Als erstes habe ich den Ordner `images` im Verzeichnis meines Programms an- und dort das Bild eines rosa Aliens (`alien.png`) aus dem wunderbaren freien Fundus von [Kenney.nl](https://www.kenney.nl/assets) abgelegt. Und dann dieses kleine Scriptchen geschrieben:

~~~python
import pgzrun

WIDTH = 400
HEIGHT = 400
TITLE = "🐍🐍 Hällo Alien 🐍🐍"

alien = Actor("alien")
alien.pos = (200, 250)

def draw():
    screen.fill((0, 80, 125))
    alien.draw()

def update():
    alien.left += 2
    if alien.left > WIDTH:
        alien.right = 0

pgzrun.go()
~~~

Mit diesen wenigen Zeilen schwebt das rosa Alien majestätisch vor einem blauen Hintergrund von links nach rechts durch das Fenster.

<%= html.getLink(imageref("pgzintro03-b"), "Intro to Pygame Zero 3") %>

Es ist doch schon sehr beeindruckend, was man in Pygame Zero mit diesen wenigen Zeilen Code schon alles auf den Bildschirm zaubern kann. Schaun wir mal, was weiter bei meinen Erkundungen herauskommt. *Still digging!*

<%= imageref("alien") %>

Damit Ihr das Progrämmchen nachprogrammieren könnt, hier auch noch das Bild des rosa Aliens. Ihr könnt natürlich auch jedes andere Bild verwenden.
 
