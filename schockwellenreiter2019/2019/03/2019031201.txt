#title "Python Linkschleuder (März 2019) – 20190312"
#maintitle "Python-Linkschleuder für das 1. Quartal 2019"
#prevp "2019031001"
#nextp "2019031202"

<%= html.getLink(imageref("sesseldelbrueckstr-b"), "Wohnsitz Delbrückstraße") %>

## Python-Linkschleuder für das 1. Quartal 2019

Ich muß gestehen, die neue Freiheit, die mir das Rentnerdasein beschert und es mir erlaubt, unbeschwert und ohne Zeitdruck sinnlose Dinge fröhlich zu programmieren (natürlich in Python), läßt einige andere Dinge zu kurz kommen. So fiel mir auf, daß die [letzte Python-Linkschleuder](Schlangenfraß – 20190121) lange her ist und im Januar dieses Jahres veröffentlicht wurde. Natürlich spülte in der Zwischenzeit wieder einiges in meinen Feedreader und so gibt es heute eine neue Riesen-Linkschleuder -- in der Hauptsache zu den Themen Jupyter und Daten-Visualisierung.

### Jupyter und JupyerLab

- Dieser Artikel gab den Anstoß für diese Linkschleuder: »[Jump Out of the Jupyter Notebook with nbconvert](https://towardsdatascience.com/jump-out-of-the-jupyter-notebook-with-nbconvert-7d4748960702)«, denn wenn ich mich tatsächlich mit [JupyterLab](https://towardsdatascience.com/jupyter-lab-evolution-of-the-jupyter-notebook-5297cacde6b) anfreunden sollte, dann will ich die Notebooks auch als (statische) HTML-Seiten hier im <del>Blog</del> Kritzelheft veröffentlichen können: [nbconvert](https://nbconvert.readthedocs.io/en/latest/) scheint dafür das Werkzeug der Wahl zu sein.

- Oder vielleicht hiermit? [Jupytext 1.0 highlights](https://towardsdatascience.com/jupytext-1-0-highlights-a49dca9baa7b): [Jupytext](https://github.com/mwouts/jupytext) is an extension for Jupyter Notebook and JupyterLab that can save Jupyter notebooks in various text formats: Markdown, R Markdown, Python, Julia and R scripts, and more!

- Auch interessant: [Interactive spreadsheets in Jupyter](https://towardsdatascience.com/interactive-spreadsheets-in-jupyter-32ab6ec0f4ff). Notiz an mich: [ipywidgets](https://ipywidgets.readthedocs.io/en/stable/) testen!

- Noch mehr zu IPython widgets: [Interactive Controls in Jupyter Notebooks](https://towardsdatascience.com/interactive-controls-for-jupyter-notebooks-f5c94829aee6). How to use interactive IPython widgets to enhance data exploration and analysis.

- [Set Your Jupyter Notebook up Right with this Extension](https://towardsdatascience.com/set-your-jupyter-notebook-up-right-with-this-extension-24921838a332). Eine Antwort auf *Joel Grus* »I Don't Like Notebooks« (wir [berichteten](http://blog.schockwellenreiter.de/2018/08/2018082801.html)).

### Datenvisualisierung

- Erkunden wir das weite Feld der Datenvisualisierung mit Python und Jupyter: [Jupyter Superpower — Interactive Visualization Combo with Python](https://towardsdatascience.com/jupyter-superpower-interactive-visualization-combo-with-python-ffc0adb37b7b).

- [Introduction to Data Visualization in Python](https://towardsdatascience.com/introduction-to-data-visualization-in-python-89a54c97fbed): How to make graphs using Matplotlib, Pandas and Seaborn.

- [Interactive Data Visualization with Python Using Bokeh](https://towardsdatascience.com/interactive-data-visualization-with-python-using-bokeh-4c71f8f7c817) und [Interactive plotting with Bokeh](https://towardsdatascience.com/interactive-plotting-with-bokeh-ea40ab10870).

- [PyViz: Simplifying the Data Visualisation process in Python](https://towardsdatascience.com/pyviz-simplifying-the-data-visualisation-process-in-python-1b6d2cb728f1). An overview of the [PyViz](http://pyviz.org/) ecosystem to make data visualizations in Python easier to use, learn and more powerful.

### Fun with Python

- [Transform Grayscale Images to RGB Using Python’s Matplotlib](https://towardsdatascience.com/transform-grayscale-images-to-rgb-using-pythons-matplotlib-6a0625d992dd): Learn about image data structures while adding two dimensions for computer vision & deep learning pipelines.

- Computer Science with Python: [Understanding time complexity with Python examples](https://towardsdatascience.com/understanding-time-complexity-with-python-examples-2bda6e8158a7).

- [How to use open source satellite data for your investigative reporting](https://towardsdatascience.com/how-to-use-open-source-satellite-data-for-your-investigative-reporting-d662cb1f9f90) -- ein sehr umfangreiches Tutorial.

- Leben ohne Microsoft: [Replacing Excel with Python](https://towardsdatascience.com/replacing-excel-with-python-30aa060d35e). Und Leben ohne Adobe: [Python for Pdf](https://towardsdatascience.com/python-for-pdf-ef0fac2808b0).

**War sonst noch was?** Ach ja, am 17. März ist [St. Patrick's Day](https://de.wikipedia.org/wiki/St._Patrick%E2%80%99s_Day) und damit Ihr ihn gebührend feiern könnt, hier eine Anregung: [How to drop Leprachaun-Hats into your website with Computer Vision](https://medium.freecodecamp.org/how-to-drop-leprechaun-hats-into-your-website-with-computer-vision-b0d115a0f1ad), denn irgendwie sind wir doch alle Iren. Und am Ende des Beitrags gibt es noch Links zu weiteren, sinnbefreiten Verschönerungen von Webseiten. Genau mein Geschmack!