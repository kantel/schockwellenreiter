#title "Schau Mama, ein Hundebild am Donnerstag! – 20190328"
#maintitle "Schau Mama, ein Hundebild am Donnerstag!"
#prevp "2019032802"
#nextp "2019040201"


<%= html.getLink(imageref("richiejoey-b"), "Joey und Richie: Zwei Zillegarten-Shelties") %>

**Schau Mama, ein Hundebild am Donnerstag!** Nein, heute ist wirklich noch kein Freitag, aber Joey und ich haben etwas vor: Wir fahren morgen früh mit der Bahn an die Ostsee nach [Warnemünde](https://de.wikipedia.org/wiki/Warnem%C3%BCnde), um an einem [Rally Obedience Turnier](https://www.hundeverein-schwaan.de/turniere/2019/rally-obedience/) des [Hundevereins Schwaan e.V.](https://www.hundeverein-schwaan.de/) teilzunehmen. Das Turnier findet im Rahmen der Messe [Tier und Natur in MV](https://www.inrostock.de/messen/tiernatur-in-mv.html) in den Rostocker Messehallen statt und ist am Sonntag gleichzeitig die DVG-Landesmeisterschaft für Mecklenburg-Vorpommern. Die Fellkugel und ich dürfen uns daher auf anspruchsvolle Parcoure freuen.

Der zweite Sheltie oben im Photo nimmt ebenfalls teil und es ist wie unsere Fellkugel ein Zillegartensheltie, ebenfalls aus dem Raum Berlin. Und zusammen mit Joey und mir fährt auch noch Joeys Kumpel Jonny mit seinem Hundeführer Klaus mit. Wir vier (Jonny, Joey, Jörg und Klaus) waren schon [im letzten Jahr dort](http://blog.schockwellenreiter.de/2018/03/2018032204.html) und es hatte uns so gut gefallen, daß wir dieses Jahr unbedingt wieder hinwollten. Der einzige Unterschied ist, daß ich dieses Mal keinen Urlaub mehr nehmen muß, wir gönnen uns als Rentner einfach zwei zusätzliche Tage an der Ostsee und haben uns dafür in eine (hoffentlich) [nette Pension](http://www.pension-zum-steuermann.de/) eingemietet. Eins bringe ich aber aus Berlin mit: Zwischen Rostock und Warnemünde ist Schienenersatzverkehr -- die kaputtgesparte Deutsche Bahn verfolgt mich überall.

Da wir morgen in aller Herrgottsfrühe mit der Bahn losfahren und erst am Montag nachmittag (so die DB will) wieder zurück in Berlin sein werden, wird es vermutlich vor Dienstag keine neuen Updates hier im *Schockwellenreiter* geben -- vor allem, da ich auch nicht weiß, ob die Pension über WLAN verfügt. Tröstet Euch daher einfach mit dem Hundebild und lest solange die alten Nachrichten -- es sind schließlich genug da.

*(Photo &copy; 2016: Cornelia Wiegank)*	