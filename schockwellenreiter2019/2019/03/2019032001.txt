#title "Updates, Updates, Updates – 20190320"
#maintitle "Updates, Updates, Updates und noch einmal Updates"
#prevp "2019031901"
#nextp "2019032002"

<%= html.getLink(imageref("kuehlschranksessel-b"), "Sessel an (gefallenem) Kühlschrank") %>

## Updates, Updates, Updates und noch einmal Updates

Gestern war für viele Firmen anscheinend Flickentag. Zumindes von PuTTY, Samsung und Mozilla wurden Sicherheitsupdates und Patches verteilt, doch auch Matomo überraschte mit einem neuen Release:

**PuTTY**: Mit der Veröffentlichung der neuen Version 0.71 des freien SSH-Klienten PuTTY für Windows beheben die Entwickler eine [Sicherheitslücke in SCP (Secure Copy Protocol)](https://www.chiark.greenend.org.uk/~sgtatham/putty/changes.html), worüber Daten zwischen verschiedenen Rechnern via SSH ausgetauscht werden können.  Bösartige SSH-Server könnten so die angeforderten Dateien durch Schadcode ersetzen.

Die neue Version kann [hier heruntergeladen](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) werden. *([[cert]])*

**Samsung**: Samsung verteilt die März Sicherheit-Updates für seine S-Modelle: S9, S9+, Note 9, S8, S8+ und Note 8. Die Sicherheitsupdates gelangen wie üblich per OTA *(Over The Air)* auf die Geräte. *([[cert]])*

**Mozilla**: Die Entwickler des [Mozilla Firefox](http://www.mozilla.com/de/firefox/) sorgen unter anderem mit dem [Update auf 66.0](https://www.mozilla.org/en-US/firefox/66.0/releasenotes/) dafür, daß unerwünschten Autoplay-Inhalte automatisch blockiert werden. Es wurden auch wieder Sicherheitslücken behoben.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*

**Matomo**: Und dann brachte das (hoffentlich) datenschutzkonforme Neugiertool, das früher Piwik hieß, aber nun [Matomo](cp^Piwik) genannt werden will, ein [Update auf die Version 3.9.0](https://matomo.org/changelog/matomo-3-9-0/) heraus. Aus Sicherheitsgründen scheint das Update nicht dringend notwendig zu sein. Bei meiner Aversion gegen Nuller-Versionen werde ich daher hier auf die fehlerbereinigte Version 3.9.1 warten, die sicher in wenigen Tagen folgen wird. So sicher, wie das Amen in der Kirche.
