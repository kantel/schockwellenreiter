#title "Ein neues Ren'Py ist draußen – 20190304"
#maintitle "Noch ein Update: Ren'Py 7.2.0 ist draußen"
#prevp "2019030402"
#nextp "2019030404"

<%= html.getLink(imageref("renpy72-b"), "cp^RenPy") %>

## Noch ein Update: Ren'Py 7.2.0 ist draußen

Von [Ren'Py](cp^Ren'Py), der freien (MIT-Lizenz), auf Python basierenden Engine zur Erstellung von *Visual Novels*, Textadventures, Präsentationen, animierten Illustrationen und mehr wurde die [Version 7.2.0](https://www.renpy.org/latest.html) veröffentlicht. Wichtige neue Sprachfunktionen sind:

[[dr]]<%= html.getLink(imageref("renpy-logo"), "cp^Ren'Py") %>[[dd]]

- Menüs nehmen jetzt Argumente an, dadurch sind nun Menüoptionen möglich.
- Die `say`-Anweisung kann jetzt ein temporäres Bildattribut annehmen, wodurch die Emotion eines Charakters für eine einzelne Anweisung geändert werden kann (siehe Screenshot oben).

Und viele kleine Änderungen mehr. Eine vollständige Liste aller Neuerungen und Bugfixes findet Ihr im [Changelog](https://www.renpy.org/doc/html/changelog.html).

Außerdem -- aber ich weiß nicht, ob das wirklich neu ist (für mich schon) -- kan man mit Ren'Py nun auch Anwendungen für Android und iOS erstellen. Dazu müssen allerdings zusätzliche Pakete geladen werden, die Ihr ebenfals auf der [Download-Seite](https://www.renpy.org/latest.html#dic) findet.