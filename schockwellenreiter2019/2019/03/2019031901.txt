#title "Wieder eine Riesenlinkschleuder für Webworker – 20190319"
#maintitle "Wieder eine Riesenlinkschleuder für Webworker"
#prevp "2019031803"
#nextp "2019032001"


<%= html.getLink(imageref("dickelindavorfrueling-b"), "Vorfrühling auf der Dicken Linda") %>

## Wieder eine Riesenlinkschleuder für Webworker

Ich schaffe es nicht, einen kompletten Relaunch des *Schockwellenreiters* durchzuziehen. Daher habe ich beschlossen, diese Seiten einfach Schritt für Schritt in kleinen Stücken zu modernisieren. Zur Vorbereitung hatte ich dann meinen Feedreader durchforstet und bin auf eine Unmenge von Links zu HTML, JavaScript, CSS und anderen interessanten Dingen für Webworker gestoßen, die ich Euch nicht vorenthalten will.

### HTML

- Bei HTML-Tabellen muß ich auch oft immer erst nachgooglen, welche Tricks man anwenden kann. *Alexander Gilmanov* weiß Bescheid: [HTML Tables: All there is to know about them](https://medium.freecodecamp.org/html-tables-all-there-is-to-know-about-them-d1245980ef96). Ein sehr ausführlicher Artikel (18 Leseminuten).

- *Abhishek Jakhar*: [A step-by-step guide to getting started with HTML tables](https://medium.freecodecamp.org/a-step-by-step-guide-to-getting-started-with-html-tables-7f43b18f962b).

### CSS / SCSS / SASS

- Von *Prossy Nakimera* gibt es eine [Basic Introduction to CSS](https://medium.com/the-andela-way/basic-introduction-to-css-81f6041b92d0), die einfach nur die Grundlagen legen will.

- *Bret Cameron*: [8 useful CSS tricks: Parallax images, sticky footers and more](https://medium.com/@bretcameron/parallax-images-sticky-footers-and-more-8-useful-css-tricks-eef12418f676).

- *Abhishek Jakhar*: [How to create an image gallery with CSS Grid](https://medium.freecodecamp.org/how-to-create-an-image-gallery-with-css-grid-e0f0fd666a5c).

- *Mary Lou*: [Image Distortion Effects with SVG Filters](https://tympanus.net/codrops/2019/03/12/image-distortion-effects-with-svg-filters/). Diese Filter sind neue CSS-Eigenschaften und funktionieren (noch) nicht in allen Browsern.

- *Jennifer Bland*: [How to implement horizontal scrolling using Flexbox](https://medium.freecodecamp.org/horizontal-scrolling-using-flexbox-f9d16817f742).

- [Gewinkelte Verläufe](https://blog.kulturbanause.de/2019/02/css-conic-gradients-gewinkelte-verlaeufe/) gehören in vielen Graphik- und Bild­bearbeitungs­programmen zur Standard-Ausstattung. Sie ermöglichen zahlreiche graphisch ansprechende Effekte, die mit linearen oder radialen Verläufen nicht möglich sind. In CSS könnt ihr gewinkelte Verläufe mit der `conic-gradient()`-Funktion erzeugen.

- Mit Hilfe der [Scroll Snap-Technik von CSS](https://blog.kulturbanause.de/2019/02/css-scroll-snap/) könnt ihr steuern, daß der Browser an bestimmten Stellen im Layout einrastet, wenn über diese Punkte hinweg gescrolled werden soll.

- *Zlatan Bekric*: [An intro to CSS Image Sprites: they’re easy to learn and great to know](https://medium.freecodecamp.org/an-intro-to-css-image-sprites-theyre-easy-to-learn-and-great-to-know-c13beec82403).

- Vom *JavaScript Teacher* gibt es [The Complete Guide to SCSS/SASS](https://medium.freecodecamp.org/the-complete-guide-to-scss-sass-30053c266b23).

- Noch einmal der *JavaScript Teacher*: [A Simple CSS Animation Tutorial](https://medium.freecodecamp.org/a-simple-css-animation-tutorial-8a35aa8e87ff).

### JavaScript

- Für umme lesen: Das [React Handbook](https://reacthandbook.com/) von *Flavio Copes*. Der gesamte Text ist online frei verfügbar und wird vom Autor regelmäßig aktualisiert. Das Buch in Dateiform versendet der Autor, nachdem man seinen Namen und seine Emailadresse angegeben hat.

- *Nash Vail*: [How to display an image on HTML5 canvas](https://www.nashvail.me/blog/canvas-image/). Nach der Lektüre dieses Tutorials dürften keine Fragen mehr offen sein. (Mit Super-Mario-Bildchen!)

### Websites bauen – Tips und Tricks

- *Jonathan Sexton*: [How to minify images with Gulp & gulp-imagemin and boost your site’s performance](https://medium.freecodecamp.org/how-to-minify-images-with-gulp-gulp-imagemin-and-boost-your-sites-performance-6c226046e08e).

- *Jonathan Sexton* zum Zweiten: [Establish your online presence with these simple ways to deploy your portfolio](https://medium.freecodecamp.org/establish-your-online-presence-with-these-simple-ways-to-deploy-your-portfolio-37101569909).

- *Jack Ballinger*: [Building a simple (cheap) website from scratch (part 1)](https://medium.com/devopslinks/building-a-simple-cheap-website-from-scratch-part-1-830c47134793). Der angekündigte zweite Teil ist bisher leider noch nicht erschienen.

- *Anurag Majumdar*: [How to write simple modern JavaScript apps with Webpack and progressive web techniques](https://medium.freecodecamp.org/how-to-write-simple-modern-javascript-apps-with-webpack-and-progressive-web-techniques-a30354eab214), denn Webpack wollte ich mir schon lange einmal anschauen.

- Noch ein Webpack-Tutorial, dieses Mal von *Samuel Omole*: [How to build modern applications with WEBPACK](https://medium.freecodecamp.org/how-to-build-modern-applications-with-webpack-c81ccf6dd54f).

- Und zumAbschluß ein *Highlight*: [Why Publii is a Great WordPress Alternative for New Bloggers](https://getpublii.com/blog/why-publii-is-great-wordpress-alternative-bloggers.html) von *Lee Batten*.

**War sonst noch was?** Ach ja, gestern berichtete ich, daß [Hackernoon Medium verlassen will](Warum will Hackernoon Medium verlassen? – 20190318) und heute fand ich diesen Artikel von *Kent C. Dodds*: [Goodbye Medium](https://kentcdodds.com/blog/goodbye-medium). Und zum 30. Geburtstag des WWW könnt Ihr [surfen wie 1989](https://www.golem.de/news/worldwideweb-rebuild-surfen-wie-1989-1902-139529.html), denn das CERN hat [Tim Berners-Lees](cp^Tim Berners-Lee) Webbrowser wiederbelebt.