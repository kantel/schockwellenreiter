#title "Schau Mama, ein Bücher-Sheltie! – 20190322"
#maintitle "Schau Mama, ein Bücher-Sheltie!"
#prevp "2019032201"
#nextp "2019032501"

<%= html.getLink(imageref("buechersheltie-b"), "Ein Bücher-Sheltie") %>

**Schau Mama, ein Bücher-Sheltie!** Wieder ist es Freitag und Freitags gibt es ein Hundebild im *Schockwellenreiter*, denn das ist langjährige Tradition. Ob es wirklich die nächsten zwei Tage weniger Updates als gewohnt hier im <del>Blog</del> Kritzelheft gibt, kann ich nicht wirklich sagen, denn auch werktags ist ja die Update-Frequenz geringer geworden. Als Rentner hat man ja nie Zeit.&nbsp;🤓

Wie dem auch sei, ich möchte die nächsten Tage wieder damit verbringen, viel zu lesen und zu schreiben, meine Experimente mit [Pygame Zero](cp^Pygame Zero) und [Processing.py](cp^processingpy) fortzusetzen und dann natürlich die kleine Fellkugel zu bespaßen.

Die Wetterfrösche haben uns einen zaghaften Frühlingsanfang versprochen. Der Sheltie und ich werden versuchen, ihn zu genießen. Macht es uns doch einfach nach.

Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photogabi]]*