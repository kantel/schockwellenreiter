#title "Harvesting Interactive Fiction – 20190304"
#maintitle "Harvesting Interactive Fiction"
#prevp "2019030403"
#nextp "2019030501"


## Im Schockwellenreiter TV: Harvesting Interactive Fiction

<iframe width="852" height="480" src="https://www.youtube.com/embed/EXW1ts6tZh4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Und da ich mit [Ren'py](cp^Ren'Py) sowieso gerade bei *Visual Novels* und *Interactive Fiction* (IF) [war](Ein neues Ren'Py ist draußen – 20190304), schiebe ich hier gleich noch einen Vortrag hinterher: In »Harvesting Interactive Fiction« führt *Heather Albano* von *Choice of Games* eine Führung durch einige wichtige Interactive-Fiction-Spiele, die in den letzten Jahren veröffentlicht wurden, einschließlich kommerzieller und freier Titel, und schlägt Möglichkeiten vor, wie IF-Techniken an andere Spielarten angepaßt werden können.

Ich habe mir das Video auch noch nicht angeschaut, aber in schnelles Durchscrollen hatte mir gezeigt, daß mit [Twine](cp^Twine 2) und [ink/Inky](cp^Ink und Inky) mindestens zwei Tools darin vorkommen, mit denen ich auch schon seit langem etwas anstellen wollte. Ich werde mir das Video heute abend reinziehen und bin sehr gespannt.