#title "Schau Mama, ein Hundebild! – 20190301"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2019030102"
#nextp "2019030301"

<%= html.getLink(imageref("joeyteltowkanal-b"), "Joey am Teltowkanal in Britz") %>

**Schau Mama, ein Hundebild!** Denn es ist Freitag und eine lange und ereignisreiche Woche geht zu Ende, die der Sheltie in diversen Tierarztpraxen erlebt hat. Aber nun sind wir durch und im großen und ganzen ging sie dann doch sehr positiv für die Fellkugel aus: Die Augen sind wieder klar und feucht und der Verband an der Pfote ist auch weg. Allerdings soll er sich noch drei Wochen ein wenig schonen und dann hoffen wir, daß er wieder fast der alte ist. Fröhlicher und frecher ist er schon wieder …

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1593278578&asins=1593278578&linkId=296a8972bf5854ebe028dfc690001346&show_border=true&link_opens_in_new_window=true"></iframe> <iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1593278675&asins=1593278675&linkId=c1591a7dbdc46d32daff5d0e4109bc0d&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Und Euch soll das Photo des Hundes darüber hinwegtrösten, daß es die nächsten zwei Tage vermutlich keine oder nur sehr wenige Updates hier im *Schockwellenreiter* geben wird. Denn »[Mission Python][a1]« und »[Math Adventures with Python][a2]« schauen mich auf meinem Schreibtisch schon erwartungsvoll an&nbsp;🐍. Genau so erwatzungsvoll ist aber auch der kleine Sheltie, der bespaßt werden will.&nbsp;🐶

[a1]: https://www.amazon.de/Mission-Python-Code-Space-Adventure/dp/1593278578/ref=as_li_ss_tl?ie=UTF8&qid=1551375397&sr=8-1&keywords=Mission+Python&linkCode=ll1&tag=derschockwell-21&linkId=50328e0c6fbf78fe284149d4d1893c9d&language=de_DE

[a2]: https://www.amazon.de/Math-Adventures-Python-Fractals-Automata/dp/1593278675/ref=as_li_ss_tl?ie=UTF8&qid=1550943742&sr=8-1&keywords=math+adventures+with+python&linkCode=ll1&tag=derschockwell-21&linkId=5d2cf639931c81a8efe1746e6e922925&language=de_DE

Natürlich soll der kleine Joey zu seinem Recht kommen. Vielleicht inspirieren mich aber auch die beiden Bücher zu neuen Experimenten, deren Ergebnis ich hier im <del>Blog</del> Kritzelheft veröffentlichen möchte. Warten wir es ab.

Für mein Wochenendprogramm und das Wochenendprogramm der Fellkugel ist also gesorgt. Ich wünsche Euch allen da draußen daher ein schönes Wochenende, in das ich mich hiermit verabschiede. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*