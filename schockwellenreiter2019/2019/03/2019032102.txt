#title "Pi Day Postponed – 20190321"
#maintitle "Pi Day Postponed"
#prevp "2019032101"
#nextp "2019032103"

## Pi Day Postponed

<iframe width="852" height="480" src="https://www.youtube.com/embed/pn2vlselv_g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Der [Tag zu Ehren der Kreiszahl Pi](https://de.wikipedia.org/wiki/Pi-Tag) war exakt vor einer Woche und *Daniel Shiffman* hat ihn gebührend mit einer schrägen *Coding Challenge* [gefeiert](http://blog.schockwellenreiter.de/2019/03/2019031401.html). Aber es geht noch schräger, denn Pi hat sich auch in den Rändern der [Mandelbrot-Menge](https://de.wikipedia.org/wiki/Mandelbrot-Menge) (aka Apfelmännchen oder [Mandelbrötchen](http://blog.schockwellenreiter.de/2017/05/2017052302.html)) [versteckt](http://www.pi314.net/eng/mandelbrot.php). Es ist sicher nicht der effizienteste Weg, die einzelnen Nachkommastellen von Pi zu berechnen, aber daß sie sich da versteckt haben, ist schon spannend.

In obigem Video zeigt *Shiffman*, wie er diese Ziffern mit [Processing (Java)](cp^Processing) aus der Mandelbrotmenge herauskitzelt. Es stellte sich heraus, daß dies mit den Processing-eigenen Bordmitteln nicht zu erreichen war, er mußte schon die Java-Klassen [BigDecimal](https://docs.oracle.com/javase/7/docs/api/java/math/BigDecimal.html) und [BigInteger](https://docs.oracle.com/javase/7/docs/api/java/math/BigInteger.html) importieren und nutzen. Aber selbst damit dauerte die Berechnung der ersten zehn Nachkommastellen von Pi in Processing etwa zwölf Stunden. Kein Wunder, daß diese Challnge eine Woche Verspätung hatte.

Wir Pythonistas haben es da einfacher. Nicht nur, daß wir mit [Numpy](cp^numpy) schnelle numerische Berechnungen durchführen können und komplexe Zahlen in Python schon fest eingebaut sind, sondern wir können uns auch mit [Numba](http://numba.pydata.org/) ein [schnelles Mandelbrötchen](http://blog.schockwellenreiter.de/2016/10/2016101304.html) backen. Das erinnert mich daran, daß eine Fortsetzung [meiner Erkundigung der Mandelbrotmenge](Die Reise ins Seepferdchental – 20190114), die ich mit Numpy und Numba fortsetzen wollte, noch aussteht. Und natürlich juckt es mich in den Fingern, auch Pi dort zu entdecken. Und noch mehr, denn dort haben sich noch viel mehr überraschende Dinge versteckt. Doch dazu mehr im [nächsten Beitrag](Entdeckungen in der Mandelbrotmenge – 20190321).