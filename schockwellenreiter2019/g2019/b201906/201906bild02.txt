#title "Schrippenfeuerwehr"
#prevp "201906bild01"
#nextp "201906bild03"

<%= html.getLink(imageref("schrippenfeuerwehr"), "https://www.flickr.com/photos/schockwellenreiter/47993068051/") %>

In Guben werden die Schrippen mit der Feuerwehr geliefert. Aufgenommen am 1. Juni 2018. *[[photojoerg]]*