#title "Her mit den Hundekeksen!"
#prevp "201902bild22"
#nextp "201902bild24"

<%= html.getLink(imageref("hundekekse"), "https://www.flickr.com/photos/schockwellenreiter/30693217687/") %>

Aufgenommen am 28.Oktober 2018 beim Rally-Obedience-Halloweencup des HSG Berlin. *(Photo &copy; 2018: Sascha Harting)*