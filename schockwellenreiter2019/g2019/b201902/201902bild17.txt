#title "Niklaus Wirth"
#prevp "201902bild16"
#nextp "201902bild18"

<%= html.getLink(imageref("niklauswirth"), "https://commons.wikimedia.org/wiki/File:Niklaus_Wirth,_UrGU.jpg") %>

Niklaus Wirth bei einer Vorlesung an der russischen Ural State University, aufgenommen am 3. Oktober 2005. *(Photo: Tyomitch, Quelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Niklaus_Wirth,_UrGU.jpg))*