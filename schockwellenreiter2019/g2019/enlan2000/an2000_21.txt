#title "Im Jahr 2000: Crocket-Partie"
#prevp "an2000_20"
#nextp "an2000_22"


<%= imageref("2000-21") %>

Diese Bilderserie wurde von *[Marc Côté](https://publicdomainreview.org/collections/france-in-the-year-2000-1899-1910/)* und anderen Zeichnern in den Jahren 1899, 1900, 1901 und 1910 erstellt. Man fand die kleinen Bildchen vorwiegend in traditionellen Zigarren- und Zigarettenboxen, aber die Motive waren auch so beliebt, daß auch Postkarten mit ihnen gedruckt wurden. (Vgl. Hans-Tommy Laeng: *»[Blicke in die Zukunft von anno dazumal][a1]«*, Berlin (Lit Verlag Dr. W. Hopf) 2017, Seiten 40 - 52)

[a1]: https://www.amazon.de/Blicke-die-Zukunft-anno-dazumal/dp/3643135122/ref=as_li_ss_tl?ie=UTF8&qid=1548248806&sr=8-1&keywords=Blicke+in+die+Zukunft+von+anno+dazumal&linkCode=ll1&tag=derschockwell-21&linkId=ec6d2f5b9a1e385b14f9162f9d702bd9&language=de_DE