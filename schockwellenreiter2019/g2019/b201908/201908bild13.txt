#title "Butt Song"
#prevp "201908bild12"
#nextp "201908bild14"

<%= html.getLink(imageref("butt_song_1"), "https://www.pri.org/stories/2014-02-14/hieronymus-bosch-painted-sheet-music-mans-butt-and-now-you-can-hear-it") %>

Ausschnitt aus dem Tryptichon *[Der Garten der Lüste](https://de.wikipedia.org/wiki/Der_Garten_der_L%C3%BCste_(Bosch))* (ca. 1490-1500) von [Hieronymus Bosch](https://de.wikipedia.org/wiki/Hieronymus_Bosch).