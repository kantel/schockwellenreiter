#title "Mexikoplatz, Slatdorpweg"
#prevp "201908bild20"
#nextp "201908bild22"

<%= html.getLink(imageref("slatdorpweg"), "https://www.flickr.com/photos/schockwellenreiter/48643843227/") %>

Berlin-Zehlendorf, Slatdorpweg am Mexikoplatz. Hier steppt der Bär! Aufgenommen am 12. August 2019. *[[photojoerg]]*