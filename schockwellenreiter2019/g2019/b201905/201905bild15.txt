#title "Sattel und Speiche"
#prevp "201905bild14"
#nextp "201905bild16"

<%= html.getLink(imageref("sarrelundspeiche"), "https://www.flickr.com/photos/schockwellenreiter/47773820422/") %>

»Sattel und Speiche« in der Neuköllner Walterstraße, aufgenommen am 29. April 2019. *[[photojoerg]]*
