#title "Röntgen und sein Apparat"
#prevp "201911bild31"
#nextp "201911bild33"

<%= html.getLink(imageref("roentgen"), "https://daily.jstor.org/the-x-ray-craze-of-1896/") %>

*[Wilhelm Conrad Röntgen](https://de.wikipedia.org/wiki/Wilhelm_Conrad_R%C3%B6ntgen)* und sein Apparat. Über die X-Ray-Besessenheit des viktorianischen Zeitalters berichtet [JSTOR Daily](https://daily.jstor.org/the-x-ray-craze-of-1896/). *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Wilhelm_Conrad_Roentgen_looking_into_an_X-ray_screen_placed_Wellcome_L0027361.jpg))*