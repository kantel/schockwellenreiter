#title "Alwins Mauerspechtgitarre (mit Sheltie)"
#prevp "201911bild09"
#nextp "201911bild11"

<%= html.getLink(imageref("alwin"), "https://www.flickr.com/photos/schockwellenreiter/49033991403/") %>

Mauerspecht [Alwin Nachtweh](http://mauer-specht.de/), [Rixdorf Music Company](https://neubritz.jimdo.com/2017/06/26/zu-gast-bei-der-rixdorf-music-company/), Photo vom 8. November 2019. *[[photogabi]]*