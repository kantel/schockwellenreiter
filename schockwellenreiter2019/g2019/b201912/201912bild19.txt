#title "Joey beim Halloweencup 2018 der HSG Berlin"
#prevp "201912bild18"
#nextp "201912bild20"

<%= html.getLink(imageref("halloweencup2018"), "https://www.flickr.com/photos/schockwellenreiter/30693217687/") %>

Aufgenommen am 28. Oktober 2018. *(Photo &copy; 2018: Sascha Harting)*