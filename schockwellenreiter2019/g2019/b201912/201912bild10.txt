#title "Kleiner Hund auf großer Fahrt"
#prevp "201912bild09"
#nextp "201912bild11"

<%= html.getLink(imageref("kleinerjoeygrossefahrt"), "https://www.flickr.com/photos/schockwellenreiter/5523093017/") %>

Aufgenommen am 13. März 2011. *(Photo &copy; 2011: Vanessa G.)*