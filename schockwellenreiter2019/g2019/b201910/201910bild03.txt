#title "Parkbühne Wuhlheide (2019)"
#prevp "201910bild02"
#nextp "201910bild04"

<%= html.getLink(imageref("parkbuehnewuhlheide"), "https://www.flickr.com/photos/schockwellenreiter/48775915832/") %>

Aufgenommen am 22. September 2019. *[[photojoerg]]*