#title "Georgios Jakobides: Lesendes Mädchen (etwa 1882)"
#prevp "201910bild16"
#nextp "201910bild18"

<%= html.getLink(imageref("jakobidesgirlreading"), "https://commons.wikimedia.org/wiki/File:Georgios_Jakobides_Girl_reading_c1882.jpg?uselang=de") %>

[Georgios Iakovidis](https://de.wikipedia.org/wiki/Georgios_Jakobides) (griechisch Γεώργιος Ιακωβίδης, * 11. Januar 1853 in Chidira auf Lesbos; † 13. Dezember 1932 in Athen)Georgios Iakovidis (griechisch Γεώργιος Ιακωβίδης, * 11. Januar 1853 in Chidira auf Lesbos; † 13. Dezember 1932 in Athen), auch Georgios Jakobides oder Georg Jakobides (signierte in Deutschland mit G. Jakobides), war ein bedeutender griechischer Maler und Vertreter der Münchner Schule. war ein bedeutender griechischer Maler und Vertreter der Münchner Schule. *(Bild: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Georgios_Jakobides_Girl_reading_c1882.jpg?uselang=de))*