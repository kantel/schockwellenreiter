#title "Einfahrt in den Hauptbahnhof Wuhlheide"
#prevp "201910bild01"
#nextp "201910bild03"

<%= html.getLink(imageref("dampflok2019"), "https://www.flickr.com/photos/schockwellenreiter/48775402228/") %>

Aufgenommen beim Dampflokfest 2019 der [Parkeisenbahn Wuhlheide](https://www.parkeisenbahn.de/startseite.html) am 22. Septemebr 2019. *[[photojoerg]]*