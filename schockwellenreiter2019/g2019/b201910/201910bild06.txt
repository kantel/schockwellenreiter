#title "Der Sheltie am Bahnhof Plänterwald"
#prevp "201910bild05"
#nextp "201910bild07"

<%= html.getLink(imageref("sheltiebhfplaenterwald"), "https://www.flickr.com/photos/schockwellenreiter/48717255182/") %>

Aufgenommen am 11. September 2019. *[[photojoerg]]*