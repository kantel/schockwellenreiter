#title "Geschafft! Durchs Ziel!"
#prevp "201901bild11"
#nextp "201901bild13"

<%= html.getLink(imageref("geschafft"), "https://www.flickr.com/photos/schockwellenreiter/28680889897/") %>

Und Joey freut sich! Aufgenommen beim RO-Turnier des HSV Kremmen/Schwante am 21. Juli 2018. *(Photo &copy; 2018: Sascha Harting)*

