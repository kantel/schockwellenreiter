#title "Ulisse Caputo: Reading Light"
#prevp "201901bild16"
#nextp "201901bild18"

<%= html.getLink(imageref("readinglight"), "https://www.flickr.com/photos/gandalfsgallery/45745532914/") %>

*Ulisse Caputo* (1872 - 1948) war ein neapolitanischer Maler, der in Paris lebte. Da er 1948 gestorben ist, sind seine Werke ab Januar dieses Jahres in Deutschland gemeinfrei.