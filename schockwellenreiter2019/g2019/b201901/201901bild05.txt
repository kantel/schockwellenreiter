#title "Weißes Sofa Bürgerstraße"
#prevp "201901bild04"
#nextp "201901bild06"

<%= html.getLink(imageref("weissessofabuergerstrasse"), "https://www.flickr.com/photos/schockwellenreiter/45844910514/") %>

Dieses weiße Sofa, das sich gestern in der Bürgersteraße verirrt hatte, wurde heute wieder eingefangen. Aufgenommen am 2. Januar 2019. *[[photojoerg]]*

