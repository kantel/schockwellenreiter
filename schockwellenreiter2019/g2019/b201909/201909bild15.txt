#title "Schaltkastenpanda in Treptow"
#prevp "201909bild14"
#nextp "201909bild16"

<%= html.getLink(imageref("schaltkastenpanda"), "https://www.flickr.com/photos/schockwellenreiter/48717253092/") %>

Auch Treptow ist – wie fast ganz Berlin – im 🐼-Rausch. Aufgenommen am 11. September 2019. *[[photojoerg]]*