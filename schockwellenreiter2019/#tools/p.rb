# encoding: utf-8

def p(a)
  title, path = html.getTitleAndPath(@adrPageTable[:title])
  # rootURL = "http://blog.schockwellenreiter.de/"
  # s1 = ""
  s=""
  if @adrPageTable[:permalink]
    # s1 << rootURL + path + "\#" + a
    s = "<i>(<a title='Kommentare via Email' href='mailto:der@schockwellenreiter.de?subject=" + title + "#" + a + "'>Kommentieren</a>)&nbsp;(<a title='Permalink zu diesem Beitrag' href='#" + a + "'>#</a>)</i>"
  end
end