# encoding: utf-8

def k()
  title, path = html.getTitleAndPath(@adrPageTable[:title])
  s=""
  if @adrPageTable[:permalink]
    s = "<i>(<a title='Kommentare via Email' href='mailto:der@schockwellenreiter.de?subject=" + title + "'>Kommentieren</a>)</i>"
  end
end