<%= pageheader() %>
<div class="navbar navbar-static-top navbar-inverse">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a href="Startseite" class="brand">Schockwellenreiter</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li><a href="Archiv 2019">Archiv</a></li>
					<li><a href="Galerie 2019">Galerie</a></li>
					<li><a href="Essays 2019">Essays</a></li>
					<li><a href="Schockwellenreiter TV 2019">TV</a></li>
					<li class="divider-vertical"></li>
					<li><a href="Wiki">Wiki</a></li>
					<li><a href="About">&Uuml;ber</a></li>
					<li class="divider-vertical"></li>
					<li><a href="shop">Shop</a></i>
					<li class="divider-vertical"></li>
					<form class="navbar-search" method="get" id="search"
					 	action="http://duckduckgo.com/">
						<input type="hidden" name="sites" value="blog.schockwellenreiter.de"/>
						<input type="text" class="search-query" name="q" maxlength="255"
						 placeholder="DuckDuckGo-Suche"/>
					</form>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>

<div class="container">
	<div class="hero-unit">
		<h1>Der Schockwellenreiter</h1>
		<p>Die tägliche Ration Wahnsinn – seit April 2000 im Netz</p>
	</div>

	<div class="row">
		<div class="span8"> <!-- Hauptspalte -->
			<div class="breadcrumbs">
				<%= breadcrumbs %> <%= title %>
			</div>
			<div class="nextprev">
				<%= nextprev2() %>
			</div>
			<br clear="all" />

			<p id="bodytext"></p>
			
		</div> <!-- Ende Hauptspalte -->
		<div class="span4"> <!-- rechte Spalte -->
			<h3>Über</h3>
			<p class="small">Der Schockwellenreiter ist seit dem 24. April 2000 das <del>Weblog</del> digitale Kritzelheft von <a href="About">Jörg Kantel</a> (Neuköllner, <del>EDV-Leiter</del> Rentner, Autor, Netzaktivist und <a href="http://cognitiones.kantel-chaos-team.de/notizblock/agility.html">Hundesportler</a> — Reihenfolge rein zufällig). Hier steht, was mir gefällt. Wem es nicht gefällt, der braucht ja nicht mitzulesen. Wer aber mitliest, ist herzlich willkommen und eingeladen, mitzudiskutieren!</p>
			<p class="small">Alle eigenen Inhalte des Schockwellenreiters stehen unter einer <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/de/">Creative-Commons-Lizenz</a>, jedoch können fremde Inhalte (speziell Videos, Photos und sonstige Bilder) unter einer anderen Lizenz stehen.</p>
			<p class="small">Der Besuch dieser Webseite wird aktuell von der <a href="http://cognitiones.kantel-chaos-team.de/webworking/tools/piwik.html">Piwik Webanalyse</a> erfaßt. Hier können Sie der <a href="http://blog.schockwellenreiter.de/impressum.html#donottrack">Erfassung widersprechen</a>.</p>
			<p class="small">Diese Seite verwendet keine Cookies. Warum auch? Was allerdings die <code>iframes</code> von Amazon, YouTube und Co. machen, entzieht sich meiner Kenntnis.</p>
			<hr>
			<h3>Die letzten Tage</h3>
			<ul class="nav nav-list small">
				<%= monatsuebersicht %>
				<li><a href="Archiv 2019">Archiv</a></li>
			</ul>
			<hr /> <!-- Werbung -->
			<p class="small"><i>Werbung</i></p>
			<p><iframe src="http://www.schockwellenreiter.de/banner_small.html" frameborder="0" width="324" height="42" marginheight="0" marginwidth="0" scrolling="no">Netscape4.x-Nutzer, die auch in den Genu&szlig; der Werbebanner kommen wollen, klicken bitte <a href="http://www.schockwellenreiter.de/banner.html" target="Banner"><b>hier</b></a>.</iframe></p>
			<hr /> <!-- Badges -->
			<div style="float:left; margin-right: 8px;">
				<a href="https://www.facebook.com/joerg.kantel"><%= imageref("facebook-micro") %></a>&nbsp;
				<a href="http://twitter.com/jkantel"><%= imageref("twitter-micro") %></a>&nbsp;
				<a href="http://www.flickr.com/photos/schockwellenreiter/?details=1"><%= imageref("flickr-micro") %></a><br />
				<a href="http://www.youtube.com/kantel"><%= imageref("youtube-micro") %></a>&nbsp;
				<a href="http://www.qype.com/people/kantel"><%= imageref("qype-micro") %></a>&nbsp;
				<a href="https://www.xing.com/app/profile/?name=Joerg_Kantel&tab=4"><%= imageref("xing-micro") %></a>
			</div><br clear="all" />
			<br />
			<p>
			</p>
			<hr /> <!-- Wieder Werbung -->
			<p class="small"><i>Wieder Werbung</i></p>
			<p>
				<script charset="utf-8" type="text/javascript" src="http://ws.amazon.de/widgets/q?rt=tf_mfw&ServiceVersion=20070822&MarketPlace=DE&ID=V20070822/DE/derschockwell-21/8001/70c8c33d-c3eb-4a96-9b9b-6b47a2871c76">
				</script>
			</p>
			<hr />
			<p><%= imageref("zone") %></p>
			<hr />
			<!-- <p>
				<script type="text/javascript" src="http://video.unrulymedia.com/wildfire_98969309.js"></script>
			</p> -->
			
		</div> <!-- Ende rechte Spalte -->
	</div> <!-- row -->

	<br />
		
	<div class="footer"> <!-- Footer -->
	<p>(<a href="http://creativecommons.org/licenses/by-nc-sa/3.0/de/">cc</a>) 2000-<%= yearnow() %> | Some Rights Reserved | Letzte Änderung: <%= clocknow() %> | <a href="Impressum">Impressum</a> | <a href="RSS-Feed">RSS-Feed</a></p>
	<p>
		<!-- RubyFrontier -->
		<a href="http://www.apeth.com/RubyFrontierDocs/default.html"><%= imageref("rubyfrontierbadge", {:width => "80", :height => "15", :alt => "RubyFrontier Badge", :title => "Made with RubyFrontier", :border => "0"}) %></a>
		<!-- Ende RubyFrontier -->
		&nbsp;
		<!-- CC-Button -->
		<a href="http://creativecommons.org/licenses/by-nc-sa/3.0/de/"><%= imageref("cc80x15", {:width => "80", :height => "15", :alt => "CC Logo", :title => "CC BY NC SA", :border => "0"}) %></a>
		<!-- Ende CC-Button -->
	</p>
	<br />
	</div> <!-- Ende Footer -->
</div>
<%= pagefooter() %>