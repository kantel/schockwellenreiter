#title "Essays 2019"
#prevp "galerie2019.html"
#nextp "schockwellenreitertv2019.html"

## Dezember 2019

- Worknote: [GitLab Pages, eine Alternative zu Amazons S3](GitLab Pages statt Amazons S3? – 20191225)? Zeit, sich nach Alternativen umzuschauen -- 25.&nbsp;Dezember 2019
- Spieleprogrammierung: [Apple Invaders in Pygame Zero](Apple Invaders in Pygame Zero – 20191222) -- 22.&nbsp;Dezember 2019
- Braintrust Query: [AWS mit eigenen Domains](Braintrust Query: AWS mit eigenen Domains – 20191219). Wie richtete man -- verdammt noch mal -- eine statische Website auf Amazon S3 ein? -- 19.&nbsp;Dezember 2019

<%= html.getLink(imageref("rubyfrontierdovsv02-s"), "RubyFrontierDocs v02") %>

- Worknote: [RubyFrontierDocs – es geht voran](RubyFrontierDocs – es geht voran – 20191218) mit den Werkzeugen für ein freies und dezentrales Web -- 18.&nbsp;Dezember 2019
- HyperCard: [Fahrrad für den Geist](HyperCard: Fahrrad für den Geist – 20191217). Wie wir mit alten Ideen und neuem Elan das Netz wieder der Bewußtseinsindustrie entreißen können -- 17.&nbsp;Dezember 2019

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1912047594&asins=1912047594&linkId=9a8272238398ad6a9a61911892563809&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Rezension von [Code the Classics, Volume 1](Code the Classics – 20191214). Ich bin begeistert! -- 14.&nbsp;Dezember 2019
- Spieleprogrammierung: [Ein rundenbasiertes Kampfsystem in Pygame Zero](Ein rundenbasiertes Kampfsystem in PGZ – 20191210) -- 10.&nbsp;Dezember 2019
- Worknote: [Getting Started with Python in Visual Studio Code](Python in Visual Studio Code – 20191209) -- 9.&nbsp;Dezember 2019
- Worknote: [Jupyter und Visual Studio Code, ein neuer Anlauf](Jupyter und Visual Studio Code – 20191208). Vielleicht werden *Visual Studio Code* und ich doch noch Freunde -- 8.&nbsp;Dezember 2019

## November 2019

- Ein Frosch fliegt durch die Luft: [Frogger in Pygame Zero](Ein Frosch fliegt durch die Luft – 20191130) -- 30.&nbsp;November 2019
- Worknote: [NW.js (Node Webkit) für Anfänger](Node Webkit für Anfänger (Links) – 20191126) (Links) -- 26.&nbsp;November 2019

[[dr]]<%= html.getLink(imageref("pygamezero"), "cp^Pygame Zero") %>[[dd]]

- (Video-) Tutorial und Worknote: [Spaß mit Pygame Zero: Froggit](Spaß mit Pygame Zero: Froggit – 20191124) -- 24.&nbsp;November 2019
- Tutorial: [Kollisionserkennung mit Processing.py und P5.js (Teil 2: Rechtecke)](Kollisionserkennung mit Python und P5 (2) – 20191120) -- 20.&nbsp;November 2019
- Tutorial: [Getting Started with Leaflet](Getting Started with Leaflet – 20191119) -- 19.&nbsp;November 2019

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3518100637&asins=3518100637&linkId=2dd92371b634da12fb47cbb6a14c1eeb&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Für umme lesen: [Raspberry Pi und Pygame Zero Bücher und Magazine](Für umme lesen: Raspy-Bücher – 20191118) -- 18.&nbsp;November 2019
- Static Sites Generatoren: [Nikola und Jekyll](Static Sites: Nikola und Jekyll – 20191117), eine Worknote -- 17.&nbsp;November 2019
- Worknote: [Coding Train Farbpalette – Update](Update: Coding Train Farbpalette – 20190913), jetzt mit grünen Hügeln und gelber Sonne -- 13.&nbsp;November 2019
- Tutorial: [Kollisionserkennung mit Processing.py und P5.js (Teil 1: Kreise)](Kollisionserkennung mit Python und P5 (1) – 20191111) -- 11.&nbsp;November 2019
- Worknote: [Digitaler Nachlaß, Online Notizen für statische Seiten und andere Ideen für ein nachhaltiges Web](Digitaler Nachlaß, statische Seiten … 20191107) -- 7.&nbsp;November 2019
- [Video-Tutorials zu openFrameworks](Video-Tutorials zu openFrameworks – 20191105), denn *Creative Coding* geht auch mit C++ -- 5.&nbsp;November 2019


## Oktober 2019

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3038002984&asins=3038002984&linkId=dbf336e657e32baaa946b991d00c7cf8&show_border=true&link_opens_in_new_window=true"></iframe> <iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=0312103530&asins=0312103530&linkId=8f033b8919dc48436dde7de854fbed5c&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Coding Mazes: [Noch mehr Irrgärten](Computer Mazes: Noch mehr Irrgärten – 20191030) -- 30.&nbsp;Oktober 2019
- [Wege aus einem Irrgarten](Wege aus dem Irrgarten mit der LHR – 20191027) mit der Linken-Hand-Methode (und Python) -- 27.&nbsp;Oktober 2019
- Programme aus der Gartenlaube: [Die Collatz-Vermutung](Die Collatz-Vermutung – 20191027) -- 27.&nbsp;Oktober 2019
- Worknote: [Freie Spiele-Assets von Bevouliin.com](Freie Spiele-Assets von Bevouliin.com – 20191024), denn freie Bilder für Spiele kann man nie genug haben -- 24.&nbsp;Oktober 2019

<%= html.getLink(imageref("maurerrose-s"), "Maurer Rose in Processing (Python)") %>

- Programmieren mit Processing.py: [Na bitte, geht doch: Maurer Rose](Na bitte, geht doch: Maurer Rose – 20191012) -- 12.&nbsp;Oktober 2019
- Worknote: [Grauwertberechnung in der Bildverarbeitung](Worknote: Grauwertberechnung – 20191008) -- 8.&nbsp;Oktober 2019
- Tutorial: [Video-Wiedergabe in P5.js](Video-Wiedergabe in P5 (JavaScript) – 20191003). Es bewegt sich was auf der Webseite -- 3.&nbsp;Oktober 2019

## September 2019

- Tutorial: [Basic Side Scroller mit P5.js](Basic Side Scroller mit P5 (3) – 20190915), Teil 3 -- 15.&nbsp;September 2019
- Worknote: [Coding Train Farbpalette](Coding Train Farbpalette – 20190913), Choo Choo -- 13.&nbsp;September 2019
- Braintrust Query: [P5.js: Kein touchStarted() unter Android?](No touchStarted() – 20190912) -- 12.&nbsp;September 2019
- [(Fast) gelöst: Basic Side Scroller mit P5.js](Basic Side Scroller mit P5 (Update) – 20190911) (Update, Worknote) -- 11.&nbsp;September 2019
- Tutorial: [Basic Side Scroller mit P5.js](Basic Side Scroller mit P5 (JavaScript), 1 – 20190910) (Teil 1) -- 10.&nbsp;September 2019
- Braintrust Query: [Altgriechisch auf dem Mac](Braintrust Query: Altgriechisch auf dem Mac – 20190905). Wo finde ich die diakritischen Zeichen auf der Tastatur? -- 5.&nbsp;September 2019
- Tutorial: [Bouncing Balls mit P5.js und ECMAScript 6 (ES6)](Bouncing Balls mit P5 und ES6 – 20190903), weil JavaScript das nächste große Ding ist -- 3.&nbsp;September 2019

<%= html.getLink(imageref("glascontainer-s"), "Glascontainer am Kranoldplatz") %>

- Rant: [So werde ich – dank des Berliner Senats – zur Umweltsau](So werde ich zur Umweltsau gemacht – 20190903) -- 3.&nbsp;September 2019

## August 2019

- Tutorial: [Erste Schritte mit Twine](Erste Schritte mit Twine – 20190825) für interaktive Geschichten und Textabenteuer -- 25.&nbsp;August 2019
- Worknote (und: Neu in meinem Wiki): [Bitsy @ localhost](Worknote: Bitsy @ localhost – 20190819) -- 19.&nbsp;August 2019
- Sisyphos & Co.: [Altgriechische Strafen mit Twine und Bitsy](Altgriechische Strafen in Twine und Bitsy – 20190816), noch eine Worknote -- 16.&nbsp;August 2019
- Worknote: [Paletten, Paletten](Paletten, Paletten – 20190814), denn (Farb-) Paletten kann man nie genug haben -- 14.&nbsp;August 2019

<%= html.getLink(imageref("alieninvaderspgz-s"), "Alien Invaders mit Pygame Zero – 20190807") %>

- Tutorial: [Alien Invaders mit Pygame Zero](Alien Invaders mit Pygame Zero – 20190807), ein neuer Anlauf -- 7.&nbsp;August 2019

## Juli 2019

- Tutorial: [Spiele programmieren mit Pygame Zero – die Basics (Teil 2)](Spiele programmieren mit Pygame Zero (Teil 2) – 20190724) -- 24.&nbsp;Juli 2019
- Tutorial: [Spiele programmieren mit Pygame Zero – die Basics (Teil 1)](Spiele programmieren mit Pygame Zero, Teil 1 – 20190722) -- 22.&nbsp;Juli 2019
- [Wireframe 18 und die Labyrinthe](Wireframe 18 und die Labyrinthe – 20190720) (Algorithmen zur Spieleprogrammierung) -- 20.&nbsp;Juli 2019
- Worknote: [Arcade gibt es ja auch noch](Arcade gibt es ja auch noch – 20190716), Python Arcade Library revisited -- 16.&nbsp;Juli 2019
- Worknote: [Wireframe, eine britische Zeitschrift für Spieleentwickler](Wireframe für umme lesen – 20190709), kann man kostenlos als PDF herunterladen -- 9.&nbsp;Juli 2019

<%= html.getLink(imageref("alieninvstage1-s"), "Alien Invaders mit Python und Pygame (Stage 1) – 20190707") %>

- Tutorial: [Alien Invaders mit Python und Pygame (Stage 1)](Alien Invaders mit Python und Pygame (Stage 1) – 20190707) -- 7.&nbsp;Juli 2019
- Tutorial: [Basic Side Scroller objektorientiert](Basic Side Scroller objektorientiert – 20190701) und in Pygame Zero programmiert -- 1.&nbsp;Juli 2019

## Juni 2019

- Tutorial: [Pygame Zero objektorientiert](Pygame Zero objektorientiert – 20190627) -- 27.&nbsp;Juni 2019
- Worknote: [Yarn, eine weitere Engine für Interactive Fiction](Yarn, eine weitere Engine für Interactive Fiction – 20191616). Ist Yarn mehr als nur ein Twine-Klon? -- 16.&nbsp;Juni 2019
- Nach MacOS 10.15 (Catalina) [wird MacOS X kastriert](MacOS 10.15 (Catalina) wird kastriert – 20190607). Ich glaube, ich sollte auf meine alten Tage doch noch zu einem »echten« UNIX wechseln  -- 7.&nbsp;Juni 2019

[[dr]]<%= html.getLink(imageref("renpy-logo"), "cp^renpy") %>[[dd]]

- Das Großvater-Paradox: [Was Visual Novels und Indie Comics voneinander lernen können](Das Großvater-Paradox – 201905605) -- 5.&nbsp;Juni 2019
- Video-Tutorial-Reihe: [Working with Data and APIs in JavaScript](Working with Data and APIs in JavaScript – 20190604) -- 4.&nbsp;Juni 2019

## Mai 2019

- Thread: [Wolfram Engine und Jupyter](Wolfram Engine und Jupyter – 20190528), wie geht das zusammen? -- 28.&nbsp;Mai 2019
- Update: [Wurzelverzeichnis für Jupyter ändern](Update: Wurzelverzeichnis für Jupyter ändern – 20190525), weil sich ein paar Kleinigkeiten geändert haben -- 25.&nbsp;Mai 2019
- Worknote: [Abenteuer Wolfram Engine – Mathematica für Arme](Abenteuer Wolfram Engine – 20190524)? -- 24.&nbsp;Mai 2019
- Rant: [Das Ende von Python 2.7? Daß ich nicht lache!](Das Ende von Python 2? Daß ich nicht lache! – 20190521) Da sei TigerJython vor -- 21.&nbsp;Mai 2019
- Tutorial: [Basic Side Scroller mit Processing.py, Teil 2](Basic Side Scroller mit Processing (Python), Teil 2 – 20190516). Jump, Ally, jump! -- 16.&nbsp;Mai 2019
- Worknote: [Auf dem Weg zur World-Markdown (mit Python)](Auf dem Weg zur World-Markdown (mit Python) – 20190509) -- 9.&nbsp;Mai 2019
- Computergeschichte(n): [Als die (Computer-) Welt noch schwarz-weiß war (Nachtrag 2)](Als die Welt noch schwarz-weiß war (Nachtrag 2) – 20190507) -- 7.&nbsp;Mai 2019

<%= imageref("particlesystem-s") %>

- Tutorial: [Ein Partikelsystem in Processing.py (1)](Ein Partikelsystem in Processing (Python) – 20190505) -- 5.&nbsp;Mai 2019
- Fingerübung: [Spaß mit Processing.py und Pi](Spaß mit Processing (Python) und Pi – 20190502) und der Viridis-Magma-Palette -- 2.&nbsp;Mai 2019
- Worknote: [Objektorientierte Spieleprogrammierung mit P5.js und Processing](OOP Game Development mit Processing (JS) – 20190502), eine Quelle der Inspiration -- 2.&nbsp;Mai 2019

## April 2019

<%= html.getLink(imageref("sidescroller1-s"), "Basic Side Scroller w/ Animation (2)") %>

- Tutorial: [Basic Side Scroller mit Processing.py (1)](Basic Side Scroller mit Processing (Python) – 20190428). Aus der Reihe »Algorithmen für Spieleprogrammierer« -- 28.&nbsp;April 2019
- Spaß mit Processing.py: [Re-Enactment und Re-Mixing](Re-Enactment und Re-Mixing – 20190425), frühe Computerkunst nachprogrammiert -- 25.&nbsp;April 2019
- Worknote: [Peasycam und Processing.py](Peasycam und Processing (Python) – 20190423), 3D super easy -- 23.&nbsp;April 2019
- Worknote: [Mit Pyinstaller Python-Apps für (fast) jedes Betriebssystem erstellen](Pyinstaller: Python-Apps für (fast) jedes OS – 20190410) -- 10.&nbsp;April 2019
- Der thüringische Staat läuft Amok und [hält das Zentrum für politische Schönheit für eine »kriminelle Vereinigung«](Staat hält Künstler für eine »kriminelle Vereinigung« – 20190404) -- 3.&nbsp;April 2019

## März 2019

- [Spaß mit Pygame Zero: Cute Space](Spaß mit Pygame Zero: Cute Space – 20190325), ein erstes kleines, vollständiges Spiel -- 25.&nbsp;März 2019
- [Pygame Zero Intro (2)](Pygame Zero Intro (2) – 20190318): Das Alien quietscht bei Mausberührung  -- 18.&nbsp;März 2019
- Großes Raunen in der Szene: [Warum will Hackernoon.com Medium verlassen?](Warum will Hackernoon Medium verlassen? – 20190318) Ist es die Flucht aus den Datensilos?  -- 18.&nbsp;März 2019

<%= html.getLink(imageref("pgzintro03-s"), "Intro to Pygame Zero 3") %>

- [Pygame Zero Intro (1)](Pygame Zero Intro (1) – 20190317), less code, more fun -- 17.&nbsp;März 2019
- Algorithmen für Spieleprogrammierer: [Tile Based Movement in Processing.py](Tile Based Movement in Processing (Python) – 20190312) -- 12.&nbsp;März 2019

<%= html.getLink(imageref("selectionsspiel-s"), "Screenshot: Shaun das Schaf") %>

- Noch mehr Spaß mit Processing.py: [Shaun das Schaf und seine Spießgesellen](Shaun das Schaf und seine Spießgesellen – 20190310) -- 10.&nbsp;März 2019
- Thread: [Was ist ein(e) Poodle Mission Control?](Poodle Mission Control? – 20190303) -- 3.&nbsp;März 2019
- Spaß mit Processing.py: [Ein handgezeichnetes Regenbogenraster](Regenbogenraster handgezeichnet – 20190301) -- 1.&nbsp;März 2019

## Februar 2019

<%= html.getLink(imageref("fuplot-s"), "Funktionenplotter in Processing (Python)") %>

- Tutorial: [Wir basteln uns einen Funktionenplotter](Wir basteln uns einen Funktionenplotter – 20190227) in Processing.py -- 27.&nbsp;Februar 2019
- Spaß mit Processing (Python): [Circle Snake](Spaß mit Processing (Python): Circle Snake – 20190224), denn das Processing (Python)-Wochenende geht weiter -- 24.&nbsp;Februar 2019
- Processing.py am Wochenende: [Vom Pointillismus zum Rectillismus](Vom Pointillismus zum Rectillismus – 20190223) -- 23.&nbsp;Februar 2019
- Computergeschichte(n) mit Python: [Als die Welt noch schwarz-weiß war (Nachtrag)](Als die Welt noch schwarz-weiß war (2) – 20190219) -- 22.&nbsp;Februar 2019

<%= html.getLink(imageref("floydsteinberg-s"), "Floyd-Steinberg-Dithering") %>

- Computergeschichte(n) mit Python: [Als die Welt noch schwarz-weiß war](Als die Computerwelt noch schwarz-weiß war – 20190219) -- 19.&nbsp;Februar 2019
- Tutorial: [Apple Invaders mit Pygame (Stage 1)](Apple Invaders mit Pygame (1) – 20190211) -- 11.&nbsp;Februar 2019
- Pygame erkunden (1): [Natürliche Bewegungen und Newtons Gesetze](Pygame erkunden (1): Motion Demo – 20190210) -- 10.&nbsp;Februar 2019
- [Neu in meinem Wiki und erste Schritte mit GUI Zero](Auch neu in Meinem Wiki: GUI Zero – 20190207). Graphische Benutzerschnittstellen in Python leichtgemacht -- 7.&nbsp;Februar 2019
- [Ein Pygame-Grundgerüst](Ein Pygame-Grundgerüst – 20190204), damit ich nicht jedesmal das Rad neu erfinden muß -- 4.&nbsp;Februar 2019

## Januar 2019

- Rezension: [Codierte Kunst – Programmieren mit Snap](Neu in meiner Bibliothek: Codierte Kunst – 20190124). Überflogen und für gut befunden -- 24.&nbsp;Januar 2019

<%= html.getLink(imageref("mandelbig-s"), "Die Reise ins Seepferdchental (1)") %>

- Chaos und Fraktale mit Python: [Die Reise ins Seepferdchental](Die Reise ins Seepferdchental – 20190114) -- 14.&nbsp;Januar 2019
- Worknote und Thread: [ApplePersistenceIgnoreState](ApplePersistenceIgnoreState – 20190112). Was will mir Apple damit sagen? -- 12.&nbsp;Januar 2019
- Worknote: [TextMate und Terminal](Worknote: TextMate und Terminal – 20190110), damit zusammenspielt, was zusammengehört -- 10.&nbsp;Januar 2019
- Rant (und Thread): [Private Repositories auf GitHub nun kostenlos – zu spät](GitHub privat – zu spät – 20190109) -- 9.&nbsp;Januar 2019
- Worknote: [Freie Graphik-Sets für Spieleprogrammierer](Freie Graphik-Sets für Spieleprogrammierer – 20190106), denn davon kann man nie genug haben -- 6.&nbsp;Januar 2019
- [Ich habe einen Plan!](Ich habe einen Plan – 20190403) Alles neu macht 2019 -- 4.&nbsp;Januar 2019
- Worknote: [RStudio und Anaconda-Python](RStudio und Anaconda-Python – 20190103) sollen nun zusammen spielen -- 3.&nbsp;Januar 2019

[[dr]]<%= imageref("pygamebunt") %>[[dd]]

- Weiter mit Pygame Zero: [Fetching Kitty](Weiter mit Pygame Zero: Fetching Kitty – 20190102) -- 2.&nbsp;Januar 2019
- Zeitvertreib: [Schöne Blümchen mit Pythons Schildkröte](Schöne Blümchen mit Pythons Schildkröte – 20180102) -- 2.&nbsp;Januar 2019

---

- **[Archiv Essays 2012 bis 2018](http://blog.schockwellenreiter.de/essays.html)**