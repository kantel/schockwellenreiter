#title "RSS-Feed August 2019"
#fileextension ".xml"
#kramdown false
#template "blank"
#xml true

<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("spaetsommersheltie-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild22.html"><img src="http://blog.schockwellenreiter.de/images/spaetsommersheltie-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Denn es ist wieder Freitag und am Sonntag endet für die Fellkugel und mich auch die hundesportliche Sommerpause. Denn dann werden der Sheltie und ich an einem Rally-Obedience-Turnier bei den Sportsfreunden des VSB Berlin teilnehmen. Es ist unsere erste Veranstaltung seit dem Turnier Ende Juni bei JuuHu und ich hoffe, daß wir in Lankwitz wieder an unseren Erfolg von damals anknüpfen können. Daher drückt uns bitte die Daumen.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019083003.html</link>
</item>
<item>
	<title>How to make a visual novel with Ren’Py</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/L9Vt7fcwkXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />SLim Games bastelt <i>Visual Novels</i> in Ren’Py, die nach eigenen Angaben nicht ganz jugendfrei sind. Daneben hat er (bisher) drei Videos auf YouTube hochgeladen, die in die Python-basierte Spieleengine einführen sollen. Das verspricht doch mal eine Abwechslung von den ewigen großäugigen und süßlichen Manga-Figuren, die sonst die gängigen Ren’Py-Tutorien bevölkern.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019083002.html</link>
</item>
<item>
	<title>Firmware Update für Logitech Unifying Empfänger schließt Sicherheitslücke</title>
	<description>
			<![CDATA[<% imageref("slatdorpweg-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild21.html"><img src="http://blog.schockwellenreiter.de/images/slatdorpweg-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Logitech behebt zwei Schwachstellen in seiner Unifying-Funktechnik, die in zahlreichen Funktastaturen und Funkmäusen des Herstellers eingesetzt sind. Insofern solche Geräte eingesetzt werden (Logitechs Unifying-Produkte sind am Stern-Symbol zu erkennen), empfiehlt es sich, das Firmware Update einzufahren.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019083001.html</link>
</item>
<item>
	<title>Computer (Graphik-) Geschichte(n): Wie die Farbe in den Computer kam</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/Tfh0ytz8S0k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Heute erscheint es fast allen selbstverständlich, daß Computer Bilder und Spiele farbig ausgeben – und zwar in Millionen verschiedenen Farben. Doch das war nicht immer so, alleine schon aus Speichergründen war die Ausgabe früher in der Regel monochrom. Erst zu Beginn der 1980er Jahre – mit dem Aufkommen der Heimcomputer und damit verbunden der Computerspielen – versuchte man, trotz des knappen Speichers und anderer Ressourcenbeschränkungen, Farbe ins Spiel zu bringen.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082901.html</link>
</item>
<item>
	<title>(Security- ?) Updates zum Ende des Monats</title>
	<description>
			<![CDATA[<% imageref("pilze201908-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild20.html"><img src="http://blog.schockwellenreiter.de/images/pilze201908-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Zum Ende des Sommers sind bei mir ein paar (Sicherheits- ?) Updates hereingeschneit. Sie betreffen Apple und Google. Ich fange mal mit Apple an:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082701.html</link>
</item>
<item>
	<title>YouTube-Playlist: Introduction to Twine</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/YDUU5yZq4og" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Solltet Ihr aufgrund meines gestrigen Tutorials Blut geleckt haben und mehr über Twine, der Game-Engine für interaktive Geschichten lernen wollen, so verweise ich auf den <i>VegetarianZombie</i>, der eine gigantische YouTube-Playlist »Introduction to Twine« veröffentlicht hat.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082601.html</link>
</item>
<item>
	<title>Tutorial: Erste Schritte mit Twine</title>
	<description>
			<![CDATA[<% imageref("twineentdecken-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild15.html"><img src="http://blog.schockwellenreiter.de/images/twineentdecken-s.jpg" width="560" height="323" alt="image" /></a><br clear="all" />Twine 2 ist ein Open Source (GPL) Programm (Linux, MacOS X, Windows), um nichtlineare, interaktive Geschichten (Interactive Fiction (IF)) wie Text-Adventures im Browser zu erzählen. Das Tool schreibt statische Seiten nach HTML5 heraus, so daß serverseitig keinerlei Ansprüche bestehen.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082501.html</link>
</item>
<item>
	<title>KI-Regenbögen mit Runway und P5.js</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/vEetoBuHj8g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />In seiner 150. Coding Challenge setzt <i>Daniel Shiffman</i> seine bisherigen Erkundungen zu Runway ML fort. Runway ML ist eine (Cloud-) KI-Plattform für Kreative, die auf einfache und intuitive Weise Methoden des maschinellen Lernens in ihren Projekten nutzen wollen. Nachdem er schon in einem Beispiel gezeigt hatte, wie man Runway ML zusammen mit Processing (Java) nutzt, verkoppelt er nun das KI-Framework mit P5.js, dem JavaScript-Mode von Processing, um Bilder von (künstlichen) Regenbögen zu erzeugen.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082303.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("joeygartenaugust2019-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild14.html"><img src="http://blog.schockwellenreiter.de/images/joeygartenaugust2019-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Und es ist ein aktuelles Hundebild von heute, das die kleine Fellkugel im Schatten unseres kleinen Gärtchens zeigt. Denn anders kann man die vermutlich letzten heißen Tage dieses Sommers nicht aushalten. So werden wir beide auch die nächsten zwei Tage verbringen: Ich mit dem Laptop auf der Terrasse und der Sheltie im Schatten unter dem Walnußbaum.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082302.html</link>
</item>
<item>
	<title>Satanischer Freitag</title>
	<description>
			<![CDATA[<% imageref("butt_song-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild13.html"><img src="http://blog.schockwellenreiter.de/images/butt_song-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Da die Religiotie anscheinend weltweit rapide zunimmt, könnt Ihr Euch zum Wochenende mit ein paar Links darüber aufregen. Es geht um Gott, der uns sagt, daß Donald Trump kein Rassist ist und keine Einwanderer in den Himmel mag, um Sonderrechte für Religioten, um das Beichtgeheimnis für Kinderficker und um die Frage, warum, wenn Linke über den Islam reden, oft Rassismus dabei herauskommt:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082301.html</link>
</item>
<item>
	<title>Updates, Updates!</title>
	<description>
			<![CDATA[<% imageref("hochsitzcwp-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild12.html"><img src="http://blog.schockwellenreiter.de/images/hochsitzcwp-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Die letzten Tage wurden wieder so viele Updates in meinen Feedreader gespült, daß ich sie wieder einmal nur <i>en bloc</i> abhandeln kann:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082101.html</link>
</item>
<item>
	<title>Video Tutorials: Learning Twine 2.3</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/l3zGvR4S27c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Das war glatt an mir vorbeigegangen: Der unermüdliche <i>Dan Cox</i> hatte schon im Mai dieses Jahres eine gigantische Playlist auf YouTube online gestellt: Learning Twine 2.3 besteht aus 29 im Schnitt etwa fünfminütigen Video-Tutorials, die Euch in alle Geheimnisse von Twine einführen wollen. Denn neben dem Default-Story-Format (der Default-»Sprache«) Harlowe werden in späteren Videos auch die alternativen Formate Snowman (für Leute mit guten JavaScript- und CSS-Kenntnissen) und SugarCube (das populäre Format aus Twine 1.4, das ebenfalls in Twine 2 genutzt werden kann) vorgestellt.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082002.html</link>
</item>
<item>
	<title>Vier kurze (mehr oder weniger politische) Links</title>
	<description>
			<![CDATA[<% imageref("radwegkms-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild11.html"><img src="http://blog.schockwellenreiter.de/images/radwegkms-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Mit diesem Photo des chronisch zugeparkten Radstreifens in der Neuköllner Karl-Marx-Straße leite ich eine kleine Linkschleuder ein, die sich mehr oder weniger mit politischen Themen beschäftigt:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019082001.html</link>
</item>
<item>
	<title>Worknote (und: Neu in meinem Wiki): Bitsy @ localhost</title>
	<description>
			<![CDATA[<% imageref("bitsylocal-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild10.html"><img src="http://blog.schockwellenreiter.de/images/bitsylocal-s.jpg" width="560" height="381" alt="image" /></a><br clear="all" />Ich konnte es nicht lassen und habe am Wochenende ein wenig mit der minimalistischen <i>Game Engine</i> Bitsy herumgespielt. Das Teil macht gerade ob seiner Beschränkungen wirklich Spaß, denn man muß schon etwas Phantasie aufbringen, um sich eine spielbare Geschichte auszudenken. Ich weiß nicht wirklich, ob ich damit jemals etwas Ernsthaftes anstelle, aber auf jeden Fall habe ich dem Teil eine Seite in meinem Wiki spendiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081901.html</link>
</item>
<item>
	<description>
			<![CDATA[ [[drot]]Das Elektro-Auto ist der neue Diesel: Heute Kaufanreize, morgen Abwrackprämie und übermorgen Fahrverbote. Sagt nicht, ich hätte Euch nicht gewarnt.[[dd]]]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081604.html</link>
</item>
<item>
	<title>Python Flappy Bird AI Tutorial (with NEAT)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/MMxFDaIOHsE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Tim von TechWithTim hat sich viel vorgenommen: Er will eine Künstliche Intelligenz (KI) erschaffen, die Flappy Bird besser spielen soll, als jeder menschliche Spieler. Sie soll dazu den Neuro Evolution of Augmenting Topologies Algorithmus (kurz NEAT) nutzen und folgerichtig muß neben Pygame auch das Modul NEAT-Python installiert werden (geht mit <code>pip install neat-python</code>).]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081603.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("julijoey-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild09.html"><img src="http://blog.schockwellenreiter.de/images/julijoey-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Ich will es kurz machen: Auch wenn ich als Rentner kaum noch etwas vom Wochenende mitbekomme – es ist heute Freitag und Freitags erscheint aus alter Tradition ein Hundebild im <i>Schockwellenreiter</i>. Auch wenn die Postingfrequenz in der letzten Woche wieder etwas angezogen hat – es waren zu viele interessante Themen im Umlauf –, kann ich weder für die nächsten zwei Tage noch für die nächste Woche etwas versprechen. Daher betrachtet das Photo der kleinen Fellkugel als Trost, wenn Ihr länger auf den nächsten Beitrag hier im <del>Blog</del> Kritzelheft warten müßt.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081602.html</link>
</item>
<item>
	<title>Sisyphos und Co.: Altgriechische Strafen mit Twine und Bitsy</title>
	<description>
			<![CDATA[<% imageref("sisyphos-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild08.html"><img src="http://blog.schockwellenreiter.de/images/sisyphos-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Pippin Barr unterrichtet kreatives Programmieren und Spieledesign an der Concordia University in Montréal. Daneben bastelt er (wie er schreibt um nicht zu verblöden) seltsame Spiele und die seltsamsten sind eine Reihe von Spielen, die sich mit altgriechischen Strafen befassen. Sie behandeln:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081601.html</link>
</item>
<item>
	<title>Neu in meiner Bibliothek: Culture and Cognition</title>
	<description>
			<![CDATA[<div style="float: right; margin-left: 12px; margin-top: 6px;"><a href="http://edition-open-access.de/proceedings/11/index.html"><img src="http://blog.schockwellenreiter.de//images/p11.jpg" width="196" height="280" alt="image" /></a></div>Im November 2011 ist mein langjähriger Lehrer, Kollege am Max-Planck-Institut für Wissenschaftsgeschichte (MPIWG) und Freund <i>Peter Damerow</i> viel zu früh verstorben – er hatte noch so viel zu erforschen. 2013 fand am MPIWG ein Workshop zu seinen Ehren statt, auf dem viele seiner Wegbegleiter Vorträge hielten, wie Peter Ihre Forschungen beeinflußt hatte und was daraus geworden war. Und da in der Wissenschaft alles sowieso etwas länger dauert, weil es gut und im Falle Peter noch etwas länger, weil es besser werden soll, erschien erst in diesem Monat (August 2019) der Proceedings-Band zu diesem Workshop:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081501.html</link>
</item>
<item>
	<title>Coding Challenge: Asteroids programmieren in P5.js</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/hacZU523FyM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />In dieser zweiteiligen <i>Coding Challenge</i>, die <i>Daniel Shiffman</i> schon vor knapp drei Jahren veröffentlicht hatte, programmiert er das Konsolenspiel Asteroids von 1979 in P5.js, der JavaScript-Version von Processing nach. Im ersten Teil baut er das Schiff und die Asteroiden, während im zweiten Teil die Laserkanone bestückt und eine Kollisionserkennung programmiert wird:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081402.html</link>
</item>
<item>
	<title>Worknote: Paletten, Paletten, denn (Farb-) Paletten kann man nie genug haben</title>
	<description>
			<![CDATA[<% imageref("punkte3-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild07.html"><img src="http://blog.schockwellenreiter.de/images/punkte3-s.jpg" width="560" height="188" alt="image" /><br clear="all" />Da man wirklich von Farbpaletten nie genug haben kann, hat <i>Silveira Neto</i> in seinem Blog zwei neue vorgestellt. Einmal Vinik24, eine Palette mit 24 aufeinander abgestimmten Pastell-Farben:]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081401.html</link>
</item>
<item>
	<description>
			<![CDATA[ [[dg]]Wenn der Klügere stets nachgibt, übernimmt am Ende die Dummheit die Macht.[[dd]]]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081303.html</link>
</item>
<item>
	<title>Runway: Maschinelles Lernen für Kreative: Die Reise geht weiter</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/7btNir5L8Jc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a><br clear="all" />Am Wochenende hatte <i>Daniel Shiffman</i> sein zweites Video zu Runway ML veröffentlicht, der Bibliothek, die »Maschinelles Lernen« Kreativen zugänglich machen will. In diesem zeigte er, wie man Runway ML mit Processing (Java) verkoppelt.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081302.html</link>
</item>
<item>
	<title>Sicherheitsupdate für Adobe Reader und Acrobat</title>
	<description>
			<![CDATA[<% imageref("auchzuverschenken-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild06.html"><img src="http://blog.schockwellenreiter.de/images/auchzuverschenken-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Adobe liefert Korrekturen für Sicherheitslücken im Reader und im Acrobat (unter Windows und Mac).]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019081301.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("bissigersheltie-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild05.html"><img src="http://blog.schockwellenreiter.de/images/bissigersheltie-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Ich will Euch nichts vormachen, meine Post-Frequenz ist seit meinem Renteneintritt spürbar zurückgegangen und so gibt es das freitägliche Hundebild eigentlich nur noch aus Tradition. Dieses Mal verbrigt sich ein kleiner Witz dahinter, aber um ihn zu erkennen, müßt Ihr schon oben auf das Photo klicken und Euch das Bild der kleinen Fellkugel in der Originalfassung anschauen.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080903.html</link>
</item>
<item>
	<title>Introduction to Runway: Machine Learning for Creators (Part 1)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/ARnf4ilr9Hc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Mein kreatives Programm für das Wochenende scheint gerettet, denn in diesem Video stellt <i>Daniel Shiffman</i> Runway ML vor, eine Plattform für Kreative, die auf einfache und intuitive Weise Methoden des maschinellen Lernens in ihren Projekten nutzen wollen. <i>Shiffman</i> zeigt, wie man Runway installiert und wie man die Ergebnisse in Processing (Java) und P5.js, dem JavaScript-Mode von Processing nutzen kann.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080902.html</link>
</item>
<item>
	<title>Google veröffentlicht Sicherheitsupdate für seinen Browser Chrome</title>
	<description>
			<![CDATA[<% imageref("sofadelbrueck-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild04.html"><img src="http://blog.schockwellenreiter.de/images/sofadelbrueck-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Google schließt mit dem Update auf die Version 76 (76.0.3809.100) wieder Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080901.html</link>
</item>
<item>
	<title>Video-Tutorial: Create a Casual Game with Pygame Zero</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/Gnq1BXvbQIc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Heute ist Pygame Zero-Tag im <i>Schockwellenreiter</i>. Denn ich habe noch diese wunderschöne Playlist »Create a Casual Game with Pygame Zero« entdeckt, die Euch zeigt, wie Ihr ein kleines Spiel mit Pygame Zero erstellen könnt. Es ist das beste Tutorial, das ich bisher zu Pygame Zero im Netz gefunden habe.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080702.html</link>
</item>
<item>
	<title>Tutorial: Alien Invaders mit Pygame Zero</title>
	<description>
			<![CDATA[<% imageref("alieninvaderspgz-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild03.html"><img src="http://blog.schockwellenreiter.de/images/alieninvaderspgz-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Vor etwa einem Monat hatte ich mit einem Tutorial begonnen, das das Spiel »Alien Invaders« mit Pygame programmieren wollte. In der Zwischenzeit bin ich jedoch mit Pygame Zero vertrauter geworden und hatte festgestellt, daß einem Pygame Zero tatsächlich sehr viele Zeilen Quellcode erspart. Daher habe ich einen Cornerturn vollzogen und das Spiel in Pygame Zero neu programmiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080701.html</link>
</item>
<item>
	<title>Security Alerts und die Chronistenpflicht</title>
	<description>
			<![CDATA[<% imageref("wssuderoder-s") %><a href="http://blog.schockwellenreiter.de/g2019/b201908/201908bild02.html"><img src="http://blog.schockwellenreiter.de/images/wssuderoder-s.jpg" width="560" height="188" alt="image" /></a><br clear="all" />Während meiner Abwesenheit die letzten anderthalb Wochen wurden ein paar Sicherheitswarnungen und Updates für Googles und Apples Geräte in meinen Feedreader gespült. Daher verlangt die Chronistenpflicht, daß ich diese auch hier im <del>Blog</del> Kritzelheft veröffentliche.]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080602.html</link>
</item>
<item>
	<title>The Boy is Back in Town (Die Zahlen)</title>
	<description>
			<![CDATA[Der Junge ist zurück in der Großstadt, hat auch die gestrige Zahnbehandlung lebend überstanden und Euch – da es das erste Posting in diesem Monat ist – ein paar Zahlen mitgebracht, die hochtrabend auch manches Mal <i>Mediadaten</i> genannt werden: Im Monat Juli 2019 hatte der Schockwellenreiter laut seinem nicht immer zuverlässigen, aber (hoffentlich!) datenschutzkonformen Neugiertool 4.824 Besucher mit 8.996 Seitenansichten. ]]></description>
	<link>http://blog.schockwellenreiter.de/2019/08/2019080601.html</link>
</item>
