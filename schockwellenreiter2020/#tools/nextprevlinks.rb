# encoding: utf-8

def nextprevlinks()
  p, n = html.getNextPrev(adrObject)
  ntitle, npath = html.getTitleAndPaths(n) if n
  ptitle, ppath = html.getTitleAndPaths(p) if p
  # rel_to_top = adrsiteroottable.relative_uri_from(adrobject)
  s = ""
  if p
    # s << "<a href=" + rel_to_top.to_s + "/" + ppath + ">" + imageref("arrow-left") + "</a>"
    pname = ptitle.gsub(".", "\\.")
    s << html.getLink(imageref("arrow-left"), pname)
  else
    s << imageref("arrow-left-grey")
  end
  s << "&nbsp;"
  if n
    # s << "<a href=../" + rel_to_top.to_s + "/" + npath + ">" + imageref("arrow-right") + "</a>"
    nname = ntitle.gsub(".", "\\.")
    s << html.getLink(imageref("arrow-right"), nname)
  else
    s << imageref("arrow-right-grey")
  end
  "<p>#{s}</p>\n"
end