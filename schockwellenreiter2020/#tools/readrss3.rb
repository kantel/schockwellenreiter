# encoding: utf-8

def readrss3()

  require 'rss'

  rss_feed = "/Applications/MAMP/htdocs/schockwellenreiter/feed/rss.xml"
  rss_content = ""
  s = "<div class='media'>\n"

  open(rss_feed) do |f|
    rss_content = f.read
  end

  rss = RSS::Parser.parse(rss_content, false)

  rss.items.each do |item|
     s << "<div class='media-body'>\n"
     if (item.title)
       s << "    <h4 class='media-heading'>#{item.title}</h4>\n"
     end
     # s << "    <h4 class='media-heading'>#{item.title}</h4>\n"
     s << "    <p>#{item.description}"
     s << "&nbsp;&nbsp;<i><a href='#{item.link}'>Mehr hier&nbsp;…</a></i>"
     s << "</p>\n"
     s << "    <hr />\n"
     s << "</div>\n"
  end
  s << "</div>\n"
  s
end