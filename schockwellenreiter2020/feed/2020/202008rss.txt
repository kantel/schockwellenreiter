#title "RSS-Feed August 2020"
#fileextension ".xml"
#kramdown false
#template "blank"
#xml true

<item>
	<description>
			<![CDATA[ [[ds]]Erbarmen! Zu den Covidioten vom Wochenende ist doch schon alles gesagt. Nur leider noch nicht von allen.[[dd]]]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020083101.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("zuheissfuerdenspitz-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild24.html"><img src="http://blog.schockwellenreiter.de/images/zuheissfuerdenspitz-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Auch wenn die Covidioten Berlin überrennen wollen und die rot-rot-grüne Berliner Politik zu doof ist, ein Demonstrationsverbot rechtssicher auszusprechen oder ein Neutralitätsgesetz verfassungskonform zu formulieren, will ich mich nicht aufregen, sondern einfach nur sagen: Es ist Freitag, lächle, denn Freitags gibt es ein Hundebild im <i>Schockwellenreiter</i>. Es soll Euch nicht nur über die Unbillen und Unfähigkeit der Politik hinwegtrösten, sondern auch darüber, daß es vermutlich die nächsten zwei Tage keine oder nur wenige Updates hier im <del>Blog</del> Kritzelheft geben wird.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082802.html</link>
</item>
<item>
	<title>Julia 👩‍🦰 lernen und die COVID-19-Pandemie modellieren</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/k0fr7XjH1_Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Da die Covidioten ja zum Sturm auf Berlin blasen und alles anstecken werden, was sich ihnen in den Weg stellt, habe ich beschlossen, an diesem Wochenende unser Haus nur für die Gassirunden mit dem Spitz zu verlassen. Daher habe ich heute ein langes Video-Tutorial und eine noch längere Playlist herausgesucht, mit der ich die Zeit nutzen will, mein Vorhaben, Julia 👩‍🦰 zu lernen, weiter voranzubringen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082801.html</link>
</item>
<item>
	<title>Eine neue Version 3.2 des Lektor CMS ist draußen</title>
	<description>
			<![CDATA[<% imageref("orfeoorfei-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild23.html"><img src="http://blog.schockwellenreiter.de/images/orfeoorfei-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wo ich schon dabei bin, liegengebliebene Themen aufzuarbeiten: Bei meinem Bemühungen für ein freies und offenes Web bin ich schon vor einigen Jahren auf Lektor gestoßen, ein freies (BSD-Lizenz), in Python geschriebenes (mit einer GUI in Node.js) CMS, das aus Markdown-Dateien statische Seiten generiert. Es sah damals sehr vielversprechend aus und ich wollte es eigentlich sofort testen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082702.html</link>
</item>
<item>
	<title>Worknote: P5.js und Visual Studio Code</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/vj9nDja8ZdQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Regelmäßige Leser dieses <del>Blogs</del> Kritzelheftes erinnern sich vielleicht daran, daß ich schon mehrmals Anläufe unternommen habe, mit P5.js, dem JavaScript-Mode von Processing, zu spielen. Doch irgendwie war das immer zu unbequem: Ich bin immer noch kein Fan von webbasierten Anwendungen und daher bin ich – trotz meiner anfänglichen Begeisterung – auch mit dem P5.js-Webeditor nie wirklich warmgeworden, meine Versuche P5.js mit meinem Leib- und Magen-Editor TextMate 2 und RubyFrontier zu verheiraten oder gar RubyFrontier als P5.js-IDE zu nutzen, waren mehr ein <i>Proof of Concept</i> denn eine reale Alternative. Und auch meine Experimente mit P5.js und MacDown waren eher Fingerübung. Sicher, alles geht, aber elegant ist anders.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082701.html</link>
</item>
<item>
	<title>Tutorial: Pandemie-Simulation in Processing.py</title>
	<description>
			<![CDATA[<% imageref("pansim-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild22.html"><img src="http://blog.schockwellenreiter.de/images/pansim-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Meine gestrige Beschäftigung mit Processing.py, dem Python-Mode von Processing, hat mich angefixt und daran erinnert, daß ich in Zeiten von Corona schon lange eine Pandemie-Simulation erstellen wollte. Und dann war in dem Repositorium mit Beispielen für GameZero.jl auch noch eine Pandemic Sim zu finden, die auf <i>David P. Sanders</i> Workshop »Learn Julia via epidemic modelling« beruht, den er auf der JuliaCon 2020 gehalten hat. Da gab es natürlich kein Halten mehr, so etwas wollte ich in Python realisieren.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082602.html</link>
</item>
<item>
	<title>Security-Updates bei Mozillas Donnervogel und Googles Surfbrett</title>
	<description>
			<![CDATA[<% imageref("glotztv-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild21.html"><img src="http://blog.schockwellenreiter.de/images/glotztv-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Sowohl bei Googles Browser Chrome wie auch bei Mozillas Email-Client Thunderbird wurden in den aktuellen neuen Versionen Sicherheitslücken geschlossen. Doch wie immer der Reihe nach:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082601.html</link>
</item>
<item>
	<title>Spaß mit Processing.py: Random Rectangles</title>
	<description>
			<![CDATA[<% imageref("randrect-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild20.html"><img src="http://blog.schockwellenreiter.de/images/randrect-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wenn ich längere Zeit nichts mit Processing.py, dem Python-Mode von Processing, angestellt habe, bekomme ich regelrecht Entzugserscheinungen. So traf es sich gut, daß mich zu der letzten Linkschleuder zu Creative Coding der Beitrag »Let’s Make Generative Art We Can Export to SVG and PNG« von Chris Coyier inspiriert hatte. Etwas Ähnliches wollte ich dann auch mal in Processing.py versuchen:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082502.html</link>
</item>
<item>
	<title>Mozilla veröffentlicht neue Version Firefox 80</title>
	<description>
			<![CDATA[<% imageref("kaffeeautomaten-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild19.html"><img src="http://blog.schockwellenreiter.de/images/kaffeeautomaten-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Entwickler des Mozilla Firefox haben jetzt die neue Version 80 und die Version ESR 78.2 veröffentlicht und darin auch wieder Sicherheitslücken behoben.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082501.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("titusbhfgruenau-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild18.html"><img src="http://blog.schockwellenreiter.de/images/titusbhfgruenau-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Es ist wieder Freitag und so darf – trotz Hitzekoma – das Hundebild zum Freitag nicht fehlen. Es soll Euch wieder darüber hinwegtrösten, daß es die nächsten zwei oder drei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird. Denn der kleine Spitz und ich müssen sehen, wie wir die voraussichtlich letzten Hitzetage dieses Jahres überstehen. Aber ab Montag sollen ja, wenn man den Wetterpropheten trauen darf, wieder angenehme Temperaturen herrschen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082102.html</link>
</item>
<item>
	<title>Wie man ein (wissenschaftliches) Buch mit Julia 👩‍🦰 schreibt</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/ofWy5kaZU3g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Meine Erkundungen der Programmiersprache Julia gehen weiter. Heute möchte ich Euch (und mir) zum Wochenende erst einmal dieses halbstündige Video präsentieren, in dem <i>Tim Wheeler</i> zeigt, wie er gemeinsam mit <i>Mykel Kochenderfer</i> ein wissenschaftliches Buch mit Julia (und LaTeX) geschrieben hat. Das sollte Euch beschäftigen, falls während der Corona-Isolation Langeweile aufkommt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082101.html</link>
</item>
<item>
	<title>Eine gute Handvoll Links zu Creative Coding</title>
	<description>
			<![CDATA[<% imageref("art-4-s") %><img src="http://blog.schockwellenreiter.de/images/art-4-s.jpg" width="560" height="auto" alt="image" /><br clear="all">Die gestrige Worknote zu PEmbroider erinnerte mich daran, daß mein Feedreader ziemlich vollgelaufen ist mit Links zum Thema <i>Creative Coding</i>. Um dem abzuhelfen, mußte ich einfach mal wieder eine Linkschleuder unters Volk bringen:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020082001.html</link>
</item>
<item>
	<title>Worknote: PEmbroider: Gesticktes mit Processing</title>
	<description>
			<![CDATA[<% imageref("pembroiderbanner-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild17.html"><img src="http://blog.schockwellenreiter.de/images/pembroiderbanner-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">PEmbroider ist eine freie (Anti-Capitalist Software License) Bibliothek für Computerstickerei mit Processing. PEmbroider wurde im <i>Endless March 2020</i> an dem <i>CMU Frank-Ratchye STUDIO for Creative Inquiry</i> von <i>Golan Levin</i>, <i>Lingdong Huang</i> und <i>Tatyana Mustakos</i> mit großzügiger Unterstützung der <i>Clinic for Open Source Arts</i> (COSA) der Universität von Denver entwickelt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081902.html</link>
</item>
<item>
	<title>Google behebt mit der neuen Version seines Browsers Chrome wieder Sicherheitslücken</title>
	<description>
			<![CDATA[<% imageref("nunsgobad-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild16.html"><img src="http://blog.schockwellenreiter.de/images/nunsgobad-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber – wenn es pressiert – auch hier geladen werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081901.html</link>
</item>
<item>
	<title>How to Code: One Snakey Boi with Perlin Noise (in pure JavaScript)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/M4WzhdQPyH0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">In diesem 45-minütigen Video-Tutorial lernt Ihr, wie man eine Art Regenbogenschnecke mit Perlin-Noise in JavaScript pur und dem HTML5-Canvas programmiert. Perlin-Noise ist in Processing schon als Funktion implementiert und so juckt es mich natürlich in den Fingern, solch eine knallbunte Schlange auch mal in Processing.py, dem Python-Mode von Processing, zu implementieren.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081802.html</link>
</item>
<item>
	<title>Great Books: Alice’s Adventures in Wonderland</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/x8Vz5MRMfDU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Futter für meinen Wunderland-Kosmos mit Twine (Update) und auch für meine mit dem CollectionBuilder als Prototyp geplante Sammlung über <i>Lewis Carroll</i> und <i>Alice</i> ist dieses wundervolle Video aus der Reihe Great Books. Es ist eine fünfzigminütige Hommage an den Klassiker, die ich unbedingt in diese Sammlung aufnehmen will.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081801.html</link>
</item>
<item>
	<title>Worknote: CollectionBuilder StandAlone ist draußen</title>
	<description>
			<![CDATA[<% imageref("collectionbuildersa-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild15.html"><img src="http://blog.schockwellenreiter.de/images/collectionbuildersa-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Bekanntlich halte ich ja den CollectionBuilder für hochinteressant, das durch Metadaten angetriebene und mit Jekyll realisierte Werkzeug, um Sammlungen als statische Seiten online zu stellen. Leider war bisher nur die Version für GitHub Pages veröffentlicht, eine Standalone-Version war jedoch in Planung. Und am Wochenende wurde ich von <i>Evan Will</i> via Twitter informiert, daß die Standalone-Version nun freigegeben sei und auf GitHub zum Download bereitstünde.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081702.html</link>
</item>
<item>
	<title>Microsoft veröffentlicht Update 16.40.0 für Office for Mac</title>
	<description>
			<![CDATA[<% imageref("swans1900-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild14.html"><img src="http://blog.schockwellenreiter.de/images/swans1900-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Microsoft liefert mit dem Update auf 16.16.25 für Office 2016 und auf 16.40.0 für Office 365 for Mac natürlich auch aktuelle Sicherheitskorrekturen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081701.html</link>
</item>
<item>
	<title>Cancel Culture und die Causa Lisa Eckhart</title>
	<description>
			<![CDATA[<% imageref("haremterrasse-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild13.html"><img src="http://blog.schockwellenreiter.de/images/haremterrasse-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Frage an all die Dauerempörten: Nehmen wir mal an, <i>Harvey Weinstein</i>, <i>Woody Allen</i> und <i>Roman Polanski</i> wären Muslime. Und Lisa Eckhart hätte darob in den <i>Mitternachtsspitzen</i> einen Witz gerissen über Muslime und ihren Harem. Wärt Ihr dann genau so dauerempört?]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081601.html</link>
</item>
<item>
	<title>Worknote: Julia 👩‍🦰, macOS und die Kommandozeile</title>
	<description>
			<![CDATA[<% imageref("juliaterminal-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild12.html"><img src="http://blog.schockwellenreiter.de/images/juliaterminal-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mein Corona-Isolations-Projekt, die Erkundung von Julia, schreitet voran. Eine Sache hatte mich da allerdings gestört. Um Julia im Terminal aufzurufen, mußte ich immer den vollen Pfad zur Applikation eintippen, in meinem Fall – wegen der gepackten App – also:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081501.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("titusbequem-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild11.html"><img src="http://blog.schockwellenreiter.de/images/titusbequem-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mit diesem Photo des kleinen Spitzes, der genau so unter der Hitze leidet wie ich, möchte ich das Wochenende einläuten. Zwar habe ich – außer daß ich im Hitze-Koma liege – eigentlich keine Entschuldigung dafür, daß es die nächsten zwei bis drei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird, denn wetterbedingt habe ich fast alle körperlichen und/oder geistig anstrengenden Aktivitäten heruntergefahren und die Fellnase will ebenfalls schon seit Tagen nur noch ausruhen. Aber wer ausruht, kann das Internet nicht vollschreiben.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081403.html</link>
</item>
<item>
	<title>JuliaCon 2020: The State of Julia</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/xKrIp4ZVOrg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Weiter geht es mit Julia 👩‍🦰: Zwar ist die JuliaCon 2020 schon vor 14 Tagen zu Ende gegangen, aber die Macher hauen immer noch Vortragsmitschnitte aus dieser (coronabedingt) online abgehaltenen Konferenz heraus. So auch diesen vierzigminütigen Vortrag, in dem <i>Jeff Bezanson</i> und <i>Stefan Karpinski</i> über den aktuellen Status von Julia (1.4) referieren und einen Ausblick auf Julia 2.0 geben.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081402.html</link>
</item>
<item>
	<title>Neu in meinem Wiki: Pluto.jl</title>
	<description>
			<![CDATA[<% imageref("julia2-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild10.html"><img src="http://blog.schockwellenreiter.de/images/julia2-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Pluto.jl, den freien (MIT-Lizenz) Notebook-Server für Julia 👩‍🦰, der all die Fehler vermeiden will, die <i>Joel Grus</i> in seinem legendären Rant »I don’t like Notebooks« anprangerte, hatte ich ja in diesem <del>Blog</del> Kritzelheft ja schon einmal vorgestellt. Der Notebook-Server ist reaktiv, wenn eine Funktion oder eine Variable irgendwo geändert wird, berechnet Pluto alle davon betroffenen Zellen neu. Dabei kommt es nicht auf die Reihenfolge der Zellen an.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081401.html</link>
</item>
<item>
	<title>Apple veröffentlicht iOS 13.6.1 und iPadOS 13.6.1</title>
	<description>
			<![CDATA[<% imageref("buerostuhlnest-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild09.html"><img src="http://blog.schockwellenreiter.de/images/buerostuhlnest-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Apple hat gestern abend iOS 13.6.1 und iPadOS 13.6.1 veröffentlicht und damit im wesentlichen Fehler behoben (unter anderem in der Speicherverwaltung, dem Hitze-Management und bei der Aktivierung der Corona-Warnungen), und natürlich Sicherheitsupdates geliefert.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081301.html</link>
</item>
<item>
	<title>Security Alerts: Google, Apple und Adobe</title>
	<description>
			<![CDATA[<% imageref("buerostuhlmorgensonne-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild08.html"><img src="http://blog.schockwellenreiter.de/images/buerostuhlmorgensonne-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Letzte Nacht kamen gleich drei Meldungen zu Sicherheitsupdates bei mir rein, so daß ich sie wieder nur en bloc abhandeln kann. Doch wie immer der Reihe nach:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081201.html</link>
</item>
<item>
	<title>(Text-) Abenteuer mit Twine im Wunderland</title>
	<description>
			<![CDATA[<% imageref("aal-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild07.html"><img src="http://blog.schockwellenreiter.de/images/aal-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nein, das hat wiederum keine Zeit bis Freitag. Ich arbeite intensiv an (m)einer Tutorialreihe, wie man mit Twine interaktive, nichtlineare Geschichten erzählen kann. Das komplette Tutorial soll im Wunderland-Kosmos spielen. Und wie der Teufel so will, sind mir bei der Recherche drei ziemlich aktuelle Video-Tutorials zu Twine 2 und zwei weitere »Alice im Wunderland«-Filme untergekommen. Doch zuerst zu den Video-Tutorials:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081101.html</link>
</item>
<item>
	<description>
			<![CDATA[ [[drot]]Mit Olaf Scholz als Kanzlerkandidaten marschiert die SPD aufrecht und erhobenen Hauptes unter die 5%-Hürde durch.[[dd]]]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081002.html</link>
</item>
<item>
	<title>Zur Causa Lisa Eckhart: Rätselhafte Femme fatale</title>
	<description>
			<![CDATA[<% imageref("girlswithguns-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild06.html"><img src="http://blog.schockwellenreiter.de/images/girlswithguns-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich habe Lisa Eckhart einige Male im TV gesehen (unter anderem auch den berühmt-berüchtigten Mitternachtsspitzen-Auftritt) und fand sie verstörend (oder soll ich sagen zerstörend) genial. Aber um das zu erkennen, muß man auch zwischen den Zeilen hören können. Und diese Fähigkeit ist anscheinend im Umfeld der selbsternannten Hamburger Antifa abhanden gekommen. Aber, um – wie die Jüdische Allgemeine – Karl Kraus zu zitieren: <i>»Satiren, die der Zensor versteht, werden mit Recht verboten.«</i> Daher hat Lisa Eckhart alles richtig gemacht.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020081001.html</link>
</item>
<item>
	<title>Visual Studio Code für Python – Tech with Tim</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/HUtHp3duGbI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Visual Studio Code ist ja gerade unter Pythonistas je nach Sichtweise ein beliebter Editor oder eine beliebte IDE. Unter anderem, weil VS Code recht gut mit Jupyter Notebooks umgehen kann. Das honorierend hat Microsoft <i>Tim Ruscica</i>, den Macher des beliebten und auch hier schon mehr als einmal erwähnten YouTube-Kanals »Tech with Tim« eingeladen und er hat einige VS-Code-Python-Erweiterungen programmiert. Herausgekommen ist natürlich auch ein Video, indem er einige wichtige Erweiterungen (nicht nur seine eigenen) vorstellt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080801.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("spitzankunst-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild05.html"><img src="http://blog.schockwellenreiter.de/images/spitzankunst-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Es ist Freitag, es ist sonnig und warm und es soll die nächsten Tage noch wärmer und sonniger werden. Daher gibt es heute wieder das Hundebild, das Euch darüber hinwegtrösten soll, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird. Denn bei den sommerlichen Aussichten werde ich die Zeit mehr mit Dösen und im Garten im Schatten liegen verbringen und sie kaum dazu nutzen, das Internet vollzuschreiben. Auch der kleine Spitz wird sich meinen (Nicht-) Aktivitäten (hoffentlich) anschließen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080702.html</link>
</item>
<item>
	<title>Manchmal muß es 3D sein (auch in Jupyter)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/s-g3E3aAgEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Weil es manchmal 3D sein muß (auch in Jupyter) und/oder weil 3D-Visualisierungen einfach Spaß machen, habe ich heute für Euch diesen Videovortrag von der EuroSciPy 2017 herausgesucht: »Interactive 3D Visualization in Jupyter Notebooks« von <i>Vidar Tonaas Fauske</i>. Die 3D-Visualisierung basiert auf der JavaScript-Bibliothek Three.js mit der Python-Bridge pythreejs und einigen anderen, kleineren Modulen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080701.html</link>
</item>
<item>
	<title>Sicherheitsupdate des Foxit Reader auf 10.0.1</title>
	<description>
			<![CDATA[<% imageref("geschuetzterkuehlschrank-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild04.html"><img src="http://blog.schockwellenreiter.de/images/geschuetzterkuehlschrank-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mit der Version 10.0.1 des unter Windows beliebten PDF Betrachters Foxit Reader haben die Entwickler mehrere kritische Schwachstellen behoben.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080601.html</link>
</item>
<item>
	<title>Schöner plotten mit Julia 👩‍🦰 (und GR)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/EK9J4pHDzQo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Der heutige Tag war vollgestopft mit Videokonferenzen für einen meiner Klienten, daher mußte ich meinen Vorsatz vertagen, mich weiter mit Julia und dem neuen, interaktiven Julia-Notebook Pluto.jl zu beschäftigen. Aber etwas neues zu Julia habe ich doch noch: In obigem Video stellt <i>Josef Heinen</i> vom <i>Forschungszentrum Jülich</i> die Neuerungen des GR Frameworks vor. GR ist ein Framework sowohl für Julia wie auch für Python für 2D- und 3D-Visualisierungen. Das Julia-Paket ist frei (MIT-Lizenz) und auf GitHub zu finden. (Um die Python-Version habe ich mich noch nicht gekümmert, aber das werde ich nachholen.)]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080501.html</link>
</item>
<item>
	<title>Einführung in Twine 2 und Harlowe 3: Passagen (und Tags)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/xYetQV4Jy6E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Nein, das hat nicht Zeit bis Freitag: Der vegetarische Zombie hat seiner bisher schon monströsen Playlist zu Twine ein weiteres Video hinzugefügt: »Using Passages - Beginning Game Development with Twine 2 and Harlowe 3 - Interactive Fiction«. Das ist das dritte Video zum Neustart nach einer mehrjährigen Pause. Es sieht so aus, als ob er sein Wissen zu Twine 2 und Harlowe 3 neu organisieren und mit uns teilen will. Ich liebe das.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080402.html</link>
</item>
<item>
	<title>Google schließt am »August-Patchday« wieder Sicherheitslücken in Android</title>
	<description>
			<![CDATA[<% imageref("rundertisch-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild03.html"><img src="http://blog.schockwellenreiter.de/images/rundertisch-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google hat mit seinem monatlichen Sicherheitsupdate für Android (und damit auch auf seinen Pixel-Geräten) wieder Sicherheitslücken geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080401.html</link>
</item>
<item>
	<title>Gute Nacht, Corona …</title>
	<description>
			<![CDATA[<% imageref("onkelfritz-s") %><img src="http://blog.schockwellenreiter.de/images/onkelfritz-s.jpg" width="560" height="auto" alt="image" /><br clear="all">Nach den Auftritten der Covidioten am Wochenende: Ich möchte mich am Liebsten zurückziehen und die verrückt gewordenen Welt ausschließen. Und an die Tür klebe ich einen Zettel: Bitte erst wieder wecken, wenn <del>Corona</del> der Wahnsinn vorbei ist.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080301.html</link>
</item>
<item>
	<title>Eine Einführung in GameZero für Julia</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/ijw27lXp_qk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Die jüngst beendete, coronabedingt online stattfindende JuliaCon 2020 ist für Überraschungen gut. Oder hättet Ihr gedacht, daß man Julia auch für die Programmierung von Spielen nutzt? Ich hatte – auch wenn der Anspruch vorhanden ist, eine »General Purtpose Language« zu sein – Julia immer als Sprache für numerisches und wissenschaftliches Rechnen eingeordnet. Doch dann tritt <i>Ahan Sengupta</i> auf und führt in obigem Video GameZero.jl vor, ein Julia-Paket zur einfachen und schnellen Entwicklung von Spielen. GameZero.jl orientiert sich an Pygame Zero und will wie dieses die Spiele-Programmierung ohne <i>Boilerplate</i> ermöglichen]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080201.html</link>
</item>
<item>
	<title>Interaktive Julia-Notebooks mit Pluto.jl</title>
	<description>
			<![CDATA[<% imageref("plutojulia-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202008/202008bild02.html"><img src="http://blog.schockwellenreiter.de/images/plutojulia-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nein, ich habe nicht vergessen, daß eines meiner Corona-Isolations-Projekte die Erkundung der Programmiersprache Julia ist. Und da trifft es sich gut, daß gerade die JuliaCon 2020 stattfindet, Coronabedingt online und frei zugänglich. Dort bin ich dann über obiges Video gestolpert, in dem <i>Fons van der Plas</i> und seine Mitstreiter Pluto.jl vorstellen. Pluto.jl ist ein interaktives Notebook für Julia, das all die Fehler vermeiden will, die <i>Joel Grus</i> in seinem legendären Rant »I don’t like Notebooks« anprangerte.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080102.html</link>
</item>
<item>
	<title>Die Zahlen</title>
	<description>
			<![CDATA[Wie jeden Monatsersten erst einmal ein paar Zahlen des Vormonats, hochtrabend manches Mal auch <i>Mediadaten</i> genannt: Im Juli 2020 hatte der <i>Schockwellenreiter</i> laut seinem nicht immer ganz zuverlässigen, aber dafür (hoffentlich!) datenschutzkonformen Neugiertool exakt <b>5.681 Besucher</b> mit <b>10.940 Seitenaufrufen</b>. Wie immer täuscht die Exaktheit der Ziffern eine Genauigkeit der Zahlen nur vor. Dennoch freue ich mich über jede Besucherin und jeden Besucher und bedanke mich bei allen meinen Leserinnen und Lesern.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/08/2020080101.html</link>
</item>
