#title "RSS-Feed Oktober 2020"
#fileextension ".xml"
#kramdown false
#template "blank"
#xml true

<item>
	<title>King Pins: Spielen in der Corona-Pandemie</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/4Av5cT9CCa8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Ihr wollt der Langeweile der Corona-Isolation während des neuerlichen Lockdowns entfliehen oder Ihr seid auf der Suche nach neuen Ideen, welche Spiele Ihr programmieren wollt? Dann schaut Euch King Pins doch einmal an: King Pins ist ein kleines Echtzeit-Strategie-Spiel <i>(Real-Time Strategy Game (RTS))</i> mit einer computer-kontrollierten Künstlichen Intelligenz, gegen die Ihr antreten könnt (der lokale Mehrspieler-Modus ist in Zeiten des Corona-Lockdowns wohl keine gute Idee) und zufällig generierten Spielfeldern, so daß jedes Spiel wieder neu für Euch ist.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020103101.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("codierterhund-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild28.html"><img src="http://blog.schockwellenreiter.de/images/codierterhund-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Kommt es mir nur so vor oder verfliegt die Woche immer so schnell? Denn ehe man sich versieht, ist es schon wieder Freitag und Zeit für ein Hundebild. Wie immer soll es Euch darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird. Denn ich habe einiges zu lesen, zu schreiben und zu programmieren und der kleine Spitz möchte auch bespaßt werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020103002.html</link>
</item>
<item>
	<title>Noch mehr Pygame-Tutorials für das Wochenende</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/E1Ujuii9pLw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Schrieb ich doch vorgestern, daß jeder Programmierer, der etwas auf sich hält, mindestens ein Mal in seinem Leben und in der Programmiersprache seines Vertrauens Snake und Flappy Bird (nach-) programmiert haben muß, so vergaß ich einen weiteren Klassiker: Super Mario. Eben das wird nun nachgeholt, im YouTube-Kanal des <i>Beta Programmers</i> bekommt Ihr gezeigt, wie man eine Pygame-Version des Jump’n’Run-Klassikers in Python programmiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020103001.html</link>
</item>
<item>
	<title>Coding Challenge: Zoom Annotations im Comic-Stil und mit Künstlicher Intelligenz</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/9z9mbiOZqSs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Heute gab es die erste Coding Challenge von <i>Daniel Shiffman</i> seit Ausbruch der Corona-Pandemie im März dieses Jahres. Und da er – wie viele von uns – seit März viel Zeit damit verbracht hat, auf Videokonferenzen herumzuhängen, hat er eine Idee von <i>Cameron Hunter</i> aufgegriffen und will gestengesteuerte Anmerkungen im Comic-Stil in Zoom-Videokonferenzen implementieren. Nutzen will er dazu die Software P5.js mit den Bibliotheken und Paketen ml5.js, Teachable Machine, OBS Studio und (natürlich) Zoom.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102901.html</link>
</item>
<item>
	<title>Python Guizero (Video-)Tutorials</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/z2HwJ-fTmmM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all"><i>Devon Schafer</i> hat auf seinem YouTube-Kanal <i>Python Dev</i> bisher zehn Videos veröffentlicht, die Tutorials sind zu Guizero, dem Python-GUI-Toolkit ohne <i>Boilerplate</i>. Leider hat er sie nicht in einer Playlist zusammengefaßt, so daß ich hier die Links zu den Videos einzeln veröffentlichen möchte:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102802.html</link>
</item>
<item>
	<title>Pygame lernen mit der Schlange und durch die Schlange 🐍</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/QFvqStqPCRU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Nicht nur Flappy Bird, sondern auch der Klassiker Snake ist ein Computerspiel, das jeder (lernende) Programmierer mindestens einmal in seinem Leben in der Programmiersprache seines Vertrauens programmiert haben muß. Dem trägt der YouTube-Kanal Clear Code Rechnung und zeigt in diesem zweistündigen Video, wie man Snake in Python und Pygame programmiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102801.html</link>
</item>
<item>
	<title>Yuja Wang: Rhapsody in Blue (very sexy)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/Ce3OERuCY0E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Bis heute hielt ich ja <i>Oscar Levant</i> für den ultimativen Interpreten von Gershwins <i>Rhapsody in Blue</i> (ich hatte in schon als Kind mit Begeisterung im Film von 1945 gesehen und ihn mir auch später immer wieder gerne angeschaut). Doch heute ist mir dieses Video mit der jungen »wilden«, chinesisch-amerikanischen Pianistin <i>Yuja Wang</i> untergekommen und ich muß sagen: Respekt (Auch ohne sexy Outfit)!]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102701.html</link>
</item>
<item>
	<title>Für umme lesen: Create Graphical User Interfaces with Python (and guizero)</title>
	<description>
			<![CDATA[<% imageref("guizeroemoji-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild27.html"><img src="http://blog.schockwellenreiter.de/images/guizeroemoji-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Im Februar 2019 hatte ich GUI Zero (neue Eigenschreibweise guizero) vorgestellt, ein GUI Toolkit für Python, das – ähnlich wie Pygame Zero – ohne Templates auskommen will. Nun ist bei <i>Raspberry Pi Press</i> das Buch »Create Graphical User Interfaces with Python« von <i>Laura Sach</i> und <i>Martin O’Hanlon</i> herausgekommen, das als Bibliothek eben dieses guizero nutzt, sie verständlich erklärt und sie in netten Projekten bis an ihre Grenzen ausreizt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102601.html</link>
</item>
<item>
	<title>Retrocomputing vom Feinsten: Tiny BASIC und Pico-8</title>
	<description>
			<![CDATA[<% imageref("asm80-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild26.html"><img src="http://blog.schockwellenreiter.de/images/asm80-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wer an Retrocomputing interessiert ist, den möchte ich auf zwei Tools hinweisen, die der Sonnabend in meine Zeitleiste spülte. Da gibt es zum ersten Tiny BASIC, einen uralten Interpreter, den jemand für die Browser-Emulation eines 8-Bit-Prozessors portiert hat. Und dann wurde ich auf Pico-8 aufmerksam (kommerziell), eine frei erfundene Spiele-Konsole, deren Beschränkungen an reale (Retro-) Konsolen erinnert. Doch wie immer der Reihe nach:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102402.html</link>
</item>
<item>
	<title>Noch mehr Pygame (Video-) Tutorials</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/ALIxdaFYWzk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Meinen gestern geäußerten Wunsch, wieder etwas mehr mit Pygame anzustellen, scheint YouTube gehört zu haben. Jedenfalls spülte der Dienst einige entsprechende Videos in meine Timeline. Der spannendste Fund scheint diese gigantische Playlist »Developing a Platform Game using Python and Pygame« von Rik Cross zu sein, die aus elf Videos mit einer Laufzeit von über drei Stunden besteht. Das letzte Video dieser Liste wurde vor fünf Tagen hochgeladen, über mangelnde Aktualität kann man daher nicht klagen. Ich werde sie mir in den nächsten Stunden (Tagen?) reinziehen. Schaun wir mal, was es bringt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102401.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("titusversteck-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild25.html"><img src="http://blog.schockwellenreiter.de/images/titusversteck-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wieder ist es viel zu früh Freitag geworden und ich habe den Eindruck, die ganze Woche nichts geschafft zu haben. Irgendwie verfliegt die Zeit auch in der Corona-Isolation viel zu schnell. Aber egal, Freitag ist Freitag und Freitag ist Hundephototag. Wie immer soll Euch das Bild der kleinen Fellnase darüber hinwegtrösten, daß es die nächsten zwei bis drei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird. Denn ich habe ein paar Programmier-Ideen, Lesefutter ist reichlich vorhanden und natürlich will und soll auch der kleine Spitz bespaßt werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102303.html</link>
</item>
<item>
	<title>Video-Tutorial: Partikelsysteme in Pygame (mit einer »Miau-Katze«)</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/yfcsB3SGsKY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Dieses gut 35-minütige Video-Tutorial, das ich Euch für das Wochenende herausgesucht habe, um die Zeit in der Corona-Isolation totzuschlagen, zeigt nicht nur, wie Ihr einfache Partikelsysteme in Pygame programmieren könnt, sondern auch wie Ihr ein »Nyan Cat«-Partikelsystem hinbekommt. Ich gestehe, das Internet-Meme <i>Nyan Cat</i> war mir nicht bekannt, ich mußte die »Regenbogenkatze« erst in der Wikipedia nachschlagen, finde die Idee aber recht witzig. Da kann man mehr daraus machen, vielleicht programmiere ich das mal mit Processing.py nach und mache ein Katzenvideo daraus.&nbsp;🤓]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102302.html</link>
</item>
<item>
	<title>Für umme lesen: History of Computer Art</title>
	<description>
			<![CDATA[<% imageref("oxo-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild24.html"><img src="http://blog.schockwellenreiter.de/images/oxo-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich weiß ja mittlerweile ziemlich viel über die Geschichte der Computergraphik (speziell Spiele und Computerkunst), sowie Hypertext und Hypermedia, aber <i>Thomas Dreher</i> weiß noch mehr. Und davon hat er vieles in seinem Buch »History of Computer Art« niedergeschrieben. Das Buch ist mittlerweile in der zweiten Auflage (2020) erschienen und kann hier kostenlos (es steht unter einer Creative-Commons-Lizenz) als PDF heruntergeladen werden (44 MB).]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102301.html</link>
</item>
<item>
	<title>Video-Tutorial: Einführung in Calibre</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/SXEr_EPU5yk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Calibre (GPL) ist eine Ebook-Management und -Konvertierungssoftware für Windows, macOS und Linux. Sie kann unter anderem zwischen diversen Ebook-Formaten konvertieren. Natürlich beherrscht Calibre auch das Epub-Format und die diversen Kindle-Formate von Amazon. Meine (Ex-) Kolleginnen und Kollegen hatten mir so ein Kindle bei meinem Abschied aus dem Berufsleben geschenkt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102201.html</link>
</item>
<item>
	<title>Künstliche Intelligenz und JavaScript: Objekterkennung mit P5.js, ml5.js und COCO-SSD</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/QEzRxnuaZCk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all" />Das auch JavaScript mittlerweile ein durchaus ernstzunehmendes Werkzeug für die Künstliche Intelligenz (was immer man darunter auch verstehen mag) ist, zeigt Euch <i>Daniel Shiffman</i> in diesem Video. Er benutzt für seine Experimente zur Objekterkennung die »freundliche« Bibliothek für maschinelles Lernen ml5.js, das vortrainierte Dataset COCO-SSD und – natürlich – P5.js, den JavaScript-Mode von Processing. Natürlich steigt dieses viertelstündige Video nicht tief in die Materie ein, aber es ist durchaus vergnüglich und die Ergebnisse können sich sehen lassen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102103.html</link>
</item>
<item>
	<title>Paris und Dresden: Die Linke muß sich empören</title>
	<description>
			<![CDATA[Der Neuköllner Linken (aber nicht nur der) von Sascha Lobo ins Stammbuch geschrieben: <i>»Ein anderer Teil der Gegenwehr ist, daß gerade diejenigen deutschen Linken, die sich – zum Glück! – sonst über jede Menschenfeindlichkeit empören, ihre Zurückhaltung gegenüber dem Islamismus aufgeben – und sich über islamistischen Terror empören. Empört euch! Sonst könnt ihr euch eure Moral in die mit sicherlich fair gehandeltem, mikroplastikfreiem Shampoo gewaschenen Haare schmieren.«</i>]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102102.html</link>
</item>
<item>
	<title>Flickentag bei Apple, Google und Oracle</title>
	<description>
			<![CDATA[<% imageref("bulandtinker-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild23.html"><img src="http://blog.schockwellenreiter.de/images/bulandtinker-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Gestern war Patchday im <i>Silicon Valley</i> bei Apple, Google und Oracle, die in ihren Produkten auch wieder etliche Sicherheitslücken stopften. Doch wie immer der Reihe nach:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102101.html</link>
</item>
<item>
	<title>Mozilla veröffentlicht Firefox 82 und ESR 78.4.0</title>
	<description>
			<![CDATA[<% imageref("departuresteam-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild22.html"><img src="http://blog.schockwellenreiter.de/images/departuresteam-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Entwickler des Mozilla Firefox haben jetzt die neue Version 82 und die Version ESR 78.4.0 veröffentlicht und darin auch wieder Sicherheitslücken behoben.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020102001.html</link>
</item>
<item>
	<title>Neu in meinem Wiki: Obsidian</title>
	<description>
			<![CDATA[<% imageref("obsidian-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild21.html"><img src="http://blog.schockwellenreiter.de/images/obsidian-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Am Wochenende ist mir dieses Video des <i>Happy Coders</i> in meine YouTube-Zeitleiste gerutscht und da mich natürlich Notizanwendungen, die auf Markdown beruhen, immer interessieren, habe ich mir nicht nur das Video reingezogen, sondern auch die Software heruntergeladen und ein wenig damit gespielt. Und ich war schnell begeistert, denn Obsidian ist nicht nur eine elektronische Zettelkasten-Anwendung (nach Niklas Luhmann), sondern auch ein Markdown-Editor, denn die Software speichert alle Notizen als Markdown-Dateien. Dadurch sind sie nicht in proprietäre Formate begraben, sondern es sind reine, für Menschen lesbare Textdateien. Das sichert hoffentlich eine Nachhaltigkeit, die Notizen auch für Jahre noch lesbar aufbewahrt.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101901.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("hundspielzeug-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild20.html"><img src="http://blog.schockwellenreiter.de/images/hundspielzeug-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die letzten zwei Tage hatte ich mir eine Auszeit gegönnt. Denn die kleine Fellnase und ich waren auf einem zweitägigen Hoopers-Seminar mit <i>Katrin Werdin</i> (Facebook-Link) auf »unserem« Hundeplatz und hatten – trotz des schiet Wetters – viel Spaß dabei. Daher hat das heutige Hundephoto auch eine rückwirkende Eigenschaft: Es soll Euch über die ausgefallenen zwei Tage hier im <i>Schockwellenreiter</i> hinwegtrösten.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101603.html</link>
</item>
<item>
	<title>Kreatives Geschichten erzählen mit Twine</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/2JAX5Yh4FU8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">An diesem Wochenende möchte ich Euch wieder mit Video-Tutorials zu Twine versorgen, die Euch helfen sollen, der Langeweile in der Corona-Isolation zu entfliehen. Den Anfang macht das obige, etwa halbstündige Video »Developing Stories for Video Games using GCSE Creative Writing Skills with Twine«. (Was GCSE ist, habe ich auch ergoogeln müssen, es ist das <i>General Certificate of Secondary Education</i> und entspricht etwa dem deutschen mittleren Schulabschluß.) Das Video konzentriert sich mehr auf die literarischen (englischen) Skills, denn auf die technische Seite der Implementierung.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101602.html</link>
</item>
<item>
	<title>Microsoft veröffentlicht Update 16.42.0 für Office for Mac</title>
	<description>
			<![CDATA[<% imageref("herbstlinda-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild19.html"><img src="http://blog.schockwellenreiter.de/images/herbstlinda-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Microsoft liefert mit dem Update auf 16.16.27 für Office 2016 und auf 16.42.0 für Office 365 for Mac natürlich auch aktuelle Sicherheitskorrekturen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101601.html</link>
</item>
<item>
	<title>Flash – gibt es das immer noch? (Security Alert)</title>
	<description>
			<![CDATA[<% imageref("gefreischw-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild18.html"><img src="http://blog.schockwellenreiter.de/images/gefreischw-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich muß gestehen, als diese Meldung heute am frühen Abend bei mir eintrudelte, wurde mir ganz nostalgisch zumute. Was hatte ich früher über die ständigen Sicherheitsupdates gelästert und – zu Recht – behauptet, daß Flash keine Sicherheitslücken habe, sondern Flash <b>die</b> Sicherheitslücke sei. Doch dann hörte ich von Adobe über Monate gar nichts mehr und ich war der Überzeugung, sie hätten ihr Schrottprodukt endlich eingestampft. Und dann das:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101303.html</link>
</item>
<item>
	<title>Thonny, die anfängerfreundliche Python-IDE reloaded</title>
	<description>
			<![CDATA[<% imageref("thonnyreloaded-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild17.html"><img src="http://blog.schockwellenreiter.de/images/thonnyreloaded-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ende Dezember 2018, wenige Tage vor meinem Renteneintritt hatte ich in diesem <del>Blog</del> Kritzelheft Thonny, eine anfängerfreundliche Python-IDE vorgestellt und es ein wenig bedauert, daß ich wohl keine Python-Schulungen mehr durchführen werde. Denn dafür käme mir Thonny wie gerufen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101302.html</link>
</item>
<item>
	<title>Worknote: Virtual Terminal Emulator in Geany</title>
	<description>
			<![CDATA[<% imageref("geanyvte-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild16.html"><img src="http://blog.schockwellenreiter.de/images/geanyvte-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Als ich mir gestern das erste der drei Videos »Mass Defense Game« von <i>Christian Thompson</i> reinzog, fiel mir Folgendes auf: <i>Christian Thompson</i> nutzt Geany als Python-IDE und die Terminalausgabe erfolgte nicht in einem separaten Terminal-Fenster, sondern in einem Terminal-Emulator am Fuße der IDE (also am unteren Fensterrand). Das wollte ich auch haben.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101301.html</link>
</item>
<item>
	<title>Spieleprogrammierung mit Python und der Turtle: Mass Defense Game</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/9NKRfgyxQ4c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Jetzt komme ich endlich dazu, Euch das Video (genauer: die Playlist) vorzustellen, die ich am Freitag zugunsten der Pandemie-Modellierung mit Julia verschoben habe. Es ist die bisher dreiteilige und etwa dreistündige Playlist »Mass Defense Game«, in der <i>Christian Thompson</i> (aka TokyoEdTech) mal wieder Unglaubliches mit Pythons Turtle Modul anstellt. Ich habe mir die Videos bisher auch noch nicht angesehen, aber die Tutorials von <i>Christian Thompson</i> sind immer etwas Besonderes. Daher bin ich sicher, daß ich und auch Ihr damit sehr viel Spaß haben werdet.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101201.html</link>
</item>
<item>
	<title>Für umme lesen: Cyberfibel, ein Nachschlagewerk für die digitale Aufklärung</title>
	<description>
			<![CDATA[<% imageref("girlwriting-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild15.html"><img src="http://blog.schockwellenreiter.de/images/girlwriting-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Das »Bundesamt für Sicherheit in der Informationstechnik« (BSI) und »Deutschland sicher im Netz« (DsiN) haben eine Cyberfibel als Nachschlagewerk für die digitale Aufklärung veröffentlicht, mit der sie mehr Kenntnisse über die digitale Welt vermitteln wollen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101101.html</link>
</item>
<item>
	<title>Pixelspiele mit Itsy, Bitsy, Teeny, Weeny …</title>
	<description>
			<![CDATA[<% imageref("bitsy72-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild14.html"><img src="http://blog.schockwellenreiter.de/images/bitsy72-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Heute ist Nele-Hirsch-Tag im <i>Schockwellenreiter</i>. Denn die Dame ist sehr umtriebig und auch wenn ihr eBildungslabor eher den Eindruck eines pädagogischen Gemischtwarenladens macht, finden sich doch etliche Perlen darunter. Aber nicht nur dort, sondern auch im #OERcamp: Dort hielt sie am 1. Juni dieses Jahres den <i>Webtalk</i> »Pixel-Spiele im Browser erstellen mit Bitsy«.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101003.html</link>
</item>
<item>
	<title>Tutorials mit Twine erstellen – ein #OERcamp-Webtalk</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/7NJ1tLAWOfU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Die im letzten Beitrag vorgestellte <i>Nele Hirsch</i> ist sehr umtriebig. Neben ihren Beiträgen in dem von ihr gegründeten eBildungslabor arbeitet sie auch regelmäßig bei den #OERcamps mit (OER steht für <i>Open Educational Resources</i>). Im Rahmen dieser Zusammenarbeit entstand im Frühjahr 2020 auch der <i>Webtalk</i> »Tutorials mit Twine erstellen«, dessen 30-minütige Aufzeichnung auf YouTube hochgeladen wurde.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101002.html</link>
</item>
<item>
	<title>Korrektur: Noch ein deutschsprachiges Twine-(Video-)Tutorial</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/yJ4KMh1KbT8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Nachdem ich am Donnerstag noch behauptet hatte, daß die Video-Tutorials von <i>Gerugon</i> (vermutlich) die einzigen deutschsprachigen Video-Tutorials zu Twine seien, strafte mich der Algorithmus von YouTube der Lüge und spielte mir obigen Screencast von <i>Nele Hirsch</i> in meine Zeitleiste. Er behandelt eher nur die Basics von Twine und ist vermutlich als Ersatz für diese (veraltete) Playlist der Autorin gedacht, aber es ist deutschsprachig.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020101001.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("tituste2-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild13.html"><img src="http://blog.schockwellenreiter.de/images/tituste2-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Langsam wird es zu einer lästigen Angewohnheit. Wie schon in den Vorwochen veröffentliche ich das obligatorische Hundebild zum Wochenende erst auf den letzten Drücker. Dieses Mal habe ich jedoch eine Ausrede: [[flickr]] ist down (vergleiche diesen Beitrag) und so habe ich nach langem Warten, daß der böse Panda den Dienst wieder freigibt, ein Photo aus dem Gesichtsbuch entnommen. Wie jedesmal soll Euch das Bild der kleinen Fellnase darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100902.html</link>
</item>
<item>
	<title>Eine Pandemie modellieren und dabei Julia 👩‍🦰 lernen</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/27OXvw3nkh4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Eigentlich hatte ich für dieses Wochenende ein ganz anderes Thema vorgesehen, das Euch als Video-Tutorial in Zeiten der Corona-Isolation beschäftigen sollte. Aber die Ereignisse dieser Woche haben gezeigt, daß die Erkenntnis, wie sich eine Epidemie ausbreitet, immer noch nicht bei genügend Menschen angekommen ist. Und da ich die Deutungshoheit nicht den feierwütigen, hedonistischen Hipstern überlassen möchte, gibt es heute doch wieder ein Video, das Euch zeigt, wie man eine Pandemie mit Julia modelliert, der Programmiersprache (nicht nur) für numerisches Rechnen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100901.html</link>
</item>
<item>
	<title>Let’s Twine Again – deutschsprachige Video-Tutorials zu Twine</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/w5u18-EQ4Es" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all"><i>Gerugon</i> ist eigentlich ein YouTuber, der sein Geld damit verdient (verdienen will?), daß er RPGs spielt, aufzeichnet und diese Aufzeichnungen auf YouTube hochlädt. Aber einmal ist es mit ihm durchgegangen, da hat er Twine entdeckt und gleich darauf ein Video-Tutorial produziert: »Softwarevorstellung von Twine – Erstelle dein eigenes Textabenteuer«. Wenn man einmal über die Tatsache hinwegsieht, daß <i>Gerugon</i> ein Fan des <i>Dark Mode</i> ist, finde ich, hat er ein nettes einführendes Tutorial aufgenommen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100801.html</link>
</item>
<item>
	<title>Neues aus der Python-Welt: Python 3.9 und verbesserter Python-Support in RStudio</title>
	<description>
			<![CDATA[<% imageref("carlimrmerostseestrand-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild12.html"><img src="http://blog.schockwellenreiter.de/images/carlimrmerostseestrand-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Python 3.9 ist draußen und ist der Beginn eines neuen, jährlichen Release-Zyklusses. Und RStudio hat in einer Preview für die Version 1.4 einen verbesserten Python-Support angekündigt. Doch schauen wir uns das der Reihe nach an:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100702.html</link>
</item>
<item>
	<title>Google behebt mit der neuen Version 86 seines Browsers Chrome (86.0.4240.75) auch wieder Sicherheitslücken</title>
	<description>
			<![CDATA[<% imageref("feldbotanik2-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild11.html"><img src="http://blog.schockwellenreiter.de/images/feldbotanik2-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google schließt mit der neuen Version (86.0.4240.75) seines Browsers Chrome wieder Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100701.html</link>
</item>
<item>
	<title>P5.js und Visual Studio Code (2)</title>
	<description>
			<![CDATA[<% imageref("p5jsvscode-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild10.html"><img src="http://blog.schockwellenreiter.de/images/p5jsvscode-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Am letzten Wochenende bin ich endlich auch der Anleitung von <i>Tim Rodenbröker</i> gefolgt und habe mithilfe des Pakets p5.vscode den Editor Visual Studio Code mit P5.js, dem JavaScript-Mode von Processing verheiratet. Auf den ersten Blick macht das Teil einen guten Eindruck auf mich. »Live Reload« funzt wie geschmiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100603.html</link>
</item>
<item>
	<title>Corona-Daten in Excel: Einmal nur mit Profis arbeiten!</title>
	<description>
			<![CDATA[<% imageref("kuppeleiszene-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild09.html"><img src="http://blog.schockwellenreiter.de/images/kuppeleiszene-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich fasse es nicht! Der britischen Gesundheitsbehörde <i>Public Health England (PHE)</i> sind zwischen dem 25. September und 2. Oktober 2020 rund 16.000 Corona-Infektionen durchgerutscht. Grund: Alle Daten wurden in <b>einer</b> (sic!) Microsoft-Excel-Tabelle erfaßt und die hatte ihre maximale Größe erreicht.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100602.html</link>
</item>
<item>
	<title>Google schließt am »Oktober-Patchday« wieder Sicherheitslücken in Android</title>
	<description>
			<![CDATA[<% imageref("biotrockenrasenmaeher-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild08.html"><img src="http://blog.schockwellenreiter.de/images/biotrockenrasenmaeher-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google hat mit seinem monatlichen Sicherheitsupdate für Android (und damit auch auf seinen Pixel-Geräten) wieder Sicherheitslücken geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100601.html</link>
</item>
<item>
	<title>Noch mehr Newton-Fraktale in Processing.py</title>
	<description>
			<![CDATA[<% imageref("newtonbanner-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild07.html"><img src="http://blog.schockwellenreiter.de/images/newtonbanner-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich konnte es nicht lassen. Angefixt durch meinen eigenen Beitrag mußte ich am Wochenende noch ein wenig mit Processing.py spielen und weitere Newton-Fraktale programmieren:]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100501.html</link>
</item>
<item>
	<title>Twine: Interaktive Geschichten im Klassenzimmer</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/cBnmJ7jHBz8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Eher zufällig spülte mir der Algorithmus von YouTube obiges Video in meinen Browser, einen halbstündigen Vortrag, in dem der Historiker und Medienpädagoge <i>Christoph Kaindel</i> seinem Publikum erklärt, wie man Twine als Mittel im Unterricht nicht nur für Game Design oder die Grundlagen des Programmierens, sondern auch als Werkzeug für kreatives Schreiben, für die Analyse literarischer Texte oder auch – gerade in Corona-Zeiten für den Fernunterricht besonders wichtig – für Online Präsentationen oder für Lernspiele/<i>Flipped Classrooms</i> (es müssen nicht immer Videokonferenzen sein) einsetzen kann.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100301.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("10jahreplaenterwald-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild04.html"><img src="http://blog.schockwellenreiter.de/images/10jahreplaenterwald-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wieder ist es Freitag, wieder ist es spät geworden und das wöchentliche Hundebild erscheint auf dem letzten Drücker. Es soll Euch – wie immer – darüber hinwegtrösten, daß es mindestens die nächsten zwei Tage keine oder nur wenige Updates hier im <del>Blog</del> Kritzelheft gibt. Zwar werde ich die 30 Jahre Ko(h)lonialisierung der DDR <b>nicht</b> feiern, aber ich werde feiern, daß ich am 1. Oktober vor 10 Jahren Mitglied des HSV Plänterwalds geworden bin. Coronabedingt haben wir dieses Ereignis (auf dem auch obiges Hundephoto aufgenommen wurde) gestern auf unserem Hundeplatz nur im kleinen Kreis und mit Abstand begangen, aber ich möchte morgen mit dem Spitz mal wieder eine große Tempelhofer-Feld-Tour unternehmen und so dieses Jubiläum würdigen.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100203.html</link>
</item>
<item>
	<title>Spiele mit JavaScript programmieren und P5.js-Hosting</title>
	<description>
			<![CDATA[<iframe width="560" height="315" src="https://www.youtube.com/embed/8xPsg6yv7TU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br clear="all">Damit Euch während der Corona-Isolation nicht die Decke auf den Kopf fällt, habe ich für dieses Wochenende ein anderthalbstündiges Video-Tutorial hervorgekramt, das von freecodecamp.org zusammengeschnitten wurde. In diesem Doppelfeature zeigt Euch <i>Ania Kubów</i>, die auch einen eigenen YouTube-Kanal besitzt, wie Ihr in JavaScript pur die Spieleklassiker »Doodle Jump« und »Flappy Bird« programmieren könnt. Jeder Teil dieses Kurses ist etwa 45 Minuten lang und kann auch separat genossen werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100202.html</link>
</item>
<item>
	<title>Apple veröffentlicht ein Ergänzungsupdate für Mojave wegen der Probleme mit dem Sicherheitsupdate 2020-005</title>
	<description>
			<![CDATA[<% imageref("feldbotanki1-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild03.html"><img src="http://blog.schockwellenreiter.de/images/feldbotanki1-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die unendliche Geschichte: Apple hat nun ein Ergänzungsupdate für macOS Mojave veröffentlicht, um die Probleme mit dem Sicherheitsupdate 2020-005 (wir berichteten mehrfach) zu beheben. Hoffen wir, daß damit das peinliche Hickhack behoben ist. Aber ich glaube noch nicht wirklich daran.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100201.html</link>
</item>
<item>
	<title>Weiter Probleme mit dem Sicherheitsupdate 2020-005 für Mojave</title>
	<description>
			<![CDATA[<% imageref("ecksofabuerger-s") %><a href="http://blog.schockwellenreiter.de/g2020/b202010/202010bild02.html"><img src="http://blog.schockwellenreiter.de/images/ecksofabuerger-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Apple hat das Sicherheitsupdate 2020-005 für Mojave sowie die neue Safari Version 14.0 wegen der vermehrt aufgetretenen Probleme (wir berichteten) offenbar zurückgezogen. Als Umgehungslösung für Betroffene besteht die Möglichkeit, entweder ein Upgrade auf macOS 10.15 Catalina durchzuführen. Dabei verliert man allerdings die Möglichkeit, 32-Bit Anwendungen ablaufen zu lassen. Die andere Alternative ist ein so genanntes In-Place-Update (einfach Darüberinstallieren) von macOS 10.14.6.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100102.html</link>
</item>
<item>
	<title>Die Zahlen für den September 2020</title>
	<description>
			<![CDATA[Ein neuer Monat der Corona-Isolation ist über mich hereingebrochen. Also gibt es – wie gewohnt – zuerst einmal ein paar Zahlen, die hochtrabend auch manches Mal <i>Mediadaten</i> genannt werden. Im September 2020 hatte der <i>Schockwellenreiter</i> laut seinem nicht immer ganz zuverlässigen, aber (hoffentlich!) datenschutzkonformen Neugiertool exakt <b>5.864 Besucher</b> mit <b>11.577 Seitenansichten</b>.]]></description>
	<link>http://blog.schockwellenreiter.de/2020/10/2020100101.html</link>
</item>
