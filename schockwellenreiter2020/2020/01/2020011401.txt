#title "Noch ein Twine-Tutorial – 20200114"
#maintitle "Noch ein Twine-Tutorial, denn Tutorials kann man nie zu viele haben"
#prevp "2020011101"
#nextp "2020011501"

## Noch ein Twine-Tutorial, denn Tutorials kann man nie zu viele haben

<iframe width="852" height="480" src="https://www.youtube.com/embed/pTOr1IpK9e8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ein YouTuber, der sich »ängstliche, quadratische Spiele« *(Scared Square Games)* nennt, hat eine [vierteilige Playlist](https://www.youtube.com/playlist?list=PLbKAX9JcsmTnk20lG1mmgmyyyxKzOVqIC) hochgeladen, die ein weiteres [Twine](cp^Twine 2)-Tutorial ausmacht. In den knapp anderthalb Stunden werden die Grundlagen von Twine behandelt, so daß man anschließend in die Lage versetzt sein sollte, mit dem Programm und mit der Dokumentation zurecht- und weiterzukommen.

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1484249194&asins=1484249194&linkId=3ab38243a33cd2f06d9465c5c29061c6&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

**BTW**: Auch wenn in diesem Tutorial wieder diese *Choose Your Own Adventure Books* als Ursprung von interaktiver Fiktion angesprochen werden, bin ich der Meinung, daß diese -- speziell wegen der *Second Person Point Of View* -- nicht gerade ein geeignetes Vorbild für moderne, interaktive Fiktion sind. Sie sind vermutlich nur für pubertierende Jungmänner von Interesse und engen die Phantasie der Autorinnen und Autoren nur ein. Da war ja selbst *Karl May* schon weiter und der wußte noch nichts von »interaktivem Hypertext«. Twine unterstützt andere *Point of Views* speziell durch die Inline-Links und das sollte man auch nutzen. Außerdem glaube ich, daß interaktive Fiktion auch damit zurecht kommt, wenn alles auf **ein** Ende hinausläuft. Die Story kann durchaus verschiendene Pfade durchlaufen (wiederum unterstützt Twine dies durch die Inline-Links), aber das Ende kann (muß aber nicht) durchaus immer das gleiche sein.

Auf der anderen Seite kann interaktive Fiktion natürlich auch wie ein [Roguelike](cp^Rogue) funktionieren: Tausend Tode muß der Rogue sterben, bevor er endlich das Amulett von Vendor und den Weg zurück aus dem Labyrinth findet. Aber auch dies muß konsequent umgesetzt werden.

Ich habe gerade mit großem Vergnügen das Buch »[Game Development with Ren'Py: Introduction to Visual Novel Games Using Ren'Py, TyranoBuilder, and Twine][a1]« gelesen (einer der Gründe, warum es die letzten Tage so ruhig hier war). In diesem läßt uns der Autor *Robert Ciesla* eine haarsträubende Räuberpistole mit den Engines [Ren'Py](cp^Ren'Py), [TyranoBuilder](https://tyranobuilder.com/) und Twine erzählen. Auch wenn mir persönlich TyranoBuilder nicht gefällt (es ist kommerziell und bleibt weit hinter den Möglichkeiten von Ren'Py), die Botschaft des Buches ist eindeutig: Die Geschichte zählt -- und nicht das Werkzeug!

[a1]: https://www.amazon.de/Game-Development-RenPy-Introduction-TyranoBuilder/dp/1484249194/ref=as_li_ss_tl?_encoding=UTF8&psc=1&refRID=CBSGPYQ4D7WJ59R71MHP&linkCode=ll1&tag=derschockwell-21&linkId=b2813f9941052779986a429c7f4c273f&language=de_DE