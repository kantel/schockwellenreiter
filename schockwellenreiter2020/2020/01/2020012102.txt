#title "Altglascontainer: Umweltschutz ist Klassenkampf – 20200121"
#maintitle "Altglascontainer: Auch Umweltschutz ist Klassenkampf"
#prevp "2020012101"
#nextp "2020012201"

<%= html.getLink(imageref("glascontainerueberfuellt-b"), "Überfüllte Altglascontainer am Kranoldplatz") %>

## Altglascontainer: Auch Umweltschutz ist Klassenkampf

Die Berliner Umweltsenatorin [Regine Günther](https://de.wikipedia.org/wiki/Regine_G%C3%BCnther) (B90/Grüne) hatte mal wieder eine ihrer seltsamen Schnapsideen: [Alle Altglassammeltonnen sollen von den Berliner Hinterhöfen außerhalb des S-Bahn-Rings verschwinden](https://www.rbb24.de/panorama/beitrag/2019/07/berlin-glas-recycling-hinterhof-tonnen-kommen-weg.html). Das heißt, während die Schönen und Reichen, die sich die Mieten in der Berliner Innenstadt noch leisten können, ihre leeren Schampus-Flaschen auch weiterhin fußnah entsorgen können, soll der ärmere Teil der Bevölkerung außerhalb des Rings doch gefälligst durch seinen Kiez irren und seine Schnapsflaschen in den spärlich aufgestellten Sammelcontainern entsorgen. Die ärmeren Bürger [machen die Drecksarbeit](https://www.tagesspiegel.de/berlin/entsorgung-von-altglas-in-berlin-jetzt-machen-wir-buerger-die-drecksarbeit/25230642.html), während die reicheren Bürger hausnah entsorgen können. Wen wundert es bei den grüngetünchten Liberalen, schließlich soll sich Leistung doch lohnen. Sowohl die Altglastonnen in den Höfen der Reichen wie auch die Sammelcontainer in den Straßen werden schließlich demokratisch von allen bezahlt -- von den Steuergeldern! Darum merke: Auch **Umweltschutz ist Klassenkampf**.

Dabei sind [Straßencontainer keine Alternative](https://www.bund-berlin.de/mitmachen/aktion-meine-altglastonne-bleibt/). Zum einen können sie einmal -- wie obiges, heute aufgenommenes Photo zeigt -- den Ansturm gar nicht fassen (man müßte dazu ja die Receycling-Unternehmen in die Pflicht nehmen, aber da glauben die Bündnisgrünen ja an das Märchen, das so etwas der Markt von alleine regelt), zum anderen können gerade ältere und körperlich eingeschränkte Menschen  die oft langen Strecken zum Straßeniglu gar nicht bewältigen oder müssen alternativ mit dem Auto zum Container fahren, also die ihnen aufgezwungene Drecksarbeit auch noch mit Dreckschleudern erledigen, die sie oft gar nicht besitzen.

Wir haben jedenfalls die Konsequenzen daraus gezogen: Unser Altglas landet seitdem wieder im Restmüll. Freßt das, Ihr grünen Öko-Kleinbürger! *[[photojoerg]]*

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Hey, da ging's den Berlinern doch bisher paradiesisch gut. Im neuerdings
ach so coolen Leipzig sind die zentralen Glascontainer der gar nicht
diskutierte Standard, bei mir als #dorfkind ebenfalls. Und ja, rund um
die Container sieht es (insbesondere nach Feiertagen mit
Trankopferbedeutung; also fast immer) zum Fürchten aus. Dafür haben die
Container in meinem Dorf aber auch eine Art "Basarfunktion". Da diese
oben nicht rund, sondern plan sind, wird gern allerlei Krimskrams zum
Mitnehmen dort abgestellt und tatsächlich anonym getauscht.   
BTW: Wie ist es in Bööörlin eigentlich mit der Müllentsorgung in
Siedlungsgebieten? Der in LE zuständige Eigenbetrieb der Kommune hat aus
Gründen der Effizienz ausschließlich Müllfahrzeuge in der gefühlten
Größe von Flugzeugträgern im Einsatz, die in kleinere Straßen nicht
reinfahren (dürfen) ... folglich entwickelt sich das Tonnenrollen zum
neuen Volkssport ...

*– André D.* <%= p("k01") %>

