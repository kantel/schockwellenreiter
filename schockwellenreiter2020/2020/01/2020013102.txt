#title "Getting started with Asciidoctor – 20200131"
#maintitle "Getting started with Asciidoctor"
#prevp "2020013101"
#nextp "2020020101"

## Getting started with Asciidoctor

<iframe width="852" height="480" src="https://www.youtube.com/embed/2dgGhi7MFqo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Andres Almiray* ist ein in der Schweiz lebender Mexikaner, Java-Entwickler, Open-Source-Advocate und Asciidoctor-Evangelist. Ihm verdanke ich das heutige, knapp einstündige und lehrreiche Video, das Euch beschäftigen soll, wenn das Wochenend-Wetter Euch ans Haus fesselt: »[Getting started with Asciidoctor](https://www.youtube.com/watch?v=2dgGhi7MFqo)«.

Bei meiner frischen Begeisterung für [AsciiDoc](cp^AsciiDoc) und [Asciidoctor](cp^Asciidoctor) konntet Ihr heute auch nichts anderes erwarten.

