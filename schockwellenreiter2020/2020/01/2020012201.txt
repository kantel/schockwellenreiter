#title "Flickr erhöht die Preise – 20200122"
#maintitle "Flickr erhöht die Preise für Flickr Pro"
#prevp "2020012102"
#nextp "2020012301"

<%= html.getLink(imageref("gutsparkmarienfelde-b"), "Gutspark Marienfelde") %>

## Flickr erhöht die Preise für Flickr Pro

Nachdem ich schon vor etwa einem Monat eine irritierende (Bettel- und Massen-) Email von *Don MacAskill*, nach eigenen Aussagen Co-Founder, CEO und Chief-Geek von SmugMug und [[flickr]] erhalten hatte, indem er mich (und all die vermutlich Millionen anderen Empfänger) bat, Werbung für [[flickr]] Pro zu machen, um die Geschäfte anzukurbeln, denn [[flickr]] führe Verluste ein, machten sie heute Ernst:

Mich erreichte eine weitere Mail, daß der Photodienst die [Preise erhöhen müsse](https://www.golem.de/news/fotodienst-flickr-erhoeht-die-preise-2001-146196.html), um die gestiegenen Betriebskosten zu decken. Als Trostpflaster könne ich mein Abo für zwei Jahre zum alten Preis verlängern, aber nur, wenn ich im Voraus zahle.

[[dr]]<%= imageref("silo") %>[[dd]]

Was sollte ich machen? [Gabi](http://www.gabi-kantel.de/) und ich sind seit 2005 -- also [seit 15 Jahren](http://www.schockwellenreiter.de/2005/07/21.html#notizAnMich) -- bei [[flickr]] und haben in diesen langen Jahren über 30.000 Photos dort hochgeladen. Eine zufriedenstellende Antwort auf die Frage »[Was tun, wenn Flickr stirbt](http://blog.schockwellenreiter.de/2015/10/2015102001.html)« habe ich bis heute leider immer noch nicht gefunden. [Theoretisch gäbe es zwar eine Lösung](http://blog.schockwellenreiter.de/2018/11/2018110201.html), doch wäre sie mit soviel Aufwand verbunden (30.000 Photos herunterladen und mithilfe eines Perl-Skriptes zu einer neuen Site zu generieren und diese dann auch noch irgendwo unterzubringen, **ist** Aufwand), daß sie praktisch kaum durchführbar ist.

Also habe ich in den sauren Apfel gebissen, meine Kreditkarte gezückt und das »großzügige« Angebot auf eine zweijährige Verlängerung bei Vorauszahlung wahrgenommen. Was blieb mir anderes übrig? Wer sich in ein Datensilo begibt, kommt darin um.

[[flickr]] war mal ein großartiger Dienst, der leider durch die Unfähigkeit des Yahoo-Managements unter die Räder gekommen ist. Und die Leute von SmugMug, die [[flickr]] dann gekauft haben, machen auf mich den Eindruck unfähiger BWL-Studenten im ersten Semester mit hochfliegenden, aber nicht durchdachten Plänen. Um [[flickr]] noch zu retten, bedarf es eines doppelten, kompetenten Teams von Beratern. Ein Team, das sich mal die Buchhaltung genau und unvoreingenommen vornimmt, und dann ein zweites Team von Kreativen und Visionären, das auf Basis dieser Zahlen neue Konzepte für die Zukunft entwickelt. Auf jeden Fall kein »Weiter so«. Ich hoffe, daß [[flickr]] solch ein Team doch noch findet.

---

**2 (Email-) Kommentar**

---

<%= a("k01") %>

>Es gibt seit ca. zwei Jahren das Opensource-Projekt [Pixelfed](https://pixelfed.org/).   
Es handelt sich hierbei um eine Art verteiltes Instagram, und daran wird aktiv entwickelt.   
Auch wenn es noch nicht viele Benutzer hat, so ist der Anfang doch gemacht und dies ein sehr interessantes Projekt.

*– Marc S.* <%= p("k01") %>

---

<%= a("k02") %>

>Der ehemalige Wettbewerber [www.ipernity.com](http://www.ipernity.com/) hat sein Team bereits gefunden! Es wurde nämlich 2017 vor der Pleite gerettet, indem die Mitglieder die Webseite per Crowdfunding aufkauften. Seitdem wird Ipernity sehr professionell von den Mitgliedern selbst gemanagt. Die öffentlich zugänglichen Jahresberichte der „Ipernity Members Association“ zeigen, dass der Verein finanziell gesund ist. Der Innovationsstau wurde ebenfalls beseitigt. In manchen Aspekten hat Ipernity inzwischen sogar die Nase vorn: Keine andere Foto-Sharing-Seite hat so gute individuelle Einstellmöglichkeiten oder einen integrierten Übersetzer für die einfache Kommunikation mit anderen Mitgliedern über Ländergrenzen hinweg.   
Ipernity ist also durchaus wieder eine Alternative zu Flickr. Auch preislich. Neben dem Standard-Tarif von jährlich 44,90 Euro gibt es nämlich auch einen Basis-Tarif für nur 22,90 Euro. Das ist weitaus günstiger als bei Flickr, ohne dass man beim Funktionsumfang oder bei der Performance Abstriche machen müsste. Wie Flickr wird Ipernity ebenfalls beim Amazon Web Service gehostet, wodurch Verfügbarkeit und Datensicherheit auf höchstem Niveau garantiert sind.   
Wer wechseln will, kann seine Inhalte einfach mit Hilfe eine Uploaders von Flickr zu Ipernity migrieren. Er unterstützt damit ein engagiertes, nicht-kommerzielles Projekt, das unabhängig von Investoren oder Großkonzernen ist. Ipernity gehört den Mitgliedern selbst, die es ohne Werbeeinnahmen nur über die Mitgliedsbeiträge finanzieren.

*– Bernhard W.* <%= p("k02") %>

