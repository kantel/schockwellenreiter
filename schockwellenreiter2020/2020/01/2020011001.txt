#title "Schau Mama, ein Hundebild! – 20200110"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020010901"
#nextp "2020011002"

<%= html.getLink(imageref("teltowj-b"), "Joey am Teltowkanal") %>

**Schau Mama, ein Hundebild!** Da ich heute noch einiges *offline* vorhabe, gibt es das wöchentliche Photo der kleinen Fellkugel etwas früher als üblich. Aber wie immer soll es Euch darüber hinwegtrösten, daß es erst Freitag ist und daß es vermutlich die nächsten zwei Tage noch weniger Updates als gewohnt im *Schockwellenreiter* geben wird. Denn ich möchte viel lesen, ein wenig programmieren, eventuell etwas schreiben und natürlich soll der kleine Sheltie auch nicht zu kurz kommen.

Ich hoffe, die Wetterfrösche haben gutes Wetter für uns alle bestellt. Ich wünsche Euch allen da draußen jedenfalls ein schönes Wochenende und wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*