#title "Boulder Dash in Pygame Zero – 20200116"
#maintitle "Boulder Dash in Pygame Zero und anderes Interessantes aus Wireframe"
#prevp "2020011601"
#nextp "2020011701"

<%= html.getLink(imageref("stuhlcwp-b"), "Stuhl an Mülleimer") %>

## Boulder Dash in Pygame Zero und anderes Interessantes aus der Wireframe

[Boulder Dash](https://de.wikipedia.org/wiki/Boulder_Dash) ist ein Computerspiel von *Peter Liepa* und *Chris Gray*, das erstmals 1984 für Ataris 8-Bit-Heimcomputer (400, 800, XL) veröffentlicht wurde. Bereits wenig später erfolgten Portierungen für fast alle zeitgenössischen Heimcomputersysteme. Das Spiel war ein Verkaufsschlager und ein Klassiker der Videospielgeschichte.

Das Spiel findet in einer mit Erde, Felsbrocken und Diamanten gefüllten Höhle statt. Ziel ist es, darin eine vorgegebene Anzahl von Diamanten einzusammeln und den Eingang zur nächsten Höhle zu passieren. Dazu muß sich die vom Spieler zu steuernde Figur Rockford durch die Erde graben, ohne zum Beispiel durch fallende Felsbrocken erschlagen zu werden.

<%= html.getLink(imageref("boulderdashpgz-b"), "Boulder Dash in Pygame Zero") %>

In der aktuellen Ausgabe 30 der Wireframe, die Ihr -- wie immer -- hier [kostenlos als PDF herunterladen](https://wireframe.raspberrypi.org/issues) gibt es eine [Hommage an Boulder Dash](https://www.raspberrypi.org/blog/code-a-boulder-dash-mining-game-wireframe-30/) in [Pygame Zero](cp^Pygame Zero) programmiert. Wie immer iest es kein vollständiges Spiel, sondern ein Progrämmchen, das Euch die grundlegende Spielmechanik erläutert. Den [Quellcode und die Assets](https://github.com/Wireframe-Magazine/Wireframe-30) findet Ihr auf GitHub.

Über die Feiertage und den Jahreswechsel habe ich irgendwie die Wireframe 29 verpaßt, die Ihr natürlich auch hier [kostenlos als PDF herunterladen](https://wireframe.raspberrypi.org/issues) könnt. Dort gibt es nicht nur als Hommage an die Spieleklassiker einen Flappy-Bird-Klon in Pygame Zero (Seiten 40-41, [Quellcode und Assets](https://github.com/Wireframe-Magazine/Wireframe-29)), sondern auch ein Tutorial, das Euch in [Ink](cp^Ink und Inky) einführt (Seiten 32ff.), einer Sprache ähnlich [Twine](cp^Twine) zur Erstellung interaktiver Geschichten. Zu Ink habe ich ebenfalls etwas in der Mache. Also laßt Euch überraschen.

Und zu Boulder Dash habe ich eine ganz persönliche Geschichte: Meine ersten Schritte zur Macintosh-Programmierung habe ich um 1988 mit dem Buch [Just Enough Pascal][a1] (JEP) von *Chip Morrison* und *Joseph Walters* unternommen, das in Symantercs [THINK Pascal](https://wiki.freepascal.org/THINK_Pascal/de) einführte. Und das Programm/Spiel, das man in diesem Büchlein programmierte, hieß *Grid Walker* und war in weitestem Sinne ein Boulder-Dash-Klon -- zumindest hatte ich das damals so wahrgenommen. Es war ein sehr witziges und lehrreiches Buch. Sollte also noch jemand einen 68000er Mac mit Symantecs Compiler haben … *[[photojoerg]]*

[a1]: https://www.amazon.de/Just-Enough-Pascal-SYMANTEC/dp/B003WVLSEG/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=Just+Enough+Pascal&qid=1579203074&sr=8-2&linkCode=ll1&tag=derschockwell-21&linkId=ff5f8042a9e9b5562fc2fa61c7fd127c&language=de_DE