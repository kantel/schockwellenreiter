#title "Die Zahlen – 20200101"
#maintitle "Die Zahlen"
#prevp "http://blog.schockwellenreiter.de/2019/12/2019123101.html"
#nextp "2020010201"

## Die Zahlen

Auch ein neues Jahr beginnt mit einem neuen Monat und daher erst einmal ein paar Zahlen, die hochtrabend auch manches mal *Mediadaten* genannt werden. Im Dezember 2019 hatte der *Schockwellenreiter* laut seinem nicht immer zuverlässigen, aber (hoffentlich) datenschutzkonformen [Neugiertool](cp^Piwik) exakt **5.853 Besucher** mit **11.317 Seitenaufrufen**. Natürlich täuscht die Genauigkeit der Ziffern eine Exaktheit der Zahlen nur vor, aber für einen Feiertagsmonat ist der Zuspruch doch gar nicht so schlecht.

Wie immer freue ich mich über jede Besucherin und jeden Besucher und bedanke mich bei allen meinen Leserinnen und Lesern.

😎 &nbsp; *Bleibt mir gewogen!*

Und da heute auch ein neues Jahr begonnen hat, wünsche ich Euch allen da draußen ein erfolgreiches (wie immer Ihr Erfolg definiert), fröhliches und friedliches Jahr 2020. Happy new year!