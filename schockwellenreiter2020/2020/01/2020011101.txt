#title "Tutorial: Hallo LiveCode! – 20200111"
#maintitle "Tutorial (und Test): Hallo LiveCode!"
#prevp "2020011003"
#nextp "2020011401"

## Tutorial (und Test): Hallo LiveCode!

Die plattformübergreifende Programmierumgebung/-sprache [LiveCode](cp^LiveCode) hat eine bewegte Geschichte hinter sich. Ihre Wurzeln liegen in [MetaCard](https://en.wikipedia.org/wiki/MetaCard), einem kommerziellen [HyperCard](cp^HyperCard)-Klon. Auf Basis der MetaCard-Engine entwickelte *Cross World Computing* 2001 das »Revolution«-Entwicklungssystem (ebenfalls kommerziell). 2003 übernahmen sie MetaCard und änderten ihren Namen in *Runtime Revolution Ltd.* 2010 wurde *Revolution* in *LiveCode* umbenannt und 2015 änderte auch die Firma ihren Namen in *LiveCode Ltd.* Schon vorher, 2013, wurde die *Live Code Community Edition* vorgestellt, eine Open-Source-Version (GPL) von LiveCode, das zweigleisig auch weiterhin als kommerzielle Software weiterentwickelt wird.

LiveCode läuft auf den großen drei Plattformen Windows, macOS und Linux/Unix und kann zusätzlich Runtimes für iOS, Android und HTML5 erzeugen. Die Programmiersprache heißt *LiveCode Script* (früher MetaTalk) und ist eine Weiterentwicklung von HyperCards *HyperTalk*.

<%= html.getLink(imageref("hallolivecode-b"), "Screenshot Hallo LiveCode") %>

Wegen seiner kommerziellen Vergangenheit hatte ich trotz meiner [Affinität zu HyperCard](War HyperCard nicht toll? – 20200108) *LiveCode* lange links liegen lassen, nun dachte ich aber, daß es an der Zeit wäre, das Teil doch einmal zu auszuprobieren. Lackmustest war mein einfaches [HyperCard-Hallo-World-Tutorial](http://www.kantel.de/hc/hypertalk1.html) von 1996. Und siehe da, mein damaliges Skript konnte unverändert übernommen werden:

~~~
on mouseUp
   if the hilite of me is true then
      put empty into cd field "Hello"
      set the hilite of me to false
   else
      put "Hello Jörg! 🤓" into cd field "Hello"
      set the hilite of me to true
   end if
end mouseUp
~~~

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3836272067&asins=3836272067&linkId=48e8d3a33744d3605bbb795613317677&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Der *Property Inspector* ist natürlich weit umfangreicher geworden, als es HyperCards *Tool Menu* je war und ich habe lange nach den Orten gesucht, in denen ich die Schriftart und -größe des Feldes festlegen, und auch die Stelle, wo ich den Cursor aus dem Feld verbannen konnte, war nicht leicht zu finden. Aber das wird sich nach einer gewissen Einarbeitung schon legen.

Anlaß für diesen Test war das Buch »[Eigene Apps programmieren: Schritt für Schritt zur eigenen App mit LiveCode][a1]« von *Hauke Fehr*, das beim ersten Durchstöbern einen guten Eindruck auf mich machte. Es wird mir sicher auch helfen, die Stellen in den diversen Inspektoren zu finden, in denen man Eigenschaften ein- oder ausschalten oder ändern kann.

[a1]: https://www.amazon.de/Eigene-Apps-programmieren-Programmieren-Vorkenntnisse/dp/3836272067/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1IXIWIHTDG6OV&keywords=eigene+apps+programmieren&qid=1578758259&sprefix=Eigene+App,aps,152&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=e3b6f45b86e2f9baae2a8124f014ea2d&language=de_DE

Das Buch ist brandneu, von November 2019 und behandelt die aktuelle Version 9.5 von LiveCode. Ich habe jedenfalls Blut geleckt und versuche mich nun zu erinnern, welche genialen Projekte ich seinerzeit (das ist über 20 Jahre her!) mit HyperCard umgesetzt hatte. Denn einige davon würde ich nun gerne in LiveCode realisieren und hier im <del>Blog</del> Kritzelheft vorstellen. *Still digging!*