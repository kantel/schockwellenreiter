#title "Sphinx in der Praxis – 20200207"
#maintitle "Sphinx in der Praxis"
#prevp "2020020502"
#nextp "2020020702"

## Video: Sphinx in der Praxis

<iframe width="852" height="480" src="https://www.youtube.com/embed/0ROZRNZkPS8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Wie das Leben so spielt. Da hatte ich mich gerade mit [AsciiDoc](cp^AsciiDoc) und [Asciidoctor](cp^Asciidoctor) angefreundet, da kommt von einem meiner Klienten die Anforderung, ihn in [reStructuredText](cp^reStructuredText) (reST) und [Sphinx](cp^Sphinx) einzuarbeiten. Es geht um Geisteswissenschaftler und Verwaltungsmitarbeiter mit geringem informationstechnischen Hintergrund (reStructuredText war ihnen von den Kooperationspartnern, die einen etwas größeren IT-Hintergrund besitzen, aufgedrängelt worden). Also bereite ich jetzt so etwas wie »reStructuredText und Sphinx für Literaten« *(reST and Sphinx for Poets)* vor.

Da ich jedoch mit diesen Werkzeugen bisher ebenfalls kaum in Berührung gekommen bin, war zum Einstieg dieser Vortrag von *Carol Willing* sehr hilfreich. Es ist mehr eine Übersicht, wie man mit Sphinx sinnvoll praktisch arbeiten kann und weniger ein Tutorial, das in die Details der Auszeichnungssprache(n) und seiner Werkzeuge geht.

Gleichzeitig fand ich, daß es ein schönes, halbstündiges Wochenendvideo für Euch da draußen ist, falls das Wetter mal wieder keine *Outdoor*-Aktivitäten zuläßt.&nbsp;🤓

