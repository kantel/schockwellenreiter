#title "Barockes Britz – 20200217"
#maintitle "Barockes Britz"
#prevp "2020021701"
#nextp "2020021703"

<%= html.getLink(imageref("barockesbritz-b"), "Barockes Britz") %>

**Barockes Britz**: Straßenmöbel am Britzer Damm. Aufgenommen am 17. Februar 2020. *[[photojoerg]]*