#title "Chapbook, ein neues Story-Format für Twine – 20200210"
#maintitle "Chapbook – ein neues Story-Format für Twine 2"
#prevp "2020020801"
#nextp "2020021002"

<%= html.getLink(imageref("kinmozendo-b"), "Kin-Mō-Zendō in Frohnau") %>

## Chapbook – ein neues Story-Format für Twine 2

Da habe ich mich gerade an [Harlowe](https://twine2.neocities.org/), dem Default-Story-Format von [Twine 2](cp^Twine 2), gewöhnt, da betritt urplötzlich ein neuer Player die Szene: [Chapbook](https://github.com/klembot/chapbook) ist ein neues, freies (MIT-Lizenz) Story-Format für Twine. Entwickler dieses Story-Formats ist kein Geringerer als *[Chris Klimas](https://chrisklimas.com/)*, der auch der Schöpfer von Twine ist.

Chapbook wurde mit dem Ziel entwickelt, einen leicht zu lesenden und zu schreibenden (Twine-) Quellcode zu produzieren und ein noch besseres, responsives Verhalten für alle möglichen Arten von Geräten bereitzustellen. Außerdem bringt es eine Art Debugger mit und das Einbinden von Multimedia-Inhalten soll einfacher geworden sein.

Die [Dokumentation zu Chapbook](https://klembot.github.io/chapbook/guide/) ist ähnlich umfangreich und vollständig wie die Dokumentation zu Harlowe und scheint -- nach einer ersten Durchsicht -- keine Fragen offen zu lassen.

Entgegen der Dokumentation muß das Format nicht mehr manuell hinzugefügt werden, sondern ist in der aktuellen Twine-Version (Twine 2.3.5) bereits enthalten. Ansonsten ist das Format aber so neu, daß es noch keine Aufnahme in das [Twine Wiki](https://twinery.org/wiki/) gefunden hat.

Aufmerksam auf Chapbook wurde ich durch den unermüdlichen *Dan Cox*, der am Sonnabend seiner YouTube-Playlist zu Twine 2.3 [sechs neue Videos zu Chapbook](https://www.youtube.com/watch?v=ToIALPHeRYo&list=PLlXuD3kyVEr5jWoG0oDygKWOgFC3qrKN-&index=30) (ab Video 30) hinzufügte.

Ich glaube, ich muß mich auch dringend mal mit Chapbook beschäftigen …