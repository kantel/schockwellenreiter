#title "TigerJython, ein neuer Versuch – 20200224"
#maintitle "TigerJython, ein dritter Versuch"
#prevp "2020022201"
#nextp "2020022402"

## Worknote: TigerJython, ein dritter Versuch

[TigerJython](cp^TigerJython) ist als plattformübergreifende [Python](cp^Python)- oder exakter [Jython](cp^Jython)-IDE für Programmiereinsteiger und sonstige »Literaten« (Python for Poets) konzipiert. Nachdem gestern das Wetter so miserabel war, daß es mich an den Laptop gefesselt hatte, hatte ich mal nachgeschaut, welche Updates es so für mich gibt. Und siehe da, TigerJython hatte im Januar dieses Jahres eine Update erhalten.

<%= html.getLink(imageref("tigerjython2-b"), "TigerJython, neuer Versuch") %>

Ich hatte mich vor [knapp vier Jahren](http://blog.schockwellenreiter.de/2016/08/2016080302.html) schon einmal mit diesem Stück Software auseinandergesetzt und beklagt, daß -- zumindest die Mac-Version -- aufgrund der Java-Architektur zu häufigen Abstürzen neigte. Im letzten Jahr hatte ich zwar [notiert](http://blog.schockwellenreiter.de/2019/05/2019052102.html) , daß diese Abhängigkeit vom System-Java nun behoben sei und [bei ersten Versuchen festgestellt](http://blog.schockwellenreiter.de/2019/05/2019052201.html), daß tatsächlich keine Abstürze mehr zu verzeichnen waren, aber dann das Teil dennoch aus den Augen verloren (ich hatte zuviel mit [Processing.py](cp^processingpy) und [Pygame Zero](cp^Pygame Zero) zu spielen&nbsp;🤓).

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3836265141&asins=3836265141&linkId=7971b2b78d8f448dbcd9d8311ae3db41&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Gestern hatte ich aber wieder Lust, etwas mit TigerJython anzustellen. Dazu mußte ich erst einmal sehen, wie es zu meinem Programmierstil paßt. Denn einmal importiert die ansonsten ausgezeichnete Dokumentation[^1] alle Bibliotheken global und provoziert somit eine unter Umständen gefährliche Namensraumverschnutzung und zum anderen ist bei der Graphik-Bibliothek der Ursprung des Koordinatensystems links unten, statt wie allgemein üblich links oben.

[^1]: *Jarka Arnold*, *Tobias Kohn* und *Aegidius Plüss*: [Programmierkonzepte mit Python und der Lernumgebung TigerJython](http://www.tigerjython.ch/index.php), zuletzt aktualisiert am 18. Dezember 2019, auch als PDF zum [Download](http://www.tigerjython.ch/pdf) (503 Seiten, 16,8 MB) und *Jarka Arnold*: [Grafik, Robotik, Datenbanken und Spielprogrammierung mit Python](http://www.jython.ch/).

Das erste war leicht zu beheben: Da die Dokumentation auch über eine (hoffentlich) vollständige Auflistung aller Methoden jeder mitgelieferten Bibliothek verfügte, konnte ich zum Beispiel mit

~~~python
import gpanel as g
~~~

eine Namensraumverschmutzung vermeiden. Auch das zweite Problem ließ sich recht leicht beheben. Mit

~~~python
WW = 300        # Fensterbreite
WH = 300        # Fensterhöhe

g.makeGPanel(g.Size(WW, WH))
g.window(0, WW, WH, 0)
~~~

wurde die y-Achse gespiegelt, so daß ich meine y-Koordinaten wie gewohnt von oben nach unten durchzählen konnte[^2].

[^2]: Mir ist unverständlich, wieso einige Leute auf die Idee kommen, daß es für Programmieranfänger einfacher sei, wenn der Ursprung des Koordinatensystems in der linken unteren Ecke liegt. Auch *Paul Vincent Craven* mit seiner [Python Arcade Library](cp^Python Arcade Library) ist diesem Irrtum aufgesessen. Spätestens dann, wenn diese armen Programmieranfänger mit »echten« Bibliotheken konfrontiert werden, müssen sie doch wieder umlernen. Und das fällt schwer, wenn man sich erst einmal an etwas anderes gewöhnt hat.

Was mir sonst noch auffiel: Die Auswahl neuer Schriften ist etwas umständlich 

~~~python
myFont = g.Font("Courier New", g.Font.PLAIN, 32)
g.font(myFont)
g.text(10, 200, "Let's Do It!")
~~~

und die so eingesetzten Schriften erfahren auch keine [Kantenglättung](https://de.wikipedia.org/wiki/Antialiasing_(Computergrafik)) *(Anti-Aliasing)*, sehen daher ziemlich verpixelt aus. Außerdem schreibt TigerJython -- entgegen den üblichen Empfehlungen der Python Community ([PEP 8](https://www.python.org/dev/peps/pep-0008/)) den Variablennamen im [*mixedCamelCase*](https://de.wikipedia.org/wiki/Binnenmajuskel#Programmiersprachen) (auch *lowerCamelCase* genannt) statt des Unterstrichs. Aber das bin ich ja schon von Processing.py gewohnt und ist dem darunterliegendem Java geschuldet.

Ein großes Plus sind die mitgelieferten Module zur Turtle-Graphik, zur Graphik-Programmierung, zur Robotik und zur Spieleprogrammierung. Ich befürchte also, daß ich weiter mit TigerJython spielen werde und hoffe, die Zeit dafür zu finden. *Still digging!*
