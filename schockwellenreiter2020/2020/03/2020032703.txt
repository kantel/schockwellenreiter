#title "Video-Tutorial: Partikel mit Pygame – 20200327"
#maintitle "Video-Tutorial: Ein Partikelsystem mit Pygame"
#prevp "2020032702"
#nextp "2020032704"

## Video-Tutorial: Ein Partikelsystem mit Pygame

<iframe width="852" height="480" src="https://www.youtube.com/embed/F69-t33e8tk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Da es zumindest morgen noch mal richtig frühlingshaftes Wetter geben soll, habe ich für dieses Wochenende nur ein recht kurzes lehrreiches Video für Euch herausgesucht: In gut 11 Minuten erklärt Euch hier *[DaFluffyPotato](http://dafluffypotato.com/)*, wie man in [Pygame](cp^Pygame) ein Partikelsystem programmieren kann.

Solltet Ihr doch mehr Zeit zur Verfügung haben, das Video ist das vorläufig letzte Video einer [18-teiligen Playlist](https://www.youtube.com/watch?v=xxRhvyZXd8I&list=PLX5fBCkxJmm1fPSqgn9gyR3qih8yYLvMj) über die Spieleentwicklung mit Pygame. Insgesamt ist sie über drei Stunden lang. Das kann Euch in Eurer erzwungenen Corona-Isolation schon eine Weile beschäftigen.

Die Reihe soll fortgesetzt werden, ich habe daher den YouTube-Kanal abonniert und werde Euch gegebenenfalls auf den Laufenden halten.

Ich selber habe zur Zeit recht viel um die Ohren, aber es würde mich schon reizen, die Ideen aus dieser Playlist mal in [Pygame Zero](cp^Pygame Zero) zu realisieren. Besonders die sechs Videos für ein [Platformer Game](https://de.wikipedia.org/wiki/Jump_%E2%80%99n%E2%80%99_Run) haben es mir angetan. *Still digging!*

<%= html.getLink(imageref("pygamezero"), "cp^Pygame Zero") %>