#title "Virenmutterschiff aka Schornsteinfeger – 20200327"
#maintitle "Virenmutterschiff aka Schornsteinfeger"
#prevp "2020032603"
#nextp "2020032702"

## Das Virenmutterschiff (aka Schornsteinfeger) kommt ins Haus

Ich habe überhaupt kein Verständnis dafür, daß ausgerechnet in Zeiten von Covid19 und den damit (zu Recht) verfügten Ausgangsbeschränkungen unbedingt ein Schornsteinfeger als Virenmutterschiff durch das Haus und unsere Wohnung schleichen muß. Ich bin 65+ und damit Risikogruppe!

<%= html.getLink(imageref("virenmutterschiff-b"), "Das Virenmutterschiff kommt ins Haus") %>

Und nein, ich bin nicht abergläubisch. Ich glaube daher nicht, daß Schornsteinfeger Glück bringen. *[[photojoerg]]*

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Ich glaube, Du übertreibst. Man muss dem Schornsteinfeger weder die Hand
geben, noch auf die Schulter klopfen, noch ihn umarmen.
Obwohl ich - zugegeben - selten Kontakt mit Schornsteinfegern habe -
ich würde auch im Normalfall eher einen Abstand von einem halben Meter
halten, und jetzt auf 1,5 m vergrößern.    
Warum sollte gerade die Schornsteinfegerin ein Virenmutterschiff sein?

*– Michael F.* <%= p("k01") %>

