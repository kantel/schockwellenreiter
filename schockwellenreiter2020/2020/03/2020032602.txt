#title "Sicherheitsupdate für iCloud for Windows – 20200326"
#maintitle "Sicherheitsupdate für iCloud for Windows behebt Sicherheitslücken"
#prevp "2020032601"
#nextp "2020032603"

<%= html.getLink(imageref("ausdemsack-b"), "Schränkchen aus dem Sack") %>

## Sicherheitsupdate für iCloud for Windows behebt Sicherheitslücken

Offensichtlich konnte Apple seinen Flickentag nicht an einem Tag durchziehen und hat daher nachträglich das [Update 7.18 von iCloud for Windows 7, 8.1](https://support.apple.com/de-de/HT211107) und das [Update 10.9.3 von iCloud for Windows 10](https://support.apple.com/de-de/HT211106) freigegeben und damit mehrere teils kritische Schwachstellen korrigiert.

Das Update erhält man am einfachsten über die integrierte Software-Aktualisierung von Apple. *([[cert]])*

*[[photojoerg]]*