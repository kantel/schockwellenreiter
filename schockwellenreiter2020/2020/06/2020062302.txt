#title "Google behebt Sicherheitslücken – 20200623"
#maintitle "Google behebt mit der neuen Version seines Browsers Chrome Sicherheitslücken"
#prevp "2020062301"
#nextp "2020062401"

<%= html.getLink(imageref("sportspindlersfeld-b"), "Sportliches Spindlersfeld") %>

## Google behebt mit der neuen Version seines Browsers Chrome Sicherheitslücken

Google schließt mit der neuen Version (83.0.4103.116) seines Browsers Chrome wieder [Sicherheitslücken](https://chromereleases.googleblog.com/2020/06/stable-channel-update-for-desktop_22.html).

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden. *([[cert]])*

*[[photojoerg]]*

