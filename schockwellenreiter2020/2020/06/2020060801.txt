#title "Creative Coding und Generative Art – 20200608"
#maintitle "Creative Coding und Generative Art (und wie man sie dokumentiert)"
#prevp "2020060701"
#nextp "2020060901"

<%= html.getLink(imageref("vandalismusoderkunst-b"), "Ist das schon Kunst oder noch Vandalismus?") %>

## Kleine Linkschleuder zu Creative Coding und Generative Art (und wie man sie dokumentiert)

Ich muß gestehen, ich habe das [Riesen](Mein Video-Programm für das Wochenende (1) – 20200605)-[Video](Video-Programm für das Wochenende (2): Sphinx – 20200605)-[Programm](Video-Tutorial (3): Python und Blender – 20200605), das ich mir für das Wochenende vorgenommen hatte, nicht geschafft, denn das Wetter war schön und der kleine Spitz und ich wollten an die frische Luft. Lediglich die 14 Videos zu »[How to Write a Platformer Game in Java](https://www.youtube.com/playlist?list=PLYNGZdVGGmHxPqhiMlKAkVYdC2d3YU_3m)« hatte ich mir reingezogen und diese waren zwar anstrengend, aber auch sehr inspirierend. Ich werde meinen eigenen, hier [angefangenen Platformer](Tutorial: Ninja Frog (Stage 1) – 20200526) in [Pygame Zero](cp^Pygame Zero) daraufhin noch einmal komplett überarbeiten müssen.

Alles andere harrt noch (m)einer Entdeckung, also das [Sphinx](cp^Sphinx) Tutorial und das [Blender](cp^Blender) und Python Tutorial. Zur Vorbereitung habe ich mir schon einmal ein paar Links herausgesucht, die mir sehr nützlich erschienen:

### Creative Coding und Generative Art

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=B085RNLGKQ&asins=B085RNLGKQ&linkId=8dc509a1c032df7c7b3c98d3b4a9edb3&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Ibby EL-Serafy: *[A gentle introduction to coding by making generative art](https://medium.com/@theibbster/a-gentle-introduction-to-coding-by-making-generative-art-c7f0a7b744a6)*, Medium.com vom 22. Februar 2019. Eine sehr ausführliche Anleitung, wie man Generative Art in [P5.js](cp^p5js) entwickelt. Aber Achtung, das ist eine von diesen seltsamen Medium-Seiten, von denen man nur drei im Monat ansehen darf. Aber da der Monat noch jung ist, habt Ihr vielleicht noch ein oder zwei Klicks frei. Am Besten, Ihr macht es wie ich und legt eine Sicherungskopie auf Eurem Rechner ab.

- Auf den Seiten von Page gibt es ein nettes Processing-Tutorial von *Tim Rodenbröker*: *[Photos rasten für Einsteiger](https://page-online.de/tools-technik/processing-mit-tim/)*, das mehr ist, als der Titel verspricht, nämlich eine komplette Einführung in [Processing (Java)](https://page-online.de/tools-technik/processing-mit-tim/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/CDItiiKmRW0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Und noch ein Video. Den [Code für diesen *Fractal Dream Attractor*](https://github.com/a1kageyama/Fractal_Dream_Attractor) (ein [Jupyter](cp^Jupyter) Notebook) gibt es samt einem [Remake](https://github.com/a1kageyama/FractalDream_Attractor--Remake--) auf GitHub. [Dort](https://github.com/a1kageyama?tab=repositories) findet Ihr noch mehr seltsame Attraktoren.

- Auf *freeCodeCamp* ein älter Beitrag (aus 2018) von Ali Spittel: *[An introduction to Generative Art: what it is, and how you make it](https://www.freecodecamp.org/news/an-introduction-to-generative-art-what-it-is-and-how-you-make-it-b0b363b50a70/)*. Er ist nicht nur ebenfalls eine Einführung in P5.js, sondern besitzt auch zahlreiche weiterführende Links.

- Diesen Vortrag habe ich im Blog *Algorithmic Art* gefunden: *[A Gentle Introduction to Processing - Coding for Artists](http://algorithmicartmeetup.blogspot.com/2017/05/a-gentle-introduction-to-processing.html)*. Das Blog betreibt auch einen interessanten [YouTube-Kanal](https://www.youtube.com/channel/UCO6iBPzIvUdzxcf87BN24FQ/), der noch meiner Entdeckung harrt. Ich habe ihn erst einmal abonniert. Im [Blog selber](http://algorithmicartmeetup.blogspot.com/) findet Ihr noch einen Haufen interessanter Artikel. Ich werde weiter unten noch einmal darauf zurückkommen.

### Python und Blender

Neben der Dokumentation habe ich hier eine Reihe von (Video-) Tutorials gefunden:

- [Blender 2.82a Python API Documentation](https://docs.blender.org/api/current/index.html)

- Blender Dokumentation: [Ein Beispielscript in Python](https://de.wikibooks.org/wiki/Blender_Dokumentation:_Ein_Beispielscript_in_Python).

- [Python in Blender Tutorial](https://www.youtube.com/watch?v=sXiagmaDgvs&list=PLj89RbJsZUGs-t_fgTgjgOS200bkXj6k-), eine neunteilige, deutschsprachige Playlist.

- [3D Programming for Beginners Using Python and Blender 2.8](https://www.youtube.com/watch?v=rHzf3Dku_cE), Video-Tutorial.

- [Blender Python Tutorial](https://www.youtube.com/watch?v=cyt0O7saU4Q): An Introduction to Scripting, Video-Tutorial.

- [Creative Coding in Blender: A Primer](https://medium.com/@behreajj/creative-coding-in-blender-a-primer-53e79ff71e). This tutorial aims to encourage creative coders to consider Blender as a platform for creating 3D artworks. Blender can be daunting to learn, so this primer is written for those who’ve tried their hand at creative coding before, but wish to expand. We’ll write some Python scripts to animate geometry and conclude with Open Shading Language to add texture to those models.

- Und ich habe ja versprochen, noch einmal das Blog *Algorithmic Art* zurückzukommen. Denn auch hier gibt es ein nettes und ausführliches Tutorial für Einsteiger: [Blender 3D Basics and Python Coding](http://algorithmicartmeetup.blogspot.com/2019/10/blender-3d-basics-and-python-coding.html).

## Software-Dokumentation

Die folgenden Artikel von [opensource.com](https://opensource.com/) beschäftigen sich nicht mit [Sphinx](cp^Sphinx) und/oder [reStructuredText](cp^reStructuredText), sondern mehr mit der generellen Frage, wie man Software-Projekte dokumentiert (wer sich damit schon mal beschäftigt hat oder beschäftigen mußte, weiß, das diese Frage nicht so einfach zu beantworten ist):

- Cameron Shorter: *[What is good documentation for software projects](https://opensource.com/article/20/4/documentation)?* Mixing experienced tech writers with open source communities reveals new approaches for creating better docs.

- Kevin Xu: *[How to write effective documentation for your open source project](https://opensource.com/article/20/3/documentation)*. Documentation quality can make the difference in people trying your project or passing it by.

- Nigel Babu: *[10 tips for better documentation](https://opensource.com/business/15/7/10-tips-better-documentation)*.

- Laurie Barth: *[What is a developer journal](https://opensource.com/article/19/4/what-developer-journal)?* Solve your future problems faster by logging the most useful things you've already learned.

**War sonst noch was?** Ach ja, *Bartosz Zaczyński* meint in »[Python vs JavaScript for Pythonistas](https://realpython.com/python-vs-javascript/)«, daß wir alle neben Python auch noch JavaScript beherrschen sollten. Daher gibt er eine nette Einführung in JavaScript und vergleicht die Sprache mit Python: *»In this tutorial, you learned about JavaScript’s origins, its alternatives, and where the language is headed. You compared Python vs JavaScript by taking a closer look at their similarities and differences in syntax, runtime environments, associated tools, and jargon. Finally, you learned how to avoid fatal mistakes in JavaScript.«* Im Prinzip gebe ich ihm ja Recht, aber ich werde mit JavaScript einfach nicht warm (und das liegt nicht nur an den geschweiften Klammern). *[[photojoerg]]*