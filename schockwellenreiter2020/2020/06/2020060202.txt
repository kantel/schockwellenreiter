#title "Google schließt Sicherheitslücken in Android – 20200602"
#maintitle "Google schließt am »Juni-Patchday« wieder Sicherheitslücken in Android"
#prevp "2020060201"
#nextp "2020060301"

<%= html.getLink(imageref("uebungsflugzeug-b"), "Übungsflugzeug der Flughafen-Feuerwehr") %>

## Google schließt am »Juni-Patchday« wieder Sicherheitslücken in Android

Google hat mit seinem [monatlichen Sicherheitsupdate für Android](https://source.android.com/security/bulletin/2020-06-01) (und damit auch auf seinen [Pixel-Geräten](https://source.android.com/security/bulletin/pixel/2020-06-01)) wieder Sicherheitslücken geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen: 01.06.2020, 05.06.2020.

Die Updates werden so nach und nach per OTA *(over the air)* auf Pixel 4 und Pixel 4 XL, Pixel 3a und 3a XL, Pixel 3 und 3 XL, Pixel 2 und 2 XL verteilt.

Die anderen Hersteller werden wie üblich in Bälde nachziehen, sofern sie überhaupt noch entsprechenden Support leisten.

Für die eigenen Pixel-Smartphones hat Google im Juni wie alle drei Monate  ein so genanntes [Feature-Drop mit zahlreichen neuen Android-Funktionen](https://www.blog.google/products/pixel/new-pixel-features-drop/) bereitgestellt. *([[cert]])*

*[[photojoerg]]*