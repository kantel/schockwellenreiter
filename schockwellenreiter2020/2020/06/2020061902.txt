#title "Schau Mama, ein Hundebild! – 20200619"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020061901"
#nextp "2020061903"

<%= html.getLink(imageref("feldbezwinger-b"), "Der (Tempelhofer) Feldbezwinger") %>

**Schau Mama, ein Hundebild!** Natürlich ist ein Freitag ohne ein Hundephoto im *Schockwellenreiter* einfach kein Freitag. Daher nun das Bild des kleinen Spitzes als großen Bezwinger des Tempelhofer Feldes, das Euch darüber hinwegtrösten soll, daß es vermutlich die nächsten zwei Tage keine oder nur wenige Updates hier im <del>Blog</del> Kritzelheft geben wird. Denn ich habe mir ein [großes Videoprogramm](Monsterplaylist zu Python und Pygame – 20200619) verpasst und möchte dazu meinen [Python-Editor](cp^Geany) und -Interpreter quälen. *Last but not least* will natürlich der kleine Titus auch noch bespaßt werden.

Die Wetteraussichten für Berlin sind bis Montag -- wenn man den Wetterfröschen glauben darf -- nur so *la la*, aber auch nicht ganz hoffnungslos. Daher wünsche ich Euch allen da draußen ein schönes Wochenende. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*