#title "Code the Classics: Gauntlet – 20200604"
#maintitle "Code the Classics in Pygame Zero: Gauntlet"
#prevp "2020060402"
#nextp "2020060501"

<%= html.getLink(imageref("gauntlet-b"), "Gauntlet") %>

## Code the Classics in Pygame Zero: Gauntlet

[Gauntlet](https://de.wikipedia.org/wiki/Gauntlet_(Computerspiel)) (deutsch Fehdehandschuh, Panzerhandschuh) ist ein Arcade-Spiel, das 1985 von Atari Games entwickelt wurde. Es ist das erste Action-Rollenspiel (Hack and Slay), in dem vier Spieler gleichzeitig miteinander antreten konnten. Das Spiel findet aus der Vogelperspektive statt und kann alleine oder bis zu drei Mitspielern an einem Gerät gespielt werden. Warum ich Euch dies erzähle? Nun, die neue [Wireframe 39](https://wireframe.raspberrypi.org/issues/39) ist draußen und Ihr könnt sich Euch wie gewohnt [kostenlos als PDF herunterladen](https://wireframe.raspberrypi.org/issues).

Denn in dieser Ausgabe ([gespiegelt auch wieder im Raspberry Pi Blog](https://www.raspberrypi.org/blog/code-gauntlets-four-player-co-op-mode-wireframe-39/)) programmiert *Mark Vanstone* auf den Seiten 64 bis 65 in Python und [Pygame Zero](cp^Pygame Zero) eine Hommage an Gauntlet für bis zu vier Spieler. Zwei können über die Tastatur am Spiel teilnehmen, Pfeiltasten und `WASD`, zwei weitere über Joysticks (wenn sie am Rechner angeschlossen sind). Dafür muß [Pygames](cp^Pygame) Joystick-Modul importiert werden.

Den [Quellcode und sämtliche Assetes](https://github.com/Wireframe-Magazine/Wireframe-39/tree/master/source-code-gauntlet) findet Ihr natürlich wieder auf GitHub.

Wer dann noch nicht genug hat: Auf den Seiten 50 bis 55 zeigt derselbe *Mark Vanstone*, wie man einen Multiplayer Space Shooter in [Unity](cp^Unity) programmiert. Auch [dieses Programm](https://github.com/Wireframe-Magazine/Wireframe-39) findet Ihr auf GitHub.

<%= html.getLink(imageref("pygamebunt"), "cp^Pygame Zero") %>