#title "Processing und Python – 20200607"
#maintitle "Processing und Python (Processing.py)"
#prevp "2020060601"
#nextp "2020060801"

<%= html.getLink(imageref("endederwelt-b"), "Das Ende der Welt (Weder-/Ecke Rungiusstraße)") %>

## Tips zu Processing und (und mit) Python (Processing.py)

Von *[Alexandre B A Villares](https://abav.lugaralgum.com/README-EN)* gibt es ein nettes [GitHub-Repositorium mit Codeschnipseln](https://github.com/villares/py.processing-play) in [Processing.py](cp^processingpy), dem Python-Mode von [Processing (Java)](cp^Processing). Und dort findet Ihr auch gleich noch seine äußerst nützlichen [Tips for porting Processing Java code to Python mode](http://abav.lugaralgum.com/py.processing-play/java_to_python.html). Zwar ist Processing.py ziemlich kompatibel mit Processing (Java), aber manchmal gibt es doch Besonderheiten, die zu beachten sind.

Und wer mehr Hintergrund-Informationen zur Implementierung braucht, dem empfiehlt er (und empfehle auch ich) den Beitrag [Python, Jython and Java](https://py.processing.org/tutorials/python-jython-java/) von den »offiziellen« [Processing.py-Seiten](https://py.processing.org/tutorials/python-jython-java/). Dort erfahrt Ihr, was geht (fast alles) und was definitiv nicht geht (ein paar -- manchmal ärgerliche -- Einschränkungen).

Mußte ich einfach einmal hier im <del>Blog</del> Kritzelheft notieren, damit ich es wiederfinde, falls ich es brauche …
