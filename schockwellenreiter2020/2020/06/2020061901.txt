#title "Monsterplaylist zu Python und Pygame – 20200619"
#maintitle "Monsterplaylist zu Python und Pygame"
#prevp "2020061801"
#nextp "2020061902"

<iframe width="852" height="480" src="https://www.youtube.com/embed/dRFxGoLOUVQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Monsterplaylist zu Python und Pygame

Da es erst am Montag wieder sonnig in Berlin werden soll, habe ich für das Wochenende eine [Monsterplaylist zu Python und Pygame](https://www.youtube.com/watch?v=bY9qWjzkxFc&list=PL1H1sBF1VAKXh0GR1O94UUguIxkCP3dHM) herausgesucht, die Euch und mich in den Zeiten der Corona-Isolation beschäftigen kann. Es sind 20 Videos mit einer Gesamtlaufzeit von mehr als drei Stunden. Zusammen mit der [gestern vorgestellten](Pong, Python und Pygame – 20200618) Playlist könnt Ihr Euch mindestens vier Stunden die Zeit vertreiben. Zwar hat *John Hammond*, der Urheber dieser Playlist, diese schon vor sechs Jahren erstellt, aber zwischenzeitlich hat sich in [Pygame](cp^Pygame) eigentlich nichts geändert (außer daß die Distribution stabiler läuft und einfacher zu installeren ist -- daher könnt Ihr Euch das erste, zweiminütige Video zur Installation sparen und mit obigem Video beginnen).

Ich weiß, ich bin ein wenig sprunghaft, aber ich habe mir vorgenommen, in nächster Zeit mich wieder vermehrt mit Pygame zu beschäftigen, da ich einmal einige Befehle zum Zeichnen graphischer Primitive benötige, die in [Pygame Zero](cp^Pygame Zero) nicht implementiert sind (zum Beispiel Dreiecke und Polygone) und zum anderen ich die Klasse `Sprite` und die *sprite groups* vermisse. Denn damit kann man einiges anstellen, das mit Pygame Zeros `Actor` nur umständlich zu realisieren ist. *Still digging!*

<%= html.getLink(imageref("pygamebunt"), "cp^Pygame") %>