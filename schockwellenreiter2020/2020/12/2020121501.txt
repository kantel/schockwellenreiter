#title "Großer Flickentag bei Apple – 20201215"
#maintitle "Großer Flickentag bei Apple"
#prevp "2020121401"
#nextp "2020121502"

<%= html.getLink(imageref("dreikoenigsfest-b"), "Der König trinkt (1620)") %>

## Großer Flickentag bei Apple

Heute ist wohl der letzte große Patchday dieses Jahres bei der Firma mit dem angebissenen Apfel. Jedenfalls sind es so viele Updates, das ich sie wieder einmal nur *en bloc* abhandlen kann. Doch wie immer der Reihe nach:

### Apple veröffentlicht macOS Big Sur 11.1

Schon gestern Abend hat Apple mit der neuen Betriebssystemversion macOS Big Sur 11.1 das erste größere Update freigegeben und neben einiger neuer Funktionen

- die Unterstützung des Kopfhörers [Airpods Max](https://www.apple.com/de/airpods-max/)
- das neue Fotoformat Apple ProRAW 
- Erweiterung des Apple TV+ und des App Store
- neue Fensteroptionen für iPhone- und iPad-Apps auf den neuen Systemen mit M1-Chip

vor allem auch Fehler und [Sicherheitslücken](https://support.apple.com/de-de/HT212011) behoben.

Das Update steht über die Systemeinstellungen zur Verfügung.

### Apple veröffentlicht Sicherheitsupdate 2020-001 für Catalina und 2020-007 für Mojave

Apple hat mit dem Sicherheitsupdate 2020-001 für Catalina und dem Sicherheitsupdate 2020-007 für Mojave auch jene [Sicherheitslücken](https://support.apple.com/de-de/HT212011) geschlossen, die auch mit macOS Big Sur 11.1 behoben wurden.

Das Update steht wie üblich über die Softwareaktualisierung bereit.

### Apple veröffentlicht iOS 14.3 und iPadOS 14.3

Apple hat gestern Abend das Update auf [iOS 14.3](https://support.apple.com/de-de/HT211808#143) und [iPadOS](https://support.apple.com/de-de/HT211807#143) 14.3 bereitgestellt. Damit erfolgen nicht nur [Sicherheitskorrekturen](https://support.apple.com/de-de/HT212003), sondern unter anderem zunächst die Unterstützung der In der letzten Wochen vorgestellten Kopfhörers [Airpods Max](https://www.apple.com/de/airpods-max/), das neue Fotoformat Apple ProRAW und das derzeit hierzulande noch nicht verfügbare Sport-Streaming-Programm *Apple Fitness+*.

Das Update auf iOS/iPadOS 14.3 kann über OTA (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung` erfolgen, an ausreichend Akku-Ladung und freien Speicherplatz denken).

Eine vorherige Sicherung des Geräts ist wie immer sehr zu empfehlen.

### Apple veröffentlicht iOS 12.5 für ältere Geräte

Apple hat Mittwoch Abend auch iOS 12.5 für ältere Geräte veröffentlicht wie zum Beispiel iPhone 5s, iPhone 6, iPhone 6 Plus, iPad Air, iPad mini 2, iPad mini 3, und iPod touch 6. Generation und bietet damit im wesentlichen die gleichen [Sicherheitskorrekturen](https://support.apple.com/de-de/HT212004) wie in 14.3.

### Apple stellt watchOS 7.2 bereit

Und schließlich hat Apple für die Apple Watch [watchOS 7.2](https://support.apple.com/de-de/HT211815#72) bereitgestellt und  bringt im wesentlichen Fehlerbehebungen und Verbesserungen und auch wieder [Sicherheitskorrekturen](https://support.apple.com/de-de/HT212009).

Die Aktualisierung wird über die Apple-Watch-App auf dem iPhone gestartet unter `Allgemein > Softwareupdate`. Dabei sollte sich die Uhr in Reichweite des iPhones befinden, mit dem Ladekabel verbunden und mindestens zu 50 Prozent geladen sein.

*([[cert]])*