#title "Video-Tutorials zum Wochenende: Pymunk – 20201230"
#maintitle "Video-Tutorials zum Wochenende: Pymunk und Pygame"
#prevp "2020123001"
#nextp "2020123003"

<iframe width="852" height="480" src="https://www.youtube.com/embed/YrNpkuVIFdg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Video-Tutorials zum (verlängerten) Wochenende: Pymunk und Pygame

Da ja -- jedenfalls für die meisten von Euch da draußen -- wieder ein verlängertes Wochenende im Corona-Lockdown ansteht, ist für mich auch wieder [Mittwoch schon Freitag](Für die Feiertage: Learning Ren'Py – 20201223) und ich habe ein paar längere Video-Tutorials gegen die Lockdown-Langweile für mich und Euch zusammengestellt. Dieses Mal unter dem Oberbegriff [Pymunk](cp^Pymunk) und/mit [Pygame](cp^Pygame). Pymunk ist eine freie (MIT-Lizenz), benutzerfreundliche Python-2D-Physikbibliothek, die immer dann verwendet werden kann, wenn in einer Simulation mit Python 2D-Starrkörperphysik benötigt wird. 

Das heißt, Pymunk ist nicht mit Pygame verheiratet, sondern unabhängig und kann zusammen mit vielen anderen (Visualisierungs-) Tools in Python genutzt werden. Das erste Video »[Simulating physics in Python](https://www.youtube.com/watch?v=YrNpkuVIFdg)«, zeigt auch sehr anschaulich, wie Pymunks (Physik-) Welt unabhängig von Pygames Spielewelt berechnet wird, aber auch, wie man das Ergebnis dann in Pygame einbindet.

<iframe width="560" height="315" src="https://www.youtube.com/embed/cCiXqK9c18g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Das [zweite Video](https://www.youtube.com/watch?v=cCiXqK9c18g) gehört zur sechsteiligen Playlist »[Pymunk Basics](https://www.youtube.com/playlist?list=PL_N_kL9gRTm8lh7GxFHh3ym1RXi6I6c50)«, die der Programmierer *Ear Of Corn* hochgeladen hat. Da der letzte Upload dieser Playlist noch sehr jung ist, kann es durchaus sein, daß noch weitere Videos hochgeladen werden. Ich werde diese Playlist beobachten und Euch gegebenenfalls über Neuigkeiten unterrichten.

<iframe width="560" height="315" src="https://www.youtube.com/embed/yJK5J8a7NFs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Aus der gleichen Quelle und ebenfalls ziemlich aktuell ist dieses 45-minütige Video »[How to Make a Coronavirus Simulator on Python using Pygame/Pymunk](https://www.youtube.com/watch?v=yJK5J8a7NFs)«. Es ist ebenfalls Teil einer [Playlist](https://www.youtube.com/playlist?list=PL_N_kL9gRTm-Rz_n8sHsZwYcq6i5CVl3x), die aber bisher nur aus diesem einen Video besteht. Aber auch hier werde ich weiter beobachten.

*Ear of Corn* nutzt übrigens [Spyder](cp^Spyder) als Python-IDE, die ich durchaus auch schon einmal als [Alternative zum Jupyter-Notebook](Alternativen zum Jupyter Notebook – 20200507) empfohlen habe.

**War sonst noch was?** Ach ja, *Coding With Russ* hat zwischenzeitlich seine 13-teilige Playlist »[Pygame Plattformer Game Beginner Tutorial](https://www.youtube.com/playlist?list=PLjcN1EyupaQnHM1I9SmiXfbT6aG4ezUvu)« vollständig hochgeladen (siehe [hier](Video-Tutorials zur Spieleprogrammierung – 20201204), [hier](Video-Tutorial zu JavaScript – 20201211) und [hier](Coding Against Corona – 20201218)). Ich habe mir alle dreizehn Videos reingezogen und kann daher diese Tutorials reinen Gewissens Euch allen empfehlen. Sie sind wirklich gut!