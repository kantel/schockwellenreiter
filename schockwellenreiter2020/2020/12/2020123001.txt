#title "Spaß mit der Turtle: Die Peano-Kurve – 20201230"
#maintitle "Spaß mit der Turtle in Processing.py: Die Peano-Kurve"
#prevp "2020122901"
#nextp "2020123002"

## Noch mehr Spaß mit der Turtle in Processing.py: Die Peano-Kurve

Weil es so viel Spaß bereitet hat und weil es so einfach war, habe ich gleich noch eine zweite flächenfüllende Kurve mit der [Schildkröte](cp^Turtle) in [Processing.py](cp^processingpy) implementiert: Die [Peano-Kurve](https://www.spektrum.de/lexikon/mathematik/peano-kurve/7741), benannt nach dem Mathematiker [Giuseppe Peano](https://de.wikipedia.org/wiki/Giuseppe_Peano)[^1]. Auch sie besitzt als raumfüllende Kurve die [Haussdorf-Dimension 2](https://science.kairo.at/physics/fba_mandelbrot/kap5.html) und entspricht in ihrer Struktur [Cantors erstem Diagonalargument](https://de.wikipedia.org/wiki/Cantors_erstes_Diagonalargument#Verallgemeinerung_des_ersten_Cantorschen_Diagonalargumentes) (das heißt, die Kurve ist in ihrem Grenzwert »[abzählbar unendlich](https://de.wikipedia.org/wiki/Abz%C3%A4hlbare_Menge)«).

[^1]: Es gibt zwei raumfüllende Kurven, die als Peano-Kurven bezeichnet werden. Die hier behandelte wird in der [Wikipedia an zweiter Stelle](https://de.wikipedia.org/wiki/Peano-Kurve) aufgeführt.

<%= html.getLink(imageref("peanokurve-b"), "Die Peano-Kurve in Processing (Python)") %>

Doch das soll hier nicht zu einer Mathematik-Stunde ausarten (obwohl Mathematik natürlich Spaß macht), daher ganz schnell den Quellcode, der sich im Prinzip nur in der Funktion `peano()` von dem der [Drachenkurve](Die Drachenkurve in Processing (Python) – 20201228) unterscheidet:

~~~python
add_library('Turtle')
import math

num_gen = 3
ds = 1

def setup():
    global p
    size(400, 400)
    this.surface.setTitle("Peano-Kurve")
    background(50)
    strokeWeight(2)
    stroke(150, 255, 100)
    p = Turtle(this)
    p.right(90)
    noLoop()

def draw():
    p.penUp()
    p.goToPoint(25, 200)
    p.penDown()
    peano(p, num_gen, 300)
    print("I did it, Babe")
    
def peano(p, n, s):
    if n == 0:
        p.forward(s)
    else:
        peano(p, n-1, s/3)
        p.left(45)
        p.forward(ds*math.sqrt(2))
        p.left(45)
        peano(p, n-1, s/3)
        p.right(45)
        p.forward(ds*math.sqrt(2))
        p.right(45)
        peano(p, n-1, s/3)
        p.right(45)
        p.forward(ds*math.sqrt(2))
        p.right(45)
        peano(p, n-1, s/3)
        p.right(45)
        p.forward(ds*math.sqrt(2))
        p.right(45)
        peano(p, n-1, s/3)
        p.left(45)
        p.forward(ds*math.sqrt(2))
        p.left(45)
        peano(p, n-1, s/3)
        p.left(45)
        p.forward(ds*math.sqrt(2))
        p.left(45)
        peano(p, n-1, s/3)
        p.left(45)
        p.forward(ds*math.sqrt(2))
        p.left(45)
        peano(p, n-1, s/3)
        p.right(45)
        p.forward(ds*math.sqrt(2))
        p.right(45)
        peano(p, n-1, s/3)
~~~

Da diese Peano-Kurve so konstruiert ist, daß über dem Generator -- einer geraden Linie -- im zweiten Drittel je in der Seitenlänge gedritteltes Quadrate über und unter der Linie gezeichnet werden, hat die Schildkröte ganz gut zu tun und darum ist die Funktion `peano()` auch etwas lang geraten. Und die Zeile `ds = 1` scheint etwas seltsam, da *Eins multipliziert mit Irgendetwas* immer nur *Irgendetwas* ergibt, also nichts am Wert von *Irgendetwas* ändert. Sie macht aber dennoch Sinn, denn wenn man weitere Generationen der Kurve zeichnen lassen möchte und die Kurve noch in das Ausgabefenster passen soll, muß der Wert von `ds` verringert werden. So sollte man bei `num_gen = 4` den Wert `ds = 0.75` einsetzen und bei `num_gen = 5` sogar `ds = 0.25`. Die fünfte Generation unterscheidet sich kaum noch von einem vollständig gefüllten Karo und man muß schon einige Sekunden auf die Ausgabe im Zeichenfenster warten.