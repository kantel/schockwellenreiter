#title "Sierpinski-Kurve als L-System – 20201203"
#maintitle "Spaß mit Processing.py: Sierpinski-Dreieck als L-System"
#prevp "2020120202"
#nextp "2020120401"

## Spaß mit Processing.py: Das Sierpinski-Dreieck als L-System

Eine [Lindenmayer- oder L-System](https://de.wikipedia.org/wiki/Lindenmayer-System) -- entwickelt von dem ungarischen theoretischen Biologen [Aristid Lindenmayer](https://de.wikipedia.org/wiki/Aristid_Lindenmayer) -- ist eine auf [Noam Chomskys](https://de.wikipedia.org/wiki/Noam_Chomsky) Arbeiten zu fromalen Grammatiken beruhende formale Sprache, die Lindenmayer 1968 als Grundlage einer axiomatischen Theorie biologischer Entwicklung entwarf. Die L-Systeme werden sowohl in der Computergraphik bei der Erzeugung von Fraktalen wie auch in der realitätsnahen Modellierung von Pflanzen genutzt.

Ein *kontextfreies* L-System *(0L-System)* besteht aus einem Axiom und einer endlichen Anzahl von Produktionsregeln. Um zum Beispiel eine Ikone der fraktalen Geometrie, das [Sierpinki-Dreieck](https://de.wikipedia.org/wiki/Sierpinski-Dreieck) (Sierpinski-Kurve) mit einem L-System zu erzeugen, benötigt man ein Axion `G` und die Produktionsregeln oder Ersetzungsregeln:

~~~python
    'F': 'G-F-G',
    'G': 'F+G+F',
~~~

Bei jeder Iteration wird nun jedes `F` durch `G-F-G` und jedes `G` durch `F+G+F` ersetzt. Dabei können `F` und `G` als Zeichnen einer Strecke und `-` und `+` als Links- respektive als Rechtsdrehung interpretiert werden. Der Drehwinkel `delta` wird im Programm vorgegeben.

<%= html.getLink(imageref("lsystemsierpinski-b"), "Sierpinski-Kurve als L-System") %>

Im Programm wird die vollständige Zeichenkette des L-Systems zur Erzeugung des Sierpinksi-Dreiecks in der Funktion `generate()` erzeugt und das Dreieck wird mit der Funktion `plot()` gezeichnet:

~~~python
iterations = 7
stroke_len = 600
delta = radians(60)
axiom = 'G'
sentence = axiom
rules = {
    'F': 'G-F-G',
    'G': 'F+G+F',
}

def setup():
    global x0, y0
    size(700, 700)
    this.surface.setTitle("Sierpinski-Kurve (mittels L-System)")
    colorMode(HSB, 360, 100, 100)
    x0, y0 = 340, height - 210
    strokeWeight(2)
    noFill()
    generate(iterations)
    noLoop()
 
def draw():
    background(0)
    rotate(radians(30))
    translate(x0, y0)
    plot(delta)
 
def generate(n):
    global stroke_len, sentence
    for i in range(n):
        stroke_len *= 0.5
        next_sentence = ''
        for c in sentence:
            next_sentence += rules.get(c, c)
        sentence = next_sentence

def plot(angle):
    k = 0
    for c in sentence:
        if c == 'F' or c == 'G':
            stroke(int(k%360), 50, 100)
            line(0, 0, 0, -stroke_len)
            translate(0, -stroke_len)
            k += 0.25
        elif c == '+':
            rotate(angle)
        elif c == '-':
            rotate(-angle)
~~~

Um das alles mit ein wenig Farbe aufzuhübschen, habe ich den HSB-Farbraum gewählt, da man dort anschaulicher den Farbkreis durchwandern kann.

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=3893196331&asins=3893196331&linkId=a41c2bdefe2e4250ec55bc7526de80aa&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Die Aufteilung des Programms in `setup()` und `draw()` ist rein artifiziell, da durch `noLoop()` keine wiederholten Durchläufe stattfinden. Man hätte also auch auf die Aufteilung in `setup()` und `draw()` verzichten und alles in einem Durchlauf sequentiell abarbeiten können. Ich finde aber, daß die Aufteilung in einzelne Funktionen den Sketch übersichtlicher gestaltet.

Das L-System für das Sierpinski-Dreieck habe ich *Dieter Hermanns* Buch [Algorithmen für Chaos und Fraktale][a1], Bonn (Addison Wesley) 1994, Seite 203 entnommen und der Processing.py-Sketch wurde durch einen [ähnlichen Sketch von Rosetta Code](https://rosettacode.org/wiki/Hilbert_curve#Processing_Python_mode) inspiriert.<br clear="all" />

[a1]: https://www.amazon.de/Algorithmen-Chaos-Fraktale-Dietmar-Herrmann/dp/3893196331/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Algorithmen+f%C3%BCr+Chaos+und+Fraktale&qid=1607016172&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=6103912e275bcb8580bd448b7686d8a6&language=de_DE