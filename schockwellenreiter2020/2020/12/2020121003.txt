#title "Künstliche Intelligenz in JavaScript – 20201210"
#maintitle "Künstliche Intelligenz in JavaScript (P5.js und ml5.js)"
#prevp "2020121002"
#nextp "2020121101"

<iframe width="852" height="480" src="https://www.youtube.com/embed/ABN_DWnM5GQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Künstliche Intelligenz in JavaScript (mit P5.js und ml5.js)

In diesem Video zeigt *Daniel Shiffman*, wie er die JavaScript-Version von [Processing](cp^processing), [P5.js](cp^p5js), zusammen mit [ml5.js](https://ml5js.org/), der »freundlichen« JavaScript-Bibliothek für maschinelles Lernen, dafür nutzt, Bilder zu erkennen. Dafür nutzt er DoodleNet, ein [vortrainiertes Modell](https://learn.ml5js.org/#/reference/image-classifier?id=doodlenet-model-biography), das Teil der ml5.js-Distribution ist. Die Trainingsdaten für dieses Modell basieren auf Googles [QuickDraw Dataset](https://quickdraw.withgoogle.com/data) und es ist zur Zeit auf 345 Objekte trainiert. Einen ersten Versuch mit DoodleNet hatte *Shiffman* schon vor zwei Jahren [hochgeladen](https://www.youtube.com/watch?v=yLuk0twx8Hc).

<iframe width="560" height="315" src="https://www.youtube.com/embed/3MqJzMvHE3E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Das Video hat nicht nur einen Vorläufer, die [Coding Challenge #158](https://www.youtube.com/watch?v=3MqJzMvHE3E), in der *Daniel Shiffman* zeigt, wie man selber in ml5.js einen Datensatz trainiert, sondern ist auch Teil einer mittlerweile auf 30 Videos angewachsenen Playlist »[Beginners Guide to Machine Learning in JavaScript](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6YPSwT06y_AEYTqIwbeam3y)«. Ich glaube, es wird immer dringender, daß ich mich mehr mit JavaScript und P5.js befasse. *Still digging!*

