#title "Xmas-Songs, Part 8: All I Want For Christmas – 20201224"
#maintitle "Xmas-Songs in Dixie-Town, Part 8: All I Want For Christmas"
#prevp "2020122303"
#nextp "2020122402"

<iframe width="852" height="480" src="https://www.youtube.com/embed/YYpUtQ1MH2k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Xmas-Songs in Dixie-Town, Part 8: All I Want For Christmas

Die Zielgerade zum Weihnachtsfest ist nur noch wenige Meter entfernt, doch noch ist es Zeit, sich etwas zu wünschen: *[Robyn Adele Anderson](https://en.wikipedia.org/wiki/Robyn_Adele_Anderson)* hat sich *[Von Smith](https://en.wikipedia.org/wiki/Von_Smith)* geschnappt und was sich die beiden wünschen, besingen sie in diesem jazzigen *Mariah Carey*-Cover: »[All I Want for Christmas Is You](https://de.wikipedia.org/wiki/All_I_Want_for_Christmas_Is_You)«. Möge ihr frommer Wunsch in Erfüllung gehen.

Neben der Sängerin und dem Sänger sind die übrigen Musiker *Logan Evan Thomas* (Piano), *Arthur Vint* (Drums), *Georgia Weber* (Bass, [immer noch](Xmas-Songs, Part 6: Santa Baby – 20201222) ein Fall für meine Miniserie »Frauen am Kontrabaß«), *Ben Golder-Novick*, (Tenor Sax) und *Peter Lin* (Posaune).

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>als Jazz-Fan bin ich immer ganz hin und weg ob der Xmas-Sangs die du so
findest.    
Danke dafür    
Wenn man sich die Damen und Herren so anhört wird noch deutlicher als
sonst eh schon wie gross die Unterschiede zum üblichen Musikgedudel
sind. Ok, da gibt's auch Nettes aber trotzdem.....    
Angenehme Feiertage

*– Ulrich W.* <%= p("k01") %>

