#title "Geographical Data Visualization in Jupyter – 20200731"
#maintitle "Geographical Data Visualization in Jupyter Notebook"
#prevp "2020073101"
#nextp "2020073103"

<iframe width="852" height="480" src="https://www.youtube.com/embed/PuJ_JUkahXQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Geographical Data Visualization in Jupyter Notebook

Dieses Wochenende soll ja warm und sonnig werden, daher habe ich heute nur ein kurzes Video-Tutorial herausgesucht, mit dem Ihr Euch beschäftigen könnt, falls es zu warm werden sollte: »[Geographical Data Visualization in Jupyter Notebook](https://www.youtube.com/watch?v=PuJ_JUkahXQ)«. Das Video nutzt [ipyleaflet](https://ipyleaflet.readthedocs.io/en/latest/#), eine [Jupyter](cp^Jupyter)-Bridge für [Leaflet](cp^Leaflet) (BSD-Lizenz), der HTML5- und JavaScript Abstraktionsschicht zu diversen (auch freien) Kartenanbietern, wie zum Beispiel [OpenStreetMap](cp^openstreetmap).

Sollte mich der Corona-Isolations-Blues überkommen, werde ich nach dem Genuß dieses Videos mal testen, ob ipyleaflet auch mit [nteract](cp^nteract) spielt. Glaube ich zwar nicht, aber *still digging!*