#title "Schau Mama, ein Hundebild! – 20200724"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020072401"
#nextp "2020072501"

<%= html.getLink(imageref("titustavernaargo-b"), "Titus auf der Terrasse der Taverna Argo") %>

**Schau Mama, ein Hundebild!** Schon wieder ist es Freitag und natürlich gibt es wieder ein Hundebild im *Schockwellenreiter*. Es entstand letzten Sonntag und war unser erster Versuch, mit der kleinen Fellnase ein Restaurant aufzusuchen. Ganz haben wir dem Frieden aber noch nicht getraut und haben ein Restaurant mit einer Terrasse besucht. Aber es lief alles relativ stressfrei ab.

Ja, und das Hundebild gibt es natürlich, weil es Euch darüber hinwegtrösten soll, daß es die nächsten zwei oder gar drei Tage keine oder nur wenige Updates hier im <del>Blog</del> Kritzelheft geben wird. Mein Programm habe ich ja schon im [letzten Beitrag](Mein Wochenendprojekt: Twine und Node Webkit – 20200724) skizziert und dann will und soll der kleine Spitz natürlich auch noch bespaßt werden. So viele Pläne und so wenig Zeit. Das ist das Rentnerdasein …

Es soll bis Sonntag erst einmal wieder freundlicher und wärmer werden, sagen die Wetterfrösche. Genießt dies. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Dienstag wieder. *[[photogabi]]*