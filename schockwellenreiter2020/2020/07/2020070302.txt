#title "Schau Mama, ein Hundebild! – 20200703"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020070301"
#nextp "2020070401"

<%= html.getLink(imageref("spitzbahnhof-b"), "Der Spitz am Bahnhof Spindlersfeld") %>

**Schau Mama, ein Hundebild!** Es ist wieder Freitag und so gibt es wieder ein Hundebild im *Schockwellenreiter*. Wie gewohnt soll es Euch darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im <del>Blog</del> Kritzelheft geben wird. Ich möchte mich nämlich weiter mit der [Python Arcade Bibliothek](cp^Arcade) auseinandersetzen und natürlich will der kleine Spitz auch bespaßt werden.

Die Wetterfrösche versprechen für Berlin nämlich angenehmes, nicht zu heißes Wetter, so daß ich vermutlich die Gelegenheit nutzen werde und den kleinen weißen Wirbelwind ein wenig auszuführen. Zwar hat die kaputtgesparte Berliner S-Bahn Neukölln für die nächsten Wochen abgehängt (Schande über sie!), aber zu Fuß kann man ja auch noch einiges anstellen. Macht es uns doch einfach nach.

Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*