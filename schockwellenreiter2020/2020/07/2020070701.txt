#title "Mit der Couch in die Cloud – 20200707"
#maintitle "Mit der Couch in die Cloud (und eine Flickr-Alternative)"
#prevp "2020070501"
#nextp "2020070702"

<%= html.getLink(imageref("cloudcouch-b"), "Die Couch für die Cloud") %>

## Mit der Couch in die Cloud (und eine [[flickr]]-Alternative)

[Couchbase](cp^Couchbase), die freie (Apache-Lizenz), plattformübergreifende und dokumentenorientierte NoSQL-Datenbank mit der von [CouchDB](cp^CouchDB) abgeleiteten JSON-basierten *Document Store* geht in die Cloud. Per Email kündigten mir die Macher [Couchbase Cloud](https://www.couchbase.com/products/cloud) an, ein komplett verwaltetes *Database-as-a-Service* (DBaaS) System. Für 30 Tage könnt Ihr das Teil kostenlos testen, danach kostet es Geld und zwar als Entwickler mindestens $ 0,31/hr per Knoten.

Aber natürlich könnt Ihr die Software weiterhin [kostenlos herunterladen](https://www.couchbase.com/downloads) und auf Euren eigenen Servern installieren.  Für die Wartung und die Sicherheits-Updates seid Ihr dann allerdings selber zuständig.

Ich bin in diesem Falle dann allerdings eher für CouchDB, auch wenn der Hosting Service [Iris Couch](cp^Iris Couch) eingeschlafen scheint. Aber da bei meiner Idee, mit der CouchDB [ein neues Frontier zu bauen](http://blog.schockwellenreiter.de/essays/frontiercouch.html), die Software sowieso eher Peer-to-Peer auf einem mit dem Netz verbundenen Desktop, denn auf einem Server läuft, benötige ich keinen Hosting-Service. Doch für diejenigen, die einen dokumentenorientierte Datenbank schmerzlos auf einem Server nutzen möchten, ist die Cochbase Cloud sicher eine schmerzfreie (mal von den Kosten abgesehen) Alternative.

**Doch wo ich gerade in der Cloud bin**: Mehr als die Frage nach einer Datenbank quält mich ja seit mindestens fünf Jahren die Überlegung, was tun, [wenn Flickr stirbt](http://blog.schockwellenreiter.de/2015/10/2015102001.html)? Hier wurde mir vor ein paar Tagen die Open-Source-Photo-Galerie-Software [Piwigo](https://de.piwigo.org/) empfohlen. Sie steht unter der GPL und den Voraussetzungen nach sollte sie auch auf meinem Mac unter [MAMP](cp^MAMP) laufen (sie benötigt in der Grundversion nur PHP und MySQL). Sie ist mit vielen Themen und Plugins erweiter- und konfigurierbar und sie soll auch mit hunderttausenden von Photos zurechtkommen, also sollten unsere (bisher) etwas über 30.000 Photo auf [[flickr]], die sich in den letzten 15 Jahren angesammelt hatten, kein Problem sein.

Auch Piwigo gibt es als [Service](https://piwigo.com/) und die [Kosten für einen privaten Account](https://piwigo.com/pricing) liegen unter dem, was ich bisher für meinen [[flickr]]-Pro-Account zahle. Die Firma existiert seit 18 Jahren und betreibt auch [ein Blog](https://piwigo.com/blog/), das natürlich sofort in meinem Feedreader gelandet ist.

Bevor ich mir die Mühe mache und ein Import-Skript schreibe, würde ich gerne wissen, ob von Euch da draußen schon Erfahrungen mit dem Service existieren? Über Kommentare hier oder in den sozialen Medien wäre ich dankbar. *[[photojoerg]]*
