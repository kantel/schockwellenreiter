#title "P5js und Visual Studio Code – 20200827"
#maintitle "P5.js und Visual Studio Code"
#prevp "2020082602"
#nextp "2020082702"

<iframe width="852" height="480" src="https://www.youtube.com/embed/vj9nDja8ZdQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Worknote: P5.js und Visual Studio Code

Regelmäßige Leser dieses <del>Blogs</del> Kritzelheftes erinnern sich vielleicht daran, daß ich schon mehrmals Anläufe unternommen habe, mit [P5.js](cp^p5js), dem JavaScript-Mode von [Processing](cp^Processing), zu spielen. Doch irgendwie war das immer zu unbequem: Ich bin immer noch kein Fan von webbasierten Anwendungen und daher bin ich -- trotz meiner [anfänglichen Begeisterung](http://blog.schockwellenreiter.de/2018/09/2018090702.html) -- auch mit dem [P5.js-Webeditor](https://editor.p5js.org/) nie wirklich warmgeworden, meine Versuche P5.js mit meinem Leib- und Magen-Editor [TextMate 2](cp^TextMate) und [RubyFrontier](cp^RubyFrontier) zu [verheiraten](http://blog.schockwellenreiter.de/2015/11/2015113001.html) oder gar [RubyFrontier als P5.js-IDE](http://blog.schockwellenreiter.de/2016/06/2016060105.html) zu nutzen, waren mehr ein *Proof of Concept* denn eine reale Alternative. Und auch meine Experimente mit [P5.js und  MacDown](http://blog.schockwellenreiter.de/2018/09/2018092102.html) waren eher Fingerübung. Sicher, alles geht, aber elegant ist anders.

[[dr]]<%= html.getLink(imageref("p5"), "cp^p5js") %>[[dd]]

Nun aber scheint *Tim Rodenbröker* eine [Lösung gefunden zu haben](https://timrodenbroeker.de/how-to-use-p5-js-with-visual-studio-code/), wie man P5.js in [Visual Studio Code](cp^Visual Studio Code) einfach und elegant nutzen kann. Er nutzt dazu die [p5.vscode Extention](https://marketplace.visualstudio.com/items?itemName=samplavigne.p5-vscode) von *[Sam Lavigne](https://lav.io/)*. Diese erzeugt nicht nur mit nur einem Mausklick ein vollständiges P5.js-Projekt, sondern erlaubt auch das einfache Einbinden von externen P5.js-Modulen, die dann auch gleichzeitig und automatisch in die `index.html` eingebunden werden. Und das Teil implementiert auch einen Live-Server, so daß man den erstellten Sketch ganz einfach aus VS Code heraus mit einem Klick starten und testen kann.

Auch wenn ich bisher mit VS Code noch nicht so richtig warm geworden bin (die Bedienung unterscheidet sich doch deutlich von TextMate 2), das Teil scheint definitiv einen Test wert zu sein. Vielleicht werden dann P5.js und ich doch noch Freunde. *Still digging!*