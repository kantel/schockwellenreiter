#title "Schau Mama, ein Hundebild! – 20200807"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020080701"
#nextp "2020080801"

<%= html.getLink(imageref("spitzankunst-b"), "Spitz an Kunst") %>

**Schau Mama, ein Hundebild!** Es ist Freitag, es ist sonnig und warm und es soll die nächsten Tage noch wärmer und sonniger werden. Daher gibt es heute wieder das Hundebild, das Euch darüber hinwegtrösten soll, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Denn bei den sommerlichen Aussichten werde ich die Zeit mehr mit Dösen und im Garten im Schatten liegen verbringen und sie kaum dazu nutzen, das Internet vollzuschreiben. Auch der kleine Spitz wird sich meinen (Nicht-) Aktivitäten (hoffentlich) anschließen.

Daher: Ein schönes Wochenende Euch allen da draußen. Wir lesen uns voraussichtlich am Montag oder Dienstag wieder. *[[photogabi]]*