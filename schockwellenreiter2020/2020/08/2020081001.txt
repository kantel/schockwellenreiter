#title "Zur Causa Lisa Eckhart – 20200810"
#maintitle "Zur Causa Lisa Eckhart: Rätselhafte Femme fatale"
#prevp "2020080801"
#nextp "2020081002"

<%= html.getLink(imageref("girlswithguns-b"), "Girls with Guns") %>

## Zur Causa Lisa Eckhart: Rätselhafte Femme fatale

Ich habe [Lisa Eckhart](https://de.wikipedia.org/wiki/Lisa_Eckhart) einige Male im TV gesehen (unter anderem auch den berühmt-berüchtigten Mitternachtsspitzen-Auftritt) und fand sie verstörend (oder soll ich sagen zerstörend) genial. Aber um das zu erkennen, muß man auch zwischen den Zeilen hören können. Und diese Fähigkeit ist anscheinend im Umfeld der selbsternannten Hamburger Antifa abhanden gekommen. Aber, um – [wie die Jüdische Allgemeine](https://www.juedische-allgemeine.de/kultur/raetselhafte-femme-fatale/) – Karl Kraus zu zitieren: *»Satiren, die der Zensor versteht, werden mit Recht verboten.«* Daher hat Lisa Eckhart alles richtig gemacht.

Aber daß jetzt ausgerechnet Dieter Nuhr sie verteidigt, diese Schmach hat sie nicht verdient.