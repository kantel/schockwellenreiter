#title "Objekterkennung mit COCO-SSD – 20201021"
#maintitle "Objekterkennung mit P5.js, ml5.js und COCO-SSD"
#prevp "2020102102"
#nextp "2020102201"

<iframe width="852" height="480" src="https://www.youtube.com/embed/QEzRxnuaZCk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Künstliche Intelligenz und JavaScript: Objekterkennung mit P5.js, ml5.js und COCO-SSD

Das auch JavaScript mittlerweile ein durchaus ernstzunehmendes Werkzeug für die Künstliche Intelligenz (was immer man darunter auch verstehen mag) ist, zeigt Euch *Daniel Shiffman* in [diesem Video](https://www.youtube.com/watch?v=QEzRxnuaZCk). Er benutzt für seine Experimente zur Objekterkennung die »freundliche« Bibliothek für maschinelles Lernen [ml5.js](https://ml5js.org/), das vortrainierte Dataset [COCO-SSD](https://github.com/ml5js/ml5-library/blob/development/docs/reference/object-detector.md#coco-ssd---model-biography) und -- natürlich -- [P5.js](cp^p5js), den JavaScript-Mode von [Processing](cp^Processing). Natürlich steigt dieses viertelstündige Video nicht tief in die Materie ein, aber es ist durchaus vergnüglich und die Ergebnisse können sich sehen lassen.

Wer tiefer einsteigen will, dem empfiehlt *Shiffman* folgende Links:

- [Object Detector ml5.js Reference](https://github.com/ml5js/ml5-library/blob/development/docs/reference/object-detector.md)
- [COCO ml5.js Model and Data Provenance](https://github.com/ml5js/ml5-library/blob/development/docs/reference/object-detector.md#model-and-data-provenance)
- [COCO (Common Objects in Context) Dataset Explore](https://cocodataset.org/#explore)
- [COCO 2020 Keypoint Detection Task](https://cocodataset.org/index.htm#keypoints-2020)
- [The COCO-Stuff dataset](https://github.com/nightrome/cocostuff)
- [Humans of AI (Editorial Essay)](https://humans-of.ai/editorial/) von *Philipp Schmitt*
- [Excavating AI](https://www.excavating.ai/) von *Kate Crawford* und *Trevor Pageln*
- [Object Detection (coco-ssd) Model on tensorflow.js Models GitHub Repository](https://github.com/tensorflow/tfjs-models/tree/master/coco-ssd)
- [List of Classes Available in COCO Dataset (ml5.js)](https://github.com/ml5js/ml5-library/blob/development/src/utils/COCO_CLASSES.js)

*Shiffmans* Bilder von Katzen und Hunden findet Ihr auf [Instagram](https://www.instagram.com/mangoandgoose/)😻😻🐶. Und ich sollte endlich mein Vorhaben wahrmachen und mit der [hier getesteten VS Code Umgebung](P5js und Visual Studio Code (2) – 20201006) tiefer in P5.js und JavaScript einsteigen. Denn da werde ich bestimmt nicht dümmer von. Dafür gibt es im YouTube-Kanal von *Dane Webster* eine Playlist mit (bisher!) 16 Videos: [p5js_Creative_Code](https://www.youtube.com/playlist?list=PLUh-iOxl55Nquy1xRgQydmpcnswJINJcs). *Still digging!*