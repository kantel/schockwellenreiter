#title "Ergänzungsupdate für Mojave – 20201002"
#maintitle "Apple veröffentlicht ein Ergänzungsupdate für Mojave wegen der Probleme mit dem Sicherheitsupdate 2020-005"
#prevp "2020100102"
#nextp "2020100202"

<%= html.getLink(imageref("feldbotanki1-b"), "Feld-Botanik (1)") %>

## Apple veröffentlicht ein Ergänzungsupdate für Mojave wegen der Probleme mit dem Sicherheitsupdate 2020-005

Die unendliche Geschichte: Apple hat nun ein Ergänzungsupdate für macOS Mojave veröffentlicht, um die Probleme mit dem Sicherheitsupdate 2020-005 (wir [berichteten](Probleme mit dem Update für Mojave – 20200929) [mehrfach](Weiter Probleme mit dem Update für Mojave – 20201001)) zu beheben.

Das Update steht wie üblich über die Softwareaktualisierung bereit. *([[cert]])*

Hoffen wir, daß damit das peinliche Hickhack behoben ist. Aber ich glaube noch nicht wirklich daran. 

*[[photojoerg]]*