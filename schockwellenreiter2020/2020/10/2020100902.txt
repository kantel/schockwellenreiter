#title "Schau Mama, ein Hundebild! – 20201009"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020100901"
#nextp "2020101001"

<%= html.getLink(imageref("tituste2-b"), "Einen Platz mit Abstand ergattert") %>

**Schau Mama, ein Hundebild!** Langsam wird es zu einer lästigen Angewohnheit. Wie schon in den Vorwochen veröffentliche ich das obligatorische Hundebild zum Wochenende erst auf den letzten Drücker. Dieses Mal habe ich jedoch eine Ausrede: [[flickr]] ist down (vergleiche [diesen Beitrag](From Here to Ipernity? – 20200928)) und so habe ich nach langem Warten, daß der böse Panda den Dienst wieder freigibt, ein Photo aus dem Gesichtsbuch entnommen. Wie jedesmal soll Euch das Bild der kleinen Fellnase darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird.

Dabei habe ich nicht viel vor. Ich möchte mich ein wenig mit [Twine](cp^Twine 2) und ein wenig mit [Processing.py](cp^processingpy) beschäftigen. Und natürlich den kleinen Spitz bespaßen. Daher sollte eigentlich Zeit sein, auch ein wenig das Internet vollzuschreiben. Vielleicht passiert das auch, [angedroht](Mit Julia 👩‍🦰 eine Pandemie modellieren – 20201009) habe ich es ja bereits.

Außerdem scheint der Herbst jetzt richtig zuzuschlagen. Es ist wolkig, grau und kalt und es soll auch die nächsten zwei Tage so bleiben. Wohl dem, der sich -- wie ich -- mit obskuren Programmiersprachen herumschlagen kann.

Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*
