#title "Flickentag bei Apple, Google und Oracle – 20201021"
#maintitle "Flickentag bei Apple, Google und Oracle"
#prevp "2020102001"
#nextp "2020102102"

<%= html.getLink(imageref("bulandtinker-b"), "Jean-Eugène Buland: Le Rétameur (1908)") %>

## Flickentag bei Apple, Google und Oracle

Gestern war Patchday im *Silicon Valley* bei Apple, Google und Oracle, die in ihren Produkten auch wieder etliche Sicherheitslücken stopften. Doch wie immer der Reihe nach:

### Oracle behebt wieder kritische Sicherheitslücken in Java

Im Rahmen seines turnusmäßigen [Critical Patch Updates](https://www.oracle.com/security-alerts/cpuoct2020.html) schließt Oracle in den neuen Java Versionen wieder zahlreiche teils kritische [Sicherheitslücken](https://www.oracle.com/technetwork/java/javase/overview/index.html).

Wer also Java auf seinem System einsetzt, sollte diese neue Version installieren. Sie kann über die eingebaute Update-Funktion oder wie immer auch [hier bezogen](http://www.oracle.com/technetwork/java/javase/downloads/index.html) werden.

### Apple veröffentlicht iOS 14.1 und iPadOS 14.1

Apple hat gestern Abend das Update auf [iOS 14.1](https://support.apple.com/de-de/HT211808#141) und [iPadOS 14.1](https://support.apple.com/de-de/HT211807#141) bereitgestellt. Damit soll neben den üblichen Fehlerbehebungen und [Sicherheitskorrekturen](https://support.apple.com/en-us/HT201222) auch die Kompatibilität mit dem neu veröffentlichten iPhone 12 und iPad Air der 4. Generation hergestellt werden.

Das Update auf iOS/iPadOS 14.1 kann über OTA erfolgen (*Over the Air* -- in 
`Einstellungen > Allgemein > Softwareaktualisierung`, an ausreichend Akku-Ladung und freien Speicherplatz denken). Eine vorherige Sicherung des Geräts ist wie immer sehr zu empfehlen.

### Google behebt mit der neuen Version seines Browsers Chrome auch wieder Sicherheitslücken

Google schließt mit der neuen Version (86.0.4240.111) seines Browsers Chrome auch wieder [Sicherheitslücken](https://chromereleases.googleblog.com/2020/10/stable-channel-update-for-desktop_20.html). 

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden.

*([[cert]])*