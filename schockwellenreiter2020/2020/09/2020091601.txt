#title "Entwarnung: Arcade funzt wieder – 20200916"
#maintitle "Entwarnung: Die Arcade-Bibliothek funzt wieder"
#prevp "2020091502"
#nextp "2020091701"

<%= html.getLink(imageref("arcadefunztwieder"), "https://www.flickr.com/photos/schockwellenreiter/50347874928/") %>

## Entwarnung: Die Arcade-Bibliothek funzt wieder

Ich bin nach dem [aufmunternden Kommentar](Arcade: Update zerschießt Installation – 20200915#k01) von *Karsten Wolf* meinem gestrigen [Problem mit der Arcade-Bibliothek](Arcade: Update zerschießt Installation – 20200915) noch einmal nachgegangen. Zwar erben -- entgegen Karstens Vermutung -- alle meine Klassen `MyGame()` von `arcade.Window`, aber ich habe das probiert, was man immer wieder bei Programmversagen probieren sollte: Neuinstallation! Mit

~~~bash
pip install -Iv arcade==2.4.2
~~~

habe ich eine komplette Neuinstallation von [Arcade](cp^Arcade) erzwungen. Und danach liefen alle meine Arcade-Programme wieder, wie sie sollten: Fehlerfrei!

Ich weiß nicht, was die Fehler gestern produziert hat. Vielleicht war einfach `arcade.Window` bei der Installation zerstört worden? Auf jeden Fall bin ich froh, daß Arcade wieder funktioniert. Denn es war eigentlich ein schönes Projekt, die Beispiele aus »The Nature of Code« von [Processing (Java)](cp^Processing) nach Python und Arcade zu portieren. Jetzt kann ich damit fortfahren. *Still digging!*