#title "Probleme mit dem Update für Mojave – 20200929"
#maintitle "Probleme mit dem Sicherheitsupdate 2020-005 für Mojave"
#prevp "2020092801"
#nextp "2020092902"

<%= html.getLink(imageref("aroundtheworld-b"), "André Henri Dargelas: Around the World (um 1860)") %>

## Probleme mit dem Sicherheitsupdate 2020-005 für Mojave

Apple hat mit dem Sicherheitsupdate 2020-005 für Mojave offenbar größere Probleme, so daß ein Update derzeit nicht als empfehlenswert gilt.

Apple sucht intensiv nach den Ursachen, hat aber bislang noch kein Ergebnis. *([[cert]])*

Wie gut, daß ich mit dem Einfahren neuer Updates immer erst ein paar Tage warte.&nbsp;🤓