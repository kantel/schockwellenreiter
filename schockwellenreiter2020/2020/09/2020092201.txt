#title "Browser-Updates – 20200922"
#maintitle "Browser-Updates – Chrome und Firefox"
#prevp "2020091901"
#nextp "2020092202"

<%= html.getLink(imageref("freischwingercwp-b"), "Freischwinger im Carl-Weder-Park") %>

## Browser-Updates – Chrome und Firefox

Über das Wochenende brachten die beiden großen Browser-Hersteller Google und Mozilla neue Versionen ihrer Flagschiffe Chrome und Firefox unters Volk. Doch wie immer der Reihe nach:

### Google behebt mit der neuen Version seines Browsers Chrome auch wieder Sicherheitslücken

Google schließt mit der neuen Version (85.0.4183.121) seines Browsers Chrome wieder [Sicherheitslücken](https://chromereleases.googleblog.com/2020/09/stable-channel-update-for-desktop_21.html).

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden.

### Mozilla veröffentlicht neue Firefox-Versionen

Die Entwickler des Mozilla Firefox  haben jetzt die neue [Version 81](https://www.mozilla.org/en-US/firefox/81.0/releasenotes/) und die [Version ESR 78.3.0](https://www.mozilla.org/en-US/firefox/78.3.0/releasenotes/) veröffentlicht und darin auch wieder Sicherheitslücken behoben.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*

*[[photojoerg]]*