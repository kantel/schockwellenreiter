#title "Update für Office for Mac – 20201918"
#maintitle "Microsoft veröffentlicht Update 16.41.0 für Office for Mac"
#prevp "2020091701"
#nextp "2020091802"

<%= html.getLink(imageref("spaetsommerlinda-b"), "Spätsommerliche Linda") %>

## Microsoft veröffentlicht Update 16.41.0 für Office for Mac

Microsoft liefert mit dem Update auf [16.16.26 für Office 2016](https://docs.microsoft.com/en-us/officeupdates/release-notes-office-2016-mac) und auf [16.41.0 für Office 365 for Mac](https://docs.microsoft.com/en-us/officeupdates/release-notes-office-for-mac) natürlich auch aktuelle Sicherheitskorrekturen. 

Sofern Office nicht schon von sich aus darauf hinweist, erhält man die Korrekturen am einfachsten über Microsofts AutoUpdate - in einem beliebigen Office-Programm über Menü: `Hilfe > Auf Updates überprüfen`. *([[cert]])*

*[[photojoerg]]*