#title "Sicherheitsupdate des Foxit Readers – 20200929"
#maintitle "Sicherheitsupdate des Foxit Readers (PDF Reader) auf 10.1"
#prevp "2020092902"
#nextp "2020093001"

<%= html.getLink(imageref("ascientist-b"), "Ernst Karl Georg Zimmermann: Die Chemiker") %>

## Sicherheitsupdate des Foxit Readers (PDF Reader) auf 10.1

Mit der Version 10.1 des unter Windows beliebten PDF Betrachters Foxit Reader haben die Entwickler [kritische Schwachstellen](https://www.foxitsoftware.com/support/security-bulletins.html) behoben.

Die neue Version kann wie immer [hier geladen](https://www.foxitsoftware.com/products/pdf-reader/) werden. *([[cert]])*