#title "Data Science Using Functional Python – 20200901"
#maintitle "Learning Data Science Using Functional Python"
#prevp "2020090101"
#nextp "2020090201"

<iframe width="852" height="480" src="https://www.youtube.com/embed/ThS4juptJjQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Learning Data Science Using Functional Python

*Joel Grus* der Autor des wunderbaren Buches »[Data Science from Scratch][a1]« (deutsche Übersetzung: »[Einführung in Data Science][a2]«) hat auf der PyData 2015 in Seattle einen Vortrag gehalten »Learning Data Science Using Functional Python«. Eine absolute Empfehlung, denn denkt immer daran: Wer nicht funktional programmiert, programmiert disfunktional!&nbsp;🤓

[a1]: https://www.amazon.de/Joel-Grus/dp/1492041130/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3EPTR8N2IL326&dchild=1&keywords=data+science+from+scratch&qid=1598960297&sprefix=Data+Science,aps,170&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=c0a7c8b906f936c9148200a044beb52b&language=de_DE
[a2]: https://www.amazon.de/Einf%C3%BChrung-Data-Science-Grundprinzipien-Datenanalyse/dp/3960091230/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=P8WLB4U6NUFD&dchild=1&keywords=einf%C3%BChrung+in+data+science&qid=1598960403&sprefix=Einf%C3%BChrung+in+Data,aps,159&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=254aa8ec265ae44d6e497d6c9d05314a&language=de_DE

Die [Beispielprogramme](https://github.com/joelgrus/stupid-itertools-tricks-pydata) gibt es auf GitHub unter dem Titel »Stupid Itertool Tricks for Data Science« und die [Vortragsfolien](https://docs.google.com/presentation/d/1eI60SL3UxtWfr9ktrv48-pcIkk4S7JiDmeXGCyyGhCs/edit#slide=id.p) bei Google Docs.