#title "Schau Mama, ein Hundebild! – 20201113"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020111302"
#nextp "2020111304"

<%= html.getLink(imageref("herbstspitz-b"), "Neuköllner Herbstspitz") %>

**Schau Mama, ein Hundebild!** Ich will nicht [schon wieder](Schau Mama, ein Hundebild! – 20201106) darüber lamentieren, daß für einen Rentner wie mich die Woche viel zu schnell vorüberzieht. Aber es ist so! Da habe ich mir -- besonders programmiertechnisch und in der Beschäftigung mit [Twine](cp^Twine 2) -- so viel vorgenommen, doch schwupps ist es schon wieder Freitag und ich habe gerade einmal die Hälfte meiner Vorhaben gepackt. Aber egal, nun ist Freitag und es ist Zeit für das Hundephoto, das Euch darüber hinwegtrösten soll, daß es die nächsten Tage nur wenige Updates hier im *Schockwellenreiter* geben wird. Ein paar schon, denn wenigstens mit meinem [hüpfenden Einhorn](Coding Train Unicorn Rainbow (Stage 2) – 20201111) bin ich weitergekommen und ich werde darüber berichten und meine [Twine-Tutorials im Wunderland-Universum](Überall ist Wunderland (Twine 2) – 20200721) möchte ich in den nächsten Tagen auch fortführen.

Ja, und dann das Übliche: Lesen, Schreiben und die Fellnase auf dem Tempelhofer Feld bespaßen. Denn solange »unser« [Hundeplatz](https://hsv-plaenterwald.jimdofree.com/startseite/news/) im Lockdown verharrt, müssen der kleine Spitz und ich uns anderweitig die Zeit vertreiben. Ich hoffe aber, daß wir ab Dezember wieder regulär trainieren können.

In diesem Sinne und dem Lockdown zum Trotz: Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*