#title "Video-Tutorials: Wie komme ich ins Web? – 20201112"
#maintitle "Wie komme ich ins Web? Bausteine für ein »Web des Wissens«"
#prevp "2020111201"
#nextp "2020111301"

<iframe width="852" height="480" src="https://www.youtube.com/embed/NQP89ish9t8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Wie komme ich ins Web? Bausteine für ein »Web des Wissens«

In den Video-Tutorials, die ich heute vorstellen möchte, geht es um mehr als das ungläubige Staunen *Boris Beckers* »Ich bin drin«. Es geht nicht darum, als Konsument das Web zu nutzen, sondern darum, als **Produzent** das Internet mit eigenen Inhalten zu füllen, also eigene Webseiten zu erstellen und an einen Platz hochzuladen, von dem aus jede und jeder auf Eure eigenen Inhalte zugreifen kann. Denn gerade in Corona-Zeiten müssen wir unsere Form der Kommunikation digital neu denken und diese Video-Tutorials können ein Baustein für ein »Web des Wissens« sein.

Als erstes habe ich obiges Tutorial ausgewählt, in dem *Beau Carnes* auf *FreeCodeCamp* Euch in einer Stunde nicht nur erklärt, wie Ihr eine Website mit HTML, CSS und JavaScript von Grund auf erstellt, sondern auch, wie Ihr an eine eigene URL kommt und *last but not least* wie Ihr einen Hoster findet, der Eure Website der staunenden Öffentlichkeit zugänglich macht.

<iframe width="560" height="315" src="https://www.youtube.com/embed/KgRENOnSCxE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Da ist natürlich noch Luft nach oben. Daher habe ich dieses Video nachgeschoben, ein Tutorial, das Euch zeigt, wie Ihr eine Portfolio-Website nur mit HTML und CSS erstellen könnt. In der Hauptsache habe ich dieses gut halbstündige Video ausgesucht, weil in der Videobeschreibung Links zu zehn weiteren interessanten Video-Tutorials für Webseitenbauer (und solche die es werden wollen) aufgeführt sind.

<iframe width="560" height="315" src="https://www.youtube.com/embed/N86lAbY0NRA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Da ja heutzutage alles *responsive* sein soll (ich gebe zu, hier schwächelt aus historischen Gründen auch der *Schockwellenreiter*), gibt es von obigem Video noch eine viertelsütndige Fortsetzung, die zeigt, wie man die kürzlich gebastelte Portfolio-Website responsive bekommt.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_delXvG9zOc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Und zu guter Letzt macht Euch *Guido Brombach* den eigenen Server schmackhaft, auch wenn Ihr bisher dachtet, keinen zu haben. Das Video ist Teil der #OERcamp-Reihe »[Open Source Tools zur Installation auf dem eigenen Server](https://www.oercamp.de/webinare/ostools/)«, die noch läuft und die ich Euch insgesamt ans Herz legen möchte, auch wenn ich der Empfehlung für WordPress nicht unbedingt folge. Denn bekanntermaßen halte ich nur statische Seiten für nachhaltig und zukunftsträchtig. Aber vielleicht muß man in Zeiten, wo Lehrkräfte ohne richtige digitale Ausbildung und Kompetenz Lehrinhalte auf einmal digital verbreiten müssen, auch Kompromisse eingehen.