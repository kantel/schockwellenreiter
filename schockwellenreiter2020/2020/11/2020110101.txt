#title "Die Zahlen Oktober 2020 – 20201101"
#maintitle "Die Zahlen für den Oktober 2020"
#prevp "2020103101"
#nextp "2020110201"

## Die Zahlen für den Oktober 2020

Wieder ist ein neuer Monat angebrochen, daher gibt es als erstes ein paar Zahlen vom Vormonat, die hochtrabend auch manches Mal *Mediadaten* genannt werden. Im Oktober 2020 hatte der *Schockwellenreiter* laut seinem nicht immer zuverlässigen, aber (hoffentlich!) datenschutzkonformen [Neugiertool](cp^Piwik) exakt **6.242&nbsp;Besucher** mit **12.224 Seitenansichten**. Natürlich täuscht die Exaktheit der Ziffern eine Genauigkeit der Zahlen nur vor, aber trotzdem sind sie erfreulich und ich bedanke mich bei allen meinen Leserinnen und Lesern.

*Bleibt mir gewogen!* &nbsp; 🎃