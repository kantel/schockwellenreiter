#title "Schau Mama, ein Hundebild! – 20201120"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2020112002"
#nextp "2020112101"

<%= html.getLink(imageref("feldbezwingerherbst-b"), "Der kleine Feldbezwinger im Herbst") %>

**Schau Mama, ein Hundebild!** Wieder erscheint das freitägliche Hundebild auf den letzten Drücker. Aber dieses Mal habe ich wenigstens eine einigermaßen plausible Entschuldigung: Ich hatte heute (Nach-) Mittag eine ausführliche Videokonferenz, die den Beginn eines neuen Projektes einläutete. Ich will noch nicht zuviel verraten, es ist ein längerfristiges Projekt und es geht um [Künstliche Intelligenz](https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz). Wundert Euch daher nicht, wenn in den nächsten Wochen und Monaten Python-Programme aus diesem Bereich hier im <del>Blog</del> Kritzelheft auftauchen.&nbsp;🤓

Ansonsten soll Euch das Photo der kleinen Fellnase wie gewohnt darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates im *Schockwellenreiter* geben wird. Denn wie immer möchte ich viel lesen und schreiben, ein wenig programmieren und natürlich auch den kleinen Spitz bespaßen.

Es ist kalt draußen, der Herbst hat jetzt endgültig das Heft in der Hand. Dennoch wünsche ich Euch allen da draußen ein schönes Wochenende. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*