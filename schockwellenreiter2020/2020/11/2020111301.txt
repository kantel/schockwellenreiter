#title "Apple veröffentlicht macOS Big Sur – 20201113"
#maintitle "Apple veröffentlicht macOS Big Sur 11.0"
#prevp "2020111202"
#nextp "2020111302"

<%= html.getLink(imageref("jansteenbuegelspiel-b"), "Bauern spielen das Bügelspiel") %>

## Apple veröffentlicht macOS Big Sur 11.0

Gestern abend (12. November 2020) hat Apple die neue Betriebssystemversion macOS Big Sur 11.0 freigegeben. Das kostenlose Update stellt an die Hardware folgende Voraussetzungen:

- MacBook seit 2015
- MacBook Air seit Mitte 2013
- MacBook Pro seit Ende 2013
- Mac mini seit Ende 2014
- iMac seit Ende 2014
- Mac Pro ab 2013
- iMac Pro ab 2017

Okay, damit bin ich raus. Mein MacBook Pro ist von Mitte 2012. Wer noch updaten kann, die neuen Features kann man [hier nachlesen](https://www.apple.com/de/macos/big-sur/features/). Aber wie immer bei neuen macOS-Versionen gilt auch hier der unter langjährigen Mac-Nutzern bekannte Rat, besser das »erste Punkt-Update« (also macOS Big Sur 11.1) abzuwarten, ehe man sich auf das Abenteuer des Umstiegs einlässt. Anwender des Virenschutzprogramms von Sophos werden sich ohnehin noch etwas gedulden müssen, da der Hersteller offenbar an der Anpassung arbeitet.

Vor einem eventuellen Upgrade ist natürlich eine entsprechende Datensicherung dringend zu empfehlen. *([[cert]])*

In der Futurezone kann man [nachlesen](https://futurezone.at/produkte/chaos-bei-big-sur-mac-update-legte-rechner-lahm/401096313), daß obige Warnung berechtigt ist: So dauerte schon der Download teilweise extrem lange. Hatte man das Update dann installiert kam es zu den nächsten Problemen. Apps ließen sich teilweise nicht ausführen oder reagierten nur äußerst schleppend. Kurioserweise war das Problem behoben, sobald man den Computer vom Netz trennte.

Wie User [laut Ars Technica](https://arstechnica.com/gadgets/2020/11/macos-big-sur-launch-appears-to-cause-temporary-slowdown-in-even-non-big-sur-macs/) relativ schnell herausgefunden hatten, war der Grund dafür ein Server-Problem bei Apples OCSP *(Online Certificate Status Protocol Service)*. Dabei handelt es sich um einen automatisierten Prozeß, der die Software beziehungsweise deren Signatur scannt und auf schädliche Inhalte überprüft. Weil diese Infrastruktur streikte, streikten auch die Programme auf dem Mac, deren Überprüfung hängen geblieben war. 