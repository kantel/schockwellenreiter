#title "Google stopft, Microsoft zieht nach – 20201112"
#maintitle "Google stopft Sicherheitslücke in Chrome, Microsoft zieht nach"
#prevp "2020111101"
#nextp "2020111202"

<%= html.getLink(imageref("herbstwohnsitzjulius-b"), "Herbstlicher Wohnsitz in der Juliusstraße") %>

## Google stopft Sicherheitslücke in Chrome, Microsoft zieht nach

Google schließt nur [wenige Tage nach dem letzten Update](Security Alert: Browser Updates erforderlich – 20201110) mit der neuen Version (86.0.4240.198) seines Browsers Chrome [weitere Sicherheitslücken](https://chromereleases.googleblog.com/2020/11/stable-channel-update-for-desktop_11.html).

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden.

Und Microsoft zieht nach und korrigiert die neu veröffentlichte Edge-Version auf die Version (86.0.622.68), die genau die [kritischen Sicherheitslücken](https://docs.microsoft.com/en-us/deployedge/microsoft-edge-relnote-stable-channel) schließt, die auch Google in der Version (86.0. 4240.198) seines Browsers Chrome behoben hat.

Auch Edge aktualisiert sich über die integrierte Update-Funktion, (kann unter Windows angestoßen werden über Menü: `3-Punkte oben rechts > Hilfe und Feedback > Infos zu Microsoft Edge`). *([[cert]])*

*[[photojoerg]]*