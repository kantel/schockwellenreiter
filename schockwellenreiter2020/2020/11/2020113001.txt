#title "Programmieren mit BASIC – 20201130"
#maintitle "QB64: Back to the Roots – Programmieren mit BASIC"
#prevp "2020112802"
#nextp "2020113002"

<%= html.getLink(imageref("basic"), "https://github.com/QB64Team/qb64") %>

## QB64: Back to the Roots – Programmieren mit BASIC

Ich habe ja bekanntlich ein Faible für historische Programmiersprachen, aber das werde ich mir vermutlich doch nicht antun: [QB64](https://www.qb64.org/portal/) ist eine freie (MIT-Lizenz), moderne erweiterte BASIC-Programmiersprache, die die QBasic/QuickBASIC 4.5-Kompatibilität beibehält und native Binärdateien für Windows, Linux und macOS kompiliert. Obwohl, ich kenne mindestens einen (Hallo [Stefan](http://www.simulationsraum.de/)!), den diese Nachricht interessieren wird (falls er das Teil nicht sowieso schon kennt).

<%= html.getLink(imageref("idemacos"), "http://www.qb64.org/wiki/Main_Page") %>

Aber da man niemals nie sagen soll und -- wie obiger Screenshot zeigt -- die QB64-IDE unter macOS läuft, habe ich es mir mal gemerkt. Außerdem soll es eine erweiterte Graphikuntertstützung besitzen. Wenn es mich also doch mal überkommt und ich unbedingt etwas Buntes mit der Programmiersprache meiner Anfänge programmieren will, hier sind die Links:

- [QB64 Home](https://www.qb64.org/portal/)
- [QB64 @ GitHub](https://github.com/QB64Team/qb64)
- [QB64 Wiki](http://www.qb64.org/wiki/Main_Page) mit Installationsanleitungen für Windows, macOS und Linux.
- [QB64](https://en.wikipedia.org/wiki/QB64) in der (englischsprachigen) Wikipedia.

Schließlich sollte jeder mal in BASIC programmiert haben.&nbsp;🤓