#title "Google behebt viele Sicherheitslücken – 20200520"
#maintitle "Google behebt mit der neuen Version 83 seines Browsers Chrome viele Sicherheitslücken"
#prevp "2020051901"
#nextp "2020052002"

<%= html.getLink(imageref("crotgruen-b"), "Sitzgruppe an Container (farblich abgestimmt)") %>

## Google behebt mit der neuen Version 83 seines Browsers Chrome viele Sicherheitslücken

Google schließt mit der neuen Version 83 (83.0.4103.61) [viele Sicherheitslücken](https://chromereleases.googleblog.com/2020/05/stable-channel-update-for-desktop_19.html).

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden. *([[cert]])*

*[[photojoerg]]*