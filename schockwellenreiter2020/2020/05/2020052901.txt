#title "Video-Tutorial(s): Basic Animation in Python 3 – 20200529"
#maintitle "Video-Tutorial(s): Basic Animation in Python 3"
#prevp "2020052801"
#nextp "2020052902"

## Video-Tutorial(s): Basic Animation in Python 3

<iframe width="851" height="480" src="https://www.youtube.com/embed/mIQR6GUxZZI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

An diesem Wochenende möchte ich noch einmal auf eine weitere Playlist von *Christian Thompson* (aka *TokyoEdTech*) zurückgreifen, die Euch über die Corona-Isolation hinweghelfen soll: [Basic Animation in Python 3](https://www.youtube.com/watch?v=mIQR6GUxZZI&list=PLlEgNdBJEO-n9U_OcG2-KT_l2ncrJWPzc) ist eine sechsteilige Tutorial-Serie mit einer Gesamtspieldauer von etwas über einer halben Stunde (Ihr braucht kein Tutorial auf Spielfilmlänge, es soll schließlich ein sonniges Pfingstwochenende werden&nbsp;😎), in der *Christian Thompson* Euch und mir zeigt, wie man animierte Sprites mit [Pythons Schildkröte](https://docs.python.org/3/library/turtle.html) programmiert. Auch hier kitzelt er wieder Unglaubliches aus dem Turtle-Modul heraus.

Wie ich schon [letztens schrieb](Eine Schildkröte als scrollender Hintergrund – 20200527): Trotz all meiner Begeisterung für [Pygame Zero](cp^Pygame Zero) hätte ich Lust, mich mal wieder mit der Schildkröte zu befassen. 

[Alle Assets und den Quellcode](https://github.com/wynand1004/Projects/tree/master/Basic%20Animation) findet Ihr auf GitHub.