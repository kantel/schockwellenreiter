#title "Oracle behebt Sicherheitslücken in Java – 20200415"
#maintitle "Oracle behebt wieder kritische Sicherheitslücken in Java"
#prevp "2020041402"
#nextp "2020041502"

<%= html.getLink(imageref("nachtmahr-b"), "Nicolai Abildgaard: Mareridt (Alptraum), (1800)") %>

## Oracle behebt wieder kritische Sicherheitslücken in Java

Im Rahmen seines turnusmäßigen *[Critical Patch Update](https://www.oracle.com/security-alerts/cpuapr2020.html)* schließt Oracle in den neuen Java Versionen wieder zahlreiche teils kritische [Sicherheitslücken](https://www.oracle.com/technetwork/java/javase/overview/index.html).

Wer also Java auf seinem System einsetzt, sollte diese neue Version installieren. Sie kann über die eingebaute Update-Funktion oder wie immer [hier bezogen](http://www.oracle.com/technetwork/java/javase/downloads/index.html) werden. *([[cert]])*
