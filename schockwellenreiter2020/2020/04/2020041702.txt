#title "Scripting for Artists – 20200417"
#maintitle "Scripting for Artists: Blender und Python"
#prevp "2020041701"
#nextp "2020041703"

## Scripting for Artists: Blender und Python

<iframe width="852" height="480" src="https://www.youtube.com/embed/hfYgCwC_4iE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ich habe nicht vergessen, daß ich mir die freie 3D-Software [Blender](cp^Blender) in Zusammenhang mit ihrer Python-Fähigkeit auf ihre Eignung für *Creative Coding* [anschauen wollte](Worknote: Python und Blender – 20200413). Und da ich Freitags sowieso immer ein lehrreiches Video veröffentlichen wollte, das Euch beschäftigt -- gerade in Zeiten der Corona-Isolation vielleicht wichtig --, wenn Ihr ans Haus gefesselt seid, biete ich Euch heute den einstündigen Vortrag [Scripting for Artists](https://www.youtube.com/watch?v=hfYgCwC_4iE) an, den *[Dr. Sybren A. Stüvel](https://www.blendernetwork.org/sybren-a-stuvel)*, Entwickler am Blender-Institut, auf der Blender Conference 2017 gehalten hat.

Es ist ein kleines Risiko, denn ich selber habe mir dieses Video auch noch nicht reingezogen, hoffe aber auf eine lehrreiche Stunde. Ein schönes Wochenende Euch allen da draußen und bleibt gesund. Wir lesen uns spätestens am Montag wieder.