#title "Die Wireframe 37 ist draußen – 20200423"
#maintitle "Die Wireframe 37 ist draußen, mit einem Roguelike und Lunar Lander"
#prevp "2020042301"
#nextp "2020042401"

## Die Wireframe 37 ist draußen, mit einem Roguelike und Lunar Lander

Die [Wireframe 37](https://wireframe.raspberrypi.org/issues) ist heute erschienen und mit zwei netten Beiträgen für Spieleprogrammierer bestückt, so daß ich es kaum erwarten kann, sie Euch vorzustellen. Der erste Beitrag behandelt den Spieleklassiker [Lunar Lander](https://de.wikipedia.org/wiki/Lunar_Lander), den ich schon in den 1970er auf einer [PDP-11](https://de.wikipedia.org/wiki/PDP-11) gespielt hatte. Die Wireframe-Homage an diesen Spieleklassiker wurde natürlich in [Pygame Zero](cp^Pygame Zero) implementiert und noch am gleichen Tag auch [im Raspberry Pi Blog veröffentlicht](https://www.raspberrypi.org/blog/code-a-homage-to-lunar-lander-wireframe-37/).

<%= html.getLink(imageref("procdungeonwf37-b"), "Procedural Generated Dungeon") %>

In meinen Augen der größte Clou dieser Ausgabe ist aber der Artikel von *Ma C Bowley*, der auf den Seiten 32 bis 39 des Magazins zeigt, wie man ein Dungeon prozedural generiert. Die meisten Leser des *Schockwellenreiters* wissen von meiner Faszination für *[Roguelikes](cp^Rogue)* und werden sich daher über meine Begeisterung für diesen Beitrag nicht wundern. Auch dieses Programm wurde in Pygame Zero implementiert, inklusive eines kleinen Spiels *(Dungeon Smash)*, das zeigt, wie man diese prozedural generierten Level einsetzt. Leider ist dieser Artikel noch nicht online, aber wie ich die Macher des *Raspberry Pi Blogs* kenne, wird sich das sicher bald ändern.

Der Quellcode mit sämtlichen Assets ist natürlich wieder auf GitHub zu finden, hier für die [prozedural generierten Dungeons](https://github.com/Wireframe-Magazine/Wireframe-37/tree/master/proc-gen-dungeons) und hier für [Lunar Lander](https://github.com/Wireframe-Magazine/Wireframe-37/tree/master/source-code-lunar-lander). Habt Spaß damit!

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1071147706&asins=1071147706&linkId=e9ef3e34cfa67fa36f38d7d30fd79f90&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

**War sonst noch was?** Ach ja, mir ist ein weiteres Buch zu Pygame Zero untergekommen. Es heißt »Coding Games With Pygame Zero & Python« und ist von *Richard Smith*. Es ist schon in der zweiten Auflage erschienen und ist als Arbeitsbuch, das der Autor für seine Studenten konzipiert hat, entstanden. Ich hatte mir zuerst die extrem günstige [Kindl-Ausgabe][a1] (2,99 Euro) heruntergeladen, nach der ersten Lektüre dann aber entschieden, daß ich in der [Totes-Holz-Ausgabe][a2] herumblättern will. Denn das Buch ist nicht schlecht. Nach der obligatorischen Kurzeinführung in Python werden die einzelnen Möglichkeiten, die Pygame Zero bietet, an kurzen Beispielen erläutert. Und da taucht so einiges auf, was die [offizielle Dokumentation](https://pygame-zero.readthedocs.io/en/stable/) verschweigt.

Den krönenden Abschluß -- ab Kapitel 8 -- bieten dann vier vollständige Spiele, die jeweils eine Spielart klassischer 2D-Spiele vorstellen und auf die Besonderheiten eingehen, die deren Programmierung verlangt.


[a1]: https://www.amazon.de/Coding-Games-Pygame-Zero-Python-ebook/dp/B07SKMQ2GD/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Coding+Games+With+Pygame+Zero+&+Python&qid=1587656375&sr=8-6&linkCode=ll1&tag=derschockwell-21&linkId=793838cb81e37fa254545a0982c07707&language=de_DE
[a2]: https://www.amazon.de/Coding-Games-Pygame-Zero-Python/dp/1071147706/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Coding+Games+With+Pygame+Zero+&+Python&qid=1587656375&sr=8-8&linkCode=ll1&tag=derschockwell-21&linkId=dc6a814ebe9235158af11ab2ea2e82df&language=de_DE