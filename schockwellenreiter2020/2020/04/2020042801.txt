#title "Noch mehr für umme lesen – 20200428"
#maintitle "Noch mehr (über maschinelles Lernen und Data Science) für umme lesen"
#prevp "2020042601"
#nextp "2020042802"

<%= html.getLink(imageref("joblotcheap-b"), "William Michael Harnett: Job Lot Cheap") %>

## Noch mehr (über maschinelles Lernen und Data Science) für umme lesen

[Die über 400 Titel](https://link.springer.com/search?facet-content-type=%22Book%22&package=mat-covid19_textbooks&facet-language=%22En%22&sortOrder=newestFirst&showAll=true), die der (wissenschaftliche) Springer-Verlag während der Corona-Pandemie zum freien Download ins Netz gestellt hat ([wir berichteten](Für umme Wissenschaft lesen – 20200425)), schlagen Wellen. Da auch mir die Auswahl schwerfiel, gefällt mir, daß *Uri Eliabayev* eine kuratierte Auswahl von 65 Titeln zu maschinellem Lernen und *Data Science* [veröffentlicht hat](https://towardsdatascience.com/springer-has-released-65-machine-learning-and-data-books-for-free-961f8181f189).

Es gibt einige Überschneidungen zu meiner vorgestern veröffentlichten Auswahl, aber dennoch hat mich der Beitrag zu einigen zusätzlichen Downloads verführt. Wann soll ich das eigentlich alles lesen, so lange kann die Corona-Isolation doch gar nicht anhalten? Und mit unserem neuen Hund muß ich ja auch regelmäßig raus.&nbsp;🤓