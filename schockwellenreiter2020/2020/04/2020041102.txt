#title "Die Klasse PVector in Pygame Zero – 20200411"
#maintitle "Die Klasse PVector in Pygame Zero (1)"
#prevp "2020041101"
#nextp "2020041201"

<%= html.getLink(imageref("bouncingballpgz-b"), "Bouncing Ball mit Vektoren in Pygame Zero") %>

## Worknote: Die Klasse PVector in Pygame Zero (1)

Bei meinen Versuchen, *Daniel Shiffmans* »[The Nature of Code][a1]« nach Python zu portieren, bin ich erst einmal von [Graphics.py](cp^graphicspy) zu [Pygame Zero](cp^Pygame Zero) gewechselt, nachdem mich die [ersten Versuche mit *John Zelles* Bibliothek nicht zufriedengestellt](Nutzer-Interaktion in Graphics.py? – 20200411) hatten. Fühlt sich irgendwie besser an und zeigt, da keinen Boilerplate-Overhead, die Algorithmen mehr oder weniger pur. Allerdings ist die Qualität der Darstellung graphischer Primitive (Kreis, Rechteck, Linie etc.) etwas gewöhnungsbedürftig. Ich glaube, ich werde in einem nächsten Schritt auf Bilder (vermutlich von [Twitters Twemojis](https://twemoji.twitter.com/) »ausgeborgt«) ausweichen.

[a1]: https://www.amazon.de/Nature-Code-Simulating-Natural-Processing/dp/0985930802/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=The+Nature+of+Code&qid=1586524315&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=f93e28856513dd37c8c1ad320064d777&language=de_DE

Ansonsten -- für Neugierige -- das Progrämmchen, soweit ich es an einem dusseligen Sonnabend-Nachmittag hingerotzt habe. Da sich Pygame Zero bei der objektorientierten Programmierung irgendwie ziert, mußte ich natürlich gegensteuern und die Bällchen erst einmal in eine Klasse auslagern. Das ist die Datei `ball.py`:

~~~python
from pvector import PVector

WIDTH = 400
HEIGHT = 400

class Ball():
    
    def __init__(self, x, y, v_x, v_y, radius, color):
        self.position = PVector(x, y)
        self.radius = radius
        self.color = color
        self. velocity = PVector(v_x, v_y)
    
    def show(self, screen):
        screen.draw.filled_circle((self.position.x, self.position.y), self.radius, self.color)
    
    def move(self):
        self.position.add(self.velocity)
    
        if (self.position.x > WIDTH - self.radius) or (self.position.x < self.radius):
            self.velocity.x *= -1
        if (self.position.y > HEIGHT - self.radius) or (self.position.y < self.radius):
            self.velocity.y *= -1
~~~

Wie üblich, ist nahezu die vollständige Logik des Algorithmus' in der Klasse implementiert worden (daher wird auch die Klasse `PVector`) nur hier benötigt. So schrumpft das Hauptprogramm auf wenige Zeilen Code zusammen:

~~~python
import pgzrun
from ball import Ball

WIDTH = 400
HEIGHT = 400
TITLE = "Bouncing Ball"


ball1 = Ball(200, 150, 1.0, 3.3, 16, "red")
ball2 = Ball(100, 100, 1.5, 5.0, 16, "blue")

def draw():
    screen.fill("yellow")
    ball1.show(screen)
    ball2.show(screen)

def update():
    ball1.move()
    ball2.move()

pgzrun.go()
~~~

Als nächstes werde ich das alles noch ein wenig aufhübschen und mich danach wieder in *Shiffmans* Werk vertiefen. Ist nämlich für mich genau die richtige Beschäftigung, um mich von Corona und anderem abzulenken. *Still digging!*

<%= html.getLink(imageref("pygamezero"), "cp^Pygame Zero") %>