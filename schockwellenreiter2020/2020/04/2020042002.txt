#title "Python im Browser: Brython Crash Course – 20200420"
#maintitle "Python im Browser: Brython Crash Course"
#prevp "2020042001"
#nextp "2020042101"

## Python im Browser: Brython Crash Course

<iframe width="852" height="480" src="https://www.youtube.com/embed/VJj-H4we71g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Ziemlich genau vor einem Jahr](http://blog.schockwellenreiter.de/2019/04/2019041701.html) machte ich Euch hier im <del>Blog</del> Kritzelheft auf [Brython](http://www.brython.info/) (**Br**owser P**ython**) aufmerksam, eine Python-3-Implementierung, die im Browser läuft und Pythonistas ersparen will, JavaScript zu lernen. Ich bin nie dazu gekommen, mir Brython näher anzuschauen, aber heute schneite ein brandneues Video-Tutorial in meinen Feedreader und dieses Video ist ein *Brython Crash Course*.

Okay, ich habe mir das Video reingezogen. Was es zeigt, ist, wie man das *Document Object Model* (DOM) von JavaScript mit Python manipuliert. Man muß dazu nicht unbedingt die JavaScript-Syntax kennen, aber man muß auf jeden Fall wissen, wie JavaScript und das DOM zusammenhängen und -arbeiten.

Aber dennoch ist Brython interessant. Auch wenn JavaScript seit ES6 viel von seinem Schrecken verloren hat, zumindest ich als Pythonista finde die Syntax von Python immer noch klarer und verständlicher. Momentan arbeite ich an keinem Web-Projekt. Aber sollte mir mal wieder eines unterkommen -- was durchaus im Bereich des Möglichen liegt, ich habe da schon ein paar Ideen --, werde ich mir überlegen, ob ich statt JavaScript Brython einsetze. Und sei es nur, weil ich von Natur aus neugierig bin.