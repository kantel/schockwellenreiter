#title "Besuch bei der Amme"
#prevp "202002bild13"
#nextp "202002bild15"

<%= html.getLink(imageref("besuchbeideramme"), "https://www.flickr.com/photos/gandalfsgallery/49383574311/") %>

Gemälde von [Martin van Cleve](https://de.wikipedia.org/wiki/Martin_van_Cleve) (1520-1570). Obwohl es unterschiedliche Interpretationen dieses Brueghelschen Themas gibt, ist die überzeugendste Erklärung, daß es sich um ein wohlhabendes bürgerliches Paar handelt, das das Landhaus der Amme ihres Kindes besucht, das in der Mitte dieses Gemäldes gezeigt wird. Van Cleve malte mehr als zwanzig Versionen dieser Szene, die sich in Größe, Details und Figurenarrangements erheblich unterschieden. Bei einigen erweiterte van Cleve seine Komposition nach links, um eine Gruppe von Trinkern an einem Tisch einzubeziehen, während er in diesem Fall durch eine offene Tür einen Blick in eine Landschaft gewährt.