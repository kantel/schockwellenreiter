#title "Sheltietreffen mit Poser"
#prevp "202002bild15"
#nextp "202002bild17"

<%= html.getLink(imageref("sheltietreffenmitposer"), "https://www.flickr.com/photos/schockwellenreiter/49408897338/") %>

Joey weiß einfach, daß er gut aussieht. Da können noch so viele andere Shelties herumwuseln. Aufgenommen am 19. Januar 2020. *[[photojoerg]]*