#title "Feldbotanik (2)"
#prevp "202010bild10"
#nextp "202010bild12"

<%= html.getLink(imageref("feldbotanik2"), "https://www.flickr.com/photos/schockwellenreiter/50394519362/") %>

Aufgenommen am 28. September 2020 auf dem Tempelhofer Feld. *[[photojoerg]]*