#title "John William Godward: A Siesta (1892)"
#prevp "202012bild22"
#nextp "202012bild24"

<%= html.getLink(imageref("asiesta"), "https://www.flickr.com/photos/gandalfsgallery/50704628713/") %>

[John William Godward](https://de.wikipedia.org/wiki/John_William_Godward) (* 9. August 1861 in Wimbledon; † 13. Dezember 1922 in London) war ein britischer Maler des Neoklassizismus.