#title "Überleben in der Corona-Isolation"
#prevp "202005bild37"
#nextp "202005bild39"

<%= html.getLink(imageref("coronaueberleben"), "https://www.flickr.com/photos/schockwellenreiter/49866849693/") %>

Wohl dem, der einen Garten, ein Bier und ein gutes Buch sein eigen nennt. Aufgenommen am 7. Mai 2020. *[[photojoerg]]*