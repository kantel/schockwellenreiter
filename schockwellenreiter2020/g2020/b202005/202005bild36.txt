#title "Ditlev Blunck: Noah i arken (1835)"
#prevp "202005bild35"
#nextp "202005bild37"

<%= html.getLink(imageref("noahfamilie"), "https://www.flickr.com/photos/gandalfsgallery/49886144187/") %>

Keine Spur von sozialer Distanz: Noah und seine Familie in der Arche. Gemälde von [Ditlev Blunck](https://de.wikipedia.org/wiki/Detlev_Conrad_Blunck).