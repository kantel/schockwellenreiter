#title "Pieter van Laer: Selbstportrait"
#prevp "202003bild06"
#nextp "202003bild08"

<%= html.getLink(imageref("pietervanlaer"), "https://www.flickr.com/photos/gandalfsgallery/49371897481/") %>

In einem der bemerkenswertesten Selbstporträts, die jemals geschaffen wurden, reagiert [Pieter van Laer](https://de.wikipedia.org/wiki/Pieter_van_Laer) (Haarlem, 15. Dezember 1599 - 30. Juni 1642) mit Entsetzen auf die furchterregenden Klauen des Teufels, die plötzlich gekommen sind, um ihn zu holen.