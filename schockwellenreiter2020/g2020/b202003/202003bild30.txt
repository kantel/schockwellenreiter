#title "Rackerbike"
#prevp "202003bild29"
#nextp "202003bild31"

<%= html.getLink(imageref("rackerbike"), "https://www.flickr.com/photos/schockwellenreiter/49696582243/") %>

Da hat ein <del>Rocker</del> Racker sein Bike im Carl-Weder-Park stehen lassen. Aufgenommen am 25. März 2020. *[[photojoerg]]*