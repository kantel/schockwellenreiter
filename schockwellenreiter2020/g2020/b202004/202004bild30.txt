#title "Gerard van Kuijl: Schäfer und Schäferin"
#prevp "202004bild29"
#nextp "202004bild31"

<%= html.getLink(imageref("schaeferundschaeferin"), "https://www.flickr.com/photos/gandalfsgallery/49741558131/") %>

[Gerard van Kuijl](https://en.wikipedia.org/wiki/Gerard_van_Kuijl) (1604, Gorinchem – 1673, Gorinchem) war ein niederländischer Maler, der unter anderem wegen seiner Genre-Bilder im Stil von *[Caravaggio](https://de.wikipedia.org/wiki/Michelangelo_Merisi_da_Caravaggio)* bekannt war.

