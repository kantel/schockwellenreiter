#title "Hans Baluschek: Vergnügungspark – In der Hasenheide"
#prevp "202004bild16"
#nextp "202004bild18"

<%= html.getLink(imageref("baluschekhasenheide1895"), "https://commons.wikimedia.org/wiki/File:BaluschekAlteHasenheide1895_02.jpg") %>

In der zweiten Hälfte der 1890er Jahre trat [Hans Baluschek](https://de.wikipedia.org/wiki/Hans_Baluschek) mehr und mehr in das Bewußtsein der Berliner Kunstszene. Obwohl es bereits vorher Darstellungen aus dem Berliner Klein- und Spießbürgertum gab, waren Baluscheks Bilder für seine Zeit neuartig und außergewöhnlich. Die Darstellung der unmenschlichen Lebensumstände und der trostlosen Arbeitsbedingungen kamen hinter der oftmals amüsanten Fassade hervor. Deutlich wird dieser Kontrast unter anderem bei Werken wie *Vergnügungspark – In der Hasenheide* (1895), in dem die oberflächliche Feststimmung durch die Gesichter der Protagonisten und die Darstellung der Jahrmarktbuden relativiert wird. *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:BaluschekAlteHasenheide1895_02.jpg))*

