#title "Kramladen am Bahnhof Spindlersfeld"
#prevp "202001bild06"
#nextp "202001bild08"

<%= html.getLink(imageref("kramladenspfeld"), "https://www.flickr.com/photos/schockwellenreiter/49316741167/") %>

Bei diesem Kramladen einer vietnamesischen Familie am Bahnhof Spindlersfeld bekommt man fast alles, was das Herz begehrt. Aufgenommen am 2. Januar 2020. *[[photojoerg]]*