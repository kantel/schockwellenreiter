#title "Gustav Bauernfeind: Markt in Jaffa (1887)"
#prevp "202001bild27"
#nextp "202001bild29"

<%= html.getLink(imageref("marktinjaffa"), "https://www.flickr.com/photos/gandalfsgallery/49448254078/") %>

[Johann Gustav Adolph Bauernfeind](https://de.wikipedia.org/wiki/Gustav_Bauernfeind) (* 4. September 1848 in Sulz am Neckar; † 24. Dezember 1904 in Jerusalem) war ein deutscher Maler, Illustrator und Architekt. Er gilt als der bekannteste Orientmaler Deutschlands.