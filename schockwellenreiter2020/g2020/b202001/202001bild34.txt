#title "Erstes Sheltie-Treffen 2020"
#prevp "202001bild33"
#nextp "202002bild01"

<%= html.getLink(imageref("sheltietreffen202001"), "https://www.flickr.com/photos/schockwellenreiter/49409373361/") %>

Mit wenigen Ausnahmen: Fast nur Shelties. Aufgenommen am 19. Januar 2020. *[[photojoerg]]*