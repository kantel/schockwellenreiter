#title "Titus auf der Terrasse der Taverna Argo"
#prevp "202007bild17"
#nextp "202007bild19"

<%= html.getLink(imageref("titustavernaargo"), "https://www.flickr.com/photos/schockwellenreiter/50130809667/") %>

Aufgenommen am 19. Juli 2020. *[[photogabi]]*