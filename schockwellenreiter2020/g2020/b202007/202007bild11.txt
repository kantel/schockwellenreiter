#title "Bal Populaire, Paris 1912"
#prevp "202007bild10"
#nextp "202007bild12"

<%= html.getLink(imageref("balpopulaire1912"), "https://commons.wikimedia.org/wiki/File:Bal_populaire_du_14_juillet_1912_(Paris).jpeg") %>

Bal Populaire, Paris, 14. Juli 1912. *[Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Bal_populaire_du_14_juillet_1912_(Paris).jpeg)]*