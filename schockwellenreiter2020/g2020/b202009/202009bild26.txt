#title "André Henri Dargelas: Around the World (um 1860)"
#prevp "202009bild25"
#nextp "202009bild27"

<%= html.getLink(imageref("aroundtheworld"), "https://www.flickr.com/photos/gandalfsgallery/50384973111/") %>

[André Henri Dargelas](https://de.wikipedia.org/wiki/Andr%C3%A9_Henri_Dargelas) (* 11. Oktober 1828 in Bordeaux; † Juni 1906 in Écouen) war ein französischer Genremaler der Kinderwelt.

