#title "Schuhgarten (nicht Schulgarten)"
#prevp "202009bild02"
#nextp "202009bild04"

<%= html.getLink(imageref("schuhgarten"), "https://www.flickr.com/photos/schockwellenreiter/50297555387/") %>

Aufgenommen am 2. September 2020 auf dem Tempelhofer Feld. *[[photojoerg]]*