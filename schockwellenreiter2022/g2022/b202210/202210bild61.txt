#title "Dorfkirche Britz vom Dorfteich aus gesehen"
#prevp "202210bild60"
#nextp "202210bild62"

<%= html.getLink(imageref("dorfkirchebritzdorfteich"), "https://www.flickr.com/photos/schockwellenreiter/52462803014/") %>

Aufgenommen am 29. Oktober 2022, mehr [hier](Quer durch das kunterbunte Britz – 20221029) … *[[photojoerg]]*

