#title "Goldener Oktober (4)"
#prevp "202210bild26"
#nextp "202210bild28"

<%= html.getLink(imageref("Oktober2022_4"), "https://www.flickr.com/photos/schockwellenreiter/52432426333/") %>

Aufgenommen am 16. Oktober 2022 im Neuköllner Carl-Weder-Park. *[[photojoerg]]*