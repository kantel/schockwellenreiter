#title "The Yellow Kid visits Buster Brown"
#prevp "feltonbild03"
#nextp "feltonbild05"

<%= html.getLink(imageref("yellowkidbusterbrown"), "https://www.lambiek.net/artists/image/o/outcault/outcault_yellowkid.jpg") %>

*[Richard Felton Outcault](https://www.lambiek.net/artists/o/outcault.htm)* (14. November 1863 - 25. September 1928)