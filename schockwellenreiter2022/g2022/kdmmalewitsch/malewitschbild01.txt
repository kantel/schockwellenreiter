#title "Schwarze Bilder"
#prevp "denslowbild15"
#nextp "malewitschbild02"

<%= html.getLink(imageref("malewitchblacks"), "https://commons.wikimedia.org/wiki/File:Malevich%27s\_black\_suprematist\_paintings\_(GRM)\_by\_shakko\_01.jpg") %>

*Kasimir Malewitsch*: Black Square, Black Circle, Black Cross (1924). *(Bild: Shakko, via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Malevich%27s_black_suprematist_paintings_(GRM)_by_shakko_01.jpg))*