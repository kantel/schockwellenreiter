#title "Der Siebenstern"
#prevp "afklintbild05"
#nextp "afklintbild07"

<%= html.getLink(imageref("siebenstern"), "https://productimages.artboxone.com/1137014727-PO-big.webp") %>

*Helma af Klint*, Die Evolution, Nr. 15, Gruppe VI, Serie, WUS / Der Siebenstern.