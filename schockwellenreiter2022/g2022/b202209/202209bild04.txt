#title "Die vier apokalyptischen Reiter"
#prevp "202209bild03"
#nextp "202209bild05"

<%= html.getLink(imageref("apokalyptischenreiter"), "https://www.archaeologie-online.de/fileadmin/_processed_/5/a/csm_4_reiter_der_apokalypse_a762a9a85a.jpg") %>

*Albrecht Dürer*, [Die vier Reiter der Apokalypse](https://www.spektrum.de/news/albrecht-duerer-als-das-ende-der-zeit-anbrach/1875148) (Ausschnitt), 1497