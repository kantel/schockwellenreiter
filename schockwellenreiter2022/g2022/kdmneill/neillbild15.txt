#title "Der Goggelmoggel (Humpty Dumpty)"
#prevp "neillbild14"
#nextp "neillbild16"

<%= html.getLink(imageref("goggelmoggel"), "https://www.flickr.com/photos/57440551@N03/16121750864/") %>

llustration by *John R. Neill* for »Alice's Adventures in Wonderland« by *Lewis Carroll*. Chicago: Reilly & Britton Co., (1908).

The seventeen full-color illustrations in this edition of »Alice's Adventures in Wonderland« are among the earliest color images for *Lewis Carroll's* novel. They were done by famed children's illustrator, *John R. Neill*, who is primarily known as the illustrator of the *Frank L. Baum* Oz books.