#title "The Gump"
#prevp "neillbild13"
#nextp "neillbild15"

<%= html.getLink(imageref("gump"), "https://upload.wikimedia.org/wikipedia/commons/d/d7/The_marvelous_land_of_Oz%3B_being_an_account_of_the_further_adventures_of_the_Scarecrow_and_Tin_Woodman_a_sequel_to_the_Wizard_of_Oz_%281904%29_%2814566654499%29.jpg") %>

*John R. Neill*, The Gump soarded swiftly and majestically away, 1904. *(Bildquelle: [Wikimedia Commons](https://en.wikipedia.org/wiki/File:The_marvelous_land_of_Oz;_being_an_account_of_the_further_adventures_of_the_Scarecrow_and_Tin_Woodman_a_sequel_to_the_Wizard_of_Oz_(1904)_(14566654499).jpg))*

[The Gump](https://en.wikipedia.org/wiki/List_of_Oz_characters_(created_by_Baum)#The_Gump) is an amalgam of parts with the mounted head of an elk-like creature, first brought to life with the magic Powder of Life in The *Marvelous Land of Oz* (1904). Wogglebug combined the head with two sofas for a body, palm trees for wings, and a broom for a tail: all tied together with clothesline and ropes. After crashing into a jackdaw's nest, Wogglebug used a Wishing Pill to fix him. After the Land of Oz is retaken by Princess Ozma, the Gump is disassembled at his request leaving him a talking head that is still living in the Royal Palace of Oz.

The Gump also appeared in *Dorothy and the Wizard in Oz* where he meets and talks to Dorothy until Ozma arrives (for some reason, she doesn't like it when he talks).

The Gump is a central character in Disney's 1985 live action fantasy film *Return to Oz*, performed and voiced by *Lyle Conway* and assisted in the performance by *Steve Norrington*. Depicted somewhat like a green-furred moose, this Gump is assembled by Tik-Tok, Jack Pumpkinhead, and Billina, with Dorothy Gale applying the Powder of Life. This Gump escapes Mombi and survives the Nome King ordeal.