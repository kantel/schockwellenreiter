#title "Humpty Dumpty, Seite 5"
#prevp "denslowbild11"
#nextp "denslowbild13"

<%= html.getLink(imageref("humptydumptyp5"), "https://commons.wikimedia.org/wiki/File:Denslow%27s\_Humpty\_Dumpty\_pg\_5.jpg") %>

*[William Wallace Denslow](https://fairylore.jimdofree.com/2019/06/12/william-wallace-denslow/)*, Humpty Dumpty, Seite 5, 1930. *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Denslow%27s_Humpty_Dumpty_pg_5.jpg))*