#title "Lilliput August 1946"
#prevp "wtrierbild09"
#nextp "wtrierbild11"

<%= html.getLink(imageref("lilliput194608"), "https://www.fulltable.com/vts/aoi/l/lpt/imm/l29.jpg") %>

*Walter Trier*, [Covers for Lilliput](https://www.fulltable.com/vts/aoi/l/lpt/tr.htm), August 1946