#title "Traffic Stop (1922)"
#prevp "jcldbild09"
#nextp "jcldbild11"

<%= html.getLink(imageref("trafficstop"), "https://cdnb.artstation.com/p/assets/images/images/026/092/353/large/gordon-chen-jc.jpg?1587848900") %>

*Joseph Christian Leyendecker* (1874-1951), [Traffic Stop](https://www.illustratedgallery.com/artwork/original/3033/by-joseph-christian-leyendecker/), Cover Illustration, The Saturday Evening Post, June 24, 1922