#title "Ins Meer Schreitende"
#prevp "kirchnerbild11"
#nextp "afklintbild01"

<%= html.getLink(imageref("badendefehmarn"), "https://www.fehmarn24.de/bilder/2010/02/23/644406/2112669876-kirchner-ins-meer-1VUsZ3wS1a7.jpg") %>

»[Ins Meer Schreitende](https://www.fehmarn24.de/fehmarn/feh-paradis-644406.html)« heißt dieses Gemälde, das *Ernst Ludwig Kirchner* 1912 an der Küste Staberhuks gemalt hat. Der Expressionist verbrachte die Sommer 1908 und von 1912 bis 1914 auf Fehmarn.