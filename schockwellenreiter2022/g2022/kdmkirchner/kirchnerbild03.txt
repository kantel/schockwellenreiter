#title "Akt vor einem Spiegel"
#prevp "kirchnerbild02"
#nextp "kirchnerbild04"

<%= html.getLink(imageref("aktmirror"), "https://arthive.net/res/media/img/oy1000/work/ad3/471085@2x.jpg") %>

*Ernst Ludwig Kirchner*, Reclining Nude before a mirror, 1910