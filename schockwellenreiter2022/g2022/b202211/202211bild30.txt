#title "Immobilienlobby (und ihre Trittbrettfahrer) abwählen!"
#prevp "202211bild29"
#nextp "202211bild31"

<%= html.getLink(imageref("immobilienlobbyabwaehlen"), "https://www.flickr.com/photos/schockwellenreiter/52510086019/") %>

Aufgenommen am 19. November 2022 vor dem Neuköllner Kulturbunker in der Rungiusstraße (mehr [hier](Immobilienlobby abwählen! – 20221119)) … *[[photojoerg]]*