#title "RSS-Feed August 2022"
#fileextension ".xml"
#kramdown false
#template "blank"
#xml true

<item>
	<title>Ein neuer Besuch (oder Versuch) mit (oder bei) Pythons Schildkröte</title>
		<description>
			<![CDATA[<% imageref("ballernturtle-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022083102.html"><img src="http://blog.schockwellenreiter.de/images/ballernturtle-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nachdem ich vorgestern mit <i>Al Sweigarts</i> »The Big Book of Small Python Projects« so viel Spaß hatte, habe ich auch das jüngst erworbene »The Recursive Book of Recursion« von ihm aus meinem Bücherregal hervorgekramt und mich darin festgelesen. Denn auch dieses Buch mit seinen Beispielprogrammen in Python und JavaScript bringt viel Spaß und verspricht viele vergnügliche Stunden vor dem Monitor.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022083102.html</link>
</item>
<item>
	<title>Google korrigiert Sicherheitslücken in Chrome</title>
		<description>
			<![CDATA[<% imageref("vieraufeinemstreich-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022083101.html"><img src="http://blog.schockwellenreiter.de/images/vieraufeinemstreich-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Pünktlich zum Monatsende hat Google eine neue Version 105 (105.0.5195.52/53/54) seines Browsers Chrome veröffentlicht und schließt auch wieder Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022083101.html</link>
</item>
<item>
	<title>Eine neue Version 0.8.2a1 von Py5 ist draußen</title>
		<description>
			<![CDATA[<% imageref("rottripy52-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022083001.html"><img src="http://blog.schockwellenreiter.de/images/rottripy52-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Zum diesjährigen Ende des Sommers (zumindest auf der nördlichen Hemisphäre – sorry <i>Alexandre Villares</i>) überraschte uns der Macher von Py5 mit der Meldung, daß er die Version 0.8.2a1 der Programmierumgebung freigegeben hat. Es ist ein Bugfix-Release ohne neue Features.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022083001.html</link>
</item>
<item>
	<title>Nostalgie pur: Waldbrandsimulation im Terminal (mit Python)</title>
		<description>
			<![CDATA[<% imageref("forestfirebext-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082901.html"><img src="http://blog.schockwellenreiter.de/images/forestfirebext-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all"><i>Al Sweigart</i> hat ein Buch geschrieben. Das ist keine wirkliche Meldung, denn <i>Al Sweigart</i> ist ein äußerst umtriebiger Autor, der als Verfasser etlicher Programmierbücher (meist zu Python) bekannt ist, von denen nicht nur viele bei No Starch Press erschienen sind, sondern die auch kostenlos von der Homepage des Autors heruntergeladen werden können. Aber – und das ist die Meldung – eines dieser Bücher heißt »The Big Book of Small Python Projects« und die Programme in diesem Buch sind so konzipiert, daß sie alle im Terminal ausgeführt werden können. Mehr Nostalgie geht eigentlich nicht.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082901.html</link>
</item>
<item>
	<title>Zurück zu Thonny: Numpy Random und Py5</title>
		<description>
			<![CDATA[<% imageref("py5randrect-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082802.html"><img src="http://blog.schockwellenreiter.de/images/py5randrect-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nachdem ich mich die letzten Tage an Py5 und JupyterLab abgearbeitet habe, möchte ich nun zu meinem Projekt »The Nature of Py5« zurückkehren und als Entwicklungsumgebung dafür wieder Thonny einsetzen. Wie ich gezeigt habe, ist dies keine Einschränkung – alle Scripte laufen unmodifiziert auch unter JupyterLab (im »Imported Mode«).]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082802.html</link>
</item>
<item>
	<title>Worknote: Py5 und JupyterLab – eine Zusammenfassung</title>
		<description>
			<![CDATA[<% imageref("py5jupytermodulemode-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082801.html"><img src="http://blog.schockwellenreiter.de/images/py5jupytermodulemode-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Da ich mich – mangels Übung – ziemlich schwer damit getan hatte, Py5 und JupyterLab auf meinem Rechner zu einer Zusammenarbeit zu bewegen, möchte ich hier noch einmal alle erforderlichen Schritte dokumentieren, damit ich nicht jedesmal <del>danach googeln</del> die Suchmaschine meines Vertrauens danach befragen muß:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082801.html</link>
</item>
<item>
	<title>Zurück zu den Wurzeln: Abenteuer (mit und in) HTML</title>
		<description>
			<![CDATA[<% imageref("seagull-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082702.html"><img src="http://blog.schockwellenreiter.de/images/seagull-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Du bist eine Möwe, also kreische wie eine Möwe! Gestern kam mir eine Meldung unter, die mich an mein Vorhaben erinnerte, wie früher Webseiten ohne Sinn und Verstand zu basteln, die einfach sind und nichts zu bedeuten haben. Denn wie BoingBoing berichtete, haben zwei Menschen ein absolut einfaches, völlig sinnbefreites, textbasiertes Abenteuerspiel für den Browser entwickelt, das trotz aller oder gerade wegen seiner Einfachheit eine existentialistische Anmutung besitzt.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082702.html</link>
</item>
<item>
	<title>Neue Frage: Was ist mit Py5 und JupyterLab Desktop?</title>
		<description>
			<![CDATA[<% imageref("jlabdeskpy5env-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082701.html"><img src="http://blog.schockwellenreiter.de/images/jlabdeskpy5env-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Monster JupyterLab und Py5 sind gezähmt. Jetzt ist nur noch eine Frage offen: Was ist mit Py5 und JupyterLab Desktop? Der Macher von Py5 hatte dafür auf Twitter eine Lösung gefunden: Eine eigene Py5-Umgebung, so wie sie auch in der Dokumentation von JuypterLab Desktop vorgeschlagen wird. Bei ihm funktionierte das auch – wie er mit einem Screenshot bewies.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082701.html</link>
</item>
<item>
	<title>Worknote: Das Monster Py5 in JupyterLab gezähmt</title>
		<description>
			<![CDATA[<% imageref("py5importedmode-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082601.html"><img src="http://blog.schockwellenreiter.de/images/py5importedmode-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nachdem eine Welle der Hilfsbereitschaft in meinen Kommentaren und den diversen sozialen Medien mich nahezu überschwemmt hat, konnte ich ja nicht anders und mußte das Problem Py5 und JupyterLab neu angehen. Der entscheidende Hinweis kam vom Schöpfer von Py5 selber, der mich darauf hinwies, daß im »Imported Mode« (das war der, an dem ich gescheitert war), der Aufruf von <code>run_sketch()</code> am Ende des Skriptes obligatorisch sei. Hätte ich auch von alleine darauf kommen können, zumal es auch so in der Dokumentation steht.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082601.html</link>
</item>
<item>
	<title>Auf dem Weg zur World Markdown mit PanWriter? Wohl eher nicht!</title>
		<description>
			<![CDATA[<% imageref("panwritertestmaster-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082503.html"><img src="http://blog.schockwellenreiter.de/images/panwritertestmaster-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Konstantin, den ich überredet hatte, mit mir zusammen die <del>Weltherrschaft anzustreben</del> World Markdown zu erkunden (statt LaTeX zu lernen), machte mich auf PanWriter aufmerksam. PanWriter, das ein freier (GPL, Quellcode auf GitHub), plattformübergreifender (da Electron-Anweendung) Markdown-Editor sein will, hatte ich Anfang des Jahres schon einmal auf dem Schirm. Seine Besonderheit – wenn nicht gar das Alleinstellungsmerkmal der Software – liegt darin, daß sie auf ein im Hintergrund werkelndes Pandoc zurückgreifen kann, das den Im- und Export nach unzähligen Formaten ermöglicht.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082503.html</link>
</item>
<item>
	<title>Back to the Roots: AppleSoft Basic Snake Game</title>
		<description>
			<![CDATA[<% imageref("applesoftbasic-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082502.html"><img src="http://blog.schockwellenreiter.de/images/applesoftbasic-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wie ich hier vor einigen Jahren schon einmal schrieb, werde ich – auch wenn es manchmal verlockend erscheint – sicher nicht meine verschütteten BASIC-Kenntnisse wieder auffrischen wollen und mich mit irgendwelchen Emulatoren herumquälen. Aber da ich mindestens einen kenne, der sich an der Uni Bonn die nächsten drei Jahre mit dem Thema »Computerphilologie: Technische Lektüren der BASIC-Programmierkultur« herumschlagen wird, kann ich diese Meldung nicht links liegen lassen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082502.html</link>
</item>
<item>
	<title>Neue Mozilla Thunderbird Version 102.2.0 freigegeben</title>
		<description>
			<![CDATA[<% imageref("birdswosindsie-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082501.html"><img src="http://blog.schockwellenreiter.de/images/birdswosindsie-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wie (fast) immer folgt kurz nach dem Feuerfuchs der <del>Donnerbalken</del> Donnervogel: Die Entwickler des Mozilla Thunderbird haben das Update auf die Version 102.2.0 freigegeben.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082501.html</link>
</item>
<item>
	<title>Thonny in der neuer Version 4.0.0 ist draußen</title>
		<description>
			<![CDATA[<% imageref("thonny4-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082401.html"><img src="http://blog.schockwellenreiter.de/images/thonny4-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Fast jede Beta-Version erreicht einmal ihr geplantes Ende: So auch Thonny 4.0.0b3, das bei mir monatelang als Texteditor und Arbeitspferd speziell für Py5 und Pygame Zero sowohl unter macOS wie auch auf meinem Chromebook seine zuverlässigen und treuen Dienste leistete. Der Editor überraschte mich gestern mit der Meldung, daß die Version 4.0.0 mit einem eingebauten Python 3.10 endlich freigegeben sei.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082401.html</link>
</item>
<item>
	<title>Py5 und Jupyter/JupyterLab: Ein mittleres Fiasko</title>
		<description>
			<![CDATA[<% imageref("jupyterlabpy5-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082302.html"><img src="http://blog.schockwellenreiter.de/images/jupyterlabpy5-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich hatte gestern abend und heute vormittag versucht, Py5 und Jupyter respektive JupyterLab zu verbandeln. Dieser Versuch endete leider in einem mittleren Fiasko. Dabei liegt das Problem nicht bei Py5, sondern bei Jupyter, das einfach zu viel Geheimwissen voraussetzt und so viele Fall- und Hintertürchen besitzt, daß ein Gelegenheitsuser wie ich einfach überfordert ist. Doch der Reihe nach:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082302.html</link>
</item>
<item>
	<title>Mozilla veröffentlicht neue Firefox-Version</title>
		<description>
			<![CDATA[<% imageref("buergerstrassenrudel-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082301.html"><img src="http://blog.schockwellenreiter.de/images/buergerstrassenrudel-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Entwickler des Mozilla Firefox haben die neue Firefox-Versionen 104.0 und ESR 91.13 veröffentlicht und darin auch wieder Sicherheitslücken behoben.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082301.html</link>
</item>
<item>
	<title>Nachdenken über »The Nature of Py5« – erste Konkretisierungen</title>
		<description>
			<![CDATA[<% imageref("bouncingballpy5-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082201.html"><img src="http://blog.schockwellenreiter.de/images/bouncingballpy5-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mein gestern spontan gefaßter Plan, <i>Daniel Siffmans</i> Processing-Buch »The Nature of Code« zu überarbeiten und in einer Py5-Version zu veröffentlichen, hat mir keine Ruhe gelassen. Nachdem ich noch einmal durch die Py5-Dokumentation gesteppt bin und durch die gestern erwähnte Python- und Processing-Literatur (und einige Bücher mehr, zum Beispiel <i>Peter Farrells</i> »Math Adventures with Python«, <i>Amit Sahas</i> »Doing Math with Python« oder <i>Tristan Bunns</i> (aka <i>TabReturn</i>) »Learn Python Visually«) geblättert hatte. kristallisierte sich ein erster Gliederungsversuch für die ersten Kapitel heraus:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082201.html</link>
</item>
<item>
	<title>Thinking about Refactoring »The Nature of Code« (with Py5?)</title>
		<description>
			<![CDATA[<% imageref("py5vector-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082101.html"><img src="http://blog.schockwellenreiter.de/images/py5vector-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Als ich heute vormittag mit meinem Chromebook auf unserer Terrasse saß und mich freute, daß ich dort mit Python, Py5 und Thonny spielen konnte, kam mir der verwegene Gedanke, daß ich ja Daniel Shiffmans in Processing implementiertes, wunderbares Buch »The Nature of Code« nach Py5 portieren könnte. Der Gedanke mag zwar verwegen sein, wirklich neu ist er nicht. Denn seit 2018 versucht Shiffman die Zeit zu finden, eine Neuinterpretation dieses Buches nach P5.js zu schreiben und inspiriert davon und parallel dazu habe ich mich auch schon an diversen Python-Ports versucht (zum Beispiel nach Processing.py, der Python Arcade-Bibliothek, Pygame Zero oder TigerJython).]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082101.html</link>
</item>
<item>
	<title>Zum Wochenende: Video-Tutorials zu Ren’Py</title>
		<description>
			<![CDATA[<% imageref("renpyzeil-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082002.html"><img src="http://blog.schockwellenreiter.de/images/renpyzeil-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Da es mir ja gelungen war, selbst Ren’Py als Entwicklungsumgebung lokal auf meinem Chromebook zu installieren, möchte ich damit natürlich auch etwas anstellen. Und bei der Befragung der Suchmaschine meines Vertrauens wurden mir ein paar nette Video-Tutorials angezeigt, die ich für brauchbar hielt und die ich Euch nicht vorenthalten möchte:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082002.html</link>
</item>
<item>
	<title>Worknote: LaTeX or not to LaTeX – oder doch besser Pandoc?</title>
		<description>
			<![CDATA[<% imageref("airmail1929-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022082001.html"><img src="http://blog.schockwellenreiter.de/images/airmail1929-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Von einem guten Freund und Journalisten bekam ich die Frage gestellt, ob ich es für sinnvoll halte, daß er ganz ohne mathematische und naturwissenschaftliche Ambitionen LaTeX lernen solle. Mir wurde die Frage gestellt, weil ich in seinen Augen (unter den Blinden ist der Einäugige König) so etwas wie ein LaTeX-Guru wäre. Um diesem Ruf wenigstens im Ansatz gerecht zu werden, hier meine Antwort (redaktionell bearbeitet):]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022082001.html</link>
</item>
<item>
	<title>Apple veröffentlicht Sicherheitsupdate auf macOS Big Sur und Catalina</title>
		<description>
			<![CDATA[<% imageref("gehwegelidl-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081902.html"><img src="http://blog.schockwellenreiter.de/images/gehwegelidl-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wie erwartet hat Apple gestern auch das Sicherheitsupdate für macOS Big Sur und Catalina freigegeben (Safari 15.6.1). Näheres dazu findet sich in diesem Support-Dokument.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081902.html</link>
</item>
<item>
	<title>Pazifismus ist kein Verbrechen</title>
		<description>
			<![CDATA[<% imageref("dertriumphdestodes-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081901.html"><img src="http://blog.schockwellenreiter.de/images/dertriumphdestodes-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Unter dieser Überschrift hatte ich auf Facebook eine ziemlich sinnlose Diskussion losgetreten, die dazu führte, daß eine Meute von Ostlandrittern losheulte, die am liebsten Rache für Stalingrad nehmen und die Kornkammer des Reiches heim in die EU holen wollten. Dabei war das schon 1941 in die Hose gegangen. Doch die Horde gab sie sich nicht einmal die Mühe, den Schaum von ihrem Mund zu wischen. Da mich die Frage jedoch bewegt, möchte ich mich hier – wo ich eine größere Kontrolle darüber habe, wer seinen Senf dazugeben kann – noch einmal in Ruhe zu diesem Punkt auslassen. Danach ist die Sache für mich gegessen. Also, das Thema heute lautet: Pazifismus ist kein Verbrechen, auch wenn <i>Sascha Lobo</i> da anderer Ansicht ist.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081901.html</link>
</item>
<item>
	<title>Monterey und Mobil: Zwei Apple Updates</title>
		<description>
			<![CDATA[<% imageref("escootergruen-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081802.html"><img src="http://blog.schockwellenreiter.de/images/escootergruen-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Apple hat gestern abend noch zwei Sicherheitsupdates herausgehauen. Ich hatte mit der Meldung noch gewartet, bis die eigentlich üblichen Updates für die noch unterstützten Betriebssystemversionen nachgeschoben werden. Das ist aber bis jetzt nicht der Fall, also gibt es jetzt erst einmal die Updates, von denen ich weiß:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081802.html</link>
</item>
<item>
	<title>Neu in meinem Wiki: »Mein« Chromebook und seine Tastaturbelegung</title>
		<description>
			<![CDATA[<% imageref("chromebooktic80terrasse-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081801.html"><img src="http://blog.schockwellenreiter.de/images/chromebooktic80terrasse-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nachdem ich gestern abend mal wieder eine Art Blackout und (gefühlt) stundenlang <del>Google</del> DuckDuckGo durchsucht hatte, um die Tastaturbelegung meines Chroembooks für die Sonderzeichen zu bekommen (speziell fand ich einfach die Tilde (<code>~</code>) nicht), war ich es leid und habe dem Chromebook und seiner Tastaturbelegung eine Seite in meinem Wiki spendiert, in der auch die wichtigsten Tastenkürzel stehen:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081801.html</link>
</item>
<item>
	<title>Microsoft veröffentlicht Update 16.64 für Office for Mac</title>
		<description>
			<![CDATA[<% imageref("escooternachschub-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081703.html"><img src="http://blog.schockwellenreiter.de/images/escooternachschub-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Microsoft liefert mit dem Update auf 16.64 für Office 365, 2021 und 2019 for Mac natürlich auch wieder aktuelle Sicherheitskorrekturen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081703.html</link>
</item>
<item>
	<title>Worknote: Spritesheets, Py5, copy() und Open Pixels</title>
		<description>
			<![CDATA[<% imageref("openpixels-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081702.html"><img src="http://blog.schockwellenreiter.de/images/openpixels-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich weiß, es geht momentan ein wenig durcheinander und ohne rechte Struktur auf diesen Seiten zu. Aber mir geht nicht nur momentan einiges durch den Kopf, sondern es kommen mir auch von außen viele Ideen unter, die ich einfach mal ausprobieren muß, damit ich sie nicht vergesse. So auch dieses Beispiel aus der Werkstatt von <i>Silveira Neto</i>, in dem er zeigt, wie man einzelne Sprites aus einem Spritesheet mit Hilfe des <code>copy()</code>-Befehls in Processing herauslöst und anspricht. Auch wenn ich mir eigentlich sicher war, daß das Beispiel auch in Py5 funktionieren würde, mußte ich es dennoch ausprobieren:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081702.html</link>
</item>
<item>
	<title>Google korrigiert in Version 104 seines Browsers Chrome auch wieder Sicherheitslücken</title>
		<description>
			<![CDATA[<% imageref("slalomparcours-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081701.html"><img src="http://blog.schockwellenreiter.de/images/slalomparcours-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber – wenn es pressiert – auch hier geladen werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081701.html</link>
</item>
<item>
	<title>Retrogaming, Python und KI: Pygame Zero Hack (Stage 0)</title>
		<description>
			<![CDATA[<% imageref("pgzhack-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081601.html"><img src="http://blog.schockwellenreiter.de/images/pgzhack-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich weiß noch nicht, wo das hinführt und was daraus wird. Ich weiß nur, daß ich schon viel zu lange nichts mehr mit Pygame Zero angestellt hatte und daß ich die Nevanda-, respektive die UnTekhack-Tiles ganz witzig fand und daher unbedingt etwas mit ihnen anstellen wollte. Dies hier ist das Ergebnis:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081601.html</link>
</item>
<item>
	<title>Neu in meinem Wiki: Trizbort.io, ein Game-Mapping-Werkzeug</title>
		<description>
			<![CDATA[<% imageref("trizbortio-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081301.html"><img src="http://blog.schockwellenreiter.de/images/trizbortio-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ein Leser des <i>Schockwellenreiters</i> meinte, daß ich sicher Trizbort kennen würde, eine Kartensoftware für Interactive Fiction und Adventure Games. Ich muß gestehen, daß ich manches Mal doch beschämt bin, wenn meine Expertise gnadenlos überschätzt wird. So auch dieses Mal: Von Trizbort hatte ich noch nie etwas gehört, zumal das Teil auch nur unter Windows läuft. Aber egal, im nächsten Satz machte mich dieser Leser auf <b>Trizbort.io</b> aufmerksam, einer noch recht jungen Online-Version dieses Programms, die natürlich auf allem läuft, was mit einem JavaScript-fähigen Browser ausgestattet werden kann.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081301.html</link>
</item>
<item>
	<title>Codierte Kunst mit Py5: Hommage â Vasa Mihich</title>
		<description>
			<![CDATA[<% imageref("hommageanvasamihich-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081101.html"><img src="http://blog.schockwellenreiter.de/images/hommageanvasamihich-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Das konnte nicht warten, ich mußte unbedingt etwas mit meinem frisch installierten Py5 anstellen, und was liegt näher, als mich weiter in meine Experimente zu »Generativer Art zwischen zwei Pappedeckel« zu vertiefen. Und so habe ich das auf diesen Seiten schon mehrfach gelobte Buch »Codierte Kunst« von <i>Joachim Wedekind</i> aus seinem Dornröschenschlaf im Bücherregal gerissen und daraus die <i>Hommage â Vasa Mihich</i> (Seite 75ff) nachprogrammiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081101.html</link>
</item>
<item>
	<title>Py5 Update abgeschlossen (auch auf meinem Chromebook)</title>
		<description>
			<![CDATA[<% imageref("py5081transparenz-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022081001.html"><img src="http://blog.schockwellenreiter.de/images/py5081transparenz-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Das Update auf die neue Version 0.1.8a1 (siehe Beitrag von gestern) von Py5, der Python3-Version von Processing, verlief ohne Probleme (auch auf meinem Chromebook). Erstes Testergebnis: Die neuen Matplotlib-Farben können auch Transparenz.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022081001.html</link>
</item>
<item>
	<title>Noch ein Update: Neues Release Py5 0.8.1a1</title>
		<description>
			<![CDATA[<% imageref("randcirc3-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080903.html"><img src="http://blog.schockwellenreiter.de/images/randcirc3-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Heute jagt ein Update das nächste: Nach Bitsy und Adobe meldet nun auch Py5, die von mir geliebte Python3-Version von Processing, daß sie mit der Version 0.8.1a1 ein neues Release freigegeben hat. Neben einer Anzahl von Bug-Fixes gibt es etwa eine Handvoll neuer Features, von denen ich diese beiden besonders interessant für meine Projekte finde:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080903.html</link>
</item>
<item>
	<title>Sicherheitsupdate für Adobe Reader und Acrobat (APSB22-39)</title>
		<description>
			<![CDATA[<% imageref("gruenrot-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080902.html"><img src="http://blog.schockwellenreiter.de/images/gruenrot-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Adobe liefert Korrekturen für Sicherheitslücken im Reader und in Acrobat (unter Windows und Mac).]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080902.html</link>
</item>
<item>
	<title>Neu im August: Großes Update auf Bitsy 8</title>
		<description>
			<![CDATA[<% imageref("bitsy81-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080901.html"><img src="http://blog.schockwellenreiter.de/images/bitsy81-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Das Video gestern zeigte mir, daß Bitsy mit einer neuen Homepage (und einer neuen URL) an den Start gegangen war. Da ich ja Bitsy lokal auf meinem (MAMP-) Server betreibe, sind diese Änderungen komplett an mir vorbeigegangen. Vor allem aber auch, daß es mittlerweile eine neue und völlig überarbeitete Version 8 (genauer: 8.1) der minimalistischen Game-Engine gibt. Die mußte ich natürlich <i>stante pede</i> auch auf meinem lokalen Server installeren.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080901.html</link>
</item>
<item>
	<title>NarraScope 2022: Spiele schreiben heißt Geschichten zu erzählen</title>
		<description>
			<![CDATA[<% imageref("hawkandpuma-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080801.html"><img src="http://blog.schockwellenreiter.de/images/hawkandpuma-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Dies ist jedenfalls das Credo der NarraScope (aber auch meines), einer Konferenz, die Computerspielen huldigt, die eben dies tun – interaktive Abenteuer, die Geschichten erzählen. Nach einer pandemiebedingten Pause 2021 fand sie dieses Jahr wieder statt, wenn auch »nur« online. Dieses Format hat auf jeden Fall dem Vorteil, daß die Vorträge als Videos vorliegen und so einem breiten Publikum zugänglich sind.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080801.html</link>
</item>
<item>
	<title>Worknote und Thread: Ren’Py, Geany und das Chromebook</title>
		<description>
			<![CDATA[<% imageref("chromebookgeanyrenpy-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080701.html"><img src="http://blog.schockwellenreiter.de/images/chromebookgeanyrenpy-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich habe die Entdeckungsreise durch die Möglichkeiten meines Chromebooks noch lange nicht abgeschlossen. Mein heutiges Tuning war ein Versuch, das Arbeiten mit Ren’Py möglichst einfach zu gestalten. Der erste Schritt war leichter als ich dachte: Ren’Py startete per Default Geany als seinen »System«-Texteditor. Warum, weiß ich nicht, aber ich hatte damit gerechnet, hier doch noch einiges an Bastelarbeit investieren zu müssen und so nahm ich dies einfach dankbar an.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080701.html</link>
</item>
<item>
	<title>StackEdit auf meinem Chromebook und auf meinem MacBook Pro</title>
		<description>
			<![CDATA[<% imageref("chromebookbiotop-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080601.html"><img src="http://blog.schockwellenreiter.de/images/chromebookbiotop-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Eine weitere, extrem einfache Möglichkeit, (Markdown-) Notizen zwischen meinem Chromebook und meinem MacBook Pro auszutauschen und zu synchronisieren, ist der freie Online-Editor StackEdit (Apache-Lizenz, Quellcode auf GitHub). Den hatte ich während meiner Reha im Dezember letzten Jahres schon einmal getestet, aber das Teil irgendwie aus den Augen verloren. Erst seitdem ich mein Chromebook zum schnellen Erfassen meiner Notizen auf unserer Terrasse nutze, die ich anschließend auf meinem MacBook Pro in unserem Arbeitszimmer weiterbearbeiten möchte, kam mir das Teil wieder in den Sinn.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080601.html</link>
</item>
<item>
	<title>Abenteuer TIC-80: Steuerung der Spielfigur mit der Tastatur</title>
		<description>
			<![CDATA[<% imageref("playermovmap-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080501.html"><img src="http://blog.schockwellenreiter.de/images/playermovmap-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich hatte Euch gewarnt! Die Programmierung der kleinen Fantasiekonsole TIC-80 macht mir soviel Spaß, daß ich Euch unbedingt an meinen Fortschritten teilhaben lassen muß. Da müßt Ihr jetzt durch, da habt Ihr keine andere Chance. In meinem ersten Beitrag möchte ich Euch zeigen, wie Ihr die Spielfigur mit Hilfe der Tastatur durch das Spielfeld bewegen könnt – auch diagonal:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080501.html</link>
</item>
<item>
	<title>Tutorial: Eingabedialoge (Schieberegler) in TigerJython</title>
		<description>
			<![CDATA[<% imageref("slidertjchromebook-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080401.html"><img src="http://blog.schockwellenreiter.de/images/slidertjchromebook-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">»Schuld« an diesem Tutorial besitzt das (schon mehrfach lobend erwähnte) Buch »Mathematische Algorithmen mit Python« von <i>Veit Steinkamp</i>. Darin wird nämich (auf den Seiten 243 ff) eine Eigenschaft der Matplotlib vorgestellt, die ich bis dahin noch nicht kannte: Die Matplotlib kann nämlich GUI-Elemente (Widgets) erzeugen und die Werte aus diesen Widgets dynamisch während der Laufzeit an das Programm (und damit auch an die Graphen der Matplotlib) übergeben.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080401.html</link>
</item>
<item>
	<title>Creative Coding mit Python und Processing – eine Übersicht und (m)ein Programm</title>
		<description>
			<![CDATA[<% imageref("pypresentation-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080303.html"><img src="http://blog.schockwellenreiter.de/images/pypresentation-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Im Python- und Processing-Ökosystem ist in den letzten Monaten soviel in Bewegung geraten, daß man leicht die Übersicht verlieren kann. Da gibt es Processing.py, Py5, P5py und pyp5js – um nur die wichtigsten Akteure zu nennen. Damit wir dennoch den Durchblick behalten, hat <i>Tristan Bunn</i> unter dem Titel »Overview of Tools Combining Processing and Python« Material zusammengestellt, das er für einen Vortrag auf der diesjährigen SIGGRAPH verwendet hat (unter diesem Link gibt es auch ein Video zum Download, das ich Euch sehr empfehle).]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080303.html</link>
</item>
<item>
	<title>Google korrigiert auch wieder Sicherheitslöcher in Chrome</title>
		<description>
			<![CDATA[<% imageref("gehwegzuparker20220803-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080302.html"><img src="http://blog.schockwellenreiter.de/images/gehwegzuparker20220803-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google veröffentlicht eine neue Version 104 (104.0.5112.79/80/81) seines Browsers Chrome und schließt auch wieder Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080302.html</link>
</item>
<item>
	<title>Rant: Aber bitte mit Farbe (oder: Rheinwerk veräppelt seine Kunden)!</title>
		<description>
			<![CDATA[<% imageref("aberbittemitfarbe-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080301.html"><img src="http://blog.schockwellenreiter.de/images/aberbittemitfarbe-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Zwar besitze ich von dem Buch »Let’s code Python« von <i>Hauke Fehr</i> schon die erste Auflage aus dem Jahr 2018, aber da ich mir mein kindliches Gemüt bewahrt habe und auf bunte Bilder stehe, hatte ich mir vor wenigen Tagen die zweite Auflage vom Mai dieses Jahres bestellt. Denn hier versprach die Verlagswerbung auf den Seiten von Amazon, daß die Neuauflage komplett in Farbe sei und zum Zeitpunkt meiner Bestellung waren da auch noch bunte Produktphotos mit farbigen Seiten zu sehen (die sind mittlerweile verschwunden):]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080301.html</link>
</item>
<item>
	<title>Google schließt am »August-Patchday« wieder Sicherheitslücken in Android</title>
		<description>
			<![CDATA[<% imageref("keindurchkommen-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080202.html"><img src="http://blog.schockwellenreiter.de/images/keindurchkommen-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google hat mit seinem monatlichen Sicherheitsupdate für Android (und damit auch auf seinen Pixel-Geräten) wieder Sicherheitslücken geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080202.html</link>
</item>
<item>
	<title>Mit der Fantasiekonsole TIC-80 (auf dem Chromebook) spielen</title>
		<description>
			<![CDATA[<% imageref("chasingevading-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080201.html"><img src="http://blog.schockwellenreiter.de/images/chasingevading-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nachdem es mir am Wochenende gelungen war, die kleine Fantasiekonsole TIC-80 auch auf meinem Chromebook zu installieren, mußte ich damit auch ein wenig spielen und ich hatte dabei viel Spaß. Aber natürlich ist alles noch relativ neu für mich. Damit ich nicht immer wieder <del>googeln</del> DuckDuckGo befragen muß, hier ein paar gesammelte Hinweise zum Nachschlagen:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080201.html</link>
</item>
<item>
	<title>Sicherheitsupdate des Foxit Reader (PDF Reader) auf 12.0.1</title>
		<description>
			<![CDATA[<% imageref("limetier-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080102.html"><img src="http://blog.schockwellenreiter.de/images/limetier-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mit der Version 12.0.1 des (nicht nur) unter Windows beliebten PDF Betrachters Foxit Reader haben die Entwickler auch wieder Schwachstellen behoben.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080102.html</link>
</item>
<item>
	<title>Die Zahlen für den Monat Juli 2022 (aka Mediadaten)</title>
		<description>
			<![CDATA[<% imageref("mediadaten202207-s") %><a href="http://blog.schockwellenreiter.de/2022/08/2022080101.html"><img src="http://blog.schockwellenreiter.de/images/mediadaten202207-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Schon wieder hat ein neuer Monat begonnen und daher ist es auch wieder Zeit für ein paar Zahlen, die hochtrabend auch manches Mal <i>Mediadaten</i> genannt werden: Im Juli 2022 hatte der <i>Schockwellenreiter</i> laut seinem nicht immer ganz zuverlässigen, aber dafür (hoffentlich!) datenschutzkonformen Neugiertool exakt <b>4.145 Besucherinnen und Besucher</b> mir insgesamt <b>8.782 Seitenansichten</b>. Natürlich täuscht die Exaktheit der Ziffern eine Genauigkeit der Zahlen nur vor, aber genauso natürlich freue ich mich über alle, die auf diese Seiten gefunden haben und bedanke mich bei allen meinen Leserinnen und Lesern.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/08/2022080101.html</link>
</item>
