#title "RSS-Feed März 2022"
#fileextension ".xml"
#kramdown false
#template "blank"
#xml true


<item>
	<title>The Nature of Code: Mutual Attraction</title>
	<description>
			<![CDATA[<% imageref("mutualattraction-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022033102.html"><img src="http://blog.schockwellenreiter.de/images/mutualattraction-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Es gibt eine neue Folge von <i>Daniel Shiffmans</i> lange vernachlässigtem Projekt »The Nature of Code Reloaded«, in dem er nicht nur alle Processing (Java)-Sketche nach P5.js portieren, sondern diese auch aktualisieren und um neue Erkenntnisse erweitern möchte.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022033102.html</link>
</item>
<item>
	<title>Eine kleine Linkschleuder zum Thema »Generative Art«</title>
	<description>
			<![CDATA[<% imageref("comichelvetic-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022033101.html"><img src="http://blog.schockwellenreiter.de/images/comichelvetic-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Da das Wochenende bedrohlich näher rückt, gibt es heute eine kleine Linkschleuder zum Thema »Creative Coding«, die mir – und Euch – Inspirationen vermitteln soll, was man an den voraussichtlich kalten und trüben Tagen am Schreibtisch vor dem Rechner alternativ anstellen kann.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022033101.html</link>
</item>
<item>
	<title>Ein neues Zuhause für den Schockwellenreiter? (Update)</title>
	<description>
			<![CDATA[<% imageref("cloudflarepages-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022033002.html"><img src="http://blog.schockwellenreiter.de/images/cloudflarepages-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Kaum hatte ich meine Überlegungen niedergeschrieben, wo ich mein <del>Blog</del> Kritzelheft denn in Zukunft hosten solle, flatterte dieser etwas ältere Beitrag (mein Feedreader verschluckt sich manchmal und dann spuckt er einen Feed erst Wochen später aus) von <i>Flavio Copes</i> bei mir herein: »Moved the blog hosting to Cloudflare Pages«, in dem er erklärt, warum er von Netlify zu Cloudflare Pages gewechselt ist. Und seine Argumente überzeugen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022033002.html</link>
</item>
<item>
	<title>Google veröffentlicht die Jubiläumsausgabe 100 von Chrome</title>
	<description>
			<![CDATA[<% imageref("dreibaeren-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022033001.html"><img src="http://blog.schockwellenreiter.de/images/dreibaeren-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google veröffentlicht die Jubiläumsausgabe 100 (100.0.4896.60) seines Browsers Chrome und schließt kritische Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022033001.html</link>
</item>
<item>
	<title>Ein weiteres Experiment mit der Nodebox 1: Denslow im Gitter</title>
	<description>
			<![CDATA[<% imageref("denslowgitter-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032902.html"><img src="http://blog.schockwellenreiter.de/images/denslowgitter-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wie versprochen habe ich mir die Grid-Bibliothek der Knotenschachtel für mein nächstes Nodebox-Experiment vorgenommen. Als Hommage an den aktuellen Künstler des Monats im <i>Schockwellenreiter</i> – W.&nbsp;W.&nbsp;Denslow – habe ich Illustrationen von ihm in ein Gitter gepreßt.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032902.html</link>
</item>
<item>
	<title>Python Updates und News (TigerJython, Processing und Nodebox)</title>
	<description>
			<![CDATA[<% imageref("webtiger-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032901.html"><img src="http://blog.schockwellenreiter.de/images/webtiger-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all"><b>Go Web, Young Man</b>: WebTigerJython ist ein Online-TigerJython für Chromebook- und Mobile-Nutzer. Ich weiß nicht, was für eine Engine dahintersteckt (Jython scheint es nicht zu sein). Momentan sind die Funktionen noch etwas eingeschränkt (das Teil bringt bisher nur Unterstützung für <code>gTurtle</code> und <code>gPanel</code> – letzteres noch nicht getestet – mit), kann dafür aber Python&nbsp;3 und Emojis. Der Editor wird im Web-Browser mit der URL webtigerjython.ethz.ch gestartet.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032901.html</link>
</item>
<item>
	<title>Ein neues Zuhause für den Schockwellenreiter?</title>
	<description>
			<![CDATA[<% imageref("github-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032801.html"><img src="http://blog.schockwellenreiter.de/images/github-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Je älter ich werde, desto häufiger ertappe ich mich bei der Frage, was eigentlich mit meinen vielen Webseiten passiert, wenn ich – aus welchen Gründen auch immer, ich will ja nicht gleich das Schlimmste annehmen – die Hostingkosten nicht mehr aufbringen kann? Besonders nach meinem Schlaganfall im Oktober letzten Jahres kommt mir der Gedanke immer häufiger.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032801.html</link>
</item>
<item>
	<title>Fischiges (runter vom Sofa und ran an die Fische!)</title>
	<description>
			<![CDATA[<% imageref("mitpfeife-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032702.html"><img src="http://blog.schockwellenreiter.de/images/mitpfeife-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Beim Aufräumen gestern hatte ich zufällig den Artikel »HTML/CSS: The Fun Parts (Make a Personal Web Page)« von <i>Kelly Lougheed</i> vom Februar 2018 ausgegraben (ich hatte ihn damals ausgedruckt, denn ich bin ein Internetausdrucker&nbsp;🤡). Neben einem Grundkurs in HTML und CSS ist er eine ziemlich gute Einführung in Glitch, die – in der <i>Community Edition</i> – kostenlose Online-IDE für statische Seiten mit JavaScript und Node.js, daher ein ideales Experimentierfeld für P5.js-Anwendungen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032702.html</link>
</item>
<item>
	<title>Re-enactment: Hommage an Piet Mondrian mit Processing.py</title>
	<description>
			<![CDATA[<% imageref("hommagemondrian-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032701.html"><img src="http://blog.schockwellenreiter.de/images/hommagemondrian-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Meine Idee, mit Generative Art auf Papier dem derzeit grassierenden NFT-Hype den Stinkefinger zu zeigen hat mir – zusammen mit der Tatsache, daß mein Processing.py jetzt wieder mit Processing 4 spielt – keine Ruhe gelassen. Und so habe ich mich gestern abend hingesetzt und eine kleine Hommage an <i>Piet Mondrian</i> programmiert.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032701.html</link>
</item>
<item>
	<title>Google veröffentlicht die neue Version 99 seines Browsers Chrome</title>
	<description>
			<![CDATA[<% imageref("babybunting-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032601.html"><img src="http://blog.schockwellenreiter.de/images/babybunting-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber – wenn es pressiert – auch hier geladen werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032601.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("spitzschatz-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032501.html"><img src="http://blog.schockwellenreiter.de/images/spitzschatz-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Es ist schon spät und ich habe heute ein wenig mit ein paar halbgaren Projektideen die Zeit verplempert und mein zweites Gehirn mit ihnen gefüttert, daher gibt es jetzt erst einmal auf die Schnelle das freitägliche Hundebild. Es soll Euch wie gewohnt darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird, da mich die angedachten Projekte in der nächsten Zeit noch weiter beschäftigen. Ich hoffe aber, daß ich Euch spätestens Anfang der nächsten Woche erste Ergebnisse (und seien es nur <i>Proposals</i>) mitteilen kann.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032501.html</link>
</item>
<item>
	<title>Zweieinhalb Stunden Python: Fraktale, Turtle und Pygame (Zero)</title>
	<description>
			<![CDATA[<% imageref("turtlefraktale-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032402.html"><img src="http://blog.schockwellenreiter.de/images/turtlefraktale-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Knotenschachtel lockt und da Processing.py ja jetzt auch wieder mit Processing 4 spielt, ruft hier eine weitere Aufgabe nach mir, in der eventuell auch noch Py5 mit Thonny involviert ist. Man sollte meinen, daß dies genug für die nächsten paar Tage ist, aber nein, da habe ich mir auch noch drei (Python-) Video-Tutorials aus meiner ständig anwachsenden »Später ansehen«-Liste herausgesucht, bei denen ich das »später« durch »jetzt« ersetzen will. Diese drei insgesamt zweieinhalb stündigen Tutorials behandeln die Themen Pygame Zero, Fraktale mit Pythons Turtle und Kollisionserkennung in Videospielen mit Pygame.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032402.html</link>
</item>
<item>
	<title>Processing 4.0b7 spielt wieder mit Processing.py</title>
	<description>
			<![CDATA[<% imageref("processing4b7-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032401.html"><img src="http://blog.schockwellenreiter.de/images/processing4b7-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich bekomme auch nicht immer alles mit. So habe ich heute erst per Zufall erfahren, daß die Beta 7 von Processing 4 schon seit 20 Tagen zum Download bereitsteht. Und sie soll wieder mit Processing.py spielen. Spoiler: Sie tut es.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032401.html</link>
</item>
<item>
	<title>Experimente mit Nodebox 1: Die Knotenschachtel hinter Gittern</title>
	<description>
			<![CDATA[<% imageref("gridnodebox-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032302.html"><img src="http://blog.schockwellenreiter.de/images/gridnodebox-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Meine Entdeckungsreise durch den Inhalt der Knotenschachtel (aka Nodebox 1) geht weiter. Heute möchte ich eine Funktion vorstellen, die es meines Wissens sonst nirgendwoanders in der Python-Welt gibt, die ein Alleinstellungsmerkmal der Nodebox ist. Nodebox besitzt nämlich eine Funktion <code>grid(cols, rows, colsize=1, rowsize=1)</code> (Manual), die ein Gitter zurückgibt, über das iteriert werden kann. Jede Zelle, über die iteriert wird, kann dabei ihren eigenen <code>scope</code> besitzen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032302.html</link>
</item>
<item>
	<title>Security Alert: Kritische Sicherheitslücke in HP Druckern</title>
	<description>
			<![CDATA[<% imageref("humptydumptyp5-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032301.html"><img src="http://blog.schockwellenreiter.de/images/humptydumptyp5-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Der Drucker-Hersteller HP warnt vor mehreren kritischen Sicherheitslücken in einem Großteil seiner Drucker-Modelle. Angreifern sei es hierüber möglich, über Netzwerk-Drucker in die dahinter liegenden Systeme einzudringen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032301.html</link>
</item>
<item>
	<title>Zufallskreise in der Knotenschachtel</title>
	<description>
			<![CDATA[<% imageref("nodeboxrandcircles-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032101.html"><img src="http://blog.schockwellenreiter.de/images/nodeboxrandcircles-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Nachdem ich gestern ein paar Farbpaletten in mein Wiki eingepflegt hatte, wollte ich mit ihnen auch etwas anstellen. Und wegen meiner neu erwachten Liebe zur Knotenschachtel (aka Nodebox&nbsp;1) sollte diese die zu verwendene Entwicklungsumgebung sein. Das Ergebnis waren fünfzehn zufällig angordnete Kreise mit ebenso zufälligem Durchmesser, die aber einen Anstandsabstand zum Rand der Zeichenfläche einhielten.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032101.html</link>
</item>
<item>
	<title>(Neue) Farbpaletten braucht das Land (neu in meinem Wiki)</title>
	<description>
			<![CDATA[<% imageref("newmoon-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032002.html"><img src="http://blog.schockwellenreiter.de/images/newmoon-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Ich habe auch da gerade einen Lauf und daher meinem Wiki drei neue Seiten mit Farbpaletten spendiert, denn Farbpaletten kann man nie genug haben. Um dem heutigen Datum gerecht zu werden, sind sie (fast) alle in hellen frühlingshaften Farbtönen gehalten:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032002.html</link>
</item>
<item>
	<title>Oops, I did it! Hallo Notable! 🙋‍♀️</title>
	<description>
			<![CDATA[<% imageref("hallonotable-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022032001.html"><img src="http://blog.schockwellenreiter.de/images/hallonotable-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Man soll ein Pferd ja nicht mitten im Rennen wechseln und daher hatte ich meinen hier angekündigten Umstieg vom langsam in die Jahre gekommenen MacDown <i>(Mac only)</i> zu Notable <i>(Windows, macOS, Linux)</i> erst einmal verschoben. Nun ist das Projekt – für das ich viele Protokolle (nicht nur von Videokonferenzen) und andere Texte mit MacDown erstellt und dann als PDFs verteilt hatte – aber ausgelaufen und es ist daher Zeit, meine Ankündigung wahrzumachen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022032001.html</link>
</item>
<item>
	<title>Tutorial: Ein Apfelmännchen mit der Knotenschachtel</title>
	<description>
			<![CDATA[<% imageref("mandelbrotnodebox-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031901.html"><img src="http://blog.schockwellenreiter.de/images/mandelbrotnodebox-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Um tiefer in die Knotenschachtel (aka Nodebox&nbsp;1) einzutauchen, wollte ich einige der mitinstallierten Bibliotheken testen. Den Anfang machte Core Image, eine Bibliothek, die die Fähigkeiten von Photoshop oder Gimp als simple Python-Funktionen dem Programmierer zur Verfügung stellt.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031901.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("hochbeetspitz-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031803.html"><img src="http://blog.schockwellenreiter.de/images/hochbeetspitz-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Es ist nicht nur unvermittelt wieder Freitag geworden, sondern genauso unvermittelt ist der Frühling und mit ihm die Sonne über unser kleines Gärtchen hereingebrochen. Das mußte ich natürlich sofort photographisch dokumentieren, damit ich einen Beweis habe, warum es die nächsten zwei Tage keine oder nur wenige Updates hier in diesem <del>Blog</del> Kritzelheft geben wird.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031803.html</link>
</item>
<item>
	<title>WordPress 5.9.2 behebt Sicherheitslücken</title>
	<description>
			<![CDATA[<% imageref("lionporridge-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031802.html"><img src="http://blog.schockwellenreiter.de/images/lionporridge-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Das frisch veröffentlichte Update von WordPress auf die Version 5.9.2 behebt wieder Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031802.html</link>
</item>
<item>
	<title>Neu in meinem Wiki: Farbpalette(n) nach Piet Mondrian</title>
	<description>
			<![CDATA[<% imageref("mondrian1-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031801.html"><img src="http://blog.schockwellenreiter.de/images/mondrian1-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Da man ja bekanntlich nie genug Farbpaletten zur Verfügung haben kann, habe ich meinem Wiki eine weitere Seite mit Farbpalette(n) nach Piet Mondrian spendiert. Momentan enthält sie nur eine Palette, aber da ich eine Reihe »Re-enactment« mit Nodebox 1 und/oder Drawbot plane und <i>Piet Mondrian</i> da eine Rolle spielen soll, werden sicher noch weitere Paletten hinzukommen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031801.html</link>
</item>
<item>
	<title>Mit Shoebot aus der Mac-only-Falle? Leider nicht!</title>
	<description>
			<![CDATA[<% imageref("atomshoebot-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031702.html"><img src="http://blog.schockwellenreiter.de/images/atomshoebot-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Bei all meiner Begeisterung für DrawBot und meiner vorgestern frisch erwachten Liebe zur Knotenschachtel (aka Nodebox 1) störte mich doch ein wenig, daß ich mit beiden Programmen wieder sehenden Auges in die <i>Mac only</i>-Falle tappe. Daher habe ich mich noch ein wenig umgeschaut und gehofft, daß ich mit Shoebot dieser Falle entgehen kann. Spoiler: Leider nicht!]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031702.html</link>
</item>
<item>
	<title>Logseq und Blogposts – ein paar Fragen sind noch offen</title>
	<description>
			<![CDATA[<% imageref("logseqfragen-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031701.html"><img src="http://blog.schockwellenreiter.de/images/logseqfragen-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Wie ich hier vor ein paar Tagen schrieb, bereite ich seit etwa zwei Monaten einen großen Teil meiner <del>Blogbeiträge</del> Beiträge hier im Kritzelheft in dem markdown- und zettelkastenfähigen Outliner Logseq vor und bin damit im Großen und Ganzen zufrieden. Aber Logseq ist ein sehr mächtiges Werkzeug und ich kratze bisher erst an der Oberfläche. Zur Zeit habe ich drei Fragen, für deren Antwort ich die Weisheit der Cloud benötige:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031701.html</link>
</item>
<item>
	<title>Microsoft veröffentlicht Update 16.59.0 für Office for Mac</title>
	<description>
			<![CDATA[<% imageref("goldencup-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031602.html"><img src="http://blog.schockwellenreiter.de/images/goldencup-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Microsoft liefert mit dem Update auf 16.59.0 für Office 365, 2021 und 2019 for Mac natürlich auch wieder aktuelle Sicherheitskorrekturen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031602.html</link>
</item>
<item>
	<title>Google veröffentlicht neue Version 99 seines Browsers Chrome</title>
	<description>
			<![CDATA[<% imageref("hexedesnordens-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031601.html"><img src="http://blog.schockwellenreiter.de/images/hexedesnordens-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google veröffentlicht die neue Version 99 (99.0.4844.74) seines Browsers Chrome, behebt Fehler und schließt Sicherheitslücken.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031601.html</link>
</item>
<item>
	<title>Die Knotenschachtel lebt! Jetzt auch mit Python 3</title>
	<description>
			<![CDATA[<% imageref("nodeboxlebt-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031503.html"><img src="http://blog.schockwellenreiter.de/images/nodeboxlebt-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Da ich lange nichts mehr von <i>Karsten Wolf</i>, der die aktive Fork der Knotenschachtel (aka NodeBox&nbsp;1) betreibt, gehört hatte, bin ich in meinem gestrigen Beitrag zu Drawbot fälschlicherweise davon ausgegangen, daß Nodebox nicht mehr weitergepflegt wird. Da erhielt ich von Karsten – zu Recht – eine empörte Mail. Natürlich lebt die Knotenschachtel weiter, ich hatte nur nicht mitbekommen, daß sie in ein eigenes GitHub-Repositorium umgezogen ist.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031503.html</link>
</item>
<item>
	<title>Apple Flickentag</title>
	<description>
			<![CDATA[<% imageref("robinson-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031502.html"><img src="http://blog.schockwellenreiter.de/images/robinson-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Der Technikriese aus Cupertino mit dem angebissenen Apfel im Logo hat an seinem monatlichen Patchday gestern wieder so viele teils sicherheitsrelevante Updates herausgehauen, daß ich sie wieder nur <i>en bloc</i> abhandeln kann. Daher wie immer der Reihe nach:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031502.html</link>
</item>
<item>
	<title>Happy Pi Day 2022</title>
	<description>
			<![CDATA[<% imageref("piday2022-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031501.html"><img src="http://blog.schockwellenreiter.de/images/piday2022-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Gestern war der 14. März, in der US-amerikanischen Datumsschreibweise 3/14. Da dies auch die ersten drei Ziffern der Kreiszahl 𝜋 sind, feiert die mathematische Welt diesen Tag als Pi-Tag (engl. <i>Pi Day)</i>. Auch <i>Daniel Shiffman</i> bringt an diesem Tag seit Jahren ein Video heraus, das irgendetwas mit Pi (und mit Processing und/oder P5.js) zu tun hat.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031501.html</link>
</item>
<item>
	<title>Die Erkundung von Drawbot geht weiter: Random Rectangles</title>
	<description>
			<![CDATA[<% imageref("drawbotrandomrect-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031402.html"><img src="http://blog.schockwellenreiter.de/images/drawbotrandomrect-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Regelmäßige Leser dieses <del>Blogs</del> Kritzelhefts wissen es: Wenn ich ein neues Spielzeug habe, muß ich damit spielen. Und so habe ich am vergangenen Wochenende unter anderem mit Drawbot gespielt, der Python-Software (nicht nur) für generatives Design.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031402.html</link>
</item>
<item>
	<title>Dell warnt wieder vor Schwachstellen in seiner Firmware</title>
	<description>
			<![CDATA[<% imageref("gnome-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031401.html"><img src="http://blog.schockwellenreiter.de/images/gnome-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Dell warnt vor Sicherheitslücken in seinem BIOS, die ein lokaler Angreifer in Dell Systemen ausnutzen könnte, um Schadcode auszuführen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031401.html</link>
</item>
<item>
	<title>Nachtrag: Noch einmal Drawbot (und PageBot)</title>
	<description>
			<![CDATA[<% imageref("pagebot-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031201.html"><img src="http://blog.schockwellenreiter.de/images/pagebot-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Im Nachtrag zu meinen gestrigen Überlegungen, daß DrawBot doch ein geeignetes Werkzeug sei, um computergenerierte Bildchen (hochtrabend auch manchmal <i>Generative Art</i> genannt) zu produzieren, um sie dann zwischen zwei Pappedeckel gepreßt als Kunstband zu verkaufen, mußte ich feststellen, daß ich vor etwa einem halben Jahr schon einmal nahe an der Idee war. Damals hatte ich aber leider nicht die nötigen Schlußfolgerungen gezogen (es gab ja auch noch nicht diesen NFT-Hype, dem ich heute gerne den Stinkefinger zeigen möchte).]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031201.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("abendspitz-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031102.html"><img src="http://blog.schockwellenreiter.de/images/abendspitz-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Es ist Freitag, aber es ist schon spät und daher mache ich es kurz: Wieder gibt es ein Hundebild im <i>Schockwellenreiter</i> und wieder soll es Euch darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031102.html</link>
</item>
<item>
	<title>Generative Art auf Papier und zwischen zwei Pappedeckel gepreßt</title>
	<description>
			<![CDATA[<% imageref("tivoli2021-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031101.html"><img src="http://blog.schockwellenreiter.de/images/tivoli2021-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Bei meinen Überlegungen, wie man dem derzeitigen NFT-Hype um <i>Generative Art</i> den Stinkefinger zeigen kann, brachte mich der Artikel »Slovenia-based publishing house Look Back and Laugh prints innovative Risograph art books« auf eine Idee: Wie wäre es, wenn man die Ergebnisse seiner Progammierbemühungen – statt sie in den geldgierigen Rachen der NFTs zu versenken – lieber auf Papier drucken und zwischen zwei Pappedeckel pressen und dann als Kunstband (via einem der zahlreichen Print-on-Demand-Anbieter) vermarkten würde? Bildbände gehen immer und sie sind auch wenig vom wachsenden Ebook-Boom betroffen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031101.html</link>
</item>
<item>
	<title>Auf dem Weg zur (World-) Markdown: Logseq zur Vorbereitung meiner Blogposts</title>
	<description>
			<![CDATA[<% imageref("logseqblogposts-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022031001.html"><img src="http://blog.schockwellenreiter.de/images/logseqblogposts-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Vor einigen Wochen habe ich ja das Werkzeug zur Vorbereitung meiner Blogposts gewechselt: Ich bin vom (elektrischen) Trommler auf Logseq umgestiegen. Damals war ich mir noch nicht sicher, ob das eine gute Idee war, daher ist es Zeit für einen ersten Erfahrungsbericht: Hatte schon der kleine Trommler meine Arbeitsweise, wie ich (Microblog-) Beiträge erstelle, geändert, so hat sie Logseq komplett auf den Kopf gestellt. Denn nicht nur das Microblogging, sondern nahezu alle Blogposts – die keine Worknotes oder Tutorials sind – bereite ich nun im Outliner vor.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022031001.html</link>
</item>
<item>
	<title>Mozilla bessert mit einer neuen Version Firefox 98.0 nach</title>
	<description>
			<![CDATA[<% imageref("basketwoman-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030803.html"><img src="http://blog.schockwellenreiter.de/images/basketwoman-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Entwickler des Mozilla Firefox haben die neuen Versionen 98.0 und ESR 91.7 veröffentlicht und darin weitere Sicherheitslücken behoben. Diese Sicherheitslücken sollen bereits aktiv ausgenutzt werden, daher ist ein zeitnahes Einfahren der Updates sicher nicht verkehrt.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030803.html</link>
</item>
<item>
	<title>Völlig schwerelos … – ein weiteres, geniales PuzzleScript Game</title>
	<description>
			<![CDATA[<% imageref("gravityglow-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030802.html"><img src="http://blog.schockwellenreiter.de/images/gravityglow-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mit zwei weiteren <i>Let’s Play</i>-Videos verteidigt <i>Joseph Mansfield</i> souverän seinen Titel »Master of PuzzleScript Games«, den ich ihm vor wenigen Tagen verliehen hatte. Denn nach »Remnant Labs« hat er sich mit »That Gravity Glow« von <i>Toombler</i> ein weiteres Spiel vorgenommen, das in PuzzleScript, der bewußt minimalistisch gehaltenen Spiele-Engine, realisiert wurde. Und er hat seine letztendlich erfogreichen Bemühungen in diesen zwei Videos dokumentiert:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030802.html</link>
</item>
<item>
	<title>Google schließt am »März-Patchday« wieder Sicherheitslücken in Android</title>
	<description>
			<![CDATA[<% imageref("blacksheep-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030801.html"><img src="http://blog.schockwellenreiter.de/images/blacksheep-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Google hat mit seinem monatlichen Sicherheitsupdate für Android (und damit auch auf seinen Pixel-Geräten) wieder Sicherheitslücken geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030801.html</link>
</item>
<item>
	<title>Code the Classics in Pygame Zero: Braid (2008)</title>
	<description>
			<![CDATA[<% imageref("braid-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030702.html"><img src="http://blog.schockwellenreiter.de/images/braid-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Die Wireframe ist mit der Nummer 60 erschienen und wie in jeder Ausgabe gibt es auch in dieser eine Hommage an einen Spieleklassiker, die <i>Mark Vanstone</i> in Pygame Zero programmiert hat. Und – ebenfalls wie in jeder Ausgabe – ist dieser Beitrag parallel in den Raspberry Pi News online erschienen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030702.html</link>
</item>
<item>
	<title>Neu in meinem Wiki: Farbpalette nach Vera Molnár</title>
	<description>
			<![CDATA[<% imageref("farbenmolnar-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030701.html"><img src="http://blog.schockwellenreiter.de/images/farbenmolnar-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Zur Vorbereitung neuer Spielereien mit Python, TigerJython und/oder Processing(.py) habe ich meinem Wiki eine Seite mit einer Farbpalette nach <i>Vera Molnár</i> spendiert, denn Paletten kann man nie genug haben.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030701.html</link>
</item>
<item>
	<title>Generative Art mit Python – ohne Processing.py und TigerJython</title>
	<description>
			<![CDATA[<% imageref("beforexmas-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030601.html"><img src="http://blog.schockwellenreiter.de/images/beforexmas-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Der derzeit grassierende Hype um Blockchain, NFTs und die damit verbundene (irrationale) Gier nach dem schnellen Geld spült viele entsprechende Meldungen in meinen Feedreader. Die meisten sind Schrott, aber man findet auch einige Perlen darunter, deren Fokus weniger auf NFTs, sondern darauf liegt, wie man <i>Generative Art</i> erzeugen kann, ohne das Web mit langweiligen Pixel-Bildern häßlicher Affen zu fluten. Ich habe dazu mal einige herausgesucht, die zeigen, wie man dafür Python ((fast) ohne Processing.py und TigerJython) nutzen kann. Die meisten Beiträge haben NFT nur wegen der Klicks im Titel und erwähnen das nur am Rande, bei einigen anderen muß man das einfach ausblenden. Und wer weiß, vielleicht inspirieren mich einige der Tutorials auch für eigene Python-Projekte. Denn es muß nicht immer Processing(.py) oder TigerJython sein.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030601.html</link>
</item>
<item>
	<title>Mozilla Updates (Firefox und Thunderbird)</title>
	<description>
			<![CDATA[<% imageref("desnlowozbanner-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030501.html"><img src="http://blog.schockwellenreiter.de/images/desnlowozbanner-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Zum Monatsbeginn gab es – wie gewohnt – aus dem Hause Mozilla die turnusmäßigen (Sicherheits-) Updates für den Feuerfuchs und den Donnervogel. Dieses mal flatterten sie gleichzeitig bei mir rein, so daß ich sie <i>en bloc</i> abhandeln kann:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030501.html</link>
</item>
<item>
	<title>Schau Mama, ein Hundebild!</title>
	<description>
			<![CDATA[<% imageref("kuechenspitz-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030402.html"><img src="http://blog.schockwellenreiter.de/images/kuechenspitz-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Meine Pläne für die nächsten zwei Tage sind ambitionierter als die Lücken, die mir mein Kalender läßt. Daher kommt mir das freitägliche Hundebild gerade recht, das Euch darüber hinwegtrösten soll, daß es bis Montag keine oder nur wenige Updates hier im <i>Schockwellenreiter</i> geben wird. Aber wenn es welche gibt, dann werden es (hoffentlich!) solche sein, die über Fortschritte in meinen Plänen berichten können.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030402.html</link>
</item>
<item>
	<title>Soll ich es mal wieder mit R, RStudio und R Markdown probieren?</title>
	<description>
			<![CDATA[<% imageref("learningr-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030401.html"><img src="http://blog.schockwellenreiter.de/images/learningr-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Zu Beginn der Pandemie vor zwei Jahren wollte ich unbedingt die Zeit nutzen, eine neue Programmiersprache – nämlich Julia 👩 – zu lernen, doch mit Julia bin ich nie so richtig warmgeworden. Daher ist das Projekt sanft entschlafen. Als ich jedoch jüngst über den Blogbeitrag »How to use R Markdown to learn R« (Teil 1 und Teil 2) von <i>Luka Negoita</i> stolperte, kam mir der verwegene Gedanke, ob ich es alternativ vielleicht mal wieder mit R, RStudio und R Markdown versuchen sollte?]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030401.html</link>
</item>
<item>
	<title>Abenteuer PuzzleScript: Remnant Labs – Finale</title>
	<description>
			<![CDATA[<% imageref("remnantlabfinale-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030302.html"><img src="http://blog.schockwellenreiter.de/images/remnantlabfinale-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all"><i>Joseph Mansfield</i> ist (m)ein Held und er hat sich den Titel »Master of PuzzleScript Games« ehrlich verdient. Denn er hat das am Montag vorgestellte, in der minimalistischen Game Engine PuzzleScript realisierte Spiel »Remnant Labs« von <i>Neonesque</i> tatsächlich komplett durchgespielt und gemeistert. Und er hat sein Spiel in vier Videos auf YouTube dokumentiert:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030302.html</link>
</item>
<item>
	<title>Abenteuer TigerJython: Pi springt im Dreieck (Random Walk)</title>
	<description>
			<![CDATA[<% imageref("pidreieck-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030301.html"><img src="http://blog.schockwellenreiter.de/images/pidreieck-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Der Pi-Tag rückt immer näher und das ist Anlaß genug für mich, meine Experimente fortzusetzen, die die Nachkommastellen der Kreiszahl auf eine Zufallswanderung schicken. In Anlehnung an dieses Video von Numberphile habe ich dieses Mal die Richtung auf ein gleichwinkliges Dreieck begrenzt, abhängig von der Nachkommastelle bewegt sich TigerJythons Schildkröte um jeweils einen 60°-Winkel nach rechts:]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030301.html</link>
</item>
<item>
	<title>Google veröffentlicht die neue Version 99 seines Browsers Chrome</title>
	<description>
			<![CDATA[<% imageref("mothergoose54-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030202.html"><img src="http://blog.schockwellenreiter.de/images/mothergoose54-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber – wenn es pressiert – auch hier geladen werden.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030202.html</link>
</item>
<item>
	<title>Flucht und Verfolgung in Processing.py (ohne OOP)</title>
	<description>
			<![CDATA[<% imageref("fluchtundverfolgung1-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030201.html"><img src="http://blog.schockwellenreiter.de/images/fluchtundverfolgung1-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Mit diesem Beitrag möchte ich zum einen das Projekt »The Nature of Code in Python« wieder aufnehmen und zum anderen meine Idee, »Methoden der Künstlichen Intelligenz für Spiele (-programmierer)« realisieren. Ich beginne da mit einem einfachen Beispiel, das zeigt, wie man Flucht und Verfolgung per Programm so realisieren kann, daß es für den Spieler wenigstens wie ein halbwegs intelligentes Verhalten der Opponenten aussieht.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030201.html</link>
</item>
<item>
	<title>Die Zahlen für den Monat Februar 2022</title>
	<description>
			<![CDATA[<% imageref("mediadaten202202-s") %><a href="http://blog.schockwellenreiter.de/2022/03/2022030101.html"><img src="http://blog.schockwellenreiter.de/images/mediadaten202202-s.jpg" width="560" height="auto" alt="image" /></a><br clear="all">Heute hat ein neuer Monat begonnen und daher ist es Zeit für die Zahlen des Vormonats, die hochtrabend manches Mal auch <i>Mediadaten</i> genannt werden: Im Februar 2022 hatte der <i>Schockwellenreiter</i> laut seinem nicht immer ganz zuverlässigen, aber dafür (hoffentlich!) datenschutzkonformen Neugiertool exakt 5.413 Besucher mit 11.459 Seitenaufrufen.]]></description>
	<link>http://blog.schockwellenreiter.de/2022/03/2022030101.html</link>
</item>
