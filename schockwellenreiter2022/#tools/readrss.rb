# encoding: utf-8

def readrss()

  require 'rss'

  rss_feed = "/Applications/MAMP/htdocs/schockwellenreiter/feed/rss.xml"
  rss_content = ""
  s = ""

  open(rss_feed) do |f|
    rss_content = f.read
  end

  rss = RSS::Parser.parse(rss_content, false)

  rss.items.each do |item|
     s << "<p><b>#{item.title}</b>: #{item.description}"
     s << "&nbsp;&nbsp; <i><a href='#{item.link}'>Mehr hier&nbsp;…</a></i></p>\n<hr />"
  end
  s
end