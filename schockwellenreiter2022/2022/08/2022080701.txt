#title "Ren'Py, Geany und das Chromebook – 20220807"
#maintitle "Worknote und Thread: Ren'Py, Geany und das Chromebook"
#prevp "2022080601"
#nextp "2022080801"

<%= html.getLink(imageref("chromebookgeanyrenpy-b"), "Ren\'Py, Geany und mein Chromebook") %>

## Worknote und Thread: Ren'Py, Geany und das Chromebook

Ich habe die Entdeckungsreise durch die Möglichkeiten [meines Chromebooks](Chromebook tunen – 20220604)  noch lange nicht abgeschlossen. Mein heutiges Tuning war ein Versuch, das Arbeiten mit [Ren'Py](cp^renpy) möglichst einfach zu gestalten. Der erste Schritt war leichter als ich dachte: Ren'Py startete per Default [Geany](cp^Geany) als seinen »System«-Texteditor. Warum, weiß ich nicht, aber ich hatte damit gerechnet, hier doch noch einiges an Bastelarbeit investieren zu müssen und so nahm ich dies einfach dankbar an.

Trotzdem hätte ich jetzt gerne doch noch einmal die Hilfe der Cloud: Wie bringt man Geany bei, Dateien mit der Endung `.rpy` -- also Ren'Py-Script-Dateien -- automatisch als Python-Dateien zu erkennen? In der Datei `filetype_extensions.conf` habe ich sie in dieser Zeile

~~~bash
#~ Python=*.py;*.pyde;*.pyw;*.rpy;*.rpym;SConstruct;SConscript;wscript;
~~~

eingetragen, aber Geany zeigt sich störrisch. Ich kann damit leben und den Dateitpy jedesmal händisch via `Dokument -> Dateityp festlegen …` einstellen, aber eine automatische Erkennung wäre einfach schöner. Wenn also von meinen Leserinnen oder Lesern da draußen jemand einen Tip hat … ??

---

**2 (Email-) Kommentare**

---

<%= a("k01") %>

>Ich habe mich am Geany-Problem versucht und auf dem Chromebook in der Shell mit `sudo vi /usr/share/geany/filetype_extensions.conf` editiert und in der Zeile mit Python einfach nochmal "*.rpy" ergänzt. Denn die filetype_extension.conf, die Du im Editor ergänzt legt nur eine
Datei in Deinem /home an, allerdings scheint der override da nicht zu
funktionieren. Erst, wenn Du die Originaldatei unter /usr/share/geany editierst, dann erkennt er die *.rpy-Files als Python!   
Ich hoffe, ich konnte Dir auch diesmal weiterhelfen!

*– Robert Sch.* <%= p("k01") %>

---

<%= a("k0101") %>

>>Konnte ich leider nicht verifizieren, denn da ich den vi nicht beherrsche, habe ich mir bei dem Versuch die Datei `/usr/share/geany/filetype_extensions.conf` zu editieren, diese komplett zerschossen. Jetzt darf ich Geany erst einmal neu installeren und hoffe dann auf eine weniger gefährliche Lösung.&nbsp;🤪

*– Jörg Kantel* <%= p("k0101") %>



