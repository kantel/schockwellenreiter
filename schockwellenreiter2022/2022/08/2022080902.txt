#title "Sicherheitsupdate für Reader und Acrobat – 20220809"
#maintitle "Sicherheitsupdate für Adobe Reader und Acrobat (APSB22-39)"
#prevp "2022080901"
#nextp "2022080903"

<%= html.getLink(imageref("gruenrot-b"), "Grün-rotes Ensemble") %>

## Sicherheitsupdate für Adobe Reader und Acrobat (APSB22-39)

Adobe liefert [Korrekturen für Sicherheitslücken](https://helpx.adobe.com/security/products/acrobat/apsb22-39.html) im Reader und in Acrobat (unter Windows und Mac).

Das Update gelingt am einfachsten über den internen Update-Mechanismus. *([[cert]])*

*[[photojoerg]]*
