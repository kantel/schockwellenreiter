#title "Pazifismus ist kein Verbrechen – 20220819"
#maintitle "Pazifismus ist kein Verbrechen"
#prevp "2022081802"
#nextp "2022081902"

<%= html.getLink(imageref("dertriumphdestodes-b"), "Der Triumph des Todes, 1562") %>

## Pazifismus ist kein Verbrechen

Unter dieser Überschrift hatte ich auf Facebook eine ziemlich sinnlose Diskussion losgetreten, die dazu führte, daß eine Meute von Ostlandrittern losheulte, die am liebsten Rache für Stalingrad nehmen und die Kornkammer des Reiches heim in die EU holen wollten. Dabei war das schon 1941 in die Hose gegangen. Doch die Horde gab sie sich nicht einmal die Mühe, den Schaum von ihrem Mund zu wischen. Da mich die Frage jedoch bewegt, möchte ich mich hier – wo ich eine größere Kontrolle darüber habe, wer seinen Senf dazugeben kann -- noch einmal in Ruhe zu diesem Punkt auslassen. Danach ist die Sache für mich gegessen. Also, das Thema heute lautet: Pazifismus ist kein Verbrechen, auch wenn *Sascha Lobo* da anderer Ansicht ist.

Ich bin 1953 geboren, also acht Jahre nach dem Ende des 2. Weltkriegs und des Nazi-Terrors. Wer noch rechnen kann, kann sich leicht ausrechnen, daß meine Eltern Krieg und Terror mit ihrem ganzen Schrecken am eigenen Leib erlitten hatten. Daraus gab es eines, das sie ihren Kindern auf jeden Fall mitgeben wollten: Nie wieder Krieg – und das »nie wieder« bedeutete ohne Ausnahme. Auch ein wildgewordener Putin ist keine Entschuldigung. 
 
Am 11. Mai 1952 – auf den Tag genau ein Jahr vor meiner Geburt – wurde in Essen der damals 21-jährige Philpp Müller mit einem Rückenschuß von der Polizei getötet, als er gegen die Wiederbewaffnung der Bundesrepublik Deutschland demonstrierte. In späteren Jahren habe ich dieses Ereignis, an dem ich an jedem meiner Geburtstage erinnert werde, immer als Verpflichtung gesehen, für dieses Gelöbnis meiner Eltern einzutreten: Nie wieder Krieg!  

Folgerichtig habe ich 1970 noch vor meiner Musterung meinen Antrag auf Wehrdienstverweigerung eingereicht und bis 1974 für meine Anerkennung gekämpft. Am Tag meiner letzten Ablehnung hatte ich mich in einen Flieger gesetzt und es tatsächlich noch am gleichen Abend geschafft, mich in Westberlin polizeilich anzumelden, Das war wichtig, denn ohne Einberufung (die tatsächlich am nächsten Tag im Briefkasten meiner damals Mannheimer Wohnung lag) war ich nicht fahnenflüchtig, sondern hatte nur eine Ordnungswidrigkeit begangen (Verlassen des Bereichs der Wehrüberwachung ohne Genehmigung). Und da schützte mich das alliierte Viermächteabkommen vor einer Auslieferung an die Bundeswehr. Dennoch – da ich der bundesrepublikanischen Justiz nicht traute – hatte ich in den ersten Jahren (bis zur Aufnahme meines Studiums etwa 1980) Westberlin nie verlassen, sondern mich hier eingeigelt. (Warum auch nicht, Westberlin war schön.)  

Nach dem Mauerfall hatte ich noch einmal kurz Angst, daß ich nach all den Jahre dennoch einberufen würde, aber selbst für den hartnäckigsten westdeutschen Militaristen war ich wohl zu alt.  

Das Krieg niemals erfolgreich ist, lehrt auch ein Blick in die Geschichte.  Nehmen wir zum Beispiel Afghanistan: Erst wurden die Taliban als Freiheitskämpfer hofiert (als es gegen die Sowjetunion ging), dann zu Terroristen erklärt (als es um die USA ging) und schließlich mit der geballten Intelligenz und militärischen Macht des Westens bekämpft – bis sie gesiegt hatten. Wie wird wohl der Krieg gegen Russland ausgehen? Mir schwant da was ... (nach den Erfahrungen im Balkan-Krieg).  

Zum Abschluß noch eine Anmerkung: Die immer wieder aufgestellte Behauptung, Putin sei gleich Hitler wird auch durch ständige Wiederholung nicht richtiger. Sie verleugnet die Singularität des deutschen Faschismus und verharmlost ihn nicht nur,  sondern verhöhnt auch deren Opfer. Und vom Faschismus-Verharmloser zum Faschisten ist es nur ein kurzer Weg.  

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>danke für diesen Beitrag. Auch wenn ich die Quelldiskussion bei facebook nicht kenne, kann ich mir gut vorstellen, was dort los war/ist.   
Ich bin 1942er Jahrgang und verstehe Dich sehr gut. Der Schaum vorm Mund der Bellizisten ist nicht zu ertragen.   
Wir sind nicht allein mit unserer Einstellung, wenn auch leider (zur Zeit noch) kaum hörbar.

*– Reiner K.* <%= p("k01") %>

