#title "Waldbrandsimulation im Terminal – 20220829"
#maintitle "Nostalgie pur: Waldbrandsimulation im Terminal"
#prevp "2022082802"
#nextp "2022083001"

<%= html.getLink(imageref("forestfirebext-b"), "Waldbrandsimulation im Terminal (mit Bext)") %>

## Nostalgie pur: Waldbrandsimulation im Terminal (mit Python)

*Al Sweigart* hat ein Buch geschrieben. Das ist keine wirkliche Meldung, denn *Al Sweigart* ist ein äußerst umtriebiger Autor, der als Verfasser etlicher Programmierbücher (meist zu Python) bekannt ist, von denen nicht nur viele bei [No Starch Press erschienen sind](https://nostarch.com/search/Al%20Sweigart), sondern die auch kostenlos von der [Homepage des Autors heruntergeladen](https://inventwithpython.com/) werden können. Aber -- und das ist die Meldung -- eines dieser Bücher heißt »[The Big Book of Small Python Projects][a1]« und die Programme in diesem Buch sind so konzipiert, daß sie alle im Terminal ausgeführt werden können. Mehr Nostalgie geht eigentlich nicht.

[a1]: https://www.amazon.de/Big-Book-Small-Python-Programming/dp/1718501242?crid=1MO7N8E8T7OC0&keywords=the+big+book+of+small+python+projects&qid=1661788017&sprefix=%2Caps%2C632&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=33cd4fbbda233f11dd786e13fbf5706a&language=de_DE&ref_=as_li_ss_tl

[[dr]]<iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1718501242&asins=1718501242&linkId=1eb51700564b5db9b87882ec021e15a2&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Damit die Programme auch einen Hauch von Interaktivität besitzen und der Leser seine gewohnte GUI nicht allzusehr vermißt, hat *Al Sweigart* in vielen Beispielen dafür **[Bext](https://github.com/asweigart/bext)** genutzt, ein Python-Moduls, das er auch selber geschrieben hat. Bext ist ein freies (eigene Lizenz), plattformübergreifendes Pyhton-2/3-Modul, um Farbe in sonst langweilige, textbasierte Terminal-Programme zu bringen. Das Modul kann verwendet werden, um den Cursor im Terminalfenster zu bewegen und Text farbig auszugeben, ähnlich wie damals (1978-1983) die VT100-Terminals. Es ist als ein eingedampftes Replacement für das *Curses*-Modul aus der Python-Standard-Bibliothek gedacht, da dieses zwar unter Linux und macOS, aber (ohne seltsame Klimmzüge) nicht unter Windows funktioniert.

Ich habe aus diesem Buch mal den Waldbrand-Simulator nachprogrammiert (Seite 132-136). Ein ähnliches Programm hatte ich vor Jahren selber auch einmal in [Processing.py geschrieben](http://py.kantel-chaos-team.de.s3-website-us-east-1.amazonaws.com/12zellautom/#der-waldbrand-simulator), damals natürlich mit einer graphischen Oberfläche. *Al Sweigart* hat sich -- genau wie ich -- zu diesem Programm von *Nick Cases* Aufsatz »[Simulating the World in Emojis](https://ncase.me/simulating/index_old.html)« aus dem Jahre 2016 inspirieren lassen. Hier ist der leicht modifizierte Quellcode der Terminal-Fassung von *Al Sweigart*:

~~~python
import random, sys, time, bext

# Konstanten-Deklaration
WIDTH = 79
HEIGHT = 22
TREE  = "A"
FIRE  = "W"
EMPTY = " "

# Parameter, mit denen experimentiert werden kann
INITIALER_BAUM_BESTAND = 0.2
WACHSTUMSRATE = 0.01           # Rate, wie oft auf einer leeren Zelle ein Baum wächst
BRANDGEFAHR   = 0.05           # Rate, wie oft ein Brand ausbricht
PAUSE         = 0.5

def main():
    forest = create_new_forest()
    bext.clear()
    
    while True:               # Hauptschleife der Simulation
        display_forest(forest)
        
        # Simulations-Schritte
        next_forest = {"width": forest["width"], "height": forest["height"]}
        for x in range(forest["width"]):
            for y in range(forest["height"]):
                if (x, y) in next_forest:
                    # Wenn der Wert schon in einer vorherigen Iteration
                    # gesetzt wurde, tue nichts
                    continue
                
                if ((forest[(x, y)] == EMPTY) and (random.random() <= WACHSTUMSRATE)):
                    # Neuer Baum
                    next_forest[(x, y)] = TREE
                elif ((forest[(x, y)] == TREE) and (random.random() <= BRANDGEFAHR)):      
                    # Zünde Baum an
                    next_forest[(x, y)] = FIRE
                elif forest[(x, y)] == FIRE:
                    # Wenn der Baum brennt, untersuche die Nachbarfelder …
                    for ix in range(-1, 2):
                        for iy in range(-1, 2):
                            # Wenn Baum, dann anzünden
                            if forest.get((x + ix, y + iy)) == TREE:
                                next_forest[(x + ix, y + iy)] = FIRE
                    # und lösche den brennenden Baum
                    next_forest[(x, y)] = EMPTY
                else:
                    # Kopiere das Feld in die nächste Iteratrion
                    next_forest[(x, y)] = forest[(x, y)]
        
        forest = next_forest
        time.sleep(PAUSE)
        
def create_new_forest():
    forest = {"width": WIDTH, "height": HEIGHT}
    for x in range(WIDTH):
        for y in range(HEIGHT):
            if (random.random()*100) <= INITIALER_BAUM_BESTAND:
                # Pflanze einen Baum
                forest[(x, y)] = TREE
            else:
                forest[(x, y)] = EMPTY
    return(forest)

def display_forest(forest):
    bext.goto(0, 0)
    for y in range(forest["height"]):
        for x in range(forest["width"]):
            if forest[(x, y)] == TREE:
                bext.fg("green")
                print(TREE, end="")
            elif forest[(x, y)] == FIRE:
                bext.fg("red")
                print(FIRE, end="")
            elif forest[(x, y)] == EMPTY:
                print(EMPTY, end="")
        print()
    bext.fg("reset")     # Auf Terminal-Standard-Einstellung zurücksetzen
    print("Wachstumsrate: {}%  ".format(WACHSTUMSRATE*100), end="")
    print("Brandgefahr: {}%  ".format(BRANDGEFAHR*100), end="")
    print("Mit CTRL-C beenden.")
    
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        sys.exit()
~~~

Natürlich muß man das Programm im Terminal laufen lassen, wenn man es in seiner ganzen Pracht genießen will. In [Thonny](cp^Thonny) erreicht man das entweder über `CMD-T` oder das Menü `Ausführen -> Aktuelles Skript im Terminal ausführen`. Und man sollte während der Ausführung die Größe des Terminal-Fensters nicht mehr verändern, es stellt sich beim Start schon auf die optimale Größe ein.

Natürlich ist das alles Nostalgie pur, aber das ist ja momentan *en vogue*. So könnte man mit dem Modul beispielsweise das jüngst vorgestellte »[AppleSoft BASIC Snake Game](Back to the Roots: AppleSoft Basic – 20220825)« nachprogrammieren, ohne einen Apple&nbsp;II-Emulator zu bemühen oder sich mit BASIC herumumquälen zu müssen. Und Hardcore-Nostalgiker könnten ein [Roque-like](cp^Rogue) wie zum Beispiel [NetHack](cp^NetHack) mit der klassischen ASCII-Oberfläche nachprogrammieren -- mit dem Spieler als `@`. Wäre doch ziemlich stilecht für mein Projekt »Retrogaming und Künstliche Intelligenz«, oder? *Still digging!*

Wer jetzt Blut geleckt hat: Bext ist [auf PyPI](https://pypi.org/project/Bext/) zu finden und kann mit `pip install bext` installiert werden.