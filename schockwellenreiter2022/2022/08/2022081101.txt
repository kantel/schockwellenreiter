#title "Codierte Kunst mit Py5: Vasa Mihich – 20220811"
#maintitle "Codierte Kunst mit Py5: Hommage â Vasa Mihich"
#prevp "2022081001"
#nextp "2022081301"

<%= html.getLink(imageref("hommageanvasamihich-b"), "Hommage â Vasa Mihich") %>

## Codierte Kunst mit Py5: Hommage â Vasa Mihich

Das konnte nicht warten, ich mußte unbedingt etwas mit meinem frisch installierten [Py5](cp^Py5) anstellen, und was liegt näher, als mich weiter in meine Experimente zu »[Generativer Art zwischen zwei Pappedeckel](Generative Art auf Papier – 20220311)« zu vertiefen. Und so habe ich das auf diesen Seiten schon mehrfach gelobte Buch »[Codierte Kunst](http://blog.schockwellenreiter.de/2019/01/2019012402.html)« von *Joachim Wedekind* aus seinem Dornröschenschlaf im Bücherregal gerissen und daraus die *Hommage â Vasa Mihich* (Seite 75ff) nachprogrammiert.

[[dr]]<%= html.getLink(imageref("codiertekunst"), "http://digitalart.joachim-wedekind.de/ueber-das-buch/") %>[[dd]]

Gegenüber dem [Snap!](cp^Snap)-Programm von *Joachim Wedekind* habe ich einige Änderungen vorgenommen:

1. Ich habe eine andere Farbpalette gewählt, nämich das [New Moon Farbschema](cp^New Moon) (ohne die Grautöne). Das nimmt zwar etwas vom Acryl-Charakter der Originalfarben, aber hat dafür eine sehr bunte Anmutung, die mir einfach gefiel.
2. Es gibt drei unterschiedliche Paletten, jede Palette besteht aus drei Farben. Alle Paletten besitzen je drei unterschiedliche Farben (das heißt, es gibt keine Farbüberscheidungen zu den anderen Paletten).
3. Auch die Reihenfolge der Linien habe ich verändert: Zuerst habe ich die Hälfte der vertikalen Linien mit der ersten Palette (`primaer`) gezeichnet, dann die erste Hälfte der horizontalen Linien mit der zweiten Palette (`sekundaer`). Danach in die vertikalen Zwischenräume wieder mit den »Primärfarben« die zweite Hälfte der Linien eingefügt und zu guter Letzt mit der dritten Farbpalette (`tertiaer`) die Linien in den verbleibenden horizontalen Zwischenräumen gezeichnet.

Durch diese Anordnung hoffe ich, die Illusion eines Flechtwerkes besser herausgearbeitet zu haben (wobei mir die Farbpalette da auch noch sehr entgegenkam -- die beiden Gelbtöne liegen doch sehr dicht beieinander, daher ist schwierig festzustellen, welcher den anderen überlappt).

Hier ist der komplette Quellcode meiner Übung:

~~~python
# Hommage â Vasa Mihich
# Nach einer Idee von Joachim Wedekind
# Codierte Kunst, Seiten 75ff
from random import randint

WIDTH = 800
HEIGHT = 800

primaer   = [color(242, 119, 122), color(255, 212, 121), color(106, 126, 243)]
sekundaer = [color(252, 163, 105), color(255, 238, 166), color(118, 212, 214)]
tertiaer  = [color(146, 209, 146), color(225, 166, 242), color(172, 141, 88)]

def setup():
    size(WIDTH, HEIGHT)
    window_move(1400, 30)
    window_title("Hommage â Vasa Mihich")
    background(45, 45, 45)
    stroke_weight(10)
    no_loop()
    
def draw():
    for i in range(width//24):
        stroke(primaer[randint(0, len(primaer) - 1)])
        line(i*width//12, 0, i*width//12, height)
    for j in range(height//24):
        stroke(sekundaer[randint(0, len(sekundaer) - 1)])
        line(0, j*height//12, width, j*height//12)
    for k in range(width//24):
        stroke(primaer[randint(0, len(primaer) - 1)])
        line((k*width//12) + width//24 , 0, (k*width//12) + width//24, height)
    for l in range(height//24):
        stroke(tertiaer[randint(0, len(tertiaer) - 1)])
        line(0, (l*height//12) + height//24, width, (l*height//12) + height//24)

print("I did it, Babe!")
~~~

Wie immer gibt es ihn auch in meinem [GitHub-Repositorium](https://github.com/kantel/py5/blob/main/art/lines.py), aber die geneigte Leserin oder der geneigte Leser werden ihn besser nachvollziehen können, wenn die den Code in [Thonny](cp^Thonny) eintippen -- so, jetzt habe ich den Editor auch noch in diesem Text untergebracht.