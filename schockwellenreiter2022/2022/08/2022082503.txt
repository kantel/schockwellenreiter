#title "World Markdown mit PanWriter? – 20220825"
#maintitle "Auf dem Weg zur World Markdown mit PanWriter? Wohl eher nicht."
#prevp "2022082502"
#nextp "2022082601"

<%= html.getLink(imageref("panwritertestmaster-b"), "PanWriter Master") %>

## Auf dem Weg zur World Markdown mit PanWriter? Wohl eher nicht!

Konstantin, [den ich überredet hatte](LaTeX or not to LaTeX – 20220820), mit mir zusammen die <del>Weltherrschaft anzustreben</del> World Markdown zu erkunden (statt LaTeX zu lernen), machte mich auf [PanWriter](https://panwriter.com/) aufmerksam. PanWriter, das ein freier (GPL, [Quellcode](https://github.com/mb21/panwriter) auf GitHub), plattformübergreifender (da [Electron](cp^Electron)-Anwendung) Markdown-Editor sein will, hatte ich Anfang des Jahres [schon einmal auf dem Schirm](PanWriter und mdBook – 20220129). Seine Besonderheit -- wenn nicht gar das Alleinstellungsmerkmal der Software -- liegt darin, daß sie auf ein im Hintergrund werkelndes [Pandoc](cp^Pandoc) zurückgreifen kann, das den Im- und Export nach unzähligen Formaten ermöglicht.

Schon damals hatte ich PanWriter auf meine Testliste gesetzt und nun, da mich Konstantin erneut darauf gestoßen hatte, gab es keine Entschuldigung mehr und ich mußte die Software nun endlich testen. Das Ergebnis war sowohl erfreulich wie auch ernüchternd. Denn PanWriter macht das, was man von einem Markdown-Editor als minimale Voraussetzung erwartet, aber eben nicht mehr. Zum Vergleich habe ich den gleichen Markdown-Text mit [MacDown](cp^MacDown) und [Typewriter](http://blog.schockwellenreiter.de/2021/09/2021092801.html) gesetzt. die von mir zur Zeit bevorzugten Markdown-Editoren für die schnellen Notizen und Protokolle zwischendurch (ja ich weiß, mit beiden habe ich mich in die *Mac only*-Falle verrannt).

<%= html.getLink(imageref("panwriter-b"), "PanWriter Test") %>

PanWriter kann *Commonmark* mit ein paar Erweiterungen wie zum Beispiel Fußnoten, *Fenced Code Blocks* und mathematische Formeln (letzteres wahlweise mit [KaTeX](https://katex.org/)  oder MathJax). Damit deckt es die minimalen Anforderungen ab, die ich an einen Markdown-Editor stelle. Aber es kann in den *Code Blocks* kein *Syntax Highlighting* und die Default-Schrift im Texteditor ist mir für meinen Geschmack etwas zu groß geraten. Wenn man das zum Beispiel mit MacDown (links) und Typewriter (rechts) vergleicht:

<%= html.getLink(imageref("macdown-b"), "MacDown vs PanWriter") %>&nbsp;<%= html.getLink(imageref("typewritervspanwriter-b"), "Typewriter vs PanWriter") %>

Und noch etwas fällt auf: MacDown und Typewriter beherrschen [Mermaid](cp^Mermaid) für Diagramme, während PanWriter keine Untersützung für Mermaid besitzt. Doch das ist nur eine Petitesse, da es sicher nicht zu den Anforderungen gehört, die man in der Regel an ein Werkzeug wie PanWriter stellt.

<%= html.getLink(imageref("panwritertestexport-b"), "PanWriter Test") %>

Denn das ist das, wofür PanWriter entwickelt worden ist und was die Existenzberechtigung dieser Software ausmacht: Unverbesserlichen Mausschubsern wie mir möglichst viele der Möglichkeiten von Pandoc zur Verfügung zu stellen, aber dabei den Weg über die Kommandozeile zu ersparen. Und diese Aufgabe erledigt PanWriter sehr gut.

Allerdings gibt es auch hier mit [Quarto](cp^Quarto) schon ein *New Kid on the Blog(osphere)*. Vielleicht mit einem leicht verschobenen Anwendungsfokus, aber sehr vielversprechend. Ich werde daher erst einmal nicht auf PanWriter setzen, bevor ich nicht auch Quarto getestet habe. *Still digging!*