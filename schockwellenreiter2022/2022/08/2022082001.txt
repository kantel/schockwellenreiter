#title "LaTeX or not to LaTeX – 20220820"
#maintitle "LaTeX or not to LaTeX – oder doch besser Pandoc?"
#prevp "2022081902"
#nextp "2022082002"

<%= html.getLink(imageref("airmail1929-b"), "Luftpost, 1929") %>

## Worknote: LaTeX or not to LaTeX – oder doch besser Pandoc?

Von einem guten Freund und Journalisten bekam ich die Frage gestellt, ob ich es für sinnvoll halte, daß er ganz ohne mathematische und naturwissenschaftliche Ambitionen LaTeX lernen solle. Mir wurde die Frage gestellt, weil ich in seinen Augen (unter den Blinden ist der Einäugige König) so etwas wie ein LaTeX-Guru wäre. Um diesem Ruf wenigstens im Ansatz gerecht zu werden, hier meine Antwort (redaktionell bearbeitet):

Willst Du Dir das wirklich antun? LaTeX ist keine Aufgabe, nicht einmal als Gehirnjogging. Die Präambel bastelt man sich einmal zusammen und ist dann zufrieden, daß sie funktioniert – auch wenn man nicht wirklich verstanden hat, warum. Und der Rest besteht aus `(sub(sub(sub(section)))` für die verschiedenen Gliederungsstufen der Überschriften und dem eigentlichen Text. Und da (zumindest Xe-) LaTeX mittlerweile soweit UTF-8-fähig ist, daß es die meisten europäischen Umlaute und Sonderzeichen beherrscht, gibt es auch hier keine Herausforderung. Also merkst Du dir nur noch `\emph{für kursiven}` und `\textbf{für fetten}` Text (hier gibt es mit `(\em kursiven Text)` und `(\bf fetten Text)` noch Syntax-Unterschiede ohne Auswirkungen auf die Ausgabe). Und schon bist Du ein LaTeX-Guru.

Nein, komplex wird LaTeX wirklich erst, wenn es um mathematische Formeln geht. Da ist LaTeX einzigartig und es ist eine Wissenschaft für sich. Aber nur interessant für Menschen, die wirklich Wert auf gut gesetzte, typographisch korrekte Formeln legen.

[[dr]]<iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1505218799&asins=1505218799&linkId=66061e1ba0da6aee52ff5346248de79c&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Da Du ja ein Textarbeiter bist, wäre mein Vorschlag: Setze Dich mit [Pandoc](cp^Pandoc) auseinander. Das ist ein Open Source Textpaket rund um eine Markdown Engine, die in ihrer Grundstruktur aus Markdown heraus nach LaTeX- (und damit PDF), XHTML-, .docx- und vielen anderen Dokumenten-Formaten exportiert. Per Default wird Pandoc-Markdown als Ausgangsformat verwendet, aber auch GFM (GitHub Flavored Markdown), CommonMark und andere Markdown-Dialekte werden akzeptiert. Und wenn es ganz eng wird, frißt Pandoc auch Word- oder Orgmode- (und andere) Formate als Input (hier entstehen aber natürlich Konvertierungsverluste).

Pandoc dient unter anderem als Grundlage für [R Markdown](cp^R Markdown) und [Quarto](cp^Quarto) ([RStudio](cp^RStudio), das jetzt [Posit](https://www.rstudio.com/blog/rstudio-is-becoming-posit/) heißt) und die Ideen-Prozessoren [Obsidian](cp^Obsidian), [Zettlr](cp^Zettlr) und [Logseq](cp^Logseq), und ist ein wichtiges Werkzeug für die Erstellung von Dokumentationen. Aber auch jeder andere Textarbeiter, der an einem Werkzeug für *»write once - publish anywhere«* interessiert ist, sollte sich Pandoc einmal anschauen. Zumal es als reines Textformat mit jeder Versionskontrolle zusammenarbeitet. Und meine Erfahrungen als Autor lehrten mich: Mindestens jeder Verfasser wissenschaftlicher Texte sollte eine Versionsverwaltung nutzen (aber auch für sonstige Texte ist sie extrem nützlich).

Und spätestens bei der Erzeugung eines PDFs wirst Du auch wieder mit LaTeX konfrontiert, aber dieses Mal in einem sinnvollen Zusammenhang.

Mein eigener (derzeit im Entstehen begriffener) Pandoc-Workflow ist Logseq -> RStudio oder Texteditor (wegen R Markdown/Bookdown/Quarto) -> Pandoc -> Ausgabe als HTML/PDF und Epub. Logseq (als Markdown-fähiger Outliner) ist nicht jedermanns Baby, schau Dir daher auch Obsidian und Zettlr einmal genauer an.

Im Prinzip ist zwar Pandoc ähnlich trivial wie LaTeX, aber der Teufel steckt im Detail. Und da Textarbeit eher zu Deiner Berufswirklichkeit gehört, lohnt es sich meiner Meinung nach für Dich, ein Pandoc-Guru zu werden.

Und für den Einstieg habe ich auch eine Literaturempfehlung: »[Das ZEN von Pandoc. Bücher und E-Books einfach und professionell produzieren][a1]« von *Jan Ulrich Hasecke*, Solingen (Selbstverlag) 2015. Das Buch ist via Amazon erhältlich.

[a1]: https://www.amazon.de/Das-ZEN-Pandoc-Books-professionell/dp/1505218799?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=HS99BQCIQKGO&keywords=Das+ZEN+von+Pandoc&qid=1661009625&sprefix=das+zen+von+pandoc%2Caps%2C89&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=d0f50e8d90a3d2e3cfb90b5d883954bc&language=de_DE&ref_=as_li_ss_tl

---

**3 (Email-) Kommentare**

---

<%= a("k01") %>

>Feedback: Obsidian ist mir schon in der jüngeren Vergangenheit aufgefallen, als ich glaubte, mein Mangel an kreativen Ideen wäre eher auf meine Unfähigkeit zurückzuführen, besagte Ideen zu strukturieren (nur teilweise korrekt, diese Annahme). Obsidian werde ich mir jetzt sicher näher angucken, ebenso die anderen von Dir genannten Werkzeuge. Aber weil ich noch viel zu lernen habe - u.a. das systematische Einarbeiten in neue Problemfelder -, werde ich zunächst Deiner Leseempfehlung folgen. Das Buch ist schon auf meinem Kindle.   
Und vielleicht sollte ich Fortschritt und Erkenntnisgewinn zum Selber-nach- und Mitlesen dokumentieren. Eine ungenutzte Domain habe ich ja da noch am Herumliegen... Ich hatte mir schon überlegt, ob ich diese Dokumentation mit unserem initialen Mailwechsel eröffnen soll; nun bist Du mir zuvorgekommen.   
Danke für die Tipps!

*– Konstantin K.* <%= p("k01") %>

---

<%= a("k02") %>

>UTF-8 als Eingabeformat ist seit 2015 im LaTeX-Kernel eingebaut, die
Engines können auch Unicode-Fontencoding, beides ist also schon lange
kein Problem mehr.   
Wer mit Konvertern arbeiten möchte, sollte sich darüber im Klaren sein,
dass die Eingabedatei nur ein minimales Markup enthalten sollte. Jede
Konvertierung ist verlustbehaftet. Und je einfacher das Markup, desto
weniger geht bei der späteren Umsetzung verloren. Beispielsweise
Fußnoten und viele Querverweise oder Literaturzitate sprechen eher für
direkte Eingabe in LaTeX als für Umwege, welche auch immer. (Wir
sprachen noch nicht über mathematische oder chemische Formeln oder über
Abbildungen, Grafiken und Plots.)   
Markdown und Pandoc ist eine gute Lösung für Bleiwüsten. Org-Mode ist
auch eine; der Konverter ist in Org-Mode schon eingebaut. Wenn man im
Emacs mit Org-Mode arbeitet, kann man vom Outliner bis zur fertigen
Druckvorstufe alles in einem Mode machen. Wer lieber in einer
Textverarbeitung schreibt, könnte aber auch mit LibreOffice und
ODT-Ausgabe glücklich werden; dann weiter mit Pandoc; falls Java
vorhanden und kein Problem ist, auch Writer2LaTeX, das neuerdings wieder
gepflegt wird. Der jüngste Konverter für die Richtung LaTeX nach HTML
ist übrigens das Paket lwarp, es scheint ganz erfolgversprechend zu
sein, neben dem älteren tex4ht, ich habe es aber noch nicht ausprobiert.   
Wichtig scheint mir, dass das Konzept der Weichen Formatierung bzw. des
Generischen Markups erst einmal verstanden und beherrscht wird. Dann
kann man mit jeder Lösung gut arbeiten. Das können leider die wenigsten.
Ich habe in über zwanzig Jahren nur sehr wenige mathematische Formeln
mit LaTeX gesetzt, aber ich habe es nie bereut, LaTeX und die
dazugehörenden Grundlagen gelernt zu haben, ich verwende es täglich.   
Zur Literatur: Die dritte Auflage des LaTeX Companion sei in Arbeit,
hörte ich sagen. Alles übrige steht im Netz. Und wenn es da noch nicht
stehen sollte, sollten wir daran dringend etwas ändern. :)   
Keep TeXing! :)

*– Juergen F.* <%= p("k02") %>

---

<%= a("k0202") %>

>>Ein kleines bißchen Literatur und aktive Hilfe gibts übrigens auch noch bei der Deutschsprachigen Anwendervereinigung TeX e. V., a. k. a. <a href= "http://dante.de">Dante</a>!

*– Moss  (the TeXie)* <%= p("k0202") %>

