#title "Google korrigiert Sicherheitslücken in Chrome – 20220831"
#maintitle "Google korrigiert in Version 105 seines Browsers Chrome auch wieder Sicherheitslücken"
#prevp "2022083001"
#nextp "2022083102"

<%= html.getLink(imageref("vieraufeinemstreich-b"), "Vier auf einem Streich") %>

## Google korrigiert Sicherheitslücken in Chrome

Pünktlich zum Monatsende hat Google eine neue Version 105 (105.0.5195.52/53/54) seines Browsers Chrome veröffentlicht und schließt auch wieder [Sicherheitslücken](https://chromereleases.googleblog.com/2022/08/stable-channel-update-for-desktop_30.html).

Chrome aktualisiert sich (außer bei Linux) über die integrierte Update-Funktion, kann aber -- wenn es pressiert -- auch [hier geladen](https://www.google.com/intl/de/chrome/browser/) werden. *([[cert]])*

*[[photojoerg]]*