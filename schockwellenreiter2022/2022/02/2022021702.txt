#title "Chromebook für alles? – 20220217"
#maintitle "Speziell für ältere Rechner: ChromeOS für (fast) alles?"
#prevp "2022021701"
#nextp "2022021801"

<%= html.getLink(imageref("chromeosflex-b"), "Chrome OS Flex") %>

## Speziell für ältere Rechner: ChromeOS für (fast) alles?

Unser aller Datenkrake startet mal wieder einen Angriff auf die Desktop-Platzhirschen Windows und macOS, und das gar nicht mal so ungeschickt, denn mit dem brandneuen Produkt [Chrome OS Flex](https://www.googlewatchblog.de/2022/02/chrome-os-flex-google/) läßt sich Chrome&nbsp;OS sehr einfach auch auf alten Windows-Computern oder Macs installieren. Es steht als Preview zur Verfügung und kann frei heruntergeladen werden. Chrome OS Flex ist ein vollwertiges Chrome OS-Betriebssystem, das sich -- so Google -- auf die Fahnen geschrieben hat, alte Hardware zu retten.

[Es handelt es sich um eine vorwiegend für Unternehmen und Schulen konzipierte Variante](https://futurezone.at/produkte/google-chrome-os-alte-pcs-macs-betriebssystem/401907031), die genauso aussehen und sich bedienen lassen soll wie das normale Chrome OS. Auch die Releases sollen in ähnlichen Zeitabständen erfolgen. Hardwarebedingt könne es jedoch bei einigen Funktionen zu Einschränkungen kommen.

[Die Installation von Chrome OS Flex gestaltet sich dabei ausgesprochen einfach](https://www.ifun.de/chrome-os-flex-will-alte-macs-und-pcs-wiederbeleben-182307/). Einzige Voraussetzung ist ein USB-Anschluß an den genutzten Geräten, über den das Google-Betriebssystem von einem startfähigen USB-Stick aus auf die einzelnen Rechner aufgespielt wird, den man als Nutzer problemlos selbst erstellen kann. Die ganze Prozedur läßt sich laut Google innerhalb von nur wenigen Minuten erledigen.

Dennoch glaube ich, daß Chrome OS Flex nicht für alle Nutzer geeignet ist. Zum Beispiel möchte ich den Weg des hier beschriebenen Hacks »[Blender for Chromebook](https://m.all3dp.com/2/blender-for-chromebook/)« nicht gehen wollen, da versorge ich -- wenn es soweit ist -- mein betagtes MacBook Pro lieber mit einem Linux-Unterbau oder weiche auf diesen [DeskPi Lite](https://www.cnx-software.com/2022/01/31/deskpi-lite-a-raspberry-pi-4-enclosure-with-full-hdmi-ports-two-extra-usb-ports/) aus.

Dennoch bin ich natürlich froh, daß ich mir prophylaktisch [ein Chromebook zugelegt habe](http://blog.schockwellenreiter.de/2021/12/2021120701.html), das nun [meinen Fuhrpark ergänzt](Der elektrische Trommelwirbel vom Tage – 20220107) (und ein bislang ungenutzter [Raspberry Pi](cp^Raspberry Pi) liegt hier auch noch irgendwo rum). <del>Man</del> Ich will schließlich gegen alle Gemeinheiten der Hardwarehersteller gewappnet sein.