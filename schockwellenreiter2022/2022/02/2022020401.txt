#title "Neugiertool-Update erfolgreich eingefahren – 20220204"
#maintitle "Neugiertool-Update (hoffentlich!) erfolgreich eingefahren"
#prevp "2022020301"
#nextp "2022020402"

<%= html.getLink(imageref("oldtimer-b"), "Oldtimer, etwa 1942") %>

## Neugiertool-Update (hoffentlich!) erfolgreich eingefahren

Heute war wieder die Zeit für feuchte Hände und einen rasendenden Puls, denn mein [Neugiertool](cp^Piwik), das früher einmal Piwik hieß, nun aber Matomo genannt werden will, verlangte nach einem Update. Regelmäßige Leser dieses <del>Blogs</del> Kritzelhefts wissen, daß ich wegen einschlägiger, schlechter Erfahrung, die Nuller-Versionen dieser Software grundsätzlich ignoriere, aber diesen Monat ging es Schlag auf Schlag (billiger Wortwitz):

Gestern nacht erhielt ich eine Email, daß Matomo in der Version 4.7.0 eines Updates harre. Nun gut, daß war die Nuller-Version, die konnte also ignoriert werden. Aber exakt 24 Stunden später, heute Nacht, wurde das Update auf die Version 4.7.1 freigegeben. Das ging aber schnell und ich möchte allen danken, die sich die Zeit nehmen und diese buggigen Nuller-Versionen dennoch installieren und die Fehler nach Hause telephonieren, so daß die Bananen-Software nicht zu lange beim Kunden reifen muß.

Aber egal, ich habe mir dann ein Herz gefaßt und die Einser-Version eingespielt und es sieht so aus, als ob dieses Update auch bei meinem [Spielzeug-Provider](http://www.strato.de/) funktioniert. Hoffen wir also das Beste.

Und ja, das Bannerbild ist eine Anspielung auf [Urgesteine auf der Langstrecke](Urgestein auf der Langstrecke – 20220202), denn wenn heute schon der Tag der billigen Witze ist, dann aber auch richtig billig!