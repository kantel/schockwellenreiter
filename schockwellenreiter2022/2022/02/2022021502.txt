#title "Sicherheitsupdate macOS Big Sur und Catalina – 20220215"
#maintitle "Apple veröffentlicht Sicherheitsupdate macOS Big Sur 11.6.4 und Catalina Sicherheitsupdate 2022-002"
#prevp "2022021501"
#nextp "2022021601"

<%= html.getLink(imageref("trafficstop-b"), "Traffic Stop (1922)") %>

## Apple veröffentlicht Sicherheitsupdate macOS Big Sur und Catalina

Apple hat [noch ein Sicherheitsupdate](Flickentag in Cupertino – 20220211) macOS Big Sur 11.6.4 und  das Sicherheitsupdate 2022-002 für Catalina freigegeben. Näheres dazu finden sich in [diesem Support-Dokument](https://support.apple.com/en-us/HT201222).

Das Update steht wie üblich über die Softwareaktualisierung bereit. *([[cert]])*