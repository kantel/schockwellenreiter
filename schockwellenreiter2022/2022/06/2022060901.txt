#title "Nachts im Bürgerpark, Stage 5 – 20220609"
#maintitle "Bitsy-Devlog: Nachts im Bürgerpark (Stage 5)"
#prevp "2022060802"
#nextp "2022061001"

<%= html.getLink(imageref("bitsyparkstage5-b"), "Nachts im Bürgerpark (Stage 5)") %>

## Bitsy-Devlog: Nachts im Bürgerpark (Stage 5)

Ich bin in meiner [Bitsy](cp^Bitsy)-Bastelstube wieder ein wenig vorangekommen, langsam nimmt das Experiment den Charakter eines Spiels an. Denn nun kann die Spielerin oder der Spieler nicht nur ziellos durch den Park streifen, sondern bis zu fünf Diamanten einsammeln und den Bürgerpark so als reicher Mensch verlassen.

Außerdem besitzt der Park -- im Gegensatz zur Anfangskonfiguration -- nun eine nennenswerte Anzahl von Bewohnern, die alle um Mitternacht zum Leben erwecken und meinen Avatar mehr oder wenig sinnlos vollquatschen. Denn hier ist die nächste Baustelle: Die Dialoge der Akteure sollten zum Spielgeschehen passen und im Idealfall auf das Spielgeschehen reagieren.

Da Videotutorials den Nachteil haben, daß man in ihnen nicht blättern kann, habe ich mir für das weitere Vorgehen vier Tutorials herausgesucht, die man notfalls ausdrucken und mit Eselsohren versehen kann:

- Schon einmal verlinkt hatte ich auf das »offizielle« [Bitsy-Tutorial](https://www.shimmerwitch.space/bitsyTutorial.html) von *Claire Morwood*. Allerdings habe ich alles, was da behandelt wird, in »meinem« Bitsy schon eingebaut.
- Etwas weiter geht das 22-seitige PDF »[Bitsy – erstelle Dein eigenes Spiel](https://mint-zirkel.de/wp-content/uploads/2020/08/Bitsy_Tutorial-neu.pdf)« des MINT-Zirkels. Auch auf dieses hatte ich schon (mindestens) einmal verlinkt.
- Ähnlich informativ ist der »[Bitsy-Workshop](https://static1.squarespace.com/static/58930a6c893fc0a33ae624db/t/5bacd94ac83025ead3937071/1538054510407/BITSY-WORKSHOP.pdf)« von *Cecile Richard* (aka *araiva*), ein 18-seitiges PDF.
- Und vom User *ayolland* stammt das »[Bitsy Variables Tutorial](https://ayolland.itch.io/trevor/devlog/29520/bitsy-variables-a-tutorial)«, das die Konfusion über Variablen in Bitsy beseitigen will. Seine [Seite auf Itch.io](https://ayolland.itch.io/) beherbergt noch einige weitere Bitsy-Perlen.

Ich habe die derzeitige Fassung jedenfalls erst einmal wieder hier hochgeladen: **[Nachts im Bürgerpark](https://nachts-im-buergerpark.glitch.me/)**. Ihr solltet sie problemlos durch»spielen« können. Sollte ich dennoch einen Fehler übersehen haben, bitte ich um eine kurze Nachricht (auch bei Tippfehlern).

**War sonst noch was?** Ach ja, viele Bitsy-Spiele, -Tutorial und/oder -Assets findet Ihr auf [Itch.io](https://itch.io/), dem »[Kabinett des kuriosen Zeitvertreibs](https://www.zeit.de/digital/games/2015-07/itch-io-games-plattform-spiele-indie)«, wie *Eike Kühl* die Indie-Game-Plattform auf Zeit Online titulierte. Seit der Lektüre bin ich hin- und hergerissen: Ich glaube, ich werde mir dort (nicht nur) für meine Bitsy-Experimente ebenfalls einen Account zulegen.
