#title "Neue Farb (-paletten) braucht das Land – 20220623"
#maintitle "Neue Farb (-paletten) braucht das Land"
#prevp "2022062202"
#nextp "2022062401"

<%= html.getLink(imageref("goetzpal-b"), "Farbpalette nach Lothar Götz") %>

## Neue Farb (-paletten) braucht das Land

**Lothar Götz** (geboren 1963 in Gunzburg) ist ein in London, Berlin und Sunderland lebender Künstler, der mit Vorliebe knallige Farben benutzt, mit denen er unter anderem -- wie hier in [Hereford](https://www.bbc.co.uk/news/uk-england-hereford-worcester-61869923.amp) -- die Fassaden von Gebäuden verschönert. Und da man ja bekanntlich nie genug Farbpaletten haben kann, habe ich aus diesen Farben eine Palette gebastelt und sie in **[meinem Wiki](cp^Farbpalette nach Lothar Götz)** veröffentlicht.

Ich weiß noch nicht was, aber irgendetwas werde ich damit schon noch anstellen, dazu sind die Farben einfach zu knallig.&nbsp;🤡