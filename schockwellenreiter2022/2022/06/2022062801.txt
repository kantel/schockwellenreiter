#title "Mozilla veröffentlicht neue Firefox-Version – 20220628"
#maintitle "Mozilla veröffentlicht neue Firefox-Version"
#prevp "2022062701"
#nextp "2022062901"

<%= html.getLink(imageref("jugendno3-b"), "Jugend (Nummer 3)") %>

## Mozilla veröffentlicht neue Firefox-Version 102.0

Die Entwickler des Mozilla Firefox  haben die neue [Version 102.0](https://www.mozilla.org/en-US/firefox/102.0/releasenotes/) und die [Version ESR 91.11](https://www.mozilla.org/en-US/firefox/91.11.0/releasenotes/) veröffentlicht und darin auch wieder Sicherheitslücken behoben.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*