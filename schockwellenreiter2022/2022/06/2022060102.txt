#title "Mozilla Thunderbird neue Version freigeegeben – 20220601"
#maintitle "Mozilla Thunderbird neue Version 91.10.0 freigegeben"
#prevp "2022060101"
#nextp "2022060201"

<%= html.getLink(imageref("klint01-b"), "Altarbild 1") %>

## Mozilla Thunderbird neue Version 91.10.0 freigegeben

[Auf den Feuerfuchs](Mozilla veröffentlicht neue Firefox-Version – 20220531) folgt fast immer auch der Donnervogel: Die Entwickler des [Mozilla Thunderbird](http://www.mozillamessaging.com/) haben das  [Sicherheits-Update auf die Version 91.10.0](https://www.thunderbird.net/en-US/thunderbird/91.10.0/releasenotes/) freigegeben.

Thunderbird weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Thunderbird` angestoßen werden. *([[cert]])*

