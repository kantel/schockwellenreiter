#title "Sicherheitsupdate des Foxit Reader – 20220629"
#maintitle "Sicherheitsupdate des Foxit Reader (PDF Reader) auf 12.0"
#prevp "2022062902"
#nextp "2022070101"

<%= html.getLink(imageref("schwan71915-b"), "Der Schwan 7, 1915") %>

## Sicherheitsupdate des Foxit Reader (PDF Reader) auf 12.0

Mit der Version 12 (12.0.0.12394) des nicht nur unter Windows beliebten PDF Betrachters Foxit Reader haben die Entwickler auch [Schwachstellen](https://www.foxit.com/support/security-bulletins.html) behoben.

Die neue Version kann [hier geladen](https://www.foxitsoftware.com/downloads/#Foxit-Reader/) werden. *([[cert]])*