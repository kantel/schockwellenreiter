#title "Replit: Eine Online-IDE für Chromebook-Nutzer – 20220626"
#maintitle "Replit: Noch eine Online-IDE für Chromebook-Nutzer"
#prevp "2022062501"
#nextp "2022062701"

<%= html.getLink(imageref("replit-b"), "Replit, eine Online-IDE") %>

## Neu in meinem Wiki: Replit, noch eine Online-IDE für Chromebooks

Nachdem ich gestern meinen [Beitrag zu Trinket](Trinket: Python für Chromebook-Nutzer – 20220625), der Python-Online-IDE für Chromebook-Nutzer, veröffentlicht hatte, machte mich jemand via Twitter auf **[Replit](cp^Replit)** als Alternative zu [Trinket](cp^Trinket) aufmerksam. Neugierig wie ich bin, habe ich mir das Teil mal angeschaut:

**Replit**, ehemals **Repl.it**, ist ein in San Francisco ansässiges Start-up und eine Online-IDE. Der Name leitet sich vom Akronym REPL ab, das für »read-eval-print loop« steht. Das Unternehmen wurde 2016 gegründet. 

Replit unterstützt über 50 Programmiersprachen. Für »normale« User gibt es einen kostenlosen Account mit einer unbeschränkten Anzahl von öffentlichen Repos (im Replit-Jargon »Repls« genannt), keine privaten Repos, 500 MB Speicherplatz, 500 MB Hauptspeicher und 0,2 bis 0,5 virtuelle CPUs. Das dürfte für die meisten Anwendungen ausreichen. Wer mehr benötigt, muß löhnen.

Replit unterstützt die Versionsverwaktung mit [GitHub](cp^GitHub) und kann mit [Glitch](cp^Glitch) kooperieren. Beide Features prädestinieren den Dienst für das Arbeiten mit [meinem Chromebook](http://blog.schockwellenreiter.de/2021/12/2021120701.html).

### Tutorials

- Jessica Wilkins: *[Code, create, and learn together](https://www.freecodecamp.org/news/how-to-use-replit/). Use our free, collaborative, in-browser IDE to code in 50+ languages – without spending a second on setup*, Free Code Camp vom 4. November 2021

### Literatur

- Packy McCormick: *[Replit: Remix the Internet](https://www.notboring.co/p/replit-remix-the-internet). Replit Raises $80M Series B to Bring the Next Billion Software Creators Online*, Not Boring vom 9. Dezember 2021

### Links

- [Replit Home](https://replit.com/)
- [Replit Blog](https://blog.replit.com/)
- [Replit Pricing](https://replit.com/pricing)
- [Replit in der Wikipedia](https://en.wikipedia.org/wiki/Replit)

Ähnlich wie Trinket oder Glitch macht der Dienst einen für mich hinreichend verspielten Eindruck. Da ich so etwas mag, habe ich mir dort nicht nur einen Account zugelegt, sondern **[Replit](cp^Replit)** auch eine Seite in [meinem Wiki](cp^Startseite) spendiert. Schaun wir mal. was daraus wird. *Still digging!*