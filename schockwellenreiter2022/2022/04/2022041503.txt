#title "GameDev und Computer Science – 20220415"
#maintitle "GameDev und Computer Science (noch mehr Video-Tutorials)"
#prevp "2022041502"
#nextp "2022041504"

<%= html.getLink(imageref("mapsgreenfoot-b"), "Using Maps in Greenfoot") %>

## GameDev und Computer Science (noch mehr Video-Tutorials)

Für den Fall daß bei mir und/oder bei Euch über die Feiertage Langeweile aufkommen sollte, noch ein paar nette Playlists  und Video-Tutorials von *Jed Wilshire*, der auch umgehend in meinem Feedreader gelandet ist. Ansonsten steht es hier nur, damit ich es nicht vergesse:

- Das [Tile Game Pygame Tutorial](https://www.youtube.com/playlist?list=PLFXF5FMqkMX-SI0l5FRgAD5fPRS2rQzZX) ist ein zwölfteilige Playlist (Gesamtlaufzeit etwa vier Stunden, letztes Update im Dezember 2021, also noch halbwegs aktuell), die eine gründliche Erkundung von [Pygame](cp^Pygame) verspricht.

- [Platform Game Starter in 30 Minutes](https://www.youtube.com/watch?v=ktrGhtC5WZk): In this video, I describe how to make a basic platforming game in Python in 30 (ish) minutes.  We use the popular Pygame framework. ([Quellcode @ GitHub](https://github.com/jedwilshire/Platformer-30-Minutes)).

- [Flying Ace Pygame Project 1](https://www.youtube.com/playlist?list=PLFXF5FMqkMX_aK1apQBNx5ANqDjJ3uqGg): 20 Minuten eines offensichtlich unfertigen Pygame-Projekts. Die (graphischen) Assets sind vermutlich von [Kenny.nl](https://www.kenney.nl/assets/tappy-plane).

- [Greenfoot](cp^Greenfoot) ist eine Java-Plattform zur Spieleprogrammierung (nicht nur) für Anfänger. Auch hierfür hat *Jed Wilshire* eine nette Playlist parat: »[Greenfoot Java Programming](https://www.youtube.com/playlist?list=PLFXF5FMqkMX_IlXfhqZvlSrIpRWAXika5)« (16 Videos, etwa acht Stunden Gesamtspieldauer – also nur etwas für Menschen mit viel Geduld und Zeit, die unbedingt Greenfoot lernen wollen). Spoiler: Es lohnt sich (wenn man von Java keine Pickel bekommt), denn Greenfoot ist eine nette Bibliothek.

Daß mit Greenfoot auch recht komplexe und anspruchsvolle Projekte implementiert werden können, wird unter anderem hier gezeigt: »[Greenfoot and GridWorld - the AP* Computer Science Case Study](https://www.greenfoot.org/doc/ap)«. Und auch im Beitrag »[Using Maps](https://www.greenfoot.org/doc/dynamic)« werden einige anspruchsvolle Simulationen vorgestellt. Ich muß das wissen, denn bevor ich [Processing](cp^Processing)([.py](cp^processingpy)) für mich entdeckte, hatte ich durchaus ernsthaft auch mit Greenfoot experimentiert.