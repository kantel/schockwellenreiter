#title "Gelöst: Fenced Code Blocks in Logseq – 20220401"
#maintitle "Gelöst: Fenced Code Blocks in Logseq"
#prevp "2022040102"
#nextp "2022040104"

<%= html.getLink(imageref("codeblockslogseq-b"), "Eingezäunter Quellcode in Logseq") %>

## Gelöst: Fenced Code Blocks in Logseq

Hier hatte ich vor ein vierzehn Tagen noch [ein paar offene Fragen](Logseq-Fragen – 20220317) zu [Logseq](cp^Logseq) gestellt, dem markdown- und zettelkastenfähigen Outliner, den ich seit etwa zwei Monaten unter anderem dazu nutze, die Beiträge im *Schockwellenreiter* vorzubereiten und parallel dazu mir eine verlinkte Wissensbasis als digitalen [Zettelkasten](cp^Zettelkasten) anzulegen. Nun habe ich durch wildes Herumprobieren die Lösung einer dieser Fragen -- nämlich wie man Codeblöcke anlegt -- herausgefunden:

Berücksichtig man nämlich, daß innerhalb einer Einrückung (eines Spiegelstrichs) neue Zeilen mit `[Shift]+[Enter]` erzeugt werden und beherzigt man dies auch bei eingezäunten Code-Blöcken (`fenced code`), dann bekommt man wunderschönen, eingefärbten Quelltext angezeigt -- sogar mit Zeilennummern (siehe Screenshot). Eigentlich ganz einfach, da hätte ich auch schon früher darauf kommen können (aber manchmal sieht man den Wald vor lauter Bäumen nicht).

Als Code-Block-Begrenzung funktionieren -- wie in fast allen Markdown-Dialekten üblich -- sowohl die drei Backtics (<code>```</code>) wie auch drei Tilden (`~~~`). Und man kann da natürlich auch den Identifier für die  Programmiersprache anhängen, um das *Syntx Highilighting* zu aktivieren, also zum Beispiel `~~~python`.

Ich weiß nicht, welcher *Syntax Highlighter* hinter den Kulissen von Logseq werkelt. aber da müßte eigentlich noch mehr drin sein. *Still digging!*