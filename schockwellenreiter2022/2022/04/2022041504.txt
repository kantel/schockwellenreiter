#title "Schau Mama, ein Hundebild! – 20220415"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2022041503"
#nextp "2022041701"

<%= html.getLink(imageref("hierwachteinspitz-b"), "Hier wacht ein Spitz") %>

**Schau Mama, ein Hundebild!** Auch ein Karfreitag ist ein Freitag und für einen strenggläubigen Atheisten wie mich sogar ein ganz normaler Freitag mit den üblichen Freitagsritualen »Freitags keinen Fisch« und »Hundebild«. Und wie jede Woche soll Euch das Photo der kleinen Fellnase darüber hinwegtrösten, daß es die nächsten <del>zwei</del> drei Tage vermutlich keine, aber mit Sicherheit nur wenige Updates hier im *Schockwellenreiter* geben wird.

Denn ich habe gerade -- was das Thema »Retrogaming und Künstliche Intelligenz« angeht -- einen Lauf und werde daher die Feiertage nutzen, das Thema noch etwas auszuarbeiten und in [Pygame Zero](cp^Pygame Zero) so etwas wie eine »Blaupause« zu erstellen, die den Rahmen für die zukünftigen KI-Programme vorgeben soll. Das entbehrt nicht einer gewissen Ironie, weil Pygame Zero ja mit dem Motto »Spieleprogrammierung ohne Blaupase« *([creating games without boilerplate](https://pygame-zero.readthedocs.io/en/stable/))* angetreten ist. Aber wenn man sich auf die einzelnen Algorithmen konzentrieren will, ist solch ein immer wiederverwertbarer Rahmen einfach sinnvoll.

Warum das Thema jetzt gerade hochkocht und ich froh über den momentanen »Lauf« bin? Weil da irgendjemand schändlicherweise etwas von einer *deadline* gemurmelt hat. Die liegt zwar noch weit in der Zukunft, aber anbetracht der Tatsache, daß ich durch meinen [Schlaganfall](http://blog.schockwellenreiter.de/2021/11/2021111201.html) in meinen Fähigkeiten immer noch eingeschränkt bin, bin ich über alles froh, was ich jetzt erledigen kann. Ich bin aber guter Dinge, über die Feiertage ein schönes Stück voranzukommen.

Auch bei diesem Projekt ist übrigens [Logseq](cp^Logseq) ein (für mich) hilfreiches Werkzeug. Denn da meine Schreibgeschwindigkeit immer noch sehr langsam ist, ist es ungemein nützlich, wenn ich meine Gedanken ohne Rücksicht auf mögliche Tippfehler (und davon produziere ich zur Zeit noch viele) im Outliner vorformulieren und gliedern kann.

**Ist sonst noch was?** Ach ja, Sonnabend ist [Wochenmarkt](https://www.diemarktplaner.de/die-dicke-linda/) und da werde ich wieder auf die [Jagd nach Fischbrötchen](https://www.diemarktplaner.de/barracuda-fischfeinkost/) gehen (an allen Tagen, die kein Freitag sind, dürfen auch strenggläubige Atheisten Fisch essen) und in den Genuß von Kaffee  kommen (denn [Blech & Bohne](https://www.diemarktplaner.de/blech-bohne/) sind wieder da). Und natürlich will und soll der Spitz ebenfalls bespaßt werden.

Eigentlich hatte ich vor, aus alter Tradition heute das Video mit dem [Leben des Brian](https://de.wikipedia.org/wiki/Das_Leben_des_Brian) in den Rekorder zu schieben (denn dieser Film ist von den christlichen Dunkelmännern [am Karfreitag verboten worden](https://www.inside-digital.de/news/karfreitag-das-leben-des-brian-und-heidi-darum-sind-diese-filme-verboten) -- das hat er übrigens mit »Heidi« gemeinsam), aber ich glaube, ich werde heute abend zur Abwechslung mal [Hook](https://de.wikipedia.org/wiki/Hook_(Film)) schauen (ZDF Neo, 22:00 Uhr), denn ein Film mit *Robin Williams* ist unchristlich genug und nur vergessen worden, von den Dunkelmännern auf den Index zu setzen.

Nachdem das Wetter heute richtig Sch… war, soll ab morgen die Sonne wieder scheinen. Daher -- wie jedes Jahr an diesen Feiertagen -- mein »Fröhliche Ostern, fröhliche Western« an Euch alle da draußen. Wir lesen uns spätestens am Dienstag wieder. *[[photogabi]]*