#title "Creative Coding, KI und mehr – 20221011"
#maintitle "Creative Coding, Künstliche Intelligenz, Simulationen und mehr"
#prevp "2022101001"
#nextp "2022101102"

<%= html.getLink(imageref("mlprocessing-b"), "Maschinelles Lernen in Processing") %>

## Creative Coding, Künstliche Intelligenz, Simulationen und mehr

Es ist mal wieder an der Zeit, in meinem [zweiten Gehirn](cp^Logseq) für ein wenig Ordnung zu sorgen. Daher folgt hier eine mehr oder weniger unsortierte Linkschleuder mit allem, was auch nur im Entfernten mit kreativem Programmieren, Künstlicher Intelligenz und/oder Simulationen zu tun hat. Doch der Reihe nach:

- *[Jeongin Lee](https://twitter.com/JeonginLee/status/1579577874906247168)* hat sein diesejähriges »Google Summer of Code«-Projekt fertiggestellt und auf [GitHub veröffentlicht](https://github.com/jjeongin/creative-machine) (MIT-Lizenz). Es heißt »[Creative Machine](https://jjeongin.github.io/creative-machine/)« und ist eine Bibliothek für machinelles Lernen mit [Processing](cp^Processing). Eine ihrer Spezialitäten scheint Gesichtserkennung zu sein (siehe [Screenshot](Maschinelles Lernen in Processing)). Da ich ähnliches auch mal in [Processing.py](cp^processingpy) mit [OpenCV](https://de.wikipedia.org/wiki/OpenCV) eher amateurhaft [probiert hatte](http://py.kantel-chaos-team.de.s3-website-us-east-1.amazonaws.com/08images/#opencv-und-processingpy), sollte ich diese Bibliothek unbedingt einmal testen. Ich bin gespannt, wie sie sich schlägt.

- Es muß nicht immer Visual Studio Code sein: Auch wenn man es wegen der Omnipräsenz des Platzhirschen kaum wahrnimmt, es gibt durchaus (Texteditor- und IDE-) Alternativen. *Fadin Geek* hat einige von ihnen [vorgestellt](https://medium.com/codex/text-editors-ides-which-deserve-more-attention-d406f2abcf94) (Medium.com-Link). Ein paar davon sollen auch durchaus für Chromebook-Nutzer geeignet sein.

<%= html.getLink(imageref("jsoncrack-screenshot-b"), "JSON Crack") %>

- Wer JSON-Dateien unter die Lupe nehmen möchte, sollte einen Blick auf [JSON Crack](https://jsoncrack.com/), das freie (GPL), browerbasierte Tool ([Quellcode auf GitHub](https://github.com/AykutSarac/jsoncrack.com)) bietet eine wunderschöne und übersichtliche Visualisierung selbst hochkomplexer JSON-Strukturen an. Es kann sowohl online im Browser wie auch lokal in einem Docker-Container genutzt werden.

<%= html.getLink(imageref("chessboardpythonemoji-b"), "Ein Schachbrett im Browser (mit Python und Emojis)") %>

- [Ein Schachbrett im Browser](https://t.co/OAltyp67IQ) mit Python [Pyp5.js](cp^pyp5js) und Emojis hat hier jemand ziemlich genial zusammengebastelt. Es sagt mir nicht nur, daß ich mir Pyp5.js (als Alternative zu [P5.js](cp^p5js)) genauer anschauen sollte, sondern ich sollte auch über die Verwendung von Emojis in (Python-) Programmen (als Alternative zu herkömmlichen Bildchen) nachdenken.

- Noch mehr »Bildchen« (nicht nur) für kreative Programmierer: Auf der Seite [SVG Silh](https://svgsilh.com/de/) findet Ihr hunderte von SVGs für umme, denn sie stehen alle unter der [CC0-Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de) zum Download und zur freien Verwendung zu Eurer Verfügung.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lJ2VlcI_JuY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**War sonst noch was?** Ach ja, der YouTuber *Gravitar* hat mit Python und [Pygame](cp^Pygame) eine [Waldbrandsimulation in nur 28 Zeilen](https://www.youtube.com/watch?v=lJ2VlcI_JuY) programmiert. Die Simulation kann über Parameter (Anfangsbestand der Bäume, Wachstumsrate und Brandwahrscheinlichkeit) angepaßt werden. Natürlich gibt es seinen [Quellcode](https://github.com/Gravitar64/A-beautiful-code-in-Python) unter einer MIT-Lizenz auf GitHub. Und er schreibt dazu: *»Auf die Idee bin ich durch [diesen Artikel des Schockwellenreiters](Waldbrandsimulation im Terminal – 20220829) (aka Jörg Kantel) gekommen.«* Danke für die Blumen.

Bei der Gelegenheit möchte ich auch noch einmal auf meinen eigenen [Waldbrand-Simulator](http://py.kantel-chaos-team.de.s3-website-us-east-1.amazonaws.com/12zellautom/#der-waldbrand-simulator) hinweisen, den ich vor einigen Jahren in Processing.py (und leider noch nicht mit Emojis -- siehe oben -- Processing.py kann nicht mit Emojis, als Alternative habe ich auf [Twitters Twemojis](https://github.com/twitter/twemoji) zurückgegriffen) verfaßt hatte.

Und wo ich schon bei Waldbränden bin: Von *Matheus Feijoo* gibt es den Beitrag »[Visualising satellite data with Pydeck and Streamlit to discover possible forest fires worldwide](https://medium.com/codex/visualising-satellite-data-with-pydeck-and-streamlit-to-discover-possible-forest-fires-worldwide-7cb4f25351ba)« (leider wieder ein Medium.com-Link), der das Thema noch einmal auf die Spitze treibt. Auch seinen [Quellcode](https://github.com/MatheusFeijoo/forestfires) gibt es auf GitHub und die [fertige Applikation](https://matheusfeijoo-forestfires-app-q9x54z.streamlitapp.com/) könnt Ihr in der [Streamlit](cp^Streamlit)-Cloud begutachten.
