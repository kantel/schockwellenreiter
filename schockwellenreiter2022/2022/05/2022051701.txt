#title "Patchday bei Apple – 20220517"
#maintitle "Patchday bei Apple"
#prevp "2022051601"
#nextp "2022051801"

<%= html.getLink(imageref("architektkirchner-b"), "Kirchner als Architekt") %>

## Security Alert: Monatlicher Patchday bei Apple

Gestern war Flickentag im Hause mit dem angebissenen Apfel im Logo. Obwohl dieses Mal seltsamerweise (noch?) kein Update für Apples schlaue Uhr dabei ist, sind sie doch so zahlreich, daß ich sie -- wie nahezu jeden Monat -- auch dieses Mal nur *en bloc* abhandeln kann:

### Apple veröffentlicht macOS Monterey 12.4

Apple hat das Update auf die neue Betriebssystemversion macOS Monterey 12.4 freigegeben und bietet unter anderem [Sicherheitsupdates](https://support.apple.com/de-de/HT213257).

Das Update steht über die Systemeinstellungen zur Verfügung.

### Apple veröffentlicht Sicherheitsupdate auf macOS Big Sur und Catalina

Apple hat auch das Sicherheitsupdate für [macOS Big Sur 11.6.6](https://support.apple.com/de-de/HT213256) und [Catalina 2022-004](https://support.apple.com/de-de/HT213255) freigegeben.

Das Update steht wie üblich über die Softwareaktualisierung bereit.

### Apple veröffentlicht Update auf iOS 15.5 und iPadOS 15.5

Apple hat das Update auf [iOS 15.5](https://support.apple.com/de-de/HT212788) und [iPadOS 15.5](https://support.apple.com/de-de/HT212789) bereitgestellt und behebt damit  [kritische Sicherheitsprobleme](https://support.apple.com/de-de/HT213258).

Das Update auf auf iOS 15.5 und iPadOS 15.5 kann über OTA (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung`, an ausreichender Akku-Kapazität und freien Speicherplatz sollte gedacht werden) oder über iTunes erfolgen.

Eine vorherige Sicherung des Geräts ist wie immer sehr zu empfehlen.

*([[cert]])*
