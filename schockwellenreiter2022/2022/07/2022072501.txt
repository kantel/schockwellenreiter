#title "Pimp my Chromebook with TigerJython – 20220725"
#maintitle "Pimp my Chromebook – with TigerJython"
#prevp "2022072402"
#nextp "2022072601"

<%= html.getLink(imageref("chromebooktigerjython-b"), "Pimp my Chromebook: TigerJython") %>

## Pimp my Chromebook continued – with TigerJython

Das Projekt »[Pimp my Chromebook](Wilde Chromebook-Träume – 20220716)« schreitet immer weiter voran. Dieses Mal war es [wieder *Robert Sch.*](RStudio auf meinem Chromebook – 20220724), der mir geholfen hat, auch [TigerJython](cp^TigerJython) auf [meinem Chromebook](http://blog.schockwellenreiter.de/2021/12/2021120701.html) zum Laufen zu bekommen:

Im Prinzip hatte ich schon vor alles richtig gemacht, das Programmpaket mit

~~~bash
wget https://www.tjgroup.ch/download/TigerJython.tar.gz
tar -xf TigerJython.tar.gz
~~~

heruntergeladen und entpackt. Lediglich der letzte, winzige Schritt fehlte mir noch. Nach dem Wechsel in das Verzeichnis `TigerJython` kann nämlich das Programm nicht einfach mit

~~~bash
tigerjython
~~~

aufgerufen, sondern es muß mit dem Kommando

~~~bash
./tigerjython
~~~

gestartet werden. Das Voranstellen der Pfadangabe `./` hatte ich einfach nicht mehr auf dem Schirm, obwohl es bei [Ren'Py auch schon notwendig war](Ren'Py auf meinem Chromebook (2) – 20220720).

Zu meiner Entschuldigung kann ich nur sagen, daß ich an meinem letzten (und bisher einzigen) UNIX-Kurs Anfang der 1990er Jahre teilgenommen hatte und daß er nicht Linux, sondern [System&nbsp;V](https://de.wikipedia.org/wiki/System_V) behandelte. Ich bin also deutlich aus der Übung und kann mich wieder nur bei *Robert Sch.* bedanken, daß er mir auf die Sprünge geholfen hat.

Auch wenn noch ein paar Projekte mehr realisiert werden könnten (zum Beispiel [Twine](cp^Twine 2) oder [GitHub Desktop](cp^https://meshworld.in/install-github-desktop-on-ubuntu-20-04-or-ubuntu-based-distributions/)), bin ich mit meinem Chromebook weitergekommen, als ich je zu träumen gewagt hätte. Ich kann mit [Thonny](cp^Thonny) in Python, [Pygame Zero](cp^Pygame Zero) und [Py5](cp^Py5) programmieren, mit [R](cp^GNU R) und [RStudio](cp^RStudio) seltsame Dinge anstellen (hier weiß ich selber noch nicht, wohin die Reise gehen wird) und ich kann mich mit [Ren'Py](cp^renpy) in der Programmierung interaktiver Geschichten versuchen. Und das alles auch offline und ohne meine Seele an Google verkaufen zu müssen. Herz, was willst Du mehr?