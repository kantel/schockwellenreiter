#title "Apfel-Fickentag – 20220721"
#maintitle "Apple veröffentlicht Sicherheitsupdates"
#prevp "2022072002"
#nextp "2022072102"

<%= html.getLink(imageref("bacchantin1862-b"), "Bachantin spielt mit einer Ziege") %>

## Apple veröffentlicht Sicherheitsupdates

Ich irre mich sicher, aber es ist gefühlt eine halbe Ewigkeit her, seit die Firma aus Cupertino mit dem angebissenen Apfel im Logo ihren letzten Patchday hatte. Aber nun ist es wieder so weit und es sind -- wie fast immer -- so viele Updates, daß ich sie hier nur *en bloc* veröffentlichen kann. Doch der Reihe nach:

### Apple veröffentlicht macOS Monterey 12.5

Apple hat das Update auf die  neue Betriebssystemversion macOS Monterey 12.5 freigegeben und bietet unter anderem [Sicherheitsupdates](https://support.apple.com/de-de/HT213345).

Das Update steht über die Systemeinstellungen zur Verfügung.

### Apple veröffentlicht Sicherheitsupdate auf macOS Big Sur und Catalina

Apple hat auch die Sicherheitsupdates für [macOS Big Sur 11.8](https://support.apple.com/de-de/HT213344) und [Catalina 2022-05](https://support.apple.com/de-de/HT213343) freigegeben.

Die Updates stehen wie üblich über die Softwareaktualisierung bereit.

### Apple veröffentlicht Update auf iOS 15.6 und iPadOS 15.6

Apple hat das Update auf iOS 15.6 und iPadOS 15.6 bereitgestellt und behebt damit [kritische Sicherheitsprobleme](https://support.apple.com/de-de/HT213346).

Das Update auf auf iOS 15.6 und iPadOS 15.6 kann über OTA  (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung`, an ausreichender Akku-Kapazität und freien Speicherplatz sollte gedacht werden) oder über iTunes erfolgen.

Eine vorherige Sicherung des Geräts ist wie immer sehr zu empfehlen.

### Apple stellt watchOS 8.7 bereit

*Last but not least* hat Apple für die Apple Watch watchOS 8.7 bereitgestellt und behebt damit  auch [Sicherheitsprobleme](https://support.apple.com/de-de/HT213340).

Die Aktualisierung wird über die Apple-Watch-App auf dem iPhone gestartet unter `Allgemein > Softwareupdate`. Dabei sollte sich die Uhr in Reichweite des iPhones befinden, mit dem Ladekabel verbunden und mindestens zu 50 Prozent geladen sein. 

*([[cert]])*