#title "Wie man Pi erschießt (in TigerJython mit GUI) – 20220724"
#maintitle "Wie man Pi erschießt (in TigerJython mit GUI)"
#prevp "2022072401"
#nextp "2022072501"

<%= html.getLink(imageref("pierschiessen-b"), "Wie man Pi mit TigerJython erschießt") %>

## Wie man Pi erschießt (in TigerJython mit GUI)

Als ich [gestern schrieb](TigerJython: Hochauflösende Bilder abspeichern – 20220723), daß mit [TigerJython](cp^TigerJython) sich einige Dinge leichter und eleganter erledigen lassen, als mit [Processing](cp^Processing), hatte ich vor allen Dingen die Erstellung von graphischen Benutzeroberflächen (GUI) im Kopf, denn das ist in TigerJython fest eingebaut, während man es in Processing nur über Klimmzüge erledigt bekommt. Um das zu illustrieren, habe ich einen Klassiker ausgekramt, den *A. K. Dewdney* schon zu Beginn der 1980er Jahre in der *Scientific American* respektive in der deutschprachigen Schwesterzeitschrift *Spektrum der Wissenschaft* veröffentlichte[^1]: »Wie man Pi erschießt«.

[^1]: Ich selber habe den Text als Nachdruck hier ausgegraben: *Compter Kurzweil. Verständliche Forschung*, hrsg. von *Ingo Diemer*. Heidelberg 1988, S. 56-62.

Die TigerJython-Version erschien in »[Programmierkonzepte](https://programmierkonzepte.ch/index.php?inhalt_links=navigation.inc.php&inhalt_mitte=grafik/widgets.inc.php)« von *Jarka Arnold*, *Tobias Kohn* und *Aegidius Plüss* und dient dort als Beispiel für die Widgets, die in *GPanel* als GUI-Elemente zur Verfügung stehen. Ich habe das nahezu 1:1 übernommen und nur optisch ein wenig aufgeübscht:

~~~python
# PI – Monte Carlo Simulation

from gpanel import *
from javax.swing import *
from random import random

WIDTH = 600
HEIGTH = 600

def actionCallback(e):
    wakeUp()

def createGUI():
    addComponent(lbl1)
    addComponent(tf1)
    addComponent(btn1)
    addComponent(lbl2)
    addComponent(tf2)
    validate()
    
def init():
    tf2.setText("")
    clear()
    setColor(230, 226, 204)
    fillRectangle(0, 0, 1, 1)
    setColor(4, 21, 31)
    rectangle(0, 0, 1, 1)
    arc(1, 0, 90)

def doIt(n):
    hits = 0
    for _ in range(n):
        zx = random()
        zy = random()
        if zx*zx + zy*zy < 1:
            hits += 1
            setColor(226, 107, 67)
        else:
            setColor(60, 76, 97)
        # point(zx, zy)
        move(zx, zy)
        fillCircle(0.002)
    return(hits)

lbl1 = JLabel("Anzahl der Punkte: ")
lbl2 = JLabel("PI = ")
tf1 = JTextField(6)
tf2 = JTextField(10)
btn1 = JButton("OK", actionListener = actionCallback)


makeGPanel(Size(WIDTH, HEIGTH))
window(-0.1, 1.1, -0.1, 1.1)
windowPosition(1400, 80)
bgColor(240, 245, 248)
title("Pi via Monte Carlo Simulation")

createGUI()
tf1.setText("200000")
init()

while True:
    putSleep()
    init()
    n = int(tf1.getText())
    k = doIt(n)
    pi = 4*k/n
    tf2.setText(str(pi))
~~~

Für die Bereitstellung der GUI-Elemente ist die Zeile

~~~python
from javax.swing import *
~~~

verantwortlich, und dies macht deutlich, das hier direkt auf die Java-Swing-Bibiliothek durchgegriffen wird. Die Kommunikation mit dem Programm erfolgt via *Callbacks*, dabei versetzt `putSleep()` das Skript in einen Wartezustand, aus dem es mit dem *Callback* `wakeUp()` wachgerufen wird, um dann mit `validate()` die Veränderungen an den GUI-Elementen abzufragen. Dieses Trio bildet also die Einheit, die die Interaktivität des Programms mit der GUI übernimmt.

Wie alle echten GUI-Programme läuft auch dieses in einer Endlosschleife, die durch die Events des `actionListener = actionCallback` getriggert wird.

Die Dokumentation erwähnt die GUI-Bibliothek nur im Zusammenhang mit *GPanel*, aber ich sehe keine Hindernisse, warum sie nicht auch zusammen mit der *Turtle* funktionieren sollte. Das muß ich allerdings noch testen.

**And now something completely different**: Ich weiß, das ist wie mit dem kleinen Finger und der ganzen Hand, aber hat jemand von Euch da draußen schon einmal versucht, TigerJython auf einem Chromebook zum Laufen zu bekommen? Ich habe mit

~~~bash
wget https://www.tjgroup.ch/download/TigerJython.tar.gz
tar -xf
~~~

TigerJython heruntergeladen und ausgepackt, aber der Aufruf von `TigerJython` (wie auch `tigerjython`) läuft auf meinem Chromebook dennoch ins Leere (`Command not found`), obwohl die Dateien beide da sind. Wenn also jemand von Euch da draußen einen Tip hat … ? Ich frage ja nur, weil das mit RStudio [so gut gelaufen ist](RStudio auf meinem Chromebook – 20220724).&nbsp;🤓