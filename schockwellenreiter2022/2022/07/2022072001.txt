#title "Ren'Py auf meinem Chromebook (2) – 20220720"
#maintitle "Erfolge und Mißerfolge: Ren'Py auf meinem Chromebook"
#prevp "2022071901"
#nextp "2022072002"

<%= html.getLink(imageref("chromebookrenpy2-b"), "Ren\'Py auf meinem Chromebook (2)") %>

## Erfolge und Mißerfolge: Ren'Py auf meinem Chromebook

Bei meinem Abenteuer »[Pimp my Chromebook](Wilde Chromebook-Träume – 20220716)« gibt es Erfolge und Mißerfolge zu melden: So ist der Versuch, [RStudio](cp^RStudio) zu installieren, erst einmal gescheitert. Dafür lief die Installation von [Ren'Py](cp^renpy) recht flott und gelang schon beim ersten Durchlauf. Doch der Reihe nach:

Ich beginne mit dem Mißerfolg, dem Versuch, RStudio zu installeren: Das Debian-Linux [meines Chromebooks](http://blog.schockwellenreiter.de/2021/12/2021120701.html) kommt mir einem vorinstalliertem [R](cp^GNU R). Die Frage nach der Version 

~~~bash
sudo apt search r-base | grep ^r-base
~~~

sagte mir, daß mein vorinstalliertes R ein R Version 4.0.4-1 sei. Diese Version ist von Februar 2021 und schien mir aktuell genug, so daß ich mir eine – durchaus nicht triviale – Neuinstallation von R erspart habe (RStudio läuft mit Version 3.3 oder größer). Das [Herunterladen](https://www.rstudio.com/products/rstudio/download/#download) eines aktuellen RStudios (`rstudio-2022.07.0-548-amd64.deb`) gelang auch problemlos und nach einem Doppelklick auf die Datei und einem folgenden `Install` tauchte das RStudio-Icon auch in meinem Launcher auf. Nur … nach einem Doppelklick auf dieses Icon kreiste minutenlang das Wartesymbol, um dann lautlos im Nirwana zu verschwinden. Irgendetwas muß da schiefgelaufen sein.

Erfolgreicher verlief die [Installation von Ren'Py](Ren\'Py auf (m)einem Chromebook – 20220718). Hier folgte ich weitestgehend der [offiziellen Dokumentation](https://www.renpy.org/doc/html/chromeos.html#linux-on-chrome-os), nur daß ich mit

~~~bash
wget https://www.renpy.org/dl/8.0.1/renpy-8.0.1-sdkarm.tar.bz2
tar xaf renpy-8.0.1-sdkarm.tar.bz2
~~~

die aktuelle Version 8.0.1 herunterlud und entpackte. Die Installation erfolgte wieder mit einem Doppelklick auf die entpackte Datei und mit

~~~bash
cd ~/renpy-8.0.1-sdkarm
./renpy.sh
~~~

konnte ich Ren'Py dann starten (siehe [Screenshot](Ren\'Py auf meinem Chromebook (2)) im Banner oben).

Damit steht für weitere Abenteuern mit Ren'Py auch mein Chromebook zur Verfügung. Bis ich eine endgülitige, datenschutzkonformere Lösung gefunden und installiert habe (sei es [Git](cp^Git) -- [GitHub](cp^GitHub) oder [GitLab](cp^GitLab) -- oder sei es [Syncthing](cp^Syncthing)), werde ich die Synchronisation mit meinem MacBook Pro erst einmal via [Google Drive](cp^Google Drive) vornehmen, das bekomme ich als Chromebook-Nutzer ja fast geschenkt.

Und RStudio? Das lasse ich jetzt erst einmal sacken, bis mir eine Erleuchtung kommt (vielleicht sollte ich doch vorher ein R (neu) installieren?). Dann werde ich eine Neuinstallation versuchen. Mich drängelt ja niemand …

---

**3 (Email-) Kommentare**

---

<%= a("k01") %>

>Obwohl ich keine Ahnung von R habe, dafür aber gerne mit meinem Chromebook experimentiere, habe ich versucht, R und RStudio bei mir zu installieren.   
Ich installiere Linux-Pakete grundsätzlich in der Shell, da diese gesprächiger ist, als irgendwelches Geklicke. Ich habe also die Datei für RStudio heruntergeladen und mit

~~~bash
sudo dpkg -i rstudio-2022.07.0-548-amd64.deb
~~~

>installiert. Dabei bekam ich einen Fehlermeldung:

~~~bash
dpkg: dependency problems prevent configuration of rstudio:
  rstudio depends on libclang-dev; however:
   Package libclang-dev is not installed.
~~~

>ein beherztes

~~~bash
sudo apt -f install
~~~

>bereinigte dann die Abhängigkeiten und RStudio war erfolgreich installiert. Der Start von rstudio brachte dann, dass ich r-base noch gar nicht installiert hatte, auch dies erledigte ich mit

~~~bash
sudo apt install r-base
~~~

>und alles wurde ordnungsgemäß installiert. Vielleicht hilft Ihnen das ja weiter.   
Wie Sie unschwer auf dem Screenshot erkennen können, läuft das Programm ohne Probleme.   
<%= imageref("screenshotrstudiochromebook") %>

*– Robert Sch.* <%= p("k01") %>

---

<%= a("k0101") %>

>>Ich habe Ihren Beitrag nochmals genauer gelesen!   
`apt search` sucht im Repository und nicht auf Ihrer Maschine nach Paketen, d.h. im Repository ist R in der Version vorhanden und Sie müssen mit

~~~bash
sudo apt install r-base
~~~

>>das Paket erst auf Ihrer Maschine installieren, dann sollte eigentlich auch R-Studio laufen!

*– Robert Sch.* <%= p("k0101") %>

---

<%= a("k0102") %>

>>Ich habe versucht, nach dieser Anleitung RStudio noch einmal zu installieren. Leider keine Änderung im Verhalten. Das Icon ist zwar im Launcher, aber beim Versuch, RStudio zu starten, hängt das Programm im Startvorgang fest. Trotzdem danke für den Versuch, einem notorischen Mausschubser wie mir auf die Sprünge zu helfen.

*– Jörg Kantel* <%= p("k0102") %>

