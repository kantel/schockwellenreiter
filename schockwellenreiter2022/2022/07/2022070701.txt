#title "Google schließt Sicherheitslücken in Android – 20220707"
#maintitle "Google schließt am »Juli-Patchday« wieder Sicherheitslücken in Android"
#prevp "2022070602"
#nextp "2022070702"

<%= html.getLink(imageref("lanuit1883-b"), "La Nuit (1883)") %>

## Google schließt am »Juli-Patchday« wieder Sicherheitslücken in Android

Google hat mit seinem [monatlichen Sicherheitsupdate für Android](https://source.android.com/security/bulletin/2022-07-01) (und damit auch auf seinen [Pixel-Geräten](https://source.android.com/security/bulletin/pixel/2022-07-01)) wieder  Sicherheitslücken geschlossen. Die Patches teilt Google üblicherweise in Gruppen auf, um damit den Herstellern entgegen zu kommen: 01.07.2022, 05.07.2022.

Die Updates werden so nach und nach per OTA *(over the air)* auf  Pixel 6/6 pro, Pixel 5, Pixel 4a 5G, Pixel 4a, Pixel 4 und Pixel 4 XL, Pixel 3a und 3a XL verteilt. 

Die anderen Hersteller werden wie üblich in Bälde nachziehen, sofern sie überhaupt noch entsprechenden Support leisten. Samsung hat für einiger seiner Modelle (S21, S22) ebenfalls bereits Updates bereitgestellt. *([[cert]])*