#title "Zufallskreise mit Py5 (Processing) – 20220718"
#maintitle "Generative Art: Zufallskreise mit Py5 (Processing)"
#prevp "2022071601"
#nextp "2022071802"

<%= html.getLink(imageref("randomcircles-b"), "Zufallskreise nach Matt Pearson") %>

## Generative Art: Zufallskreise mit Py5 (Processing)

Die Beschäftigung mit der Python-3-[Processing](cp^Processing)-Variante [Py5](cp^Py5) macht einfach wahnsinnig Spaß, zumal mit [Thonny](cp^Thonny) und *Tristan Bunns* `thonny-py5mode` so richtig das Feeling aufkommt, als würde man mit Processings PDE arbeiten. So wundert es nicht, daß ich mir hierfür ein weiteres Betätigungsfeld ausgesucht habe:

Ich habe mir nämlich den dritten Teil von *[Matt Pearsons](http://zenbullets.com/zbmmxx/)* Buch »[Generative Art][a1]« vorgenöpft, das die Themen Komplexität, Autonome Agenten und Fraktale behandelt. Zur Einstimmung habe ich daraus ein recht einfaches Beispiel nach Py5 portiert und mit meiner [Farbpalette nach Kasimir Malewitsch](cp^Farbpaletten nach Kasimir Malewitsch) ein wenig aufgeübscht, das zufällige Kreise in das Bildschirmfenster zeichnet.

[a1]: https://www.amazon.de/Generative-Art-Practical-Guide-Processing/dp/1935182625?crid=17GXUGHH7TEO7&keywords=generative+art&qid=1658148208&s=books&sprefix=Generative%2Cstripbooks%2C91&sr=1-2&linkCode=ll1&tag=derschockwell-21&linkId=0bc92f511c2e4717175d92cedba9952d&language=de_DE&ref_=as_li_ss_tl

[[dr]]<iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1935182625&asins=1935182625&linkId=01116417e442471960d3b9073f82b8a8&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

~~~python
WIDTH = 640
HEIGHT = 480

malewitsch1 = [color(42, 40, 45), color(54, 50, 80), color(50, 80, 105),
               color(160, 51, 46), color(180, 144, 55), color(140, 82, 48),
               color(215, 158, 40)]

def setup():
    size(WIDTH, HEIGHT)
    window_move(1400, 30)
    window_title("Random Circles")
    background(230, 226, 204)
    stroke_weight(1)
    # ellipse_mode(RADIUS)

def draw():
    pass

def mouse_released():
    x = random(width)
    y = random(height)
    radius = random(100) + 10     # Jeder Außenkreis hat mindestens einen Radius >= 10
    fill(malewitsch1[floor(random(len(malewitsch1)))], 150)
    no_stroke()
    circle(x, y, 2*radius)
    fill(malewitsch1[floor(random(len(malewitsch1)))], 255)
    stroke(0, 150)
    circle(x, y, 10)
~~~

Ich werde aus dem Büchlein sicher noch weitere und komplexere Inspirationen ziehen. *Still digging!*