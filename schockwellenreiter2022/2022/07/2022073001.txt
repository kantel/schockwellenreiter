#title "TIC-80 auf (m)einem Chromebook – 20220730"
#maintitle "TIC-80 auf (m)einem Chromebook"
#prevp "2022072901"
#nextp "2022080101"

<%= html.getLink(imageref("chromebooktic80-b"), "TIC-80 auf meinem Chromebook") %>

## Die Installation von TIC-80: Das »Abenteuer Chromebook« geht weiter

Eigentlich stand ja heute etwas ganz anderes auf meinem Stundenplan. Aber wie das so geht, da lachte mich auf YouTube ein [TIC-80-Tutorial](https://www.youtube.com/watch?v=pu--inMR4FQ) an, das den Code in einem externen Editor erstellte (und dann simpel per *Copy-and-Paste* in die kleine Fantasiekonole kopierte) und da dachte ich mir: Ein [Plugin für die TIC-80-Syntax-Hervorhebung](https://github.com/paul59/TIC-80-Geany-Syntax-Highlighting) in [Geany](cp^Geany) hatte ich ja schon einmal gefunden und erfolgreich [installiert](Erste Schritte mit TIC-80 – 20220614). Und da ich ja nun auch auf [meinem Chromebook](Chromebook tunen – 20220604) Geany als Editor [zum Laufen bekommen hatte](Geany auf meinem Chromebook – 20220728), wäre es doch einfach schön, wenn dort auch [TIC-80](cp^TIC-80) liefe.

Gedacht, getan: Mit `wget https://github.com/nesbox/TIC-80/releases/download/v1.0.2164/tic80-v1.0-linux.de` habe ich das Teil heruntergeladen und mit einem Doppelklick auf die Datei einfach installiert. Ich muß gestehen, ich habe keinen Schimmer, wo das Programm auf meinem Chromebook nun versteckt ist (das Verzeichnis `TIC-80` im [Screenshot](TIC-80 auf meinem Chromebook) stammt von einem früheren, erfolglosen Installationsversuch), aber TIC-80 startete nach der Installation und ließ sich in der Taskleiste »anpinnen« (im Launcher ist das Teil unter »Linux-Apps« versteckt), so daß ich das Programm jederzeit schnell wiederfinden kann.

Okay, auf dem Chromebook könnte ich natürlich TIC-80-Spiele auch im Browser entwickeln und auch da den Quellcode vom Editor per *Copy-and-Paste* rüberkopieren, aber ich wollte einfach wissen, ob ich das Teil auch lokal installiert bekomme. Warum? Weil es geht!

