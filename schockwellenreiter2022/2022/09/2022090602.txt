#title "Google schließt Sicherheitslücken in Android – 20220906"
#maintitle "Google schließt am »September-Patchday« wieder Sicherheitslücken in Android"
#prevp "2022090601"
#nextp "2022090701"

<%= html.getLink(imageref("leda1934-b"), "Leda und der Schwan (1934)") %>

## Google schließt am »September-Patchday« Sicherheitslücken in Android

Google hat mit seinem monatlichen [Sicherheitsupdate für Android](https://source.android.com/docs/security/bulletin/2022-09-01) (und damit auch auf seinen [Pixel-Geräten](https://source.android.com/security/bulletin/pixel/2022-09-01)) wieder  Sicherheitslücken geschlossen.

Die Updates werden so nach und nach per OTA *(over the air)* auf  Pixel 6/6 pro, Pixel 5, Pixel 4a 5G, Pixel 4a, Pixel 4 und Pixel 4 XL verteilt. 

Die anderen Hersteller werden wie üblich in Bälde nachziehen, sofern sie überhaupt noch entsprechenden Support leisten. *([[cert]])*
