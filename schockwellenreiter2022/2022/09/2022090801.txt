#title "Am Tag als der Regen kam … – 20220908"
#maintitle "Am Tag als der Regen kam …"
#prevp "2022090701"
#nextp "2022090802"

<%= html.getLink(imageref("endlichregen-b"), "Am Tag als der Regen kam …") %>

## Am Tag als der Regen kam …

Glaubt man dem [Niederschlagsradar](https://weather.com/de-DE/weather/radar/interactive/l/Kreuzberg?canonicalCityId=49875d033efa3f8a515cc07b7ab1897df30ce65616175ffb4591cb95c5f646ff), können wir endlich auch in Berlin-Britz für die nächsten Stunden mit einem anhaltenden, kräftigen Landregen rechnen. Unser ausgedörrtes Gärtchen jauchzt auf.

Lediglich die Handwerker auf dem Baugerüst des Nachbarhauses lärmen unbeirrt weiter. *[[photojoerg]]*