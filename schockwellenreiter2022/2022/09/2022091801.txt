#title "Fraktaler Spirograph – 20220918"
#maintitle "Fraktaler Spirograph als Punktmenge"
#prevp "2022091702"
#nextp "2022091802"

<%= html.getLink(imageref("fraktalerspirograph-b"), "Fraktaler Spirograph als Punktmenge") %>

## Fingerübung am Sonntag: Fraktaler Spirograph als Punktmenge

Heute als kleine Fingerübung für zwischendurch (bevor ich mich wieder an größere Projekte wage): Ein [fraktaler Spirograph](https://commons.wikimedia.org/wiki/File:FractalSpirograph2880p60_20200409.webm) als Punktmenge, realisiert in [Py5](cp^Py5):

~~~python
# Fractal Spirograph

WIDTH  = 600
HEIGHT = 600

mikart = [color(255, 13, 12), color(0, 187, 100), color(1, 152, 221),
         color(251, 177, 0), color(253, 84, 3), color(221, 15, 160)]

ci = 0    # Farbindex

def setup():
    size(WIDTH, HEIGHT)
    window_move(1400, 30)
    window_title("Fraktaler Spirograph als Punktmenge")
    background(235, 215, 182)   # Packpapier
    stroke_weight(2)
    # stroke(caseincolors[1])
    
def draw():
    global ci
    stroke(mikart[ci])
    translate(width/2, height/2)
    R, k = 120, 1
    for i in range(5):
        rotate(frame_count*k*5**i/36)
        translate(R + R/3, 0)
        R /= 3
        k = -k
    point(0, 0)
    if frame_count%500 == 0:
        ci += 1
        ci %= len(mikart)
        # print(ci)
    if frame_count == 7500:
        print("I did it Babe!")
        no_loop()
~~~

Die [Idee](https://twitter.com/pickover/status/1568949131380686848) ist von *[Clifford A. Pickover](cp^pickover)*, den [P5](cp^p5py)-Quelltext habe ich [hier geklaut](https://console.basthon.fr/?script=eJxVTzFuhDAQ7JH4w3RnE0f44EgR6aoLSpWGFKmtsIDFYSNjct-POUJItlhpZ3ZmdhtnB4wF9DBa55HEURzV1GAiP4-MP8cRQn06Up4uynypiR2llGJpfCWVaa_0ZmtiL-VrVZbvP_jkne3pg3TbeZb9A9nBUX3gW1jt1O03yztlpmuIYzdd-y7NBLq7R7p5VAI9zshOucBxhRrroKENgrglVmxmSznrF7fGqYEudjbhy6BPUCBJgihF_sT37T2-wgOqhRbyDx-QM_J9Xi557Nd5tNp4JgXk_TU3G8aBbz3IV40) und nach Py5 übertragen. Ich habe meine alten Pickover-Bücher mal wieder hervorgekramt und werde daraus sicher noch einige Py5-, aber auch [TigerJython](cp^TigerJython)-Portierungen hier vorstellen. Jetzt steht aber erst einmal ein Ausflug ins [Wunderland-Universum](http://blog.schockwellenreiter.de/2021/01/2021010401.html) mit [Twine](cp^Twine 2) auf meiner Agenda. *Still digging!*
