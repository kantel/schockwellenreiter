#title "Neugiertool aktualisiert – 20220929"
#maintitle "Matomo erfolgreich auf 4.11.0 aktualisiert"
#prevp "2022092902"
#nextp "2022093001"

<%= html.getLink(imageref("herbsttomaten-b"), "Die letzten Tomaten im Herbst") %>

## Matomo erfolgreich auf 4.11.0 (sic!) aktualisiert

Ich weiß, so kurz vor Monatsende ist es eigentlich unvernünftig. Und außerdem wollte ich ja auch keine Nuller-Versionen installieren. Aber da dieses Mal schon wochenlang die Version 4.11.0 auf Installation drängte und es einfach kein Update auf 4.11.1 gab … 

Und dann wollte [Gabi](http://www.gabi-kantel.de/) für die [Website unseres kleinen Stadtteilvereins](https://proneubritz.jimdofree.com/) (die sie betreut) unbedingt auch ein Neugiertool haben. Gehostet kostet es aber knapp zwanzig Euro im Monat. Da ich weiß, daß Gabi -- wenn sie sich einmal etwas in den Kopf gesetzt hat -- keine Ruhe geben wird, habe ich dann in den sauren Apfel gebissen und ungeachtet der üblichen feuchten Hände und der schwitzenden Stirn das Update gewagt.

Was soll ich sagen? Es lief dieses Mal fehlerfrei und ohne Probleme durch. Sogar das obligatorische Datenbank-Update dauerte nur wenige Sekunden. Jetzt muß ich »nur« noch Gabi und ihre Website darauf einrichten. Drückt mir die Daumen, daß das alles ebenfalls gut geht. *Still digging!*