#title "Hoch- und Tiefbau Britzer Damm – 20220909"
#maintitle "Hoch- und Tiefbau Britzer Damm"
#template "photo2"
#prevp "2022090803"
#nextp "2022091001"

<%= html.getLink(imageref("hochtiefbritzerdamm"), "https://www.flickr.com/photos/schockwellenreiter/52345324516/") %>

## Hoch- und Tiefbau Britzer Damm

Schon seit Jahren verstopfen diese nicht sehr formschönen Absperrgitter das Trottoir und gefährden damit Fußgänger und Radfahrer. Doch nichts passiert. (Kleiner Tip an das Bezirksamt: Wie wäre es mit Bauen?) Nur der Umgang der Passanten mit diesem Ärgernis wird immer kreativer. 

Aufgenommen am 9. September 2022. *[[Photojoerg]]*