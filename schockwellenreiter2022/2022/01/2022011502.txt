#title "Update für Office for Mac – 20220115"
#maintitle "Microsoft veröffentlicht Update 16.57.0 für Office for Mac"
#prevp "2022011501"
#nextp "2022011503"

<%= html.getLink(imageref("lilliput194608-b"), "Lilliput August 1946") %>

## Microsoft veröffentlicht Update 16.57.0 für Office for Mac

Microsoft liefert mit dem [Update auf 16.57.0 für Office 365, 2019 for Mac](https://docs.microsoft.com/en-us/officeupdates/release-notes-office-for-mac) natürlich auch wieder aktuelle Sicherheitskorrekturen.

Sofern Office nicht schon von sich aus darauf hinweist, erhält man die Korrekturen am einfachsten über Microsofts AutoUpdate - in einem beliebigen Office-Programm über Menü: `Hilfe > Auf Updates überprüfen`. *([[cert]])*