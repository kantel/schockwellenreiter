#title "Mozilla repariert Firefox-Update – 20220114"
#maintitle "Mozilla repariert Firefox-Update"
#prevp "2022011302"
#nextp "2022011402"

<%= html.getLink(imageref("gefaengnis-b"), "Im Knast") %>

## Mozilla veröffentlicht neue Version Firefox 96.0.1

Nachdem das [jüngste Update](Mozilla veröffentlicht neue Version Firefox 96 – 20220111) des Firefox ein Problem mit einem HTTP/3-Fehler hatte, der zu fehlerhaftem Seitenaufbau führte, liefert nun Mozilla mit der [Version 96.0.1 eine Korrektur](https://www.mozilla.org/en-US/firefox/96.0.1/releasenotes/) dafür. 

Die ESR-Version erhält kein Update.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*
