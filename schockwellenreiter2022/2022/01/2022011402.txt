#title "Schau Mama, ein Hundebild! – 20220114"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2022011401"
#nextp "2022011501"

<%= html.getLink(imageref("kauknochenspitz-b"), "Spitz mit Kauknochen") %>

**Schau Mama, ein Hundebild!** Die Tage während meiner Anschlußheilbehandlung (vulgo »Reha«) rennen mir nur so davon und so ist es schon wieder ganz unvermittelt Freitag geworden und Freitags gehört nun einmal ein Hundebild in den *Schockwellenreiter*. Durch meine besondere Situation bedingt soll es Euch nicht über fehlende Updates während der nächsten zwei Tage hinwegtrösten, sondern es soll Euch auf eine Updateflut für Sonnabend und Sonntag vorbereiten. Denn ich habe einiges geplant und wer weiß, wann und ob ich die nächste Woche während meiner Reha dazu komme, dieses <del>Weblog</del> Kritzelheft zu füttern. Also seid gewarnt!

Ansonsten noch der freitägliche Blick auf das Wetter zum Wochenende … ach, vergessen wir's.

Dem Wetter zum Trotz wünschen der Spitz und ich Euch allen da draußen ein schönes Wochenende. Wir lesen auf jeden Fall morgen und übermorgen und danach (hoffentlich!) spätestens am nächsten Freitag zum nächsten Hundebild wieder. *[[photojoerg]]*