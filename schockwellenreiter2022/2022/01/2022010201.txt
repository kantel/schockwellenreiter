#title "Endlich wieder programmieren (mit TigerJython) – 20220102"
#maintitle "Endlich wieder programmieren (mit TigerJython)"
#prevp "2022010101"
#nextp "2022010501"

<%= html.getLink(imageref("codingtraindots-b"), "Coding Train Dots") %>

## Endlich wieder programmieren können (mit TigerJython)

Heute habe ich mich zum ersten Mal nach meinem Schlaganfall wieder an einen Code-Editor herangetraut und eine Kleinigkeit mit [TigerJython](cp^TigerJython) programmiert. Ich wollte wissen, ob ich es noch/wieder kann. TigerJython habe ich ausgewählt, weil zum einen die IDE als Lernumgebung mit sehr aussagekräftigen Meldungen auf Tippfehler (die mir seit meinem Schlaganfall verstärkt unterkommen) reagiert. Und zum anderen plane ich, TigerJythons Roboter-Bibliothek verstärkt bei einem zukünftigen Projekt zur Künstlichen Intelligenz einzusetzen. Daher kann es nicht verkehrt sein, diese [Jython](cp^Jython)-Programmierumgebung rechtzeitig wieder hervorzukramen und mich wieder mit ihr vertraut zu machen.

Denn TigerJython hatte ich vor [knapp einem Jahr](http://blog.schockwellenreiter.de/2021/03/2021030802.html) letztmalig eingesetzt, danach hatten die Bücher »[Math Adventures with Python][a1]« von *Peter Farrell* und vor allem »[Learn Python Visually][a2]« von *Tristan Bunn* (das damals frisch mein Bücherregal zierte) mich wieder zu [Processing.py](cp^processingpy) gedrängt, so daß der Tiger völlig aus meinem Blickfeld verschwand.&nbsp;🐅

[a1]: https://www.amazon.de/Math-Adventures-Python-Fractals-Automata/dp/1593278675?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1FBVY6YX2YKT6&dchild=1&keywords=math+adventures+with+python&qid=1615227528&sprefix=math+adventures%2Caps%2C174&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=024d17130ad75bb019fc395c586fd077&language=de_DE&ref_=as_li_ss_tl
[a2]: https://www.amazon.de/Learn-Python-Visually-Tristan-Bunn/dp/1718500963?crid=15SGWIZQOOE20&keywords=learn+python+visually&qid=1641141289&sprefix=Lern+Python+vis%2Caps%2C82&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=06918ffdb63f3c38975a3453d7a9d964&language=de_DE&ref_=as_li_ss_tl

Wie ich finde zu Unrecht. Um mich wieder damit vertraut zu machen, habe ich erst einmal mit vielen kleinen bunten Kreisen experimentiert (nach einer Idee von *Hauke Fehr* aus seinem Buch »[Let's code Python][a3]«). Herausgekommen ist dabei dieser Quellcode:

[a3]: https://www.amazon.de/Lets-code-Python-Programmiereinstieg-Jugendliche/dp/3836265141?crid=2KTALYEGCUK66&keywords=let%27s+code+python&qid=1641141823&sprefix=Let%27s+Code%2Caps%2C88&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=1c86f70bbc5e418f810844539735f6af&language=de_DE&ref_=as_li_ss_tl

~~~python
from gturtle import *
from random import randint

codingtrain = ["#f05025", "#f89e50", "#f8ef22", "#31c5f4", "#f063a4",
               "#9252a1", "#817ac6", "#62c777"]

tf = TurtleFrame("Coding Train Dots")
alice = Turtle(tf)
alice.hideTurtle()

for _ in range(3000):
    alice.setPenColor(codingtrain[randint(0, 7)])
    x = randint(-400, 400)
    y = randint(-300, 300)
    alice.setPos(x, y)
    alice.dot(randint(4, 40))

print("I did it, Babe!")
~~~

Man muß sich bei diesem Beispiel vor Augen halten, daß `makeTurtle()` nicht nur eine Turtle erzeugt, sondern auch automatisch ein Fenster, in dem diese Turtle lebt (also eine Schildröte inklusiv Habitat). Will man dagegen eine oder mehrere Schildkröten in einem speziellen Habitat (Fenster) leben und arbeiten lassen, dann muß man dieses Fenster mit `TurtleFrame()` erzeugen und den Schildkröte(n) jeweils in einer eigenen Initialisierungfunktion `Turtle()` dieses Fenster zuweisen. Das ist alles.&nbsp;🐢

Die Farben habe ich -- als Reminiszens an *Daniel Shiffman* -- meiner selbstgebastelten [Coding Train Farbpalette](cp^Coding Train Farbpalette) entnommen.

Für viele von Euch mag das alles vielleicht trivial sein, aber für mich ist die Fähigkeit, wieder programmeren zu können (wenn auch noch sehr langsam und in kleinen Häppchen), ein großer Schritt in Richtung Normalität.