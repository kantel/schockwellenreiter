#title "Streamlit und Streamlit Cloud – 20220123"
#maintitle "Neu in meinem Wiki: Streamlit und Streamlit Cloud"
#prevp "2022012301"
#nextp "2022012701"

<%= html.getLink(imageref("balloonsstreamlit-b"), "Streamlit Balloons") %>

## Neu in meinem Wiki: Streamlit und Streamlit Cloud

Vor etwa vierzehn Tagen ist mir [das erste Mal Streamlit untergekommen](Noch mehr Baukästen für Avatare – 20220110). Ich bin zwar bekanntlich ein Verfechter statischer Seiten, aber ich verkenne nicht, daß es gelegentlich notwendig ist, dynamische Seiten im Netz bereitzustellen. Und da scheint mir **[Streamlit zusammen mit Streamlit Cloud](cp^Streamlit)** eine exzellente Möglichkeit zu sein, zumal der Dienst auch noch in Python programmiert wird. Ich glaube, ich muß das Teil unbedingt testen und wie immer in solchen Fällen habe ich ihm zur Vorbereitung erst einmal **[eine Seite](cp^Streamlit)** in [meinem Wiki](cp^Startseite) spendiert:

### Streamlit

**Streamlit** ist der Name einer Bibliothek und eines Frameworks für die Programmiersprache Python mit der interaktive Web-Apps erstellt werden können. Diese Web-Apps können anschließend via Streamlit-Cloud im Internet veröffentlicht werden. Für die Streamlit-Cloud gibt es einen kostenlosen Community-Account mit einer privaten App, drei *Workspace Nutzern* und einer unbegrenzten Anzahl öffentlich zugänglicher Apps. Das dürfte für die meisten Hobby-Anwender und auch für viele Wissenschaftler ausreichen. Wer mehr benötigt, muß zahlen -- und das nicht zu knapp.

Die Software selber ist Open Source und steht unter der freier Apache-Lizenz. Erschienen ist sie im Jahr 2019. Die Entwicklung fand vom gleichnamigen Software-Unternehmen Streamlit statt, das im Jahr 2018 von *Adrien Treuille*, *Amanda Kelly* und *Thiago Teixeira* gegründet wurde. Die Firma hat ihren Sitz in San Francisco, Kalifornien.

#### Literatur

- *Stefan Luber* und *Nico Litzel*: [Was ist Streamlit](https://www.bigdata-insider.de/was-ist-streamlit-a-974962/)? Big-Data Sinsider vom 19. Oktober 2020
- [Erste Erwähnung von Streamlit](http://blog.schockwellenreiter.de/2022/01/2022011001.html) im *Schockwellenreiter* am 10. Januar 2022

#### Videos

<iframe width="560" height="315" src="https://www.youtube.com/embed/R2nr1uZ8ffc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Obiges Video ist Teil 11 der Playlist »[Streamlit Tutorials](https://www.youtube.com/playlist?list=PLgkF0qak9G4-TC9_tKW1V4GRcJ9cdmnlx)«, es wird in den Teilen 12 bis 14 fortgesetzt (falls der Playlist neue Videos hinzugefügt werden, kann sich die Numerierung ändern).

#### Tutorials

- *Adrien Treuille*: [Turn Python Scripts into Beautiful ML Tools](https://towardsdatascience.com/coding-ml-tools-like-you-code-ml-models-ddba3357eace). Introducing Streamlit, an app framework built for ML engineers, Towards Data Science vom 1. Oktober 2019 (Bezahlschranke)
- *Adrien Treuille*: [Introducing Streamlit Sharing](https://towardsdatascience.com/introducing-streamlit-sharing-b4f520e3d95). Deploy, manage, and share your Streamlit apps for free, Towards Data Science vom 15. Oktober 2020
- *Ran (Reine)*: [Build and Deploy a Web App in 30 mins with Python](https://reine-ran.medium.com/build-and-deploy-a-web-app-in-30-mins-in-python-7519ff48b185). For people with zero or little coding experience, Medium.com vom 19. Dezember 2021 (Bezahlschranke)
- *Adrien Treuille*: [Introducing Streamlit Cloud](https://blog.streamlit.io/introducing-streamlit-cloud/)! Streamlit is the most powerful way to write apps. Streamlit Cloud is the fastest way to share them. Streamlit Blog 2. November 2021
- *Qiusheng Wu*: [Creating satellite timelapse with Streamlit and Earth Engine](https://blog.streamlit.io/creating-satellite-timelapse-with-streamlit-and-earth-engine/). How to create a satellite timelapse for any location around the globe in 60 seconds, Streamlit Blog vom 15. Dezember 2021
- *Chanin Nantasenamat*: [How to master Streamlit for data science](https://blog.streamlit.io/how-to-master-streamlit-for-data-science/). The essential Streamlit for all your data science needs, Streamlit Blog vom 18. Januar 2022
- *Sebastian Flores Benner*: [How to create interactive books with Streamlit in 5 steps](https://blog.streamlit.io/how-to-create-interactive-books-with-streamlit-and-streamlit-book-in-5-steps/). Use streamlit_book library to create interactive books and presentations, Streamlit Blog vom 20. Januar 2022

#### Links

- [Streamlit Home](https://streamlit.io/)
- [Streamlit Cloud](https://streamlit.io/cloud)
- [Streamlit Blog](https://blog.streamlit.io/)
- [Streamlit @ GitHub](https://github.com/streamlit/streamlit)
- [Streamlit Kanal auf YouTube](https://www.youtube.com/channel/UC3LD42rjj-Owtxsa6PwGU5Q)

<%= html.getLink(imageref("streamlitlogo"), "cp^Streamlit") %>
