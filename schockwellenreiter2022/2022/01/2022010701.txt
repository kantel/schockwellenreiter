#title "Code the Classics: Skate or Die! – 20220107"
#maintitle "Code the Classics with Pygame Zero: Skate or Die!"
#prevp "2022010601"
#nextp "2022010702"

<%= html.getLink(imageref("skateordie-b"), "Code the Classics with Pygame Zero: Skate or Die!") %>

## Code the Classics with Pygame Zero: Skate or Die!

[Skate or Die!](https://de.wikipedia.org/wiki/Skate_or_Die!) ist ein Computerspiel, das im Jahr 1987 von der Firma Electronic Arts unter anderem für den Commodore C64 und den Sinclair ZX Spectrum auf den Markt gebracht wurde. Und in der aktuellen Ausgabe der [Wireframe (#58)](https://wireframe.raspberrypi.com/articles/out-now-the-gnarliest-yet-wireframe-58) hat sich *Mark Vanstone* diesen Klassiker vorgenommen und die *Half Pipe* Episode als Hommage in [Pygame Zero](cp^Pygame Zero) nachprogrammiert. Dieses Programm hat er wie immer zeitgleich im [Raspberry Pi Blog veröffentlicht](https://www.raspberrypi.com/news/code-an-homage-to-skate-or-die-wireframe-58/) und den [Quellcode mit sämtlichen Assetes](https://github.com/Wireframe-Magazine/Wireframe-58/) findet Ihr -- ebenfalls wie immer -- auf GitHub.

Auch die aktuelle Wireframe könnt Ihr [hier kostenlos als PDF](https://wireframe.raspberrypi.com/issues/58/pdf) herunterladen. Neben dieser Hommage an den Klassiker findet Ihr dieses mal unter anderem viele Überlegungen zum Design von Computerspielen darin und auch die Hardware-Bastelfreaks kommen nicht zu kurz.

<%= html.getLink(imageref("gangs-b"), "Rival Gangs, ein GTA-Demake für den ZX-Spektrum") %>

**War sonst noch was?** Ach ja, da hat jemand unter dem Namen *Rival Gangs* ein »Grand Theft Auto«-Demake für den ZX Spektrum 128k [herunterprogrammiert](https://www.indieretronews.com/2021/12/rival-gangs-grand-theft-auto-demake.html). Das nenne ich »Code the Classics Redux«. Ein [Video davon](https://www.youtube.com/watch?v=YruU-ZY7jjs) gibt es auf YouTube und das Spiel könnt Ihr von itch.io [herunterladen](https://zxpresh.itch.io/rival-gangs).