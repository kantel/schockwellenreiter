#title "Immobilienlobby abwählen! – 20221119"
#maintitle "Wahlwiederholung 2023: Immobilienlobby abwählen!"
#prevp "2022111801"
#nextp "2022112001"

<%= html.getLink(imageref("immobilienlobbyabwaehlen-b"), "Immobilienlobby (und ihre Trittbrettfahrer) abwählen!") %>

## Wahlwiederholung 2023: Immobilienlobby abwählen!

Und ihre Trittbrettfahrer von der Linken und den Grünen gleich mit.

Ich werde im Februar 2023 »[Die PARTEI](https://die-partei-berlin.de/)« wählen. Denn nicht nur, daß sie nichts verspricht, sie verspricht auch, sich nicht daran zu halten. Sie ist die einzige Partei, von der ich nichts zu erwarten habe und die mich daher nicht enttäuschen kann. *([[photojoerg]])*