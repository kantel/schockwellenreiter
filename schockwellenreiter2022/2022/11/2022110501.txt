#title "Heute Nachmittag bei uns im Kiez – 20221105"
#maintitle "Heute Nachmittag bei uns im Kiez"
#template "photo2"
#prevp "2022110402"
#nextp "2022110601"

<%= html.getLink(imageref("eschrottundmuell"), "https://www.flickr.com/photos/schockwellenreiter/52478389667/") %>

**Heute Nachmittag bei uns im Kiez**: Wo Müll ist, ist meist auch Elektroschrott nicht weit. Und wir sollten nie vergessen, welchem Andi wir diese be**Scheuer**te Idee verdanken. (Ja, ja, ich weiß -- keine Witze über Namen, aber wenn jemand diese Witze selber provoziert?) Aufgenommen am 5. November 2022 am Kulturbunker in der Neuköllner Rungiusstraße. *[[photojoerg]]*

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Lieber Jörg, ich sehe durchaus ein, dass du kein großer Freund des Elektrokleinstfahrzeuges bist. Wenn sie aber nun so Platz sparend wie nur möglich nebeneinander parken, ist die Bezeichnung Elektroschrott doch leicht übertrieben ...? Zumal die ortsüblich auf der Straße wohnenden Sofas (ist es nicht ein wenig spät im Jahr dafür, oder hat auch das der Klimawandel durcheinander gebracht?) viel weniger „preußisch” in Reih und Glied stehen.

*– Alexander A.* <%= p("k01") %>

