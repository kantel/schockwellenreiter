#title "Game Development: Missiles in Py5 – 20221102"
#maintitle "Game Development: Missiles in Py5 (Processing)"
#prevp "2022110201"
#nextp "2022110301"

<%= html.getLink(imageref("bulletsprocessing-b"), "Bullets in Py5 (Processing)") %>

## Spieleprogrammierung in Py5 (Processing) – ein neuer Plan

Im Rahmen meiner [Abkehr von TigerJython](Mein Abschied von TigerJython – 20221030) mußte ich feststellen, daß sich mangels einer entsprechenden Bibliothek wie *GameGrid* (oder [Pygame](cp^Pygame)/[Pygame Zero](cp^Pygame Zero)) die Spieleprogrammierung in Py5 doch komlizierter gestaltet, als ursprünglich gedacht. Es ist nicht wirklich schwieriger, doch da viele Basics, wie zum Beispiel eine Kollisionserkennung, fehlen, muß man diese von Grund auf selber programmieren.

Und da es da so viele Möglichkeiten gibt, war ich neugierig, wie andere das angestellt haben. Ind eine wahre Fundgrube in dieser Hinsicht sind die Video-Tutorials von *John McCaffrey*. Da dieser keine ordentliche Playlist führt (er wirft alles in [eine](https://www.youtube.com/playlist?list=PLoMpb3ppb873XgfCsG3tyuMC8kK5pc--n)), muß ich die Videos, die ich am insprierendsten fand, hier einzeln aufführen. Da wären erst einmal ein paar ältere:

- [Programming projectiles in processing](https://www.youtube.com/watch?v=9aSx40aeeak)
- [Programming bullets using processing](https://www.youtube.com/watch?v=5yQ_qHQLDm4)
- [Programming Projectiles in Processing Part 1](https://www.youtube.com/watch?v=v9hfLCA2VbI)
- [Programming Projectiles in Processing Part 2](https://www.youtube.com/watch?v=OTywa4CZMhY)

Und dann hat wohl das Semester neu begonnen und *John McCaffrey* hat eine neue Reihe gestartet (die Anfänge davon mit den ersten zehn Videos hatte ich [hier schon einmal](Video-Tutorials bis zum Abwinken – 20221014) erwähnt). Neu hinzugekommen in den letzten Tagen sind darin diese vier Videos:

- [Adding Images to a Processing Project](https://www.youtube.com/watch?v=DNcK5UyuQkA)
- [Programming Keyboard Movement in Processing](https://www.youtube.com/watch?v=ELyioGdxUZU)
- [Programming Rectangle Collisions in Processing](https://www.youtube.com/watch?v=C0qfEdE0r7o)
- [Programming a Tilemap in Processing](https://www.youtube.com/watch?v=Byo5Pepe3cg)

Wegen der seltsamen Zählung sind das nun die Nummern 1 bis 14 seiner Monster-Playlist (jetzt insgesamt 139 Titel), wobei das jüngste Video immer die Nummer 1 ist.

Mein Plan ist, *alle* Spiele, die ich mit TigerJython und GameGrid erstellt hatte, nach Py5 zu portieren. Ich möchte mit dem [bonbonbunten Aquarium](http://blog.schockwellenreiter.de/2021/02/2021021201.html) ([Update](http://blog.schockwellenreiter.de/2021/02/2021021301.html)) beginnen. Wer weiß, vielleicht kommt dabei sogar eine Spielebibliothek heraus? Ich habe also zu tun. *Still digging!*