#title "Logseq und Blogposts – 20220310"
#maintitle "Logseq zur Vorbereitung meiner Blogposts"
#prevp "2022030803"
#nextp "2022031101"

<%= html.getLink(imageref("logseqblogposts-b"), "Logseq: Outliner zur Vorbereitung von Blogposts") %>

## Auf dem Weg zur (World-) Markdown: Logseq zur Vorbereitung meiner Blogposts

Vor [einigen Wochen](Das Microblog vom Tage – 20220122) habe ich ja das Werkzeug zur Vorbereitung meiner Blogposts gewechselt: Ich bin vom [(elektrischen) Trommler](cp^Drummer) auf [Logseq](cp^Logseq) umgestiegen. Damals war ich mir noch nicht sicher, ob das eine gute Idee war, daher ist es Zeit für einen ersten Erfahrungsbericht: Hatte schon der kleine Trommler meine Arbeitsweise, wie ich (Microblog-) Beiträge erstelle, geändert, so hat sie Logseq komplett auf den Kopf gestellt. Denn nicht nur das Microblogging, sondern nahezu alle Blogposts -- die keine Worknotes oder Tutorials sind -- bereite ich nun im Outliner vor.

Gegenüber Drummer hat Logseq den Vorteil einer weitgehenden Markdown- und [Zettelkasten](cp^Zettelkasten)-Unterstützung und so kann ich geplante Beiträge erst einmal in diesem Zettelkasten aufnehmen und sie mit anderen Beiträgen zum Thema verlinken, bis genug für ein Blogposting zusammenkommt. Das hat dazu geführt, daß viele Beiträge, die sonst im Microblog versandet wären, nun zusammengefaßt und unter einer eigenen Überschrift publiziert werden. Das hat die Anzahl der Microblogposts radikal gesenkt, aber ich denke, daß dies der Übersichtlichkeit und Wiederauffindbarkeit hilft.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PvFr36bcpYc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In der besten aller Welten wäre Markdown standardisiert und die Markdown-Dateien könnten ohne Pfriemelei und Konvertierungsgedöns von jedem markdownfähigen-Editor gelesen und bearbeitet werden. Leider leben wir nicht in der besten aller Welten und so sind doch hin und wieder einige zusätzliche Schritte erforderlich. In dem Video »[Taking notes on YouTube videos in LogSeq](https://www.youtube.com/watch?v=PvFr36bcpYc)« wird unter anderem auch gezeigt, wie man Logseq-Dateien in ein »Standard«-Markdown-Format konvertiert. Danach könnte man die gespeicherten Notizen nicht nur in [Obsidian](cp^Obsidian), sondern auch in [Zettlr](cp^Zettlr), [MkDocs](cp^MkDocs) oder [PanWriter](PanWriter und mdBook – 20220129) (meine *use cases*) weiterbearbeiten.

Und was die Zettelkasten-Funktionen von Logseq angeht, da kratze ich bisher erst an der Oberfläche. Diese beiden Videos des Logseq-Evangelisten *»One Stuttering Mind«* lassen mich da hoffentlich tiefer eindringen:

1. Exploring Zettelkasten note-taking with Logseq ([Part 1 - A scattered mind meets Zettelkasten](https://www.youtube.com/watch?v=MEZc2nW09Ns)).
2. Exploring Zettelkasten note-taking with Logseq ([Part 2 - Tagging, processing and structuring](https://www.youtube.com/watch?v=0Z6hxbRAtQE)).

Natürlich kann man die Zettelkasten-Methode auch zum Schreiben belletristischer Texte verwenden. Die vierteilige Reihe »The Zettelkasten Method for Fiction« will dazu ermuntern:

- Teil 1: [Knowledge is Knowledge](https://zettelkasten.de/posts/zettelkasten-fiction-writing-part-1-knowledge/).
- Teil 2: [What You Can Look For](https://zettelkasten.de/posts/zettelkasten-fiction-writing-part-2-elements-of-story/).
- Teil 3: [Tools and Products of Analysing Story](https://zettelkasten.de/posts/zettelkasten-fiction-writing-part-3-tools-analysing-story/).
- Teil 4: [Creating Stories](https://zettelkasten.de/posts/zettelkasten-fiction-writing-part-4-create-story/).

Ich denke, speziell bei der Erstellung von interaktiven Geschichten zum Beispiel mit [Twine](cp^Twine), von *Visual Novels* mit [Ren'Py](cp^renpy) oder narrativen Videospielen etwa mit [Ink und Inky](cp^Ink und Inky) kann die Zettelkasten-Methode wegen ihrer Nichtlinearität sehr hilfreich sein.

[[dr]]<%= html.getLink(imageref("drummer"), "cp^Drummer") %>[[dd]]

**War sonst noch was?** Ach ja, *Dave Winer* ist einen umgekehrten Weg gegangen und hat sein seit Jahrzehnten gepflegtes Umfeld von Mac und [Frontier](cp^Frontier) (resp. [OPML-Editor](cp^OPML Editor)) verlassen und ist zum [elektrischen Trommler unter Linux](http://scripting.com/2022/03/03.html#a132623) gewechselt. Aber er ist ja auch der Schöpfer von Drummer und zu Recht stolz auf sein Baby. Damit bin ich auch endlich losgeworden, daß die Desktop-Version von Drummer nicht mehr nur *mac only* ist.

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Markdown ‚Zettler‘ ohne Ende… <https://github.com/brunocbr/zettel-composer>

*– Erwin L.* <%= p("k01") %>

