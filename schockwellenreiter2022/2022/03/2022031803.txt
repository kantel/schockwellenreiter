#title "Schau Mama, ein Hundebild! – 20220318"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2022031802"
#nextp "2022031901"

<%= html.getLink(imageref("hochbeetspitz-b"), "Spitz an Hochbeet") %>

**Schau Mama, ein Hundebild!** Es ist nicht nur unvermittelt wieder Freitag geworden, sondern genauso unvermittelt ist der Frühling und mit ihm die Sonne über unser kleines Gärtchen hereingebrochen. Das mußte ich natürlich sofort photographisch dokumentieren, damit ich einen Beweis habe, warum es die nächsten zwei Tage keine oder nur wenige Updates hier in diesem <del>Blog</del> Kritzelheft geben wird.

Denn nicht nur will ich -- wie angekündigt -- meiner frisch neuerwachten Liebe zur [Knotenschachtel](cp^Nodebox) frönen, sondern in diesen ersten sonnigen und warmen Frühlingstagen wird auch meine Jagd auf [Fischbrötchen](https://www.flickr.com/photos/schockwellenreiter/51933471176/) auf dem lokalen [Hipster-Wochenmarkt](https://www.diemarktplaner.de/die-dicke-linda/) mit einem länger dauernden [Kaffeegenuß](https://www.diemarktplaner.de/die-dicke-linda/) gekrönt werden. Und *last but not least* will der kleine Spitz auch noch seinen Spaß haben.

Ich hoffe, daß ich dennoch ein wenig Zeit finden werde, einige Ergebnisse meiner Programmierbemühungen in den *Schockwellenreiter* zu stellen. Auf jeden Fall soll Euch aber das Photo der Fellnase über die kritzelheftfreien Tage hinwegtrösten.

Der Blick der Wetterfrösche in ihre Kristallkugel sagt: Es wird Frühling! Schiebt daher die schlechten Nachrichten von Pandemie und Krieg ein wenig zur Seite und genießt die sonnigen Tage. Denn das ist gut für die Psyche. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photojoerg]]*