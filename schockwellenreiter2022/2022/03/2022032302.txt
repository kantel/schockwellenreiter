#title "Die Knotenschachtel hinter Gittern – 20220323"
#maintitle "Die Knotenschachtel hinter Gittern"
#prevp "2022032301"
#nextp "2022032401"

<%= html.getLink(imageref("gridnodebox-b"), "Die Knotenschachtel hinter Gittern") %>

## Experimente mit Nodebox 1: Die Knotenschachtel hinter Gittern

Meine Entdeckungsreise durch den Inhalt der [Knotenschachtel](cp^Nodebox) (aka Nodebox&nbsp;1) geht weiter. Heute möchte ich eine Funktion vorstellen, die es meines Wissens sonst nirgendwoanders in der Python-Welt gibt, die ein Alleinstellungsmerkmal der Nodebox ist. Nodebox besitzt nämlich eine Funktion `grid(cols, rows, colsize=1, rowsize=1)` ([Manual](https://www.nodebox.net/code/index.php/Reference_%7C_grid().html)), die ein Gitter zurückgibt, über das iteriert werden kann. Jede Zelle, über die iteriert wird, kann dabei ihren eigenen `scope` besitzen.

Das habe ich mit diesem Skript ausprobiert:

~~~python
from random import randint

colormode(RGB, range = 255)
size(480, 640)

newmoon = [(119, 124, 133), (179, 185, 192), (242, 119, 122),
           (252, 163, 105), (255, 212, 121), (255, 238, 166),
           (146, 209, 146), (106, 176, 243), (118, 212, 214),
           (225, 166, 242), (172, 141, 88)]

simcode = [(92, 97, 130), (79, 164, 165), (202, 166, 122),
           (212, 117, 100)]

gridsize = 66
dia = gridsize - 8
for x, y in grid(7, 9, gridsize, gridsize):
    fill(color(newmoon[randint(0, len(newmoon) - 1)]))
    rect(10 + x, 10 + y, dia, dia)
    fill(color(simcode[randint(0, len(simcode) - 1)]))
    if randint(0, 100) < 50:
        oval(10 + x, 10 + y, dia, dia)
    else:
        star(10 + dia/2 + x, 10 + dia/2 + y, 6, 30, 10)
~~~

Jede Zelle besitzt dabei ihre eigene Hintergrundfarbe und je nach Ergebnis des Zufallszahlengenerators wird in die Zelle ein Kreis oder ein Stern gezeichnet.

Die Farben habe ich -- wie bei meinem [letzten Experiment](Zufallskreise mit der Nodebox 1 – 20220321) mit der Knotenschachtel -- wieder aus der [New Moon Farbpalette](cp^New Moon) von *Tania Rascia* und der [Simulated Code 2](cp^Simulated Code)-Palette von *Eric Davidson* entnommen. Und den [Quellcode](https://github.com/kantel/nodebox1/blob/master/sketches/grid/grid01.py) gibt es natürlich wie immer in meinem GitHub-Repositorium.

Doch wartet, da ist noch mehr: Für die Nodebox existiert auch noch eine [Grid-Bibliothek](https://plotdevice.io/lib/Grid) mit sehr vielen Möglichkeiten. Die werde ich mir als nächstes vorknöpfen. *Still digging!*

---

**1 Kommentar**

---

<%= a("k01") %>

>Interessant. Ich bin zwar nicht so in der Materie aber mir ist sofort aufgefallen das Dein Link auf die PlotDevice Libraries eine Funktion enthält die auf die Fibonacci Reihe hinweist. Ich meine es entspricht nicht dem goldenen Schnitt (bin mir da nicht ganz sicher), ist aber eine Grundlage der Ästhetik. Vergleiche Abschnitt "Splitting and navigating the grid" mit diesen Beispielen: <https://en.wikipedia.org/wiki/Golden_spiral> oder <https://metager.org/meta/meta.ger3?eingabe=Fibonacci-Spirale&focus=bilder>

*– Stefan W. via Mastodon* <%= p("k01") %>

