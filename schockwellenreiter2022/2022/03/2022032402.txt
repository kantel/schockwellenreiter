#title "Fraktale, Turtle und Pygame (Zero) – 20220324"
#maintitle "Zweieinhalb Stunden Python: Fraktale, Turtle und Pygame (Zero)"
#prevp "2022032401"
#nextp "2022032501"

<%= html.getLink(imageref("turtlefraktale-b"), "Fraktale mit der Schildkröte") %>

## Zweieinhalb Stunden Python: Fraktale, Turtle und Pygame (Zero)

Die [Knotenschachtel](cp^Nodebox) lockt und da [Processing.py](cp^processingpy) ja jetzt auch wieder mit Processing&nbsp;4 spielt, ruft hier eine weitere Aufgabe nach mir, in der eventuell auch noch [Py5 mit Thonny](http://blog.schockwellenreiter.de/2021/06/2021062101.html) involviert ist. Man sollte meinen, daß dies genug für die nächsten paar Tage ist, aber nein, da habe ich mir auch noch drei (Python-) Video-Tutorials aus meiner ständig anwachsenden »Später ansehen«-Liste herausgesucht, bei denen ich das »später« durch »jetzt« ersetzen will. Diese drei insgesamt zweieinhalb stündigen Tutorials behandeln die Themen [Pygame Zero](cp^Pygame Zero), Fraktale mit [Pythons Turtle](https://docs.python.org/3/library/turtle.html) und Kollisionserkennung in Videospielen mit [Pygame](cp^Pygame).

<iframe width="560" height="315" src="https://www.youtube.com/embed/hpWR4xC2sck" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Den Anfang macht das Video »[Intro to Object and Event with PyGame Zero](https://www.youtube.com/watch?v=hpWR4xC2sck)« aus der Reihe »[Python for Kids 2](https://www.youtube.com/playlist?list=PLRra2bvSiwAjWphcXw47cCQQHIKYjrhif)«. Es gibt bisher seltsamerweise noch recht wenige Tutorials zu Pygame Zero (im Vergleich zu Pygame) (oder der allwissende Algorithmus spült nur wenige davon in meine *Timeline*). Da ich aber mit Pygame Zero noch einiges vorhabe, kommt mir diese halbe Stunde gerade recht. Als IDE wird dort der [Mu-Editor](cp^Mu) verwendet, der auch ein nettes Python-Werkzeug -- gerade für Kids -- sein soll.

<iframe width="560" height="315" src="https://www.youtube.com/embed/knDjGx-hY8o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dann stach mir noch der »[Python Extension Workshop: Fractal Geometry with Python Turtle](https://www.youtube.com/watch?v=knDjGx-hY8o)« ins Auge, einfach weil ich die Python-3-Schildkröte in der letzten Zeit sträflich vernachlässigt habe. Das hat sie nicht verdient.

<iframe width="560" height="315" src="https://www.youtube.com/embed/W9uKzPFS1CI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Und da ich es liebe, wenn komplizierte Sachverhalte mit einfachen Dingen (in diesem Fall mit animierten Rechtecken) erklärt werden, füllt die letzte Stunde *Clear Code* mit seinem Video »[Collisions between moving objects](https://www.youtube.com/watch?v=W9uKzPFS1CI)« aus. Die dort erklärten Algorithmen und Lösungen sollten sich leicht zumindest nach Pygame Zero -- aber auch nach Processing.py und [TigerJythons](cp^Tigerjython) *GameGrid*-Bibliothek -- übertragen lassen.

**War sonst noch was?** Ach ja, habe ich Euch eigentlich schon erzählt, daß meine Handbibliothek mit ungelesenen Büchern zu Python (natürlich), zur Programmierung »generativer Kunst« und zum Thema »Interactive Fiction« überquillt? Und da will ich noch gar nicht von [R](cp^GNU R), [RStudio](cp^RStudio) und [R Markdown](cp^R Markdown) reden, das ich ja auch schon großspurig jüngst als [nächstes Projekt](R, RStudio und R Markdown – 20220304) angekündigt hatte. Jetzt wißt Ihr, warum Rentner nie Zeit haben …