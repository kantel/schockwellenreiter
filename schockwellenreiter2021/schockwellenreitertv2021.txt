#title "Schockwellenreiter TV 2021"
#prevp "Essays 2021"
#nextp "impressum.html"

## Dezember 2021

- Back to the Basics: [Make it static!](Make it static! – 20211231). *Thilo Billerbeck* bricht auf dem diesjährigen RC3 ein Lanze für statische Seiten, wie sie auch dieses <del>Blog</del> Kritzelheft antreiben -- 31.&nbsp;Dezember 2021
- Learning TIC-80 mit diesen [TIC-80-Tutorials und -Playlists auf YouTube](TIC-80-Tutorials auf YouTube – 20211220) -- 20.&nbsp;Dezember 2021
- Video-Tutorial: [Einführung in Logseq](Einführung in Logseq – 20211205). Ist Logseq die Ehe zwischen der *World Markdown* und der *World Outline*? -- 5.&nbsp;Dezember 2021

## November 2021

- [P5.js-Tutorials mit Tokyo EdTech und Happy Coding](Neue P5 (JavaScript) Tutorials – 20211127) -- 27.&nbsp;November 2021
- Worknote: [Chapbook – ein neues Storyformat in Twine 2](Chapbook in Twine 2 – 20211121), damit ich es nicht vergesse -- 21.&nbsp;November 2021

## Oktober 2021

- Computerspiel und interaktive Erzählung: [Eine kurze Geschichte von Myst](Eine kurze Geschichte von Myst – 202111014) -- 14.&nbsp;Oktober 2021

[[dr]]<%= html.getLink(imageref("renpy-logo"), "cp^renpy") %>[[dd]]

- [Ren’Py (Video-) Tutorials (nicht nur) für Anfänger](Ren'Py Tutorials für Anfänger – 20210111) -- 11.&nbsp;Oktober 2021
- Video-Tutorial-Reihe: [Jump and Run mit Pygame Zero](Jump and Run mit Pygame Zero – 20211007) auf (Schweizer-) Deutsch -- 7.&nbsp;Oktober 2021
- [Die Pandemie programmieren (mit Python)](Die Pandemie programmieren (mit Python) – 20211002), ein neues Projek für meine Py5-Erkundungen? -- 2.&nbsp;Oktober 2021

## September 2021

- Statische Seiten: [Vier (mehr oder weniger fette) Tutorials](Statische Seiten: Vier Tutorials – 20210929) -- 29.&nbsp;September 2021
- Video-Tutorial: [Zielloses Herumwandern (mit P5.js)](Zielloses Herumwandern (mit JavaScript) – 20210927), *Daniel Shiffman* ist aus der Sommerpause zurück -- 27.&nbsp;September 2021
- [The Art of Interactive Fiction](The Art of Interactive Fiction – 20210907), eine Videosammlung als Linkschleuder -- 7.&nbsp;September 2021

[[dr]]<%= html.getLink(imageref("drawbotIcon"), "cp^DrawBot") %>[[dd]]

- [Noch mehr über DrawBot](Noch mehr über DrawBot – 20210903): die Entdeckungsreise geht weiter -- 3.&nbsp;September 2021
- [Programmieren (DrawBot) lernen (auf Schwyzerdütsch)](DrawBot lernen (auf Schwyzerdütsch) – 20210902). Es muß nicht immer (d)english sein -- 2.&nbsp;September 2021

## August 2021

- [Neue (?) Twine-Video-Tutorials zum Wochenende](Neue (?) Twine-Video-Tutorials – 20210827). Gegen den Lockdown-Blues -- 27.&nbsp;August 2021
- Hotel Neuköllnifornia: [Ein Tribut an das Café Sandmann](Café Sandmann ist Hotel Neuköllnifornia – 20210814), das über ein Jahrzehnt lang unser verlängertes Wohnzimmer war -- 14.&nbsp;August 2021
- [Matplotlib Tutorial for Scientific Plots](Matplotlib Tutorial for Scientific Plots – 20210813), alles, was man in achtzig Minuten Video hineinquetschen kann -- 13.&nbsp;August 2021
- Creative Coding: [N-Gon-Fraktal in Processing (P5.js)](N-Gon-Fraktal in Processing (JavaScript) – 20210812), weitere Inspirationen für eigene Experimente -- 12.&nbsp;August 2021

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=148421465X&asins=148421465X&linkId=ba6c6b172a94fd1041176fd133b48828&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Video-Tutorials: [Statische Seiten mit GitHub Pages und Jekyll](Statische Seiten mit GitHub Pages und Jekyll – 20210806) von *Kevin Workman* -- 6.&nbsp;August 2021
- Künstler des Monats August 2021: [John Hassall](Künstler des Monats: John Hassall – 20210801), der englische König der Plakate -- 1.&nbsp;August 2021

## Juli 2021

- Evolutionäre-Algorithmen: [Meine nächsten Simulationsprojekte](Evolution: Meine nächsten Simulationsprojekte – 20210731) (in Python) -- 31.&nbsp;Juli 2021
- Video-Playlist: [All About Alice (Explained)](All About Alice (Explained) – 20210726), Literatur- vs. Film-Klassiker -- 26.&nbsp;Juli 2021
- [Zwei Video-Tutorials für Spieleprogrammierer](Zwei Videos für Spieleprogrammierer – 20210723), diesmal abseits der ausgetretenen Pfade von Pygame (Zero) und/oder Processing.py -- 23.&nbsp;Juli 2021
- Creative Coding: [Spielereien mit Farben und Farbpaletten in P5.js](Creative Coding: Spielereien mit Farben – 20210719) mit *Kevin Workman* -- 19.&nbsp;Juli 2021
- The Nature of Code 2.0 contd.: [Arrive Steering Behavior](The Nature of Code reloaded: Ankommen! – 20210713) -- 13.&nbsp;Juli 2021
- [Scripting Generative Art in Python (with PIL)](Scripting Generative Art in Python – 20210712). *Hamilton Greene* zeigt uns, daß es auch eher traditionell geht -- 12.&nbsp;Juli 2021
- The Nature of Code reloaded: [Korrektur zu »Flucht und Verfolgung«](The Nature of Code reloaded: Korrektur! – 20210711) (und neue Ideen) -- 11.&nbsp;Juli 2021
- Wider den Lockdown-Blues: [Video-Tutorials für das (verregnete) Wochenende](Video-Tutorials für das Wochenende – 20210709) -- 9.&nbsp;Juli 2021
- Video-Tutorial, gefunden auf der Suche nach Inspiration: [Hommage an Piet Mondrian in Processing.py](Hommage an Piet Mondrian in Processing (Python) – 20210707) -- 7.&nbsp;Juli 2021

## Juni 2021

- Creative Coding mit Pythons Schildkröte: [Make Fractal Art With Python](Creative Coding mit Pythons Turtle – 20210629), ein Video-Tutorial von *Tech with Tim* – 29.&nbsp;Juni 2021
- The Nature of Code reloaded: [It’s not a Heat Wave, it’s a Sine Wave](It's not a Heat Wave, it's a Sine Wave – 20210682), alle Videos zu Kapitel 3 – 28.&nbsp;Juni 2021
- All about Alice: [Něco z Alenky](All about Alice: Něco z Alenky – 20210627), ein tschechischer Kultfilm von 1988 – 27.&nbsp;Juni 2021
- The Nature of Code 2.0: [Fliehen, verfolgen und ausweichen](Fliehen, verfolgen und ausweichen – 20210624), weitere Anregungen für eigene Experimente – 24.&nbsp;Juni 2021
- All about Alice: [Die versteckte Mathematik im Wunderland](Die versteckte Mathematik im Wunderland – 20210619) von *Toby Hedy* – 19.&nbsp;Juni 2021
- Creative Coding: [Tim Rodenbröker schlägt Wellen](Tim Rodenbröker schlägt Wellen – 20210617) (mit Processing) – 17.&nbsp;Juni 2021

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=0985930802&asins=0985930802&linkId=91917a11e2caf588251eca02b1c6b38c&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- The Nature of Code 2.0: [Suche das Ziel](The Nature of Code 2: Suche das Ziel – 20210616) (Flucht und Verfolgung) – 16.&nbsp;Juni 2021
- [Das Video-Tutorial-Programm für das Wochenende](Das Video-Tutorial-Programm für das Wochenende – 20210604), Simulationen und mehr gegen die Lockdown-Langeweile -- 4.&nbsp;Juni 2021
- Künstler des Monats Juni 2021: [Aubrey Beardsley oder »Die Kunst, ein Dandy zu sein«](Künstler des Monats Juni 2021: Aubrey Beardsley – 20210601) -- 1.&nbsp;Juni 2021

## Mai 2021

- [Spieleprogrammierung für Anfänger in vanilla JavaScript](Spieleprogrammierung in vanilla JavaScript – 20210531). Es geht auch ohne Frameworks -- 31.&nbsp;Mai 2021
- [Firebase via Pyrebase](Firebase: Video-Tutorials zum Wochenende – 20210530): Video-Tutorials zum Wochenende -- 30.&nbsp;Mai 2021
- Neue Tutorials: [Daniel Shiffmans Einführung in P5.js (reloaded)](Processing (JavaScript) reloaded – 20210521) -- 21.&nbsp;Mai 2021
- Frauen am Kontrabaß (10): [Symphony Tidwell](Frauen am Kontrabaß (10): Symphony Tidwell – 20210513) mit »Jonny Barber & The Rhythm Razors« -- 13.&nbsp;Mai 2021
- [Träumen (Computer-) Mäuse von elektrischen Zeigern?](Träumen Mäuse von elektrischen Zeigern? – 20210505) Mäuse und künstliche Intelligenz mit *Daniel Shiffman* -- 5.&nbsp;Mai 2021
- [Riesen-Video-Tutorial-Programm](Riesen-Video-Tutorial-Programm – 20210501) gegen die Lockdown-Langeweile -- 1.&nbsp;Mai 2021
- Künstler des Monats Mai 2021: [Grandville](Künstler des Monats Mai 2021: Grandville – 20210501), der große französische Karikaturist aus der ersten Hälfte des 19. Jahrhunderts -- 1.&nbsp;Mai 2021

## April 2021

- [Animierte Quadrate 2](Noch mehr Quadrate – 20210428): Noch mehr Spaß mit Processing.py, jetzt auch mit Video -- 28.&nbsp;April 2021
- Video-Tutorials für das Wochenende: [Heute zum Thema »Game Design«](Video-Tutorials fürs Wochenende: Game Design – 20210423) -- 23.&nbsp;April 2021
- Jetzt auf Kickstarter und bald auf Eurem Rechner(?): »[Little Nemo and the Nightmare Fiends](Little Nemo and the Nightmare Fiends – 20210412)« -- 12.&nbsp;April 2021
- Video-Tutorial(s): [RPG-Programmierung mit Processing(.py?)](RPG-Programmierung mit Processing – 20210410), Grundlagen für ein RPG im Zelda-Style -- 10.&nbsp;April 2021
- PlutoCon 2021: [Creative Coding mit Julia und Pluto](PlutoCon 2021: Mit Julia zu Pluto – 20210409) (Video-Tutorials zum Wochenende) -- 9.&nbsp;April 2021

[[dr]]<%= html.getLink(imageref("arcadelogo"), "cp^arcade") %>[[dd]]

- [Coding Games mit Pythons Arcade-Bibliothek](Coding Games mit Python Arcade – 20210408), eine elfteilige Playlist von *Long Nguyen* -- 8.&nbsp;April 2021
- Da war doch noch was? [Die Python Arcade Bibliothek](Da war doch noch was? Die Arcade Bibliothek – 20210405) reloaded -- 5.&nbsp;April 2021
- Video-Tutorials für das Wochenende: [Modellbildung, Simulation und Computergeschichte(n)](Modellbildung, Simulation und mehr – 20210402) -- 2.&nbsp;April 2021
- Künstler des Monats April 2021: [Heinrich Kley](Künstler des Monats April 2021: Heinrich Kley – 20210401), für mehr »Wegguckbilder« im *Schockwellenreiter* -- 1.&nbsp;April 2021

## März 2021

- [AstraZeneca ist sicher! Wirklich?](AstraZeneca ist sicher! Wirklich? – 20210331) Was sagt maiLab dazu? -- 31.&nbsp;März 2021
- [Spieleprogrammierung und Creative Coding](Spieleprogrammierung und Creative Coding – 20210327), Videotutorials gegen die Lockdown-Langeweile zum Wochenende -- 27.&nbsp;März 2021
- [Notizen langfristig speichern mit Obsidian](Notizen langfristig speichern (Obsidian) – 20210325) (oder mit Zettlr?) -- 25.&nbsp;März 2021
- Lieder der Arbeiterklasse: Wenn alte Männer rocken: [Jeff Beck und Billy Gibbons (ZZ Top) spielen Sixteen Tons](Wenn alte Männer rocken: 16 Tons – 20210322) -- 22.&nbsp;März 2021
- Video-Tutorials zum Frühlingsanfang: [Kreatives Programmieren mit Python und Processing und kreatives Schreiben mit Markdown](Kreatives Programmieren und kreatives Schreiben – 20210320) -- 20.&nbsp;März 2021
- Happy Pi Day 2012 (2): [Annäherung an Pi durch teilerfremde Zufallszahlen](Happy Pi Day 2012 (2) – 20210314) -- 14.&nbsp;März 2021

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1593279663&asins=1593279663&linkId=ddc8fa3563619b463fec685c888f57b5&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- [Physik und Spiele (und mehr) (nicht nur) in Python programmieren](Physik und Spiele in Python programmieren – 20210312), Video-Tutorials zum Wochenende -- 12.&nbsp;März 2021
- Die fluffige Kartoffel empfiehlt: [Sauber rotieren (in Pygame)](Sauber rotieren (in Pygame) – 20210307), ein Video-Tutorial -- 7.&nbsp;März 2021
- [Python, Pygame, Gui Zero, Dear PyGui](Video-Tutorials zum Wochenende: Python! – 20210305): Video-Tutorials zum Wochenende (mit zwei Zugaben) -- 5.&nbsp;März 2021
- Künstler des Monats März 2021: [Winsor McCay](Künstler des Monats: Winsor McCay – 20210301) -- 1.&nbsp;März 2021

## Februar 2021

- [Zettlr, Zettelkästen und Pygame](Zettlr, Zettelkästen und Pygame – 20210226) – Video-Tutorials zum Wochenende -- 26.&nbsp;Februar 2021
- Das Schreiben von freier [Open Source Software (FOSS) ist ein politischer Akt](Freie Software als politischer Akt – 20210224). Ein Manifest -- 24.&nbsp;Februar 2021
- [Noch mehr Videos zu Obsidian](Noch mehr Videos zu Obsidian – 20210222), denn Obsidian ist meine neue Obsession -- 22.&nbsp;Februar 2021
- [Obsidian, P5.js und Ren’Py](Obsidian und mehr (Video-Tutorials) – 20210219) und wieder eine Zugabe, Video-Tutorials zum Wochenende -- 19.&nbsp;Februar 2021

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=069250186X&asins=069250186X&linkId=69db9f00965c9561d6b0d5e4a377f357&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

- Video-Tutorials zum Wochenende: [Trigonometrie, Physik-Engines, GUIs, Roguelikes und Ren’Py](Video-Tutorials zum Wochenende – 20210212) -- 12.&nbsp;Februar 2021
- Python, Ren’Py, P5.js (und eine Zugabe): [Videos fürs Wochenende](Videos für das Wochenende – 20210205) -- 5.&nbsp;Februar 2021
- Künstler des Monats Februar 2021: [Arthur Rackham](Künstler des Monats: Arthur Rackham – 20210201) -- 1.&nbsp;Februar 2021

## Januar 2021

- JavaScript, P5.js, Ren’Py und Python – [Video-Tutorials zum Wochenende](Video-Tutorials zum Wochenende – 20210129) -- 29.&nbsp;Januar 2021
- Worknote und (Video-) Tutorial: [Teach Me How to Ink](Teach Me How to Ink – 20210128), interaktive Spiele entwickeln mit Ink und Inky -- 28.&nbsp;Januar 2021
- Mit Video-Tutorials zu Pygame wider die Lockdown-Langeweile: Eine neue [Wochenendausgabe](Video-Tutorials zu Pygame (Wochenendauswahl) – 20210122) -- 22.&nbsp;Januar 2021
- (Digitale) [Hilfen für das Homeschooling](Hilfen für das Homeschooling – 20210119), mit einem Schwerpunkt auf Informatik und Mathematik -- 19.&nbsp;Januar 2021
- Videos für das Wochenende (2): [Pygame](Pygame-Videos für das Wochenende – 20210115), um der Langeweile des Lockdowns zu entfliehen -- 15.&nbsp;Januar 2021
- Videos für das Wochenende (1): [Kämpfen gegen Corona (mit Python)](Kämpfen gegen Corona (mit Python) – 20210115) -- 15.&nbsp;Januar 2021

[[dr]]<%= html.getLink(imageref("renpy-logo"), "cp^renpy") %>[[dd]]

- Spieleprogrammierung: [Point and Click mit Ren’Py](Point and Click mit Ren'Py – 20210113) -- 13.&nbsp;Januar 2021
- [Python (und P5?) beschleunigen mit Numba](Python (und P5?) beschleunigen mit Numba – 20210111). Ich habe da eine Idee -- 11.&nbsp;Januar 2021
- [Video-Tutorials zum Wochenende](Video-Tutorials zum Wochenende – 202010108): Ren’py und Pygame -- 8.&nbsp;Januar 2021
- Künstler des Monats: [William Heath Robinson](Künstler des Monats: William Heath Robinson – 20210107) -- 7.&nbsp;Januar 2021


---

- **[Archiv Schockwellenreiter TV 2020](http://blog.schockwellenreiter.de/schockwellenreitertv2020.html)**
- **[Archiv Schockwellenreiter TV 2019](http://blog.schockwellenreiter.de/schockwellenreitertv2019.html)**
- **[Archiv Schockwellenreiter TV 2012 bis 2018](http://blog.schockwellenreiter.de/schockwellenreitertv.html)**