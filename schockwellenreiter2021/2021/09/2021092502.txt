#title "JupyterLab Desktop App - 20210925"
#maintitle "JupyterLab Desktop App ist draußen!"
#prevp "2021092501"
#nextp "2021092701"

<%= html.getLink(imageref("jupyterlabapp-b"), "JupyterLab Desktop App") %>

## Die JupyterLab Desktop App ist draußen!

Regelmäßige Leser des *Schockwellenreiters* wissen, daß ich das Editieren von Code in einem Browserfenster nicht mag. Das ist auch der Grund, warum ich bis heute -- bei aller Sympathie für das Konzept -- mit [Jupyter](cp^Jupyter) nicht warmgeworden bin. Und auch [JupyterLab](cp^JupyterLab) konnte mich -- [bei aller Anfangseuphorie](http://blog.schockwellenreiter.de/2016/07/2016071502.html) -- nicht überzeugen: Zu sehr schimmerte das häßliche Gesicht des Browsers durch die glattpolierte Oberfläche.

Doch das könnte sich bald ändern, denn seit wenigen Tagen ist die [JupyterLab Destkop App draußen](https://blog.jupyter.org/jupyterlab-desktop-app-now-available-b8b661b17e9a). Momentan ist es noch eine simple eins-zu-eins [Electron](cp^Electron)-Version von JupyterLab, aber plattformübergreifend (Windows. Linux, macOS) und es kommt mit einer kompletten, integrierten Python-Umgebung. Das macht das Teil erst einmal fett (1,5 GB) aber das bin ich von ähnlichen Tools mittlerweile gewohnt.

Momentan unterscheidet sich die Jypterlab Desktop App ([Download und Quellcode](https://github.com/jupyterlab/jupyterlab_app) auf GitHub) nur unwesentlich von JupyterLab. Aber wenn man die Möglichkeiten denkt, die in dem Framework stecken … Man denke nur an die Entwicklung, die [Visual Studio Code](cp^Visual Studio Code) genommen hat. Ich bin neugierig …
