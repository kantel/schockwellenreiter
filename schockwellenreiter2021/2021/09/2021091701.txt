#title "Matomo-Update gelungen – 20210917"
#maintitle "Matomo-Update gelungen"
#prevp "2021091501"
#nextp "2021091702"

<%= html.getLink(imageref("lalune-b"), "La Lune (1910)") %>

## Ich habe es gewagt! Matomo-Update gelungen

Regelmäßige Leser des *Schockwellenreiters* wissen, daß ich vor einem Update meines (hoffentlich!) datenschutzkonformen Neugiertools, daß früher einmal [Piwik](cp^Piwik) hieß, nun aber Matomo genannt werden will, immer zu Schweißausbrüchen und Herzrasen neige und -- aus schlechten früheren Erfahrungen -- Nuller-Updates grundsätzlich meide.

Nun liegt aber schon seit über einem Monat die [Version 4.4.1 zum Download und zur Installation](Das nächste Matomo-Update – 20210803) vor, und heute habe ich es endlich gewagt. Um Konflikte mit obsolet gewordenen Dateien zu vermeiden, habe ich -- [wie seit dem ursprünglich fehlgeschlagenen Update auf die Version 4.0.4](http://blog.schockwellenreiter.de/2020/12/2020120502.html) -- nach Sicherung der `config.ini.php` meine alte Matomo-Installation komplett gelöscht und dann die neue Version darübergebügelt. Dann die `config.in.php` zurückgesichert. Nach dem Neustart des Servers wollte Matomo noch Änderungen in der Datenbank vornehmen (die dieses Mal ohne ein Time-out über die Bühne gingen) und dann lief mein Nuegiertool wieder wie geschmiert.

Also für die Version 4.4.1 kann ich Entwarnung geben -- zumindest bei meinem [Spielzeugprovider](http://www.strato.de/) lief sie ohne zu murren.