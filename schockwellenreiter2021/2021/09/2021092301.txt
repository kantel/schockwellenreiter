#title "Apple veröffentlicht iOS 12.5.5 – 20210923"
#maintitle "Apple veröffentlicht iOS 12.5.5 für ältere Geräte"
#prevp "2021092202"
#nextp "2021092401"

<%= html.getLink(imageref("kohl-b"), "Kohl (Cabbages)") %>

## Apple veröffentlicht iOS 12.5.5 für ältere Geräte

Apple hat auch iOS 12.5.5  für ältere Geräte veröffentlicht wie zum Beispiel iPhone 5s, iPhone 6, iPhone 6 Plus, iPad Air, iPad mini 2, iPad mini 3, und iPod touch 6. Generation und bietet damit im wesentlichen [Sicherheitskorrekturen](https://support.apple.com/de-de/HT212824). *([[cert]])*