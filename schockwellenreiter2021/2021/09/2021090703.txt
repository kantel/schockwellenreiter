#title "Neue Mozilla Thunderbird Version freigegeben"
#maintitle "Mozilla Thunderbird Version 91.1.0 freigegeben"
#prevp "2021090702"
#nextp "2021090801"

<%= html.getLink(imageref("bottledogs-b"), "Flaschenhunde") %>

## Mozilla Thunderbird Version 91.1.0 freigegeben

Die Entwickler des [Mozilla Thunderbird](http://www.mozillamessaging.com/) haben das Update auf die Version 91.1.0 freigegeben, in der [kritische Sicherheitslücken behoben](https://www.thunderbird.net/en-US/thunderbird/91.1.0/releasenotes/) wurden.

Thunderbird weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe >
Über Thunderbird` angestoßen werden. *([[cert]])*