#title "Noch mehr über DrawBot – 20210903"
#maintitle "Noch mehr über DrawBot – die Entdeckungsreise geht weiter"
#prevp "2021090203"
#nextp "2021090302"

<iframe width="852" height="480" src="https://www.youtube.com/embed/h5h6NXC8ZoY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Noch mehr über DrawBot – die Entdeckungsreise geht weiter

Ich muß gestehen, daß [DrawBot](cp^DrawBot) mir keine Ruhe läßt. Dieses kleine Python-Modul zur Erstellung zweidimensionaler Vektor (sic!) Graphiken fasziniert mich, auch auf die Gefahr hin, wieder in eine Mac-only-Falle hineinzulaufen. Daher habe ich mal wieder die [Suchmaschine meines Vertrauens](cp^DuckDuckGo) angeworfen und zwei weitere Videos zum Thema gefunden:

Da ist zum einen *David Jonathan Ross* mit seinem anderthalbstündigen Vortrag »[DrawBot: Drawing with Python](https://www.youtube.com/watch?v=h5h6NXC8ZoY)«, den er am 19. Januar dieses Jahres auf dem [Boston Python Meetup](https://www.meetup.com/de-DE/bostonpython/events/275680312/) gehalten hat. In seinem Vortrag geht er unter anderem darauf ein, welchen Vorteil gegenüber klassischer Designer-Software wie Adobes Boliden InDesign und Illustrator er darin sieht, seine Entwürfe per Programmcode zu erstellen und damit zu experimentieren. Außerdem vergleicht er DrawBot mit anderen *Creative Coding* Apps wie zum Beispiel [Processing](cp^Processing).

<iframe width="560" height="315" src="https://www.youtube.com/embed/fPasHswCB8E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Und dann ist da noch ein weiterer Vortrag, den *Petr van Blokland* (dem Akzent nach eindeutig ein Niederländer&nbsp;🤓) schon 2017 gehalten hat. Er heißt »[Code All Your Graphic Designs with PageBot, DrawBot and Variation Fonts](https://www.youtube.com/watch?v=fPasHswCB8E)«. Wobei mir [PageBot](https://github.com/PageBot/PageBot) und [varaible Fonts](https://t3n.de/magazin/variable-fonts-neue-schriftformat-erobert-web-247165/) mir bisher auch noch nicht bekannt waren. Ich hoffe daher, auch von diesem Vortrag noch viel zu lernen. *Still digging!*