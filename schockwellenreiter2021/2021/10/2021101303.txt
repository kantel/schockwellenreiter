#title "Update für Adobe Reader und Acrobat – 20211013"
#maintitle "Sicherheitsupdate für Adobe Reader und Acrobat (APSB21-104)"
#prevp "2021101302"
#nextp "2021101401"

<%= html.getLink(imageref("gartenstrasse-b"), "Gartenstraße (Wedding)") %>

## Sicherheitsupdate für Adobe Reader und Acrobat (APSB21-104)

Adobe liefert Korrekturen für Sicherheitslücken im Reader und in Acrobat (unter Windows und Mac).

Das Update gelingt am einfachsten über den internen Update-Mechanismus.

Näheres dazu findet sich auch [hier](https://helpx.adobe.com/security/products/acrobat/apsb21-104.html). *([[cert]])*