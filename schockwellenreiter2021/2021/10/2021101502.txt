#title "Guten Appetit: Menüs in Ren'Py - 20211015"
#maintitle "Guten Appetit: Menüs in Ren'Py (Mein erstes Ren'Py Tutorial)"
#prevp "2021101501"
#nextp "2021101503"

<%= html.getLink(imageref("renpytut1-2-h"), "Ren\'Py-Tutorial 1 (2)") %>

## Guten Appetit: Menüs in Ren'Py (Mein erstes Ren'Py Tutorial)

Es soll ja auch *Visual Novels* geben, die einfach eine Geschichte -- wie in einem Buch -- linear von vorne nach hinten durcherzählen. Aber Interaktivität, Ver- und Abzweigungen mit alternativen Enden sind meiner Meinung nach das Salz in der Suppe dieses Genres. In meinem ersten [Ren'Py](cp^renpy)-Tutorial will ich daher zeigen, wie man dies implementieren kann:

<%= html.getLink(imageref("renpytut1-1-b"), "Ren\'Py-Tutorial 1 (1)") %><%= html.getLink(imageref("renpytut1-2-b"), "Ren\'Py-Tutorial 1 (2)") %><%= html.getLink(imageref("renpytut1-3-b"), "Ren\'Py-Tutorial 1 (3)") %>

Schweren Herzens verlasse ich dafür mein geliebtes [Wunderland-Universum](http://blog.schockwellenreiter.de/2020/11/2020111502.html)[^1] und tauche ein in die Welt von [Hercule Poirot](https://de.wikipedia.org/wiki/Hercule_Poirot), den genialen, belgischen Detektiv. In meinem ersten Beispiel wird der auf einen britischen Landsitz gerufen, dort von dem Dienstmädchen Alice in Empfang genommen und gefragt, ob sie ihn zu den Herrschaften begleiten solle. Und an dieser Stelle muß der Spieler eine Entscheidung treffen:

[^1]: Aber keine Sorge, die nächsten [Twine](cp^Twine 2)-Tutorials sind schon fest eingeplant und da kehren Alice und das Wunderland wieder zurück.

1. Er sagt »Ja« und wird in die Empfangshalle geleitet.
2. Er sagt »Nein«, weil er seine kleinen, grauen Zellen noch auslüften möchte und bleibt im Garten.

Solche Entscheidungen werden in Ren'Py in Menüs (`menue:`) implementiert. Hier erst einmal der komplette Quellcode dieser kurzen Szene:

~~~python
# Bilder
image bg abbey_grounds = "bg abbey_grounds.jpg"
image bg abbey_drawing = "bg abbey_drawing.jpg"
image alice = "maid.png"
image hercule = "hercule.png"

# Personen
define a = Character("Alice", color = "#c8ffc8")
define h = Character("Hercule", color = "#ee1100")

# Hier beginnt das Spiel. Label "Start" ist obligatorisch!
label start:

    scene bg abbey_grounds
    show hercule at left
    with slideleft
    show alice at right
    with dissolve

    a "Guten Tag, Monsieur Poirot, die Herrschaften erwarten sie bereits."
    a "Darf ich Sie ins Haus bitten?"
    pause 1.0
    hide alice
    with dissolve
    
    menu:
    
        "Ja, gerne. Ich folge Ihnen.":
            jump hall
            
        "Danke, ich bleibe noch im Garten.":
            jump garden

label hall:

    scene bg abbey_drawing
    show hercule at left
    with slideright
    
    h "Allo, 'ier bin ich."
    h "Sie 'aben mich gerufen?"
    jump endstory

label garden:

    h "Meine kleinen, grauen Zellen brauchen noch ein wenig frische Luft."
    jump endstory
    
label endstory:

    pause 2.0
    hide hercule
    with dissolve

    "Und so endete unsere monumentale Geschichte …"
~~~

Erst einmal: Ren'Py ist in Python implementiert und kann mit Python erweitert werden. Daher sind auch hier die korrekten Einrückungen wichtig, sonst wird das Spiel gar nicht erst gestartet. Die `label` sind Sprungziele die mit `jump` angesprungen werden können. Bei `scene` wird im Regelfall das Hintergrundbild und alle anderen Bilder ausgetauscht und die Akteure (`character`) können rechts (`at right`), links (`at left`) oder mittig (default) platziert werden. `show` läßt die Darsteller auf- und `hide` sie wieder abtreten, `dissolve`, `slideleft` und `slideright` sind einige der zahlreichen, möglichen Bildübergänge und `pause` pausiert das Spiel mit der angegebenen Anzahl in Sekunden.

Die Bilder habe ich [wieder](Ren'Py ist schon kurios – 20211012) der unter einer [Creative Commnons (BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/) stehenden Sammlung von [Carlmary](https://carlmary.jp/gallery/en/materials-300/) entnommen und die Hintergründe der frei zu verwendenden [Sammlung](https://spiralatlas.itch.io/regency-visual-novel-backgrounds) von [Spiral Atlas Games](http://spiralatlas.itch.io/). Und natürlich gibt es den [Quellcode und alle Assets](https://github.com/kantel/renpy/tree/master/renpyprojects/renpytut1) in meinem Github-Repositorium.

Das aktuelle Ren'Py soll (als experimentelles Feature) nach HTML5 exportieren können. Schaun wir mal, ob ich das hinbekomme, denn dann könnte ich Euch die Ergebnisse der Tutorials im im Browser (auf [Glitch](cp^Glitch) oder [itch.io](https://itch.io/)?) präsentieren. Denn auch wenn Ren'Py reichlich geschwätzig ist -- es macht schon Spaß, mit dem Teil zu arbeiten. Daher werden sicher noch weitere Tutorials folgen. *Still digging!*