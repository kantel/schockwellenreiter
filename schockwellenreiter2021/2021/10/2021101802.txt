#title "Code the Classics: Tranz Am (1938) – 20211018"
#maintitle "Code the Classics: Tranz Am mit Pygame Zero"
#prevp "2021101801"
#nextp "2021101901"

<%= html.getLink(imageref("tranzam-b"), "Tranz Am (1983)") %>

## Code the Classics: Tranz Am mit Pygame Zero

Die neue [Wireframe (Ausgabe 55)](https://wireframe.raspberrypi.com/issues) ist erschienen. Wie in jeder Ausgabe gibt es in ihr eine Hommage an einen Spieleklassiker, nachprogrammiert in [Pygame Zero](cp^Pygame Zero) und parallel auch im [Raspberry Pi Blog veröffentlicht](https://www.raspberrypi.com/news/code-a-tranz-am-style-top-down-racer-wireframe-55/). Diesen Monat ist es das Spiel [Tranz Am](https://en.wikipedia.org/wiki/Tranz_Am), das 1983 für den [ZX Spectrum](https://en.wikipedia.org/wiki/ZX_Spectrum) veröffentlicht wurde.

In diesem Spiel durchstreift der Spieler eine postapokalyptische USA auf der Suche nach acht Pokalen, die über das ganze Land verteilt versteckt sind. Sprit ist knapp, daher müssen Tankstellen (auf-) gesucht und dort nachgetankt werden. Und zu allem Überfluß machen auch noch gegnerische Kamikaze-Autos, die versuchen, den Wagen des Spielers zu rammen, und divere Hindernisse das Leben des Spielers zur Hölle.&nbsp;😈

Die Hommage an dieses Spiel reicht nahe an das Original heran, lediglich die Kamikaze-Wagen und die Hindernisse fehlen. Diese noch in das Spiel hineinzuprogrammieren hat der Autor *Mark Vanstone* dem Leser als Übungsaufgabe hinterlassen.&nbsp;🤡

Wie immer findet Ihr den [Quellcode und sämtliche Assets](https://github.com/Wireframe-Magazine/Wireframe-55/tree/main/source-code-tranz-am) auf GitHub. Und die aktuelle Wireframe könnt Ihr Euch [hier als PDF kostenlos](https://wireframe.raspberrypi.com/issues/55/pdf) herunterladen. *Happy coding!*