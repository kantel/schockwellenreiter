#title "Security Alert: LibreOffice und OpenOffice – 20211012"
#maintitle "Sicherheitslücke in LibreOffice und OpenOffice"
#prevp "2021101202"
#nextp "2021101301"

<%= html.getLink(imageref("johannastrasse-b"), "Johannastraße") %>

## Security Alert: Sicherheitslücke in LibreOffice und OpenOffice

In den Officepaketen LibreOffice und OpenOffice existiert eine [Schwachstelle](https://www.heise.de/news/Angreifer-koennten-digitale-Unterschrift-in-LibreOffice-und-OpenOffice-faelschen-6214784.html), durch die signierte Dokumente manipuliert werden können. Betroffen sind

- LibreOffice < 7.0.5
- LibreOffice < 7.11
- Apache OpenOffice 4.1.11

Auch wenn die Forscher den Schweregrad nur als »moderat« einstufen, sollten Nutzer die Office-Pakete auf den aktuellen Stand bringen. LibreOffice ist in den Versionen ab 7.0.5 und 7.1.1 und OpenOffice ist in der Ausgabe ab 4.1.11 gegen solche Attacken abgesichert. Alle vorigen Versionen sollen bedroht sein. *([[cert]])*

