#title "Anaconda-Python, TextMate und Catalina – 20210808"
#maintitle "Worknote: Anaconda-Python, TextMate und macOS Catalina"
#prevp "2021080701"
#nextp "2021080802"

<%= html.getLink(imageref("anacondacatalina-b"), "Anaconda-Python, TextMate und macOS Catalina") %>

## Worknote: Anaconda-Python, TextMate und macOS Catalina

Natürlich wollte ich das [gestern entdeckte und vorgestellte](Neu in meinem Wiki: PySimpeGUI – 20210807) Modul [PySimpleGUI](cp^PySimpleGUI) auch gleich ausprobieren. Doch trotz gelungener Installation: Zwar ließ sich mein Testprogramm über die Kommandozeile starten, aber es verweigerte konsequent die Zusammenarbeit mit meinem [Leib- und Magen-Editor](cp^TextMate), indem es dort standhaft behauptete `PySimpleGUI not found`.

Seltsam war (das war mir bisher noch nicht aufgefallen), daß der Editor ein [Anaconda Python](cp^Anaconda) 3.7 startete, während die Kommandozeile ein aktuelles (Anaconda-) Python 3.8.x aufrief. Und da fiel es mir wieder ein: [Da war doch etwas mit Anaconda und Catalina](MacOS Catalina und (Anaconda-) Python – 20210321). Das aktuelle Python mußte doch wegen der neuen Pingligkeit von Apple in einem lokalen Unterverzeichnis des Nutzers installiert werden, in meinem Fall also in `/Users/admin/opt/anaconda3`. Also schnell mal in den TextMate Preferences hineingeschaut und siehe da, mein Python-Pfad zeigte dort unverdrossen auf `Users/admin/anaconda3`, denn das war die letzte Anaconda-Installation vor meinem Update auf Catalina. Also schnell den Pfad geändert in

~~~
TM_PYTHON = /Users/admin/opt/anaconda3/bin/python
~~~

und alles war wieder schick. Jetzt können meine Versuche mit PySimpleGUI auch aus TextMate heraus erfolgen.