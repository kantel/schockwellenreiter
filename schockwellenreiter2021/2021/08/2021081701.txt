#title "Sicherheitsupdate für iCloud for Windows – 20210817"
#maintitle "Sicherheitsupdate für iCloud for Windows behebt Sicherheitslücken"
#prevp "2021081602"
#nextp "2021081702"

<%= html.getLink(imageref("sleeping-beauty-b"), "The Sleeping Beauty") %>

## Update für iCloud for Windows behebt Sicherheitslücken

Apple hat das [Update 12.5 von iCloud for Windows](https://support.apple.com/de-de/HT212607) freigegeben und korrigiert damit mehrere teils kritische Schwachstellen.

Das Update erhält man am einfachsten über die integrierte Software-Aktualisierung von Apple. *([[cert]])*