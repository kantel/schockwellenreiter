#title "Partikel über Partikel (in Python) – 20210815"
#maintitle "Python-Tutorials: Partikel über Partikel"
#prevp "2021081401"
#nextp "2021081601"

<%= html.getLink(imageref("catparticle-b"), "Yet Another Nyan Cat") %>

## Python-Tutorials: Partikel über Partikel (dieses Mal mit Pygame)

Das Thema »Partikelsysteme« und wie man sie in Python (mit der Turtle), [Pygame](cp^Pygame), [Processing.py](cp^processingpy) oder -- lang, lang ist es her -- in der [NodeBox](cp^NodeBox) programmiert, hatte ich ja schon öfter im *Schockwellenreiter* behandelt. Jüngst hat sich auch der *Python Enthusiast* der Partikel angenommen und als Beispiel »[Cat Particles with Pygame](https://pythonprogramming.altervista.org/cat-particles-with-pygame/)« programmiert. Herausgekommen ist eine weitere Version der [Nyan Cat](http://blog.schockwellenreiter.de/2020/10/2020102302.html), die durchaus ihren eigenen Reiz hat. Der Quellcode ist vollständig auf der Seite abgedruckt, so daß Euren eigenen Experimenten nichts im Wege steht.

Für das Programm gibt es vpm *Python Enthusiasten* noch zwei vorbereitende Tutorials zu Partikel-Systemen: Einmal »[How many particles you want? Yes](https://pythonprogramming.altervista.org/how-many-particles-you-want-yes/)« und dann »[How to bind a particle effect to an event](https://pythonprogramming.altervista.org/how-to-bind-a-particle-effect-to-an-event/)«. Beide sind ebenfalls interessant und zur Lektüre empfohlen.

Die Nyan Cat mit ihrem Partikelsystem hatten mich ja zu einer *Hommage an Daniel Shiffman* inspiriert, das »[Coding Train Unicorn Rainbow Game](http://blog.schockwellenreiter.de/2020/11/2020111901.html)«, das ich natürlich in Processing.py implementiert hatte. Weitere, mehr zufällig ausgewählte Beiträge zu Partikeln und Python im *Schockwellenreiter* sind:

- Das Tutorial »[Partikelsysteme mit der NodeBox 1 (Python)](http://blog.schockwellenreiter.de/2018/05/2018052501.html)« aus dem Jahr 2018.
- »[Ein Partikelsystem in Processing.py](http://blog.schockwellenreiter.de/2019/05/2019050501.html)« folgt dem [Processing (Java)](cp^Processing) Beispiel aus *Daniel Shiffmans* wunderbarem Buch »[The Nature of Code][a1]«.
- Ebenfalls schon aus dem Jahr 2018 ist das »[Partikelsystem mit Pythons Turtle-Graphik](http://blog.schockwellenreiter.de/2018/02/2018022102.html)«.

[a1]: https://www.amazon.de/Nature-Code-Simulating-Natural-Processing/dp/0985930802/ref=as_li_ss_tl?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3DV1DUB9OZHHK&keywords=the+nature+of+code&qid=1557055934&s=gateway&sprefix=the+nature,aps,152&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=ae9e97781ebac3c346922788af5cfdc4&language=de_DE

Bei so vielen Beiträgen zu Partikel-Systemen hier im <del>Blog</del> Kritzelheft sollte ich dazu vielleicht einmal eine Seite in [meinem Wiki](cp^Startseite) anlegen. Denn die Programmierung von Partikelsystemen macht Spaß. *Still digging!*