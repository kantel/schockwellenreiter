#title "Schau Mama, ein Hundebild! – 20210813"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2021081301"
#nextp "2021081401"

<%= html.getLink(imageref("spitzicato-b"), "Spitzicato") %>

**Schau Mama, ein Hundebild!** Es ist Freitag und daher gehört heute -- auch ohne besonderen Anlaß -- ein Hundebild in den *Schockwellenreiter*. Dabei gibt es sogar einen besonderen Anlaß: Weil Freitag der 13. mein Glückstag ist, habe ich mich aufgerafft und endlich meine Steuerunterlagen bei meiner Steuerberaterin vorbeigebracht. Und wegen Glückstag nicht nur die von 2019 (da wird die Steuererklärung spätestens zum 31.&nbsp;August 2021 fällig), sondern auch die von 2020 (da ist noch ein wenig Luft nach vorne). Mir gruselt immer ein wenig vor diesen Unterlagen und ich habe die letzten Wochen mit einem Mix aus Prokrastination und hektischer Betriebsamkeit daran gewerkelt.

Aber nun ist es geschafft und ich kann mich wieder verstärkt den Themen Simulation, Künstliche Intelligenz und *Creative Coding* mit Python, [Processing.py](cp^processingpy) und [TigerJython](cp^TigerJython) widmen. Dieses Wochenende soll der Auftakt dafür sein. Und da natürlich der kleine Spitz auch auf sein Recht auf Bespaßung pocht, rechnet nicht damit, daß es in den nächsten zwei Tagen viele Updates hier im <del>Blog</del> Kritzelheft geben wird. Denn darüber soll Euch das Hundephoto hinwegtrösten. Sollte es dennoch -- im Überschwang meiner neugewonnenen Zeit -- zu Updates kommen, freut Euch einfach darüber.

Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photogabi]]*