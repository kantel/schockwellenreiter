#title "Die Zahlen für den Monat Juli 2021 – 20210801"
#maintitle "Die Zahlen für den Monat Juli 2021"
#prevp "2021073101"
#nextp "2021080102"

<%= html.getLink(imageref("mediadaten202107-b"), "Die Mediadaten für den Juli 2021") %>

## Die Zahlen für den Monat Juli 2021

Wie zu Beginn jedes Monatsersten gibt es erst einmal ein paar Zahlen zum Vormonat, die hochtrabend manches Mal auch *Mediadaten* genannt werden: Im Juli 2021 hatte der *Schockwellenreiter* laut seinem nicht immer ganz zuverlässigen, aber (hoffentlich!) datenschutzkonformen [Neugiertool](cp^Piwik) exakt **4.725 Besucher** mit **8.823 Seitenansichten**. Wie immer täuscht die Exaktheit der Ziffern eine Genauigkeit der Zahlen nur vor. Trotzdem freue ich mich über jede Besucherin und jeden Besucher und bedanke mich bei allen meinen Leserinnen und Lesern.

😎 &nbsp; *Bleibt mir gewogen!*

Die fünf meistbesuchten Seiten im letzten Monat waren:

- [Updates, Updates! Und ein paar sonstige Neuigkeiten](Updates, Updates! – 20210705)
- [Zwei weitere freie Photo-Management-Tools](Zwei weitere freie Photo-Werkzeuge – 20210630) (auch für den Mac)
- Linkschleuder: [Luftschiffe und Luftschlösser](Luftschiffe und -schlösser – 20210728) (und mehr)
- Bal Populaire: [32 Jahre Kantel-Chaos-Team](Bal Populaire: 32 Jahre Kantel-Chaos-Team – 20210714)
- [Neu in meinem Wiki: Py5](Neu in meinem Wiki: Py5 – 20210703)