#title "Schade, doch kein Pygame 2 – 20210204"
#maintitle "Schade, doch kein Pygame 2.0"
#prevp "2021020401"
#nextp "2021020403"

## Worknote: Schade, doch kein Pygame 2.0

Im Dezember hatte ich [stolz verkündet](http://blog.schockwellenreiter.de/2020/12/2020120401.html), daß es mir gelungen sei, [Pygame 2.0](https://github.com/pygame/pygame/releases/tag/2.0.0) zu installieren. Das war anscheinend etwas verfrüht. Denn nachdem *Daniel Shiffman* sein [Projekt wiederaufgenommen hat](Video-Tutorials zum Wochenende – 20210129), die Programme aus seinem Buch »[The Nature of Code][a1]« neu von [Processing (Java)](cp^Processing) nach JavaScript, genauer nach [P5.js](cp^p5js) zu implementieren, wollte ich mein Projekt, diese Programme auch nach Python, genauer nach [Pygame](cp^Pygame) zu portieren, ebenfalls wieder aufnehmen.

[a1]: https://www.amazon.de/Nature-Code-Simulating-Natural-Processing/dp/0985930802?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3LAQ0VTPTOJ45&dchild=1&keywords=the+nature+of+code&qid=1611935202&sprefix=the+nature+of+%2Caps%2C183&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=6f450a912c4e95e726a33a85d60697f7&language=de_DE&ref_=as_li_ss_tl

<%= html.getLink(imageref("boilerplatepygame-b"), "Boilerplate mit Pygame für »The Nature of Code«") %>

Nun braucht Pygame eine Art *»Boilerplate«*, ein Template mit Code, der für jedes Pygame-Skript benötigt wird und bis auf kleine Varianten immer gleich ist. Zur Vorbereitung wollte ich mir solch ein Template bereitlegen. Bei der Implementierung stellte sich dann heraus, daß Pygame 2.0 zumindest auf meinem betagten MacBook Pro mit macOS Mojave und [(Anaconda-) Python](cp^Anaconda) 3.7.9 doch nicht so recht funzte. Beim Start gab es immer kurz einen schwarzen Bildschirm mit seltsamen Farb-Artefakten (und das sowohl bei Pygame wie auch bei [Pygame Zero](cp^Pygame Zero)). Anfangs hatte ich versucht, dies zu ignorieren, aber auf Dauer störte es mich doch. Und so habe ich ein Downgrade auf Pygame 1.9.6 durchgeführt und alles war wieder schick.

Ich sollte doch mehr meinen kategorischen Imperativ für SysAdmins und Programmierer beachten: Installiere nie eine Nuller-Version. Ich warte jetzt jedenfalls auf Pygame 2.1, bevor ich einen neuen Versuch wage.

Für Neugierige hier der Quellcode meines Pygame Boilerplates:

~~~python
import pygame
from pygame.locals import *
import os

# Hier wird der Pfad zum Verzeichnis des ».py«-Files gesetzt
file_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(file_path)

# Konstanten deklarieren
WIDTH, HEIGHT = 500, 500
TITLE = "🐍 Pygäme Boilerpläte 🐍"
FPS = 60
# BG = (234, 218, 184) # Packpapier-Farbe
BG = (49, 197, 244) # Coding Train Blue

# Pygame initialisieren und das Fenster und die Hintergrundfarbe festlegen
pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption(TITLE)
clock = pygame.time.Clock()

def update():
    pass

def draw():
    pass

keep_going = True
while keep_going:
    
    clock.tick(FPS)
    for event in pygame.event.get():
        if ((event.type == pygame.QUIT)
            or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE)):
            keep_going = False

    screen.fill(BG)
    update()
    draw()
    pygame.display.update()
    pygame.display.flip()

pygame.quit()
~~~

Warum ich mich bei meinem Python-Port für Pygame entschieden habe? Reine Neugierde, ich möchte nämlich herausfinden, wie gut sich [Pygames Vector2 Modul](https://www.pygame.org/docs/ref/math.html#pygame.math.Vector2) gegen Processings PVector-Klasse schlägt. *Still digging!*

<%= html.getLink(imageref("pygamebunt"), "cp^Pygame") %>