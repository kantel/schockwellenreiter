#title "Linkschleuder: Alles außer Python – 20210219"
#maintitle "Vermischte Links zum Wochenende – alles außer Python"
#prevp "2021021702"
#nextp "2021021902"

<%= html.getLink(imageref("arackhamsimas-b"), "Masterpieces of Art") %>

## Vermischte Links zum Wochenende – alles außer Python

Ich miste immer noch meinen Feedreader aus. Daher gibt es heute eine Linkschleuder mit vermischten Links aus den letzten drei Wochen, die alle möglichen Themen behandeln – nur nicht Python und Corona. Zu Eurer Erleichterung versuche ich auch, die Links grob in die Kategorien »Kulturelles«, »Zelluläre Automaten, KI, Chaos und Fraktale«, »Spiele und interaktive Geschichten«, »(Software-) Basteleien« und »Zensur« vorzusortieren:

### Kulturelles

- *Gerrit Lungershausen*: [Comics – Eine Einführung in Einführungen](https://comicgate.de/hintergrund/einfuehrungen/). Ein ganz persönlicher und unvollständiger Blick auf Comic-Einführungsbücher.

- *Coding da Vinci*: [Kulturhackathon-Präsentationen im Stream](https://www.heise.de/news/Coding-da-Vinci-Kulturhackathon-Praesentationen-im-Stream-5042007.html). Zehn Open-Source-Projekte mit Kulturdaten in modernem Gewand gab es Anfang Februar zur Präsentation des Hackathons Coding Da Vinci zu sehen.

- *Colin Marshall*: [The Internet Archive Now Digitizing 1,000,000+ Objects from a Massive Cinema History Library](https://www.openculture.com/2021/02/the-internet-archive-now-digitizing-1000000-objects-from-a-massive-cinema-history-library.html).

- Drei Saarländer erhielten den »Technik-Oscar« für ihre Grundlagenforschung im Bereich Lichtsimulation im Film. Damit hatten sie [Spiderman ins rechte Licht gerückt](https://www.tagesschau.de/wirtschaft/technologie/technik-oscar-saarland-101.html). Glückwunsch!

### Zelluläre Automaten, KI, Chaos und Fraktale

- *Gerhard Großmann*: [Ein Automat für Pixelmuster](https://charakterziffer.github.io/automat.html). Eine Einführung in *Wolframs* eindimensionale zelluläre Automaten.

- *Golo Roden*: [Grundbegriffe der künstlichen Intelligenz](https://www.heise.de/developer/artikel/Grundbegriffe-der-kuenstlichen-Intelligenz-5054503.html). Mit einigen Erklärvideos.

- *Mark Frauenfelder*: [Vilmonic, a beautiful artificial life simulator](https://boingboing.net/2021/02/10/vilmonic-a-beautiful-artificial-life-simulator.html). [Vilmonic](https://bludgeonsoft.org/) is an artificial life simulator and genetics and evolution sandbox game.

- *Christiano L. Fontana*: [Draw Mandelbrot fractals with GIMP scripting](https://opensource.com/article/21/2/gimp-mandelbrot). Create complex mathematical images with [GIMP](cp^Gimp)'s Script-Fu language. Mach ich, sobald das Klammerschlußgesetz in Kraft tritt, denn *Script-Fu* ist ein [Lisp](cp^Lisp)-Dialekt.

- *Milecia McGregor*: [What Is a Convolutional Neural Network](https://www.freecodecamp.org/news/convolutional-neural-network-tutorial-for-beginners/)? A Beginner's Tutorial for Machine Learning and Deep Learning.

### Spiele und interaktive Geschichten

- *Mark Frauenfelder*: [Read this excellent article about the making of Zork, the iconic 1977 text adventure](https://boingboing.net/2021/02/18/read-this-excellent-article-about-the-making-of-zork-the-iconic-1977-text-adventure.html). From *Aaron A. Reed's* essential newsletter, [50 Years of Text Games](https://if50.substack.com/p/1977-zork): the story of four MIT hackers who created Zork, one of the best-selling text adventure games of all time.

- *Golo Roden*: [Game Engine Black Book: Doom](https://www.heise.de/developer/artikel/RTFM-3-Game-Engine-Black-Book-Doom-5041383.html). Das Buch »[Game Engine Black Book: Doom][a1]« von *Fabien Sanglard* ist eine Zeitreise in die 1990er Jahre.

[a1]: https://www.amazon.de/Game-Engine-Black-Book-DOOM/dp/1099819776?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Game+Engine+Black+Book%3A+Doom&qid=1613746728&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=4b313027766f08784a86f544d205c441&language=de_DE&ref_=as_li_ss_tl

- *Raymond Schlitter*: Shmup Sprite Design, [Part 1](https://www.slynyrd.com/blog/2020/12/14/pixelblog-31-shmup-sprite-design) und [Part 2](https://www.slynyrd.com/blog/2021/2/15/pixelblog-32-shmup-design-part-2)

### (Software-) Basteleien

- Über Dithering: [Ditherpunk — The article I wish I had about monochrome image dithering](https://surma.dev/things/ditherpunk/) und [Ditherpunk 2 — beyond 1-bit](https://www.makeworld.space/2021/02/dithering.html). Alles, was Ihr über Dithering wissen wolltet, aber nie zu fragen Euch getraut habt.

- *Don Watkins*: [Generate QR codes with this open source tool](https://opensource.com/article/21/2/zint-barcode-generator). [Zint](http://www.zint.org.uk/) makes it easy to generate more than 50 types of custom barcodes.

- *Wulf Rohwedder* über das [PeerTube](https://peertube.netzbegruenung.de/about/peertube)-Netzwerk: [Eine Alternative mit Tücken](https://www.tagesschau.de/faktenfinder/peertube-101.html). Das PeerTube-Projekt soll Videoanbieter von kommerziellen Anbietern unabhängig machen und vermeintliche oder tatsächliche Zensur umgehen. Allerdings hat das alternative Netzwerk auch seine Schattenseiten.

- *Dieter Petereit*: [Jpeg XL: Das macht das neue Bildformat zum Schweizer Taschenmesser des Web](https://t3n.de/news/jpeg-xl-neues-bildformat-2021-1356055/). Mit Jpeg XL steht ein Bildformat vor der Standardisierung, das die sonst gängigen Standardformate ersetzen will. Statt PNG, Gif und Jpeg hieße es nur noch: Jpeg XL. Das könnte klappen.

- Noch einmal *Dieter Petereit*: [Jpeg.rocks: Diese Web-App komprimiert eure JPG-Dateien auch offline](https://t3n.de/news/jpegrocks-web-app-offline-bilder-1355712/). Die neue Web-App [Jpeg.rocks](https://jpeg.rocks/) bringt Mozjpeg mit Webassembly in den Browser. So kann Bildkompression komplett offline stattfinden. Bilder werden nicht ins Netz gesendet, sondern bleiben auf dem eigenen Rechner.

- *Michael Stal*: [Erste Schritte mit dem Raspberry Pi Pico – die Thonny-IDE](https://www.heise.de/developer/artikel/Erste-Schritte-mit-dem-Raspberry-Pi-Pico-die-Thonny-IDE-5057160.html). Nun ist mir doch noch ein wenig Python in diese Linkschleuder gerutscht.

- *LibreOffice 7.1*: [Frische Version mit erneuerter Markenstrategie](https://www.heise.de/news/LibreOffice-7-1-Frische-Version-mit-erneuerter-Markenstrategie-5045630.html). Mit Version 7.1 bekommt die freie Office-Suite den Namenszusatz Community, der aber nichts am Entwicklungsmodell ändert, wie die Document Foundation versichert.

### Zensur

- *Dieter Petereit* zum Dritten: [App aus Play-Store geflogen: Google ist das Titanic-Magazin zu nackt](https://t3n.de/news/app-play-store-geflogen-google-1355748/). Denn Jesus mit blankem Penis zu viel für die Kalifornier.

- Zensur 2: [Märchenbuch in Ungarn soll mit Warnhinweis versehen werden](https://hpd.de/artikel/achtung-keine-traditionelle-geschlechtsdarstellung-maerchenbuch-ungarn-soll-warnhinweis-versehen-18977) von *Joscha Wölbert*.

**War sonst noch was?** Ach ja, das Land [Nordrhein-Westfalen erwirbt für 2,6 Millionen Euro Lizenzrechte für Online-Enzyklopädien zur Verwendung an Schulen](https://netzpolitik.org/2021/statt-wikipedia-und-klexikon-nrw-zahlt-26-millionen-fuer-drei-jahre-online-brockhaus-an-schulen/). Es stellt sich die Frage, ob diese Entscheidung angesichts von freien Alternativen wie Wikipedia oder Klexikon ökonomisch und didaktisch vernünftig ist. Und ein [Schweizer Vikar geißelt Selbstbefriedigung als Unzucht und Selbstzerstörung](https://hpd.de/artikel/schweizer-vikar-geisselt-selbstbefriedigung-unzucht-und-selbstzerstoerung-18998). Stattdessen rät er [in einem YouTube-Video](https://www.youtube.com/watch?v=ADIna7tTYJ4), sich nackt in den Schnee zu legen und den Satan zu vertreiben. Entscheidet selber, welche der beiden Meldungen irrsinniger ist.