#title "Zwei parallele Fenster in TigerJython – 20210207"
#maintitle "Zwei parallele Fenster in TigerJython"
#prevp "2021020601"
#nextp "2021020801"

<%= html.getLink(imageref("dawnhack3-b"), "Zwei Fenster in TigerJython mit GameGrid") %>

## Worknote: Zwei parallele Fenster in TigerJython

[TigerJython](cp^TigerJython) und [Processing.py](cp^processingpy) sind sich ja in vielen Dingen sehr ähnlich. Was TigerJython jedoch Processing.py voraus hat, ist, daß man ohne große Verrenkungen mehrere Fenster parallel öffnen und bespielen kann. Das eröffnet die Möglichkeit, statt eines HUD *(Head-Up-Display)* im Spielefenster eine Art Textausgabe in einem zweiten Fenster zu implementieren (bei [Rogue-likes](cp^Rogue) eine häufige Praxis) und zusätzlich vielleicht noch ein *Inventory* in einem dritten Fenster. Ich habe das testweise mal mit der *GameGrid*-Bibliothek und zwei Fenstern ausprobiert. Dazu muß man beide Fenster getrennt initialisieren:

~~~python
scrn1 = makeGameGrid(width, height, ts, None, "sprites/map01.png", False, keyPressed = keyCallback)
scrn1.setTitle("DawnHack")

scrn2 = GameGrid(width, height, ts, None, "sprites/map01.png", False, keyPressed = keyCallback)
scrn2.setTitle("DawnHack 2")
~~~

Ein Fenster -- ich vermute mal das erste -- muß mit `makeGameGrid()`, die folgenden Fenster dürfen nur noch als Instanz der Klasse `GameGrid` initialisiert werden.

Im Folgenden muß man natürlich dem Programm mitteilen, welcher `Actor` zu welchem Fenster gehört:

~~~python
zombie = Enemy("sprites/zombie.png", Location(7, 11))
zombie2 = Enemy("sprites/zombie.png", Location(5, 13))
enemies.append(zombie)

scrn1.addActor(zombie, zombie.startLocation)
zombie.show()
scrn2.addActor(zombie2, zombie2.startLocation)
zombie2.show()


player = Player()
scrn1.addActor(player, Location(14, 14))
player.show()

player2 = Player()
scrn2.addActor(player2, Location(18, 12))
player2.show()
~~~

Und zum Schluß darf man nicht vergessen, für jedes Fenster getrennt die `show()`- und die `doRun()`-Methode zu implementieren.

Mehr macht mein kleines Skriptchen noch nicht, aber mit mehreren Fenstern kann man viel spielen und unglaubliche Dinge anstellen. Vor allen Dingen, weil -- [wie hier gezeigt wird](http://www.python-exemplarisch.ch/index_de.php?inhalt_links=navigation_de.inc.php&inhalt_mitte=graphics/de/animation.inc.php) -- dies nicht nur mit *GameGrid* möglich ist, sondern auch mit der *GPanel*-Bibliothek. Das Verfahren ist nahezu analog zu *GameGrid*. Ich kann mir sogar vorstellen, daß in einem *GameGrid*-Fenster eine Simulation läuft und in einem *GPanel*-Fenster parallel dazu eine graphische Aufbereitung erfolgt. Das habe ich aber noch nicht getestet, das steht für die Zukunft noch an. *Still digging!*

Den kompletten [Quellcode](https://gitlab.com/kantel/tigerjython/-/blob/master/dawnlike/dawnlike3.py) findet Ihr auf GitLab, dort gibt es natürlich auch die [Sprites](https://gitlab.com/kantel/tigerjython/-/tree/master/dawnlike/sprites).