#title "Künstler des Monats: Arthur Rackham – 20210201"
#maintitle "Künstler des Monats Februar 2021: Arthur Rackham"
#prevp "2021020101"
#nextp "2021020201"

<iframe width="852" height="480" src="https://www.youtube.com/embed/kTD0csU14Qs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Künstler des Monats Februar 2021: Arthur Rackham

[Arthur Rackham](https://de.wikipedia.org/wiki/Arthur_Rackham) war ein britischer Illustrator, der durch seine zahlreichen Buchillustrationen bekannt wurde. Er war Mitarbeiter der Zeitschriften Punch und The Graphic und illustrierte Werke wie »Alice im Wunderland«, »Äsops Fabeln«, »Ein Sommernachtstraum« und unzählige weitere Bücher.

Da *Arthur Rackham* 1939 starb, sind seine Werke in Deutschland und in allen anderen Ländern mit einer gesetzlichen Schutzfrist von 70 oder weniger Jahren nach dem Tod des Urhebers gemeinfrei. Sie werden daher in diesem Monat die Seiten des *Schockwellenreiters* zieren, für die ich kein anderweitiges Bild zur Verfügung habe, denn seine Zeichnungen sind sehr originell und oft auch humorvoll. Habt daher Spaß daran.