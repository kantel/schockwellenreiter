#title "Was von der Woche übrig blieb – 20210225"
#maintitle "Linkschleuder: Was von der Woche übrig blieb"
#prevp "2021022402"
#nextp "2021022601"

<%= html.getLink(imageref("arackhamparrot-b"), "The Parrot") %>

## Linkschleuder: Was von der Woche übrig blieb

Es bleibt immer mehr in meinem Feedreader hängen, als ich in Ruhe verarbeiten kann. Damit es dennoch nicht verloren geht, haue ich heute eine Linkschleuder zu gemischten Themen heraus, von Spielen und Basteleien, zu Künstlicher Intelligenz und Webworking, einer Tabellenkalkulation für den Mac und die LINKE kriegt auch noch ihr Fett weg.

### Computerspiele und Computer-Basteleien

- MagPi, das »offizielle« Raspberry-Pi-Magazin, bringt mit der jüngsten Ausgabe 103 den »[Month of Making](https://magpi.raspberrypi.org/articles/monthofmaking-in-the-magpi-magazine-issue-103)« zurück. [Gewürdigt](https://www.raspberrypi.org/blog/monthofmaking-is-back-in-the-magpi-103/) wird dies auch im Raspberry Pi Blog. Natürlich könnt Ihr auch die Ausgabe 103 des Raspberry-Pi-Magazins hier [kostenlos als PDF](https://magpi.raspberrypi.org/issues/103/pdf) herunterladen.

- *David Crookes* würdigt in der Wireframe *Jakub Dvorský*, den Gründer der tschechischen Spieleschmiede *Amanita Design*, die uns so wunderschöne Spiele wie [Samorost](https://de.wikipedia.org/wiki/Samorost) bescherten: [Paint and click: inside the unique adventures of Amanita Design](https://wireframe.raspberrypi.org/articles/paint-and-click-inside-the-unique-adventures-of-amanita-design).

- Noch einmal die Wireframe: *Antony de Fault* über narratives Design in Spielen: [What The Red Strings Club can tell us about writing good exposition](https://wireframe.raspberrypi.org/articles/what-the-red-strings-club-can-tell-us-about-writing-good-exposition).

### Webworking

- *Chris Coyier* stellt auf CSS-Tricks ein paar kostenlose Kurse für Webentwickler vor und meint: [Teaching Web Dev for Free is Good Business](https://css-tricks.com/teaching-web-dev-for-free-is-good-business/). Von JamStack über Git bis WordPress – fast alles ist dabei.

- *Shachee Swadia*: [How to Create a Diverging Bar Chart with a JavaScript Charting Library](https://www.freecodecamp.org/news/diverging-bar-chart-javascript/). Ein interessantes Visualisierungs-Tutorial, nicht nur für Basketball-Begeisterte und Fans der [LA Lakers](https://de.wikipedia.org/wiki/Los_Angeles_Lakers).

- Zu [Brython](http://www.brython.info/), dem **Br**owser P**ython** hatte ich vor [knapp einem Jahr](http://blog.schockwellenreiter.de/2020/04/2020042002.html) schon einmal ein Video-Tutorial hier im <del>Blog</del> Kritzelheft. Nun spülte ein sehr [umfangreiches schriftliches Tutorial zu Bython](https://realpython.com/brython-python-in-browser/) (kennt Ihr das noch, so was mit Buchstaben) in meinen Feedreader. Zeit, mich ebenfalls mal mit Brython zu beschäftigen.

- Falls es jemand braucht: *Jim Campbell* erklärt Euch, [How to Build A WordPress Website Locally - What You Need To Know](https://www.freecodecamp.org/news/how-to-build-a-wordpress-website-locally/).

[[dr]]<%= html.getLink(imageref("sowjetatheism-b"), "Sowjetisches atheistisches Plakat") %>[[dd]]

### Vermischtes

- *Helmut Ortner* fragt in seinem Kommentar »[Allah und die Linke](https://hpd.de/artikel/allah-und-linke-19012)«: Der fatale Umgang der Linken mit dem Islam – aus Furcht, damit Rechten Zündstoff zu liefern, schweigt sie. Galt nicht Religionskritik spätestens mit Voltaire einmal als Selbstverständlichkeit? *»Daß der mörderische Terror »nichts mit dem Islam zu tun hat«, das behaupten auch gerne weite Teile des linken Polit-Milieus. Wer den Islam als doktrinäre, meinungs- und frauenfeindliche Ideologie brandmarkt, wird schnell des Rassismus verdächtigt. Der Begriff Islamophobie wird zum Verteidigungs-Kampfbegriff gegen jede Kritik am Islam gemacht. Das kritische linke Welt-Bewußtsein – ansonsten jederzeit und allerorten abrufbar – kommt zum Erliegen. Was ist da los?«* Das frage ich mich auch.

- Jetzt zu etwas Erfreulichem (zumindest für macOS-Nutzer): Die Tabellen-Kalkulation [TableEdit für Mac](https://www.corecode.io/tableedit/) wird seit ihrem letzten Update auf die Version 1.4.6 kostenlos angeboten, zeigt aber in der *»Full Version«* dafür aber Hinweise auf die anderen Anwendungen des Entwicklers an. Eine freie *»Lite Version«* (ohne Werbung) könnt Ihr aber ebenfalls von der Seite des Programms herunterladen. Eine kurze Übersicht über das »Einfach-Excel« gibt es auf den [Seiten von ifun.de](https://www.ifun.de/einfach-excel-tableedit-fuer-mac-fortan-kostenlos-166773/).

**War sonst noch was?** Ach ja, *Christian Muschweg* untersucht vergnüglich in seinem Beitrag »[Die Drei Musketiere, Lady Winter und der Henker von Lille](https://comicgate.de/hintergrund/die-drei-musketiere-lady-winter-und-der-henker-von-lille-was-comicadaptionen-koennen/)«, was Comicadaptionen leisten können und *Till Kreutzer* fragt, [welche Regeln für die Erzeugnisse Künstlicher Intelligenz](https://irights.info/artikel/welche-regeln-gelten-fuer-die-erzeugnisse-kuenstlicher-intelligenz/30724) gelten. Wie so oft, haben beide Beiträge wenig miteinander zu tun, außer dem Umstand, daß sie etwa zur gleichen Zeit mein Interesse geweckt hatten.