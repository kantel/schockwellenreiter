#title "iOS-Update für iPhone 12 und 13"
#maintitle "Apple veröffentlicht iOS 15.1.1 für iPhone 12 und 13"
#prevp "2021111801"
#nextp "2021111803"

<%= html.getLink(imageref("haeuserammeer-b"), "Häuser am Meer") %>

## Apple veröffentlicht iOS 15.1.1 für iPhone 12 und 13

Ein Update außer der Reihe: Apple hat gestern Abend das [Update auf iOS 15.1.1](https://support.apple.com/de-de/HT212788) bereitgestellt, um den immer wieder mal auftretenden Anrufabbrüchen zu begegnen.

Das Update auf auf iOS 15.1.1 kann über OTA oder über iTunes erfolgen (*Over the Air* - in `Einstellungen > Allgemein > Softwareaktualisierung`, an ausreichender Akku-Kapazität und freien Speicherplatz sollte
gedacht werden). Eine vorherige Sicherung des Geräts ist wie immer sehr zu empfehlen. *([[cert]])*