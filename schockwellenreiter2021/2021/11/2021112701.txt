#title "Neue P5 (JavaScript) Tutorials – 20211127"
#maintitle "P5.js-Tutorials mit Tokyo EdTech und Happy Coding"
#prevp "2021112601"
#nextp "2021112702"

<iframe width="852" height="480" src="https://www.youtube.com/embed/-HHFKpps7fQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## P5.js-Tutorials mit Tokyo EdTech und Happy Coding

Im Rahmen meiner Überlegungen, mir [ein Chromebook zuzulegen](Ein Chromebook für den Schockwellenreiter? – 20211118), rückt natürlich wegen des [Online-Editors](https://editor.p5js.org/) [P5.js](cp^p5js), die JavaScript-Version von [Processing](cp^Processing), wieder verstärkt in meinen Fokus. Und als dann auch noch *Christian Thompson* (aka *[Tokyo EdTech](https://www.youtube.com/channel/UC2vm-0XX5RkWCXWwtBZGOXg)*), der Mann, der bisher Unglaubliches aus Pythons Turtle-Modul herausgekitzelt hatte, ein P5.js-Tutorial veröffentlichte, dachte ich mir, daß es an der Zeit sei, dieses und ein paar weitere Tutorials hier im *Schockwellenreiter* vorzustellen.

*Tokyo EdTechs* Tutorial ist ein viertelstündiges, simples [»Bouncing Ball«-Beispiel](https://www.youtube.com/watch?v=-HHFKpps7fQ) und sehr anfängerfreundlich, daher führt es hier die Reihe der YouTube-Videols an.

<iframe width="560" height="315" src="https://www.youtube.com/embed/tunk2fbRrxw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Die weiteren Videos sind von *Kevin Workman*, dem Menschen hinter dem Kanal [Happy Coding](https://www.youtube.com/channel/UCjvaMO0sbJyszOw7qNuvB_Q). Den Anfang macht sein anderthalbstündiges Kunstprojekt »[46 Million Turkeys](https://www.youtube.com/watch?v=tunk2fbRrxw)«, das an die Anzahl der Truthähne erinnert, die jährlich in Amerika zu *Thanksgiving* geschlachtet werden.

Aber auch seine anderen Videos, die ich herausgesucht habe, sind des Anschauens wert. Es sind dies:

1. [Pixel Swapper](https://www.youtube.com/watch?v=-TFPNRH26wU) (35 Minuten)
2. [Fading Grid - 2D Array in p5.js](https://www.youtube.com/watch?v=06HMnogJyGw) (45 Minuten)
3. [Oscillating Lines](https://www.youtube.com/watch?v=4ijQFzw8trw) (45 Minuten)
4. [Using Variables in p5.js to Draw a Spider](https://www.youtube.com/watch?v=Oop5KS8kDp4) (etwas mehr als eine Stunde)

**War sonst noch was?** Ach ja, von *[Isaac Emerald](https://www.youtube.com/channel/UCN9op29Q8j6qS1Dv8_uQBtQ)* stammt das fünfminütige Tutorial »[Multiple Buttons with MouseClicked (p5.js)](https://www.youtube.com/watch?v=u_nGmLU1Pn0)«. Kann man immer mal gebrauchen.