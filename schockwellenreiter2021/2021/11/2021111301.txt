#title "Email-Kommentare repariert – 20211113"
#maintitle "Email-Kommentare repariert"
#prevp "2021111202"
#nextp "2021111302"

<%= html.getLink(imageref("harmonieklee-b"), "Harmonie der Flora") %>

## Email-Kommentare repariert

Über den [Nachbarn aus Bonn](The Boy is Back in Town – 20211112#k02) erfahre ich, daß mein Postfach für die Email-Kommentare wohl vollgelaufen sei, weil mein Mailserver mit `Disc Quota exceeded` jede Zusammenarbeit verweigere. Warum muß ich das aus Bonn erfahren, und nicht aus Berlin, wo mein [Spielzeugprovider](https://www.strato.de) seinen Sitz hat?

Ein Blick in die Admin-Konsole zeigte mir, daß dieser Zustand wohl schon seit einiger Zeit existieren mußte, der zugewiesene Speicherlatz für dieses Postfach war weit über sein Limit hinaus vollgelaufen. Nun war dieses <del>Blog</del> Kritzelheft nie der Ort ausufernder Kommentarschlachten -- hier ging es eher ruhig zu -- und gelegentlich rutschte doch einmal ein (Email-) Kommentar durch, daher war mir dies nie aufgefallen.

Lange Rede, kurzer Sinn: Ich habe dem Postfach weitere 10 GB an Speicherplatz spendiert. Ihr könnt also wieder hemmungslos kommentieren (eigenhändig von mir getestet).

---

**1 (Email-) Kommentar**

---

<%= a("k01") %>

>Gestern Postfach voll, heute sollte es klappen: Gute Genesung und bewundernswert, wie Du in Dein neues Leben startest.

*– Reiner K.* <%= p("k01") %>

