#title "Links für meine Reise zur World Markdown – 20210324"
#maintitle "Links für meine Reise zur World Markdown"
#prevp "2021032401"
#nextp "2021032501"

<%= html.getLink(imageref("ausgetraeumt-b"), "Little Nemo – 1910-07-10") %>

## Eine Handvoll Links für meine Reise zur World Markdown

Da ich immer noch zwischen [Zettlr](cp^Zettlr) und [Obsidian](cp^Obsidian) als meine bevorzugte [Zettelkastensoftware](cp^Zettelkasten) schwanke (die eine hat mehr Features, die andere ist dafür nicht nur frei wie Freibier, sondern wirklich *Open Source)*, gibt es erst einmal eine Handvoll Links, die mich auf meinem Weg zur <del>Weltherrschaft</del> World Markdown begleiten sollen und mir weitere Informationen zur bevorstehenden Entscheidung (irgendwann muß ich mich ja mal entscheiden) liefern sollen:

- *Jan Schaller* beschreibt in den »[Notizen organisieren in Obsidian](https://papierlos-studieren.net/2021/03/23/notizen-organisieren-in-obsidian-mein-workflow/)« seinen Workflow. Dort stellt er eine Organisationsstruktur vor, die auch durchaus für mich interessant sein könnte. Zwar will und muß ich nicht mehr unbedingt eine Dissertation verfassen, aber ich hänge an mehreren Buch-Projekten, deren Notizen ich bisher unabhängig von der Software, in der ich die Bücher schreibe, verwalte. Dabei sind alles [Markdown](cp^Markdown)-Dateien, es bietet sich also geradezu an, sie in **einer** Software zu erfassen und zu schreiben. Die Ideen sind übrigens relativ unabhängig von Obsidian, ich könnte sie vermutlich genausogut in Zettlr realisieren.

- Von *Sascha Fast* gibt es im *Zettelkasten Blog* den Beitrag »[Use Case: Investing with the Zettelkasten Method](https://zettelkasten.de/posts/use-case-investing-zettelkasten-method/)«.

- Auf ifun.de wird ein Geheimtip vorgestellt: »[Typewriter für Mac sei ein umfangreicher Markdown-Editor](https://www.ifun.de/geheimtipp-typewriter-fuer-mac-ein-umfangreicher-markdown-editor-167572/)« mit drei Ansichten. Das Teil ist Freeware und man kann es im Mac App Store [kostenlos herunterladen](https://apps.apple.com/de/app/typewriter-for-markdown/id1556419263?mt=12). Sollte ich auf jeden Fall einmal testen.

[Blogdown](https://github.com/rstudio/blogdown), der *Static Site Generator* für [RMarkdown](cp^RMarkdown) und [RStudio](cp^RStudio) hat Mitte Januar das [Release 1.0 erreicht](https://blog.rstudio.com/2021/01/18/blogdown-v1.0/). Zeit, sich das Teil auch mal wieder genauer anzuschauen:

- Das Schwesterprojekt zu Blogdown heißt [Bookdown](https://pkgs.rstudio.com/bookdown/) und *Isabella Velásquez* hat ein längeres Tutorial dazu geschrieben: »[Creating a Book Manuscript Using Bookdown](https://ivelasq.rbind.io/blog/bookdown-manuscript/)«.

- *Andrew B. Collier* schreibt, wie er [die Größe von PNG-Bildern in Blogdown optimiert](https://datawookie.dev/blog/2021/02/blogdown-optimise-png-image-size/). Diese Information kann man immer mal gebrauchen, nicht nur für Blogdown.

Warum *Blogdown* und *Bookdown* für mich wieder interessant sind? Mit [RStudio 1.4](https://blog.rstudio.com/2021/01/19/announcing-rstudio-1-4/) möchte RStudio (und damit auch RMarkdown) eine [Heimat für R- und Python-Programmierer](https://blog.rstudio.com/2021/01/13/one-home-for-r-and-python/) sein. Und da ich mit [Jupyter](cp^Jupyter) einfach nicht warm werde, mir aber [RStudios](cp^RStudio) Notebooks recht gut gefallen (bis auf die Tatsache, daß sie bisher nur [R](cp^GNU R) sirklich unterstützten), könnte das eine interessante Alternative (nicht nur) für mein geplantes Projekt »[Numerisches Rechnen mit Python](Neu in meiner Bibliothek: Python-Kurs – 20210321)« sein. Denn obwohl ich Pythonista bin, nutze ich aus reinem Pragmatismus zur Zeit Markdown und Ruby für die Publikation meiner Seiten, warum sollten es in Zukunft nicht RMarkdown und RStudio werden? *Still digging!*