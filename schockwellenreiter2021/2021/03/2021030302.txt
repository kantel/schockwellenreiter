#title "Koch-Kurve und -Schneeflocke in Processing – 20210303"
#maintitle "Koch-Kurve und -Schneeflocke in Processing.py"
#prevp "2021030301"
#nextp "2021030401"

<%= html.getLink(imageref("kochkurve-b"), "Kochkurve in Processing (Python)") %>

## Koch-Kurve und -Schneeflocke in Processing.py

Nachdem ich [gestern](Die Kochsche Schneeflocke – 20210302) erfolgreich die Kochsche Schneeflocke in [TigerJython](cp^TigerJython) implementiert hatte, hatte es sich geradezu angeboten, dasselbe auch noch mit [Processing.py](cp^processingpy) und *Leah Buechleys* [Turtle-Bibliothek](cp^Turtle) zu versuchen. Dafür habe ich zuerst Processing.py einfach die [Koch-Kurve](https://de.wikipedia.org/wiki/Koch-Kurve) zeichen lassen (siehe Banner):

~~~python
add_library('Turtle')

# Konstanten-Deklaration
WIDTH = 400
HEIGHT = 200
it = 4          # Iterationstiefe

def setup():
    global koch
    size(WIDTH, HEIGHT)
    this.surface.setTitle("Koch-Kurve")
    background(232, 226, 7)
    koch = Turtle(this)
    noLoop()

def draw():
    koch.penUp()
    koch.goToPoint(40, 140)
    stroke(200, 23, 223)
    koch.penDown()
    koch.right(90)
    kochkurve(400, it)
    print("I did it, Babe!")

def kochkurve(length, d):
    if d == 0:
        koch.forward(length)
    else:
        kochkurve(length/3, d-1)
        koch.left(60)
        kochkurve(length/3, d-1)
        koch.right(120)
        kochkurve(length/3, d-1)
        koch.left(60)
        kochkurve(length/3, d-1)
~~~

Hier zeigte sich schon, daß nahezu der einzige Unterschied zu TigerJython (und zu [(C) Pythons Turtle-Modul](https://docs.python.org/3.7/library/turtle.html)) das andere Koordinatensystem ist. Während in TigerJython die Bibliotheken *GTurtle*, *GPanel* und *GameGrid* jeweils mit einem eigenen Koordinatensystem daherkommen, wollte *Leah Buechley*, daß ihre Schildkröte innerhalb des gewohnten Koordinatensystem der Wirtsumgebung funktioniert. Und das ist (per Default) die linke obere Ecke mit den Koordinaten `(0, 0)`.

<%= html.getLink(imageref("kochislandppy-b"), "Kochsche Schneeflocke in Processing (Python)") %>

Ansonsten unterscheidet sich der Code kaum von der TigerJython-Version:

~~~python
add_library('Turtle')

# Konstanten-Deklaration
WIDTH = 400
HEIGHT = 400

colors = [color(18, 184, 116), color(200, 23, 223),
          color(95, 145, 40), color(8, 124, 127)]

seiten = 4      # Anzahl der Seiten der Schneeflocke,
                # entweder 3 oder 4
it = 5          # Iterationstiefe

def setup():
    global koch
    size(WIDTH, HEIGHT)
    this.surface.setTitle("Kochsche Schneeflocke")
    background(232, 226, 7)
    koch = Turtle(this)
    noLoop()

def draw():
    koch.penUp()
    koch.goToPoint(80, 320)
    koch.penDown()
    # koch.right(90)
    schneeflocke(400, it)
    print("I did it, Babe!")

def kochkurve(length, d):
    if d == 0:
        koch.forward(length)
    else:
        kochkurve(length/3, d-1)
        koch.left(60)
        kochkurve(length/3, d-1)
        koch.right(120)
        kochkurve(length/3, d-1)
        koch.left(60)
        kochkurve(length/3, d-1)

def schneeflocke(length, d):
    for i in range(seiten):
        stroke(colors[i%4])
        kochkurve(length, d)
        koch.right(360/seiten)
~~~

Als Hommage an *[Leah Buechley](http://leahbuechley.com/)* habe ich die nach [ihr benannte Farbpalette](Neue Farbpaletten für meine Experimente – 20210118) verwendet, denn quietschebunt sind doch auch schöne Farben.&nbsp;🤓