#title "Schau Mama, ein Hundebild! – 20210312"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2021031203"
#nextp "2021031301"

<%= html.getLink(imageref("grosseaugen-b"), "Der kleine Spitz mit den großen Augen") %>

**Schau Mama, ein Hundebild!** Denn es ist Freitag und der kleine Spitz schaut mit seinen großen Augen verwundert in die Weltgeschichte. Dabei will er Euch nur darüber hinwegtrösten, daß es die nächsten zwei Tage keine oder nur wenige Updates hier im *Schockwellenreiter* geben wird. Denn zum einen möchte ich mich dem Buch »[Physik mit Python](Neu in meiner Bibliothek – 20210312)« widmen und auch erste Programmierexperimente damit wagen, zum anderen habe ich mir (und Euch) ja [ganz viele Videotutorials](Physik und Spiele in Python programmieren – 20210312) verordnet und zum dritten will und soll die kleine Fellnase auch noch bespaßt werden.

Die Wetterfrösche haben eher mäßige Aussichten verkündet. Wir lassen uns dennoch nicht verdrießen und hoffen auf sonnige Lücken. Ein schönes Wochenende Euch allen da draußen. Wir lesen uns spätestens am Montag wieder. *[[photogabi]]*