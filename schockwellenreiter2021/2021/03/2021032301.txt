#title "Mozilla veröffentlicht Firefox 87 – 20210323"
#maintitle "Security Alert: Mozilla veröffentlicht Firefox 87"
#prevp "2021032202"
#nextp "2021032302"

<%= html.getLink(imageref("littlenemo1907-04-28-b"), "Little Nemo Goes Whaling") %>

## Security Alert: Mozilla veröffentlicht Firefox 87

Die Entwickler des Mozilla Firefox haben die neue [Version 87](https://www.mozilla.org/en-US/firefox/87.0/releasenotes/) und die neue [Version ESR 78.9](https://www.mozilla.org/en-US/firefox/78.9.0/releasenotes/) veröffentlicht und darin auch wieder Sicherheitslücken behoben.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*