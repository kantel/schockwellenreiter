#title "Manchmal geht es doch schneller – 20210310"
#maintitle "Corona-Schutzimpfung: Manchmal geht es doch schneller"
#prevp "2021031001"
#nextp "2021031101"

<%= html.getLink(imageref("impfeinladung-b"), "Impfeinladung wegen Corona") %>

## Corona-Schutzimpfung: Manchmal geht es doch schneller als ich dachte

Da fand ich doch gerade eben ein Schreiben in meinem Briefkasten, das mich ganz offiziell anmutend zur Impfung gegen SARS-CoV-2 (Corona) einlädt. Ehrlich gesagt hatte ich damit frühestens in den Sommermonaten gerechnet. Die Online-Anmeldung war zwar etwas umständlich und vor dem 16. April 2021 waren auch keine Termine für die Erstimpfung mehr zu bekommen, aber jetzt habe ich es offiziell:

Am 16. April 2021 bekomme ich meine Erstimpfung und am 7. Mai 2021 meine Zweitimpfung. Geimpft werde ich mit dem Impfstoff von BioNTech-Pfizer. Jetzt muß ich mich nur noch schlaumachen, wie ich -- ohne mich einer großartigen Ansteckungsgefahr auszusetzen -- nach Treptow in die [Arena](https://de.wikipedia.org/wiki/Arena_Berlin#) komme. Aber auch das werde ich schaffen, ich bin ja schließlich schon groß.&nbsp;😷