#title "Schau Mama, ein Hundebild! – 20210305"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2021030501"
#nextp "2021030601"

<%= html.getLink(imageref("spitzstuhl-b"), "Der Spitz im Sitz in Britz (mit Blitz)") %>

**Schau Mama, ein Hundebild!** Als ich noch gearbeitet hatte, zog sich die Woche immer in die Länge und der Freitag wurde voller Sehnsucht erwartet. Seitdem ich dagegen Rentner bin, ist die Woche viel zu schnell vorüber und der Freitag kommt immer unerwartet. So auch heute, aber ich habe natürlich nicht vergessen, daß Freitags ein Hundebild in den *Schockwellenreiter* gehört, so will es die Tradition. Also heute den kleinen Spitz in seinem Sitz (in Britz). Wie immer nehme ich mir auch für die nächsten zwei Tage viel zu viel vor (lesen, schreiben, programmieren und die kleine Fellnase bespaßen), daher kündige ich schon einmal prophylaktisch an, daß es vermutlich bis Montag nur wenige bis gar keine Updates hier im <del>Blog</del> Kritzelheft geben wird und daß Euch das Photo des Hundes darüber hinwegtrösten soll.

Der Lockdown soll ja bis Ostern verlängert werden, aber wie im Herbst mit einer Art *Lockdown Light*. Ich bin da skeptisch, denn *Lockdwon Light* ist wie ein bißchen schwanger, das geht eigentlich nicht. Im Herbst wollte man damit Weihnachten retten -- das hat aber nicht geklappt. Nun spricht die Politik davon, daß Ostern gerettet werden muß, doch nennt mir einen Grund, warum es dieses Mal klappen sollte. Mit dem Impfen kommen sie auch nicht voran, daher sehe kommen, daß das nächste Weihnachten auch noch gerettet werden will …

Wie dem auch sei, genießt den Frühling (trotz Maske und Abstand) und bleibt gesund! Ein schönes Wochenende Euch allen da draußen, wir lesen uns spätestens am Montag wieder.