#title "Publizieren im Netz und Überleben im Netz – 20210322"
#maintitle "Publizieren im Netz und Überleben im Netz"
#prevp "2021032103"
#nextp "2021032202"

<%= html.getLink(imageref("little-nemo-19070908-b"), "Little Nemo – 19070908") %>

## Publizieren im Netz und Überleben im Netz

>Ohne die Wayback-Maschine wäre ein Großteil meines Schreibens nach 1995 jetzt weg. Seit dem Aufkommen von Online-Veröffentlichungen ist das Veröffentlichen eine schlechte Möglichkeit, um veröffentlicht zu bleiben. Wenn Publisher den Besitzer wechseln oder sterben, neigen die Werke ihrer Autoren dazu, zu verdampfen.   
>– [Jon Udell, 20. März 2021](https://blog.jonudell.net/2021/03/20/original-memories/)

Einer meiner frühen Internet-Ausstellungen war *Cut and Paste um 1900 – Der Zeitungsauschnitt in den Wissenschaften*, die ich 2002 am Berliner [Max-Planck-Institut für Wissenschaftsgeschichte](https://www.mpiwg-berlin.mpg.de/) (MPIWG) für und mit *[Anke te Heesen](https://de.wikipedia.org/wiki/Anke_te_Heesen)* konzipiert hatte. Das Internet war damals noch recht jung, doch wir alle glaubten an eine Zukunft des Netzes und daß unser Projekt die Zeiten überdauern würde. Doch wäre da nicht der [gedruckte Ausstellungskatalog][a1] der »realen Ausstellung«, würde nichts mehr an dieses Projekt erinnern. Denn die Online-Ausstellung -- obwohl als statische Seiten konzipert -- ist von den Webseiten des MPIWG verschwunden.

[a1]: https://www.amazon.de/Paste-1900-Zeitungsausschnitt-Wissenschaften-Kaleidoskopien/dp/3981058410?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Cut+and+Paste+der+zeitungsausschnitt+in+den+wissenschaften&qid=1616419350&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=7b16a47c396c8dcf157c28de35174534&language=de_DE&ref_=as_li_ss_tl

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1565925378&asins=1565925378&linkId=34536b284a49cf195df4b39f45e9e284&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Ich fand dies heraus, als ich wehmütig obiges Zitat von *[Jon Udell](https://en.wikipedia.org/wiki/Jon_Udell)* las und in Folge nach meinen frühen Projekten suchte. Udells Buch »[Practical Internet Groupware][a2]« von 1999 nimmt heute noch einen Ehrenplatz in meiner Bibliothek ein. Denn es hat nicht nur meine damalige Herangehensweise an die Ausstellungseiten zu *Cut and Paste* oder dem *[The Virtual Laboratory](https://vlp.mpiwg-berlin.mpg.de/index_html)* ([zusammen mit *Sven Dierig* und *Henning Schmidgen*](https://vlp.mpiwg-berlin.mpg.de/about/staff.html)) beeinflußt, sondern es hat auch bei der [Realisierung dieses <del>Blogs</del> Kritzelhefts als statische Seiten](http://blog.schockwellenreiter.de/2018/11/2018112704.html) und meines [emailbasierten Kommentarsystems](http://blog.schockwellenreiter.de/2013/07/20130728thread02.html) Pate gestanden. Doch auch, wenn damit für mich die Flucht aus den Datensilos der Netzgiganten abgeschlossen war, die oben (indirekt) von *Jon Udell* gestellte Frage, was denn mit »verwaisten« Seiten geschehe, bleibt weiter offen.

[a2]: https://www.amazon.de/Practical-Internet-Groupware-Classique-Us/dp/1565925378?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=practical+internet+groupware&qid=1616419712&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=d9f08f7b5841702a8ee7993e78c5343a&language=de_DE&ref_=as_li_ss_tl

Und nicht nur verwaiste Seiten, sondern auch verwaiste Werkzeuge können zu einem Problem werden. Das wurde mir kürzlich bewußt, als ich meinen Rechner das [Update auf *Catalina*](Von Mojave nach Catalina – 20210318) spendierte. Denn dabei traten (einige kleinere) Probleme mit [RubyFrontier](cp^RubyFrontier) auf, dem *Static Site Generator* (SSG), der auch im Hintergrund für diese Seiten und [mein Wiki](cp^Startseite) werkelt. Ich konnnte sie auch ohne *Matt Neuburg*, dem Schöpfer von RubyFrontier, [lösen](Von Mojave nach Catalina – 20210318), wenn auch hier nur sehr [knapp](Die Farben sind zurück! – 20210320). Ich bin Pythonista und kein [Ruby](cp^Ruby)-Programmierer, daher stellt sich bei mir die sorgenvolle Frage, was ist, wenn *Matt Neuburg* an RubyFrontier die Lust verliert oder aus anderen Gründen das Projekt nicht mehr warten kann? Er ist sicher nicht viel jünger wie ich und er betreut das Projekt meines Wissens alleine.

So steht die Frage nach einem Umstieg auf einen anderen SSG im Raum. Leicht fällt sie mir nicht, denn obwohl ich gelegentlich die *Mac only*-Falle beklage, in die mich RubyFrontier gestupst hat, liebe ich das Teil wegen seiner hohen Flexibilität und habe in den vergangenen knapp zehn Jahren, in denen ich RubyFrontier nutze und bis an die Grenzen ausreize, viel gelernt und viel erreicht.

[[dl]]<%= html.getLink(imageref("henry"), "https://blog.jkl.gg/henry-jekyll-theme/") %>[[dd]]

Was wären die Alternativen? [Hugo](cp^Hugo) jedenfalls nicht, denn das ist ein kompiliertes Programm und da kann ich nicht eingreifen und es an meine Bedürfnisse anpassen. Dann wäre da noch der Platzhirsch [Jekyll](cp^Jekyll), mit dem ich schon mehrfach für Dritte gespielt und es für sie angepaßt hatte. Außerdem ist mir da kürzlich im Beitrag »[Introducing Henry for Jekyll](https://blog.jkl.gg/henry-jekyll-theme/)« von *Kaushik Gopal* ein freier (MIT-Lizenz) [Jekyll-Tweak](https://github.com/kaushikgopal/henry-jekyll) untergekommen (Henry ist mehr als nur ein einfaches *Jekyll Theme*), das sich sogar als Docker-Container installieren läßt und somit überall laufen könnte. Ich werde mir dies auf jeden Fall mal genauer anschauen.

Für Jekyll spräche auch, daß der [CollectionBuilder](https://collectionbuilder.github.io/), das [hier vorgestellte](http://blog.schockwellenreiter.de/2020/07/2020071601.html) freie (MIT-Lizenz) und von Metadaten angetriebene Werkzeug, um digitale Sammlungen online zu stellen (so kehren ich elegant an den Ausgangspunkt meiner Überlegungen zurück&nbsp;🤓) und mit dem ich auch schon lange mal etwas [im Alice-Universum anstellen wollte](http://blog.schockwellenreiter.de/2020/08/2020081702.html), ebenfalls eine Jekyll-Anwendung ist. Dagegen spräche: Es ist wieder Ruby!

Aber was gäbe es sonst für Alternativen. Auf der Python-Seite stehen da eigentlich nur [Pelican](cp^Pelican) oder [Lektor](cp^Lektor) als hinreichend dokumentiertere und umfangreichere Static Site Generatoren im Raum. Mit all den anderen Tools, die mir in den letzten Jahren untergekommen sind, habe ich mich nicht wirklich anfreunden können[^1]. Daher werde ich mir erst einmal -- alleine um das Versprechen einzulösen, irgendetwas mit dem *CollectionBuilder* anzustellen -- mich mit Jekyll vertraut machen. Aber ich werde Pelican und Lektor auf gar keinen Fall aus den Augen verlieren. Und ich habe ja Zeit: Das Update auf *Catalina* war das letzte macOS-Update, das mein betagtes MacBook Pro noch verträgt. Daher kann ich erst einmal wie gewohnt mit RubyFrontier weiterspielen. Nächstes Jahr würden diese Seiten seit zehn Jahren mit RubyFrontier angetrieben. Dieses Jubiläum möchte ich doch noch gerne feiern.

[^1]: Eine Ausnahme ist [MkDocs](cp^MkDocs), das Teil ist wirklich sehr gut, aber eben auch zu sehr auf das Schreiben von (Software-) Dokumentationen fokussiert. Dafür nutze ich es aber häufig und mit Erfolg.

Aber zur Übung und weil ich Lust darauf habe, würde ich gerne einmal eine kleine, statische Online-Ausstellung mit dem *CollectionBuilder* erstellen (vielleicht sogar für jemand Drittes, in Zeiten von Corona und Lockdown sind Online-Ausstellungen eine Möglichkeit, trotzdem sein Publikum zu finden). *Still digging!*

---

**2 (Email-) Kommentare**

---

<%= a("k01") %>

>Nicht alles, aber doch ziemlich viel ist im Netz noch zu finden: <https://web.archive.org/web/20050404162516/http://www.cut-and-paste.de/>. 

*– Karlheinz F.* <%= p("k01") %>

---

<%= a("k0101") %>

>>Wow! Danke! Daß ich an die *Wayback Machine* nicht gedacht hatte, obwohl sie *Jon Udell* explizit in seinem Blogbeitrag erwähnt. Bis auf das damalige Impressum scheint die Site vollständig erhalten zu sein, so daß ich die Seiten restaurieren könnte, wenn die Urheberin das will. Ich werde sie mal darauf ansprechen …

*– Jörg Kantel* <%= p("k0101") %>

