#title "Neu in meinem Wiki: OBS Studio und OpenShot – 20210404"
#maintitle "Neu in meinem Wiki: OBS Studio und OpenShot (und ein Audacity-Update)"
#prevp "2021040203"
#nextp "2021040501"

<%= html.getLink(imageref("openshot-b"), "OpenShot Video-Editor") %>

## Neu in meinem Wiki: OBS Studio und OpenShot (und ein Audacity-Update)

Mittlerweile habe ich die [hier empfohlenen](Tutorial: Wie werde ich ein Vodcaster? – 20210321) Programme, mit denen man ein berühmter Vodcaster werden können soll, heruntergeladen und erste Tests damit gefahren. Da diese recht erfolgreich verlaufen sind, habe ich den Programmen **[OBS Studio](cp^OBS Studio)** und **[OpenShot](cp^OpenShot)** je eine Seite in [meinem Wiki](cp^Startseite) spendiert. **[Audacity](cp^Audacity)** lebte schon dort, aber ich habe der Seite ein Update spendiert.

Damit habe ich die ersten Voraussetzungen geschaffen, um <del>schön</del> (schön bin ich doch schon) reich und ein berühmter Influenzer zu werden. Jetzt muß ich nur noch meinen Schreibtisch aufräumen und meine Videokamera mit meinem MacBook Pro verheiraten, um professionellere Bilder zu bekommen, als sie die kleine Bildschirmkamera aufnimmt.