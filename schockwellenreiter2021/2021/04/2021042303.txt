#title "Schau Mama, ein Hundebild! – 20210423"
#maintitle "Schau Mama, ein Hundebild!"
#prevp "2021042302"
#nextp "2021042401"

<%= html.getLink(imageref("blonderspitz-b"), "Der Spitz kann auch brav") %>

**Schau Mama, ein Hundebild!** Ich mache es heute kurz: Es ist wieder Freitag und Freitags gehört einfach ein Hundebild in den *Schockwellenreiter*. Es kündigt dieses Mal keine Pause an, denn morgen begeht dieses <del>Weblog</del> Kritzelheft seinen 21. Geburtstag, wird also erwachsen. Daher werde ich morgen sicher noch ein paar Zeilen ins Internet schreiben.

Ansonsten steht das gewohnte Programm auf der Tagesordnung: Ich möchte lesen, schreiben und ein wenig programmieren und wie immer will und soll der kleine Spitz auch noch bespaßt werden. Daher: Ein schönes Wochenende Euch allen da draußen. Wir lesen uns diese Woche aber schon morgen zur Geburtstagsparty des *Schockwellenreiters* wieder. *[[photojoerg]]*