#title "Neues Arcade-Tutorial: Jump'n'Run – 20210429"
#maintitle "Spieleprogrammierung mit Pythons Arcade-Bibliothek: Jump'n'Run"
#prevp "2021042802"
#nextp "2021042902"

<%= imageref("jumpnrun") %>

## Spieleprogrammierung mit Pythons Arcade-Bibliothek: Jump'n'Run

Auf *Real Python* gibt es ein neues Tutorial für die [Python Arcade-Bibliothek](cp^Python Arcade Library), die nach eigenem Anspruch ein leicht zu erlernendes Python-Framework für die Spieleprogrammierung sein will. *Jon Fincher*, der uns vor einem Jahr auf den gleichen Seiten schon einmal einen ausführlichen und gut lesbaren »[Arcade Primer](https://realpython.com/arcade-python-game-framework/)« spendiert hatte, bringt nun ein noch ausführlicheres Tutorial: »[Build a Platform Game in Python With Arcade](https://realpython.com/platformer-python-arcade/)«.

*Platform Games* (auf deutsch: [»Jump'n'Run«-Spiele](https://de.wikipedia.org/wiki/Jump_%E2%80%99n%E2%80%99_Run) 🤓) sind tatsächlich das, was nach meinen bisherigen Experimenten Arcade am Besten kann, daher ist das Tutorial sicher auch für Anfänger in die Spieleprogrammierung geeignet. Besonders, weil es als Besonderheit nicht nur noch ein ausführliches Tutorial zu [Tiled](cp^Tiled) enthält, den freien (GPL) und plattformübergreifenden Map-Editor für 2D-Computerspiele, sondern auch zeigt, wie einfach man mit Tiled erstellte Maps in Arcade integrieren kann (da wird man nämlich sonst oft alleine gelassen).

Ich jedenfalls empfehle das Tutorial uneingeschränkt für alle, die sich mit der Arcade-Bibliothek auseinandersetzen wollen. Ich habe es daher auch in meiner **[Wiki-Seite zu Arcade](cp^Python Arcade Library)** aufgenommen.