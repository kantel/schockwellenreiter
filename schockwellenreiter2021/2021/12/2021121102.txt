#title "Der elektrische Trommelwirbel vom Tage – 20211211"
#maintitle "Der elektrische Trommelwirbel vom Tage"
#prevp "2021121101"
#nextp "2021121201"

<%= html.getLink(imageref("mildprocess-b"), "Mild Process") %>

## Der elektrische Trommelwirbel vom Tage

<%= a("01") %>
**Python und RStudio**: Sollte ich jemals eine IDE nutzen wollen oder müssen, dann wäre eine IDE ähnlich wie [RStudio](cp^RStudio) die IDE meiner Wahl. Hier hat jemand [drei Möglichkeiten zusammengestellt](https://www.rstudio.com/blog/three-ways-to-program-in-python-with-rstudio/), wie man Python-Skripte in [RStudio](cp^RStudio) entwickeln kann: »[Run Python Scripts in the RStudio IDE](https://www.rstudio.com/blog/three-ways-to-program-in-python-with-rstudio/#1-run-python-scripts-in-the-rstudio-ide)«, »[Use R and Python in a single project with the reticulate Package](https://www.rstudio.com/blog/three-ways-to-program-in-python-with-rstudio/#2-use-r-and-python-in-a-single-project-with-the-reticulate-package)« und »[Use your Python editor of choice within RStudio tools](https://www.rstudio.com/blog/three-ways-to-program-in-python-with-rstudio/#3-use-your-python-editor-of-choice-within-rstudio-tools)«. Mich interessiert vor allem die erste Möglichkeit. Sollte ich daher dringend einmal testen!&nbsp;🐍&nbsp;<%= pa("01") %>

<%= a("06") %>
**Python und Tkinter**: [GUI in Python Using Tkinter](https://dev.to/anjalikumawat2002/gui-in-python-using-tkinter-gfa) ist ein ausführliches Tutorial für beginnende Schlangenbändiger.&nbsp;🐉&nbsp;<%= pa("06") %>

<%= a("02") %>
**Es ist alles so schön bunt hier**: [The 7 Best Types of Visual Elements for Content Creation](https://www.makeuseof.com/best-visual-elements-for-content-creation/) zeigt, wie man seine Webseiten mit Bildern und Graphiken aufpeppen kann.&nbsp;✏️&nbsp;<%= pa("02") %>

<%= a("03") %>
**Es muß nicht immer Photoshop sein (1)**: [Open source photo processing with Darktable](https://opensource.com/article/21/12/open-source-photo-processing-darktable). Die freie Bildbearbeitungssoftware [Darktable](cp^Darktable) hatte ich hier im <del>Blog</del> Kritzelheft ja schon mehrfach [auf dem Schirm](In meinem Wiki aktualisiert: Darktable – 20210704).&nbsp;📷&nbsp;<%= pa("03") %>

<%= a("04") %>
**Es muß nicht immer Photoshop sein (2)**: Eine weitere freie Photoshop-Alternative ist natürlich [Gimp](cp^Gimp). Und davon ist die [aktuelle Version als Installationspaket für macOS](https://www.gimp.org/news/2021/12/06/gimp-2-99-8-for-macos/) verfügbar ([Download-Link](https://www.gimp.org/downloads/devel/)).&nbsp;🦊&nbsp;<%= pa("04") %>

<%= a("05") %>
**Freie Filmschnittsoftware**: [Best Free and Open Source Alternatives to Apple Final Cut Pro](https://www.linuxlinks.com/best-free-open-source-alternatives-apple-final-cut-pro/). Das im Beitrag erwähnte [OpenShot](cp^OpenShot) hatte ich ja schon einmal [ausführlich vorgestellt](Tutorial: Wie werde ich ein Vodcaster? – 20210321), [Kdenlive](https://kdenlive.org/en/) (Linux, macOS (experimentell)) und [Shortcut](https://www.shotcut.org/) (macOS, Linux, Windows) sind hingegen neu für mich.&nbsp;🎥&nbsp;<%= pa("05") %>

<%= a("07") %>
**War sonst noch was?** Ach ja, *Note Taking* geht auch [handschriftlich](https://www.routine.co/post/flow-notes) und [Glitch](cp^Glitch) hat sein [*best of* für den Monat November 2021](https://blog.glitch.com/post/last-month-on-glitch-the-november-2021-edition) ins Netz gestellt. Erinnert mich schmerzhaft daran, daß sowohl [Logseq](cp^Logseq) (wegen *Note Taking*) wie auch [Glitch](cp^Glitch) schon lange auf meiner Testliste stehen.&nbsp;🐟&nbsp;<%= pa("07") %>
