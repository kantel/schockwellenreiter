#title "Dillinger und Stack Edit (World Markdown (2)) – 20211213"
#maintitle "Markdown Online Editoren: Dillinger und Stack Edit"
#prevp "2021121301"
#nextp "2021121401"

<%= html.getLink(imageref("stackedit-bb"), "Markdown Online Editoren im Test: Stack Edit") %>

## Markdown Online Editoren im Test: Dillinger und Stack Edit

Da heute (und morgen) meine Reha ausfällt, weil sich Klinik und/oder Krankenkasse nicht über die für eine Verlängerung der Anschlußheilbehandlung (ich liebe dieses Wort) benötigten Unterlagen einig waren (man könnte meinen, sie machen das zum ersten Mal), konnte ich mein [Sonnabend gefaßtes Vorhaben](Chromebook Notes – 20211211), die Markdown Online Editoren [Dillinger](https://dillinger.io/) und [Stack Edit](https://stackedit.io/) auf ihre Tauglichkeit für mein Chromebook zu testen, schon früher als geplant einlösen. Als erstes hatte ich mir Dillinger vorgenommen:

<%= html.getLink(imageref("dillinger-b"), "Markdown Online Editoren im Test: Dillinger") %>

Die freie (MIT-Lizenz) Software kam mit einer unaufgeregten Nutzeroberfläche und erfüllte meine wichtigsten Anforderungen: Fußnoten wurden unterstützt, mathematische Formeln wurden mit Hilfe von [KaTeX](https://katex.org/) sauber gesetzt (sowohl mehrzeilig freistehend wie auch inline -- allerding werden die freistehenden Formeln links an den Rand gequetscht statt mittig zentriert gesetzt zu werden) und die nur im angelsächsischen Raum nützliche Unart der »Smart Quotes« wurden mir auch nicht aufgedrängelt. Und angeblich sollte Dillinger auch [Mermaid-Diagramme](https://mermaid-js.github.io/mermaid/#/) beherrschen, das habe ich bei meinem Test allerdings nicht hinbekommen.

Dillinger exportiert nach HTML, Styled HTML, Markdown und PDF. Der PDF-Export ist jedoch eine Enttäuschung, da wird einfach der Markdown-Quelltext als PDF ausgegeben. Daneben gibt es auch noch etliche Austauschmöglichkeiten (sowohl Im-, wie auch Export) mit diversen Clouddiensten, doch das waren nicht meine Anforderungen.

<%= html.getLink(imageref("stackedit-b"), "Markdown Online Editoren im Test: Stack Edit") %>

Das ebenfalls freie (Apache-Lizenz) [Stack Edit](https://stackedit.io/) ([Quellcode](https://github.com/benweet/stackedit) auf GitHub) kommt mit einer etwas verspielteren Oberfläche daher und zeigt auch gleich, was es alles beherrscht: Auch hier ist KaTeX für die mathematischen Formeln zuständig, die allerdings als freistehende Formeln mittig gesetzt werden. Natürlich beherrscht auch das Markdown von Stack Edit Fußnoten und im Gegensatz zu Dillinger funktionierten Mermaid-Diagramme sofort (Stack Edit soll mit Hilfe der [ABC-Notation](https://de.wikipedia.org/wiki/ABC_(Musiknotation)) sogar musikalische Noten setzen können, das konnte ich jedoch mangels Kenntnis von ABC nicht verifizieren).

Die Austauschmöglichkeiten sind bei Stack Edit noch umfangreicher als bei Dillinger, allerdings ist der Export als PDF (via HTML-Template oder Pandoc) zahlenden Sponsoren vorbehalten. Da es mir jedoch reicht, wenn ich die Markdown-Datei auf meinem Desktop bekomme (Pandoc kann ich selber), stört mich das nicht weiter.

Daher ist die Entscheidung erst einmal gefallen: Bis mir etwas Besseres unterkommt, ist Stack Edit der Online Markdown Editor meiner Wahl.

---

**2 (Email-) Kommentare**

---

<%= a("k01") %>

>Das ist aber mal ein Satz: 442 Chars bzw. 65 Worte - da ist Anschlußheilbehandlung nichts gegen.

*– Erwin L.* <%= p("k01") %>

---

<%= a("k02") %>

>Also bei mir scheint Dillinger (in Firefox) auch nach .pdf zuexportieren. Vielleicht etwas Chrome Book spezifisches? <https://imgur.com/a/w6Lebfa>

*– Robert T.* <%= p("k02") %>

