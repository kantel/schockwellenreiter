#title "Neu in meiner Bibliothek: Learn Python Visually – 20210520"
#maintitle "Neu in meiner Bibliothek: Learn Python Visually (mit Processing.py)"
#prevp "2021051901"
#nextp "2021052101"

<%= html.getLink(imageref("lesombresfrancaises-b"), "Les Ombres Francaises") %>

## Neu in meiner Bibliothek: Learn Python Visually (mit Processing.py)

Das Buch »[Learn Python Visually: Creative Coding with Processing.py][a1]« hatte ich schon Anfang Februar dieses Jahres (vor-) bestellt und die Lieferung hatte sich zweimal nach hinten hinausgezogen. Heute konnte ich es endlich an der Paketstation meines Vertrauens coronakonform (mit FFP2-Maske) abholen. Und ein erstes Durchblättern sagt mir: Das Warten hat sich gelohnt!

[a1]: https://www.amazon.de/Learn-Python-Visually-Tristan-Bunn/dp/1718500963?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=NO36E7D6H84V&dchild=1&keywords=learn+python+visually&qid=1621521745&sprefix=Learn+Python+%2Caps%2C174&sr=8-3&linkCode=ll1&tag=derschockwell-21&linkId=531428dd21155191854fcca232d7f255&language=de_DE&ref_=as_li_ss_tl

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1718500963&asins=1718500963&linkId=3325e7796ab84991bb972ffbe3ece0ee&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Auf über 250 durchgehend farigen Seiten mit vielen Illustrationen und in 11 Kapiteln führt der Autor *Tristan Bunn* lehr- und kenntnisreich in Python und [Processing.py](cp^processingpy) ein. Dabei gelingt ihm das Spagat, zwischen [Jython](cp^Jython), der Processing.py zugrundeliegenden Python-Implementierung, die ja bekanntlich noch und für lange Zeit ein Python 2.7 ist und sein wird, und dem aktuellen Python 3 zu vermitteln. Laut seiner Aussage sind alle Sketche -- wenn man von Processing-eigenen-Befehle abstrahiert -- sowohl unter Python 2.7 als auch unter Python 3 lauffähig. Das gelingt ihm natürlich nur, weil das Buch auf Englisch ist und er sich daher nicht mit den Widrigkeiten deutscher Umlaute oder französischer Sonderzeichen herumschlagen, also sich nicht um die Zeichenkodierung (UTF-8) kümmern muß.

Die Beispielprogramme sind nett ausgewählt und behandeln jeweils ein anderes Thema in -- soweit ich das bisher übersehen konnte -- der notwendigen Ausführlichkeit, aber dennoch kompakt. Ich glaube, das Buch wird für meine nächsten, sonntäglichen Processing.py-Experimente ein reiche Quelle vieler Inspirationen sein. *Still digging!*