#title "Neue Version(en) des Mediaplayer VLC – 20210512"
#maintitle "Neue Version(en) des Mediaplayer VLC"
#prevp "2021051203"
#nextp "2021051301"

<%= html.getLink(imageref("expedition-b"), "Was denkst Du über die Expedition?") %>

## Neue Version(en) des Mediaplayer VLC (3.0.13 und 3.0.14)

Was die [Gummiente kann](Warnung vor der aktuellen Gummiente – 20210512), das kann der [Mediaplayer VLC](cp^vlc) schon lange: Da wurde heute morgen erst die neue [Version 3.0.13 des beliebten Mediaplayer VLC](http://www.videolan.org/security/sb-vlc3013.html) angekündigt, die Sicherheitslücken beheben sollte, um dann wenige Minuten danach <del>einen Rückzieher zu machen</del> ein [weiteres Update anzukündigen](https://www.videolan.org/news.html#news-2021-05-11): »Sorry, 3.0.14 ist am Start, weil der VLC Installer 3.0.13 buggy ist.«

Einmal nur mit Profis arbeiten … *([[cert]])*