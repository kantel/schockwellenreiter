#title "Neu in meinem Wiki: Screenity – 20210120"
#maintitle "Neu in meinem Wiki: Screenity"
#prevp "2021012001"
#nextp "2021012101"

<iframe width="852" height="480" src="https://www.youtube.com/embed/7vNaJnH-pj0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Neu in meinem Wiki: Screenity

Die freie (MIT-Lizenz) Screen-Recording- und Screencast-Software »[Screenity](cp^Screenity)«, eine Google-Chrome-Erweiterung, hatte ich ja schon einmal im letzten November in diesem <del>Blog</del> Kritzelheft [vorgestellt](http://blog.schockwellenreiter.de/2020/11/2020112802.html). Als mir nun das Video mit [dieser ultimativen Lobhudelei](https://www.youtube.com/watch?v=7vNaJnH-pj0) in meine Timeline gespült wurde, konnte ich nicht anders: Ich mußte dem Teil **[eine Seite](cp^Screenity)** in [meinem Wiki](cp^Startseite) spendieren, insbesondere da es zu meinem gestrigen Beitrag »[(Digitale) Hilfen für das Homeschooling](Hilfen für das Homeschooling – 20210119)« wie Faust aufs Auge paßt.

**War sonst noch was?** Ach ja, es gibt noch mehr zum Thema »Homeschooling«. So fordert die Gesellschaft für Informatik, die Unabhängigkeit und Sicherheit von Schülern und Lehrern mit Open Source zu stärken. Sie [ruft nach digitaler Souveränität durch freie Software](https://www.heise.de/news/Homeschooling-Ruf-nach-digitaler-Souveraenitaet-durch-freie-Software-5029054.html). Und (nicht nur) in [Cottbus](https://heyalter.com/cottbus/) macht die Initiative [»Hey Alter](https://heyalter.com/)« [alte Rechner wieder fit](https://www.rbb24.de/studiocottbus/panorama/2021/01/cottbus-computer-initiative-alte-rechner-ueberarbeitung-schueler.html) -- und spendet sie. Wer also irgendwo noch ein altes Notebook herumliegen hat, der weiß nun, wohin sie es bringen und damit gutes tun kann -- auch in [Berlin](https://heyalter.com/berlin/).