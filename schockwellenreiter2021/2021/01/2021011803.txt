#title "Update in meinem Wiki: TigerJython – 20210118"
#maintitle "Update in meinem Wiki: TigerJython"
#prevp "2021011802"
#nextp "2021011901"

<%= html.getLink(imageref("robinsontiger-b"), "Robinson: Filming The Indian Tiger") %>

## Update in meinem Wiki: TigerJython

Mittlerweile habe ich -- auch wegen der Corona-Situation an den Schulen -- so vielen Menschen [TigerJython](cp^TigerJython) speziell für den (Informatik-) Unterricht empfohlen, daß ich mich genötigt sah, der **[Seite](cp^TigerJython)** in [meinem Wiki](cp^Startseite) ein Update zu verpassen.

Das Update betrifft in erster Linie den Punkt der Lizenz (TigerJython ist nun Open Source) und die erweiterte Dokumentation.

Und wundert Euch nicht, wenn ich in der nächsten Zeit auch mit TigerJython spielen und ich hier im <del>Blog</del> Kritzelheft berichten werde. Denn nun müssen Butter bei die Fisch.