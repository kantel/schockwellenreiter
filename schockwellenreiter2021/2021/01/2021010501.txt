#title "Korrektur: Die »andere« Peano-Kurve – 20210105"
#maintitle "Korrektur: Die »andere« Peano-Kurve mit Processing.py und der Turtle"
#prevp "2021010402"
#nextp "2021010502"

## Korrektur: Die »andere« Peano-Kurve mit Processing.py und der Turtle

Vor ein paar Tagen hatte ich eine raumfüllende Kurve [vorgestellt](Die »andere« Peano-Kurve — 20210101), die ich für die/eine [Peano-Kurve](https://de.wikipedia.org/wiki/Peano-Kurve) hielt. Ich hatte mich hier [auf meine Quelle](http://www.jython.ch/index.php?inhalt_links=navigation.inc.php&inhalt_mitte=turtle/rekursionen.inc.php) verlassen. Dabei hätte ein genaueres Hinsehen mir zeigen müssen, daß es sich um die [Hilbert-Kurve](https://de.wikipedia.org/wiki/Hilbert-Kurve) handelt, die ich im [letzten Monat](http://blog.schockwellenreiter.de/2020/12/2020120601.html) schon einmal mit der [Turtle](cp^Turtle) und [Processing.py](cp^processingpy) zeichnen ließ.

<%= html.getLink(imageref("peanosz-b"), "Die SZ-Peano-Kurve") %>

Dies muß ich natürlich korrigieren. Als Quelle habe ich dieses Mal das Buch »[Chaos – Bausteine der Ordnung][a1]« von *Heinz-Otto Peitgen*, *Hartmut Jürgens* und *Dietmar Saupe*, Berlin und Stuttgart (Springer und Klett-Cotta), 1994, Seite 547f. ausgewählt. Doch auch diese Bibel der Chaos-Forschung ist nicht ganz fehlerfrei, bei den Produktionsregeln fehlte in der zweiten Regel ein `Z`. Daher hier die korrigierten Regeln:

[a1]: https://www.amazon.de/Chaos-Bausteine-Ordnung-Heinz-Otto-Peitgen/dp/360895435X/ref=as_li_ss_tl?_encoding=UTF8&qid=1609843498&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=dc29324dd6cee0b4f7bd586a9d35eb3e&language=de_DE

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=360895435X&asins=360895435X&linkId=e15432670d9e6fe5add04d3bed7efa04&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

~~~
Axiom: S
Produktionsregeln: S -> SFZFS+F+ZFSFZ-F-SFZFS
                   Z -> ZFSFZ-F-SFZFS+F+ZFSFZ
~~~

Im Buch fehlte das erste `Z` der zweiten Regel. Alles weitere ergibt sich aus dem Quellcode:

~~~python
add_library('Turtle')
import math

num_gen = 4
len_seg = 4 
a = 90  # Winkel
axiom = "S"
sentence = axiom
rules = {"S": "SFZFS+F+ZFSFZ-F-SFZFS",
         "Z": "ZFSFZ-F-SFZFS+F+ZFSFZ",
}


def setup():
    global p
    size(400, 400)
    this.surface.setTitle("SZ-Peano-Kurve")
    background(50)
    strokeWeight(1)
    stroke(150, 255, 100)
    p = Turtle(this)
    p.right(a)
    generate(num_gen)
    noLoop()

def draw():
    p.penUp()
    p.goToPoint(40, height-40)
    p.penDown()
    plot(p)
    print("I did it, Babe")
    
def generate(n):
    global len_seg, sentence
    for i in range(n):
        # len_seg *= 0.5
        next_sentence = ""
        for c in sentence:
            next_sentence += rules.get(c, c)
        sentence = next_sentence
    # print(sentence)
        
def plot(p):
    for c in sentence:
        if c == "F":
            p.forward(len_seg)
        elif c == "+":
            p.left(a)
        elif c == "-":
            p.right(a)
~~~

Das Ergebnis gleicht nun endlich der [ersten Peano-Kurve aus dem Wikipedia-Artikel](https://de.wikipedia.org/wiki/Peano-Kurve). Ich habe natürlich dem fehlerhaften Beitrag ein Update verpaßt, das auf diese Seite verlinkt.