#title "Haben will! Neues Buch zu Processing (Python) – 20210124"
#maintitle "Haben will! Neues Buch zu Processing.py"
#prevp "2021012301"
#nextp "2021012402"

<%= html.getLink(imageref("intelectualsummerholyday-b"), "The Intellectual Summer Holyday") %>

## Haben will! Neues Buch zu Processing.py

Es erscheint (voraussichtlich) erst am 23. März 2021, wurde aber schon vor einigen Tagen auf den Seiten der *Processing Foundation* [stolz angekündigt](https://discourse.processing.org/t/new-python-mode-processing-py-book/27258). Das Buch [Learn Python Visually][a1] von *Tristan Bunn* will ein Anfänger-Buch sein, das in die Grundlagen der Computerprogrammierung in einem visuellen, kunstorientierten Kontext einführt. Es nutzt dafür meine Lieblingsprogrammierumgebung und -sprache, nämlich [Processing.py](cp^processingpy), die Python-Version von [Processing (Java)](cp^Processing).

[a1]: https://www.amazon.de/Learn-Python-Visually-Tristan-Bunn/dp/1718500963?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=Learn+Python+Visually+%E2%80%93+Creative+Coding+with+Python+Mode+for+Processing&qid=1611489422&sr=8-1-fkmr0&linkCode=ll1&tag=derschockwell-21&linkId=7708c02e3a76ac185f564c9f2fe1ea5d&language=de_DE&ref_=as_li_ss_tl

[[dr]]<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=DE&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=derschockwell-21&language=de_DE&marketplace=amazon&region=DE&placement=1718500963&asins=1718500963&linkId=fac0a10db1e333a753d85e5f81ec6658&show_border=true&link_opens_in_new_window=true"></iframe>[[dd]]

Obwohl vom Anspruch her ein Anfängerbuch, zeigen mir die [Vorschau](https://nostarch.com/Learn-Python-Visually) und das [frei herunterladbare Kapitel 9 des Buches](https://nostarch.com/download/samples/LearnPythonVisually_sampleChapter.pdf) (<%= imageref("pdf") %>), daß ich das Buch unbedingt haben will, nein haben muß. Es scheint mit vielen, einfallsreichen Beispielen gespickt, bunt bebildert und witzig geschrieben zu sein.

Nach [Getting Started with Processing.py][a2] von *Allison Parrish*, *Ben Fry* und *Casey Reas* und [Math Adventures with Python][a3] von *Peter Farrell* (ein Buch, das ebenfalls bei *No Starch Press* erschienen ist und das ich unbedingt empfehlen kann) ist »Learn Python Visually« nun das dritte Buch zu Processing.py. Das bestätigt mich darin, daß es sich lohnt, in diese Python-Variante (genauer: [Jython](cp^Jython)-Variante) Zeit und Kraft zu investieren.

[a2]: https://www.amazon.de/Getting-Started-Processing-py-Interactive-Processings/dp/1457186837?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Processing.py&qid=1611492003&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=2c25c0fccc93aa26316d79ce77f33b24&language=de_DE&ref_=as_li_ss_tl
[a3]: https://www.amazon.de/Math-Adventures-Python-Fractals-Automata/dp/1593278675?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2N51ORVJLJLQA&dchild=1&keywords=math+adventures+with+python&qid=1611492229&sprefix=Math+Ad%2Caps%2C162&sr=8-1&linkCode=ll1&tag=derschockwell-21&linkId=f3ef2902461793d2c7153f69ffb1eba5&language=de_DE&ref_=as_li_ss_tl

In den letzten Monaten sind -- auch bedingt durch eine gewisse, psychologische Corona-Müdigkeit -- meine eigenen Anstrengungen, ein deutschsprachiges [Processing.py-Buch zu schreiben](http://py.kantel-chaos-team.de), ins Hintertreffen geraten. Nun bin ich motiviert, die Arbeit daran wieder aufzunehmen.