#title "Matplotlib-Figure als Datei sichern – 20210117"
#maintitle "Matplotlib-Zeichnung als Datei sichern"
#prevp "2021011601"
#nextp "2021011801"

<%= html.getLink(imageref("mandelbrot_ausschnitt-b"), "Mandelbrot-Ausschnitt") %>

## Worknote: Matplotlib-Zeichnung als Datei sichern

Wenn man -- wie ich gestern -- [lauter schöne, bunte Mandelbrötchen gebacken hat](ProcessingPy, Python und Numba im Vergleich – 20210116), möchte man die Ergebnisse natürlich auch abspeichern können. Die [Matplotlib](cp^Matplotlib) stellt dafür die Methode `matplotlib.pyplot.savefig()` [zur Verfügung](https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.savefig.html). Per Default erkennt sie das abzuspeichernde Dateiformat an der Dateiendung, man kann ihr aber auch ein Format als Parameter mitgeben. Sie beherrscht ziemlich viele Formate, ich habe bei meinen Tests die Dateien als PNG, JPEG, PDF und SVG herausgerendert.

Die Bedienung ist ziemlich einfach: Zu meinem gestrigen Programm sind lediglich ein paar Zeilen hinzugekommen:

~~~python
import os

...

maxiter = 100

file_path = os.path.dirname(os.path.abspath(__file__))
image_folder = os.path.join(file_path, "images")

@nb.njit(locals = dict(c = nb.complex128, z = nb.complex128))

...

axis.tick_params(left = False)

fig.savefig(os.path.join(image_folder, "mandelbrot_ausschnitt.png"),
            bbox_inches = "tight", pad_inches = 0.0)

root = tk.Tk()
~~~

Oben habe ich mit dem Modul `os` dem Skript explizit den Pfad zum `image`-Verzeichnis angegeben, das ist -- zumindest bei mir -- notwendig, weil der [Editor meines Vertrauens](cp^TextMate) immer das Projektverzeichnis als `cwd` behandelt, und das ist in den meisten Fällen nicht das Verzeichnis, in dem meine Skripte liegen.

Abgespeichert wird die Datei dann mit dieser Zeile:

~~~python
fig.savefig(os.path.join(image_folder, "mandelbrot_ausschnitt.png"),
            bbox_inches = "tight", pad_inches = 0.0)
~~~

Mit `os.path.join()` wird das Skript plattformunabhängig. Denn damit ist es egal, ob der Verzeichnistrenner ein Slash (`/`) (macOS, Linux) oder ein Backslash (`\`) (Windows) ist.

Der Parameter `bbox_inches = "tight"` sorgt zusammen mit `pad_inches = 0.0` dafür, daß tatsächlich nur die Zeichnung ohne (in diesem Fall) störende Ränder abgespeichert wird.

Die Größe der Zeichnung kann man mit

~~~python
fig = Figure(figsize = (6, 6), facecolor = "white")
~~~

beeinflussen. Bei `figsize = (6, 6)` erhält man ein 462x462 Pixel großes Bild, um den Banner für diesen Beitrag zu bekommen, habe ich den Parameter `figsize` auf `(14, 14)` gesetzt. Damit wurde das Bild auf 1078x1078 Pixel aufgeblasen. Allerdings hatte dies -- wie man oben sieht -- kaum einen Einfluß auf die Qualität des Ausschnittes.&nbsp;🤓

Das Abspeichern der Zeichnung als Datei stört übrigens in keiner Weise die Anzeige der herausgerenderten Mandelbrotmenge. Das Fenster mit dem `Tk.Canvas` ploppte auch nach dem Abspeichern des Bildes problemlos auf.