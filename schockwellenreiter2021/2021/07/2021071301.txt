#title "Mozilla veröffentlicht neue Firefox-Version 90 – 20210713"
#maintitle "Mozilla veröffentlicht neue Firefox-Version 90"
#prevp "2021071201"
#nextp "2021071302"

<%= html.getLink(imageref("mountutsu-b"), "Mount Utsu") %>

## Mozilla veröffentlicht neue Firefox-Version 90

Die Entwickler des Mozilla Firefox haben die neue [Version 90](https://www.mozilla.org/en-US/firefox/90.0/releasenotes/) und die [Version ESR 78.12](https://www.mozilla.org/en-US/firefox/78.12.0/releasenotes/) veröffentlicht und darin auch wieder Sicherheitslücken behoben.

Außerdem [blockiert die neue Version](https://stadt-bremerhaven.de/firefox-90-0-fuer-windows-macos-und-linux-erschienen/) Facebook-Skripte von Drittanbietern, damit Nutzer nicht getrackt werden, und die [Unterstützung für FTP](https://linuxnews.de/2021/07/firefox-90-endgueltig-ohne-ftp/) ist nun endgültig gestorben.

Firefox weist selbst auf dieses Update hin. Diese Prozedur kann aber auch wie immer über das Menü `Hilfe > Über Firefox` angestoßen werden. *([[cert]])*