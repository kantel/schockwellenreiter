#title "JupyterLite ist Jupyter ohne Backend – 20210716"
#maintitle "JupyterLite läuft auf WebAssembly und benötigt kein Backend"
#prevp "2021071602"
#nextp "2021071604"

<%= html.getLink(imageref("jupyterlite-b"), "Jupyter ohne Backend") %>

## JupyterLite läuft auf WebAssembly und benötigt kein Backend

Das Project-Jupyter-Team hat mit [JupyterLite](https://blog.jupyter.org/jupyterlite-jupyter-%EF%B8%8F-webassembly-%EF%B8%8F-python-f6e2e41ab3fa) eine Distribution von [JupyterLab](cp^JupyterLab) veröffentlicht, die eigenständig im Browser läuft. Grundlage für die Ausführung von Python-Code ist [Pyodide](http://blog.schockwellenreiter.de/2019/04/2019041701.html), das wiederum auf WebAssembly aufsetzt. Neben Python lassen sich weitere Kernels für andere Programmiersprachen anbinden. [Nähere Informationen gibt es bei Tante Heise](https://www.heise.de/news/Data-Science-JupyterLite-laeuft-auf-WebAssembly-und-benoetigt-kein-Backend-6140250.html).

Okay, auch JupyterLite ist eine browserbaserte Anwendung, aber JupyterLite läuft vollständig unabhängig im Browser und kommt ohne Anbindung an einen Python-Jupyter-Server aus. Das heißt, Jupyter Lite kann als statische Website ohne weitere Abhängigkeiten deployed werden (siehe hier ein [Beispiel auf ReadTheDocs](https://jupyterlite.readthedocs.io/en/latest/)). Das bedeutet nicht nur, daß Jupyter Lite als lokale Anwendung auf dem Desktop läuft (das konnten auch [Jupyter](cp^Jupyter) und [JupyterLab](cp^JupyterLab) schon), sondern wird auch sicher die Wünsche vieler Anwender erfüllen, die für ein autonomes und dezentrales Web agieren.

Für mich interessant ist auch die Tatsache, daß Jupyter Lite neben einem JavaScript- auch -- wie obiger [Screenshot](Jupyter ohne Backend) zeigt -- einen [P5.js-Kernel](cp^p5js) mitbringt. Damit sollte es möglich sein, Processing (in der JavaScript-Version) auch mit [Numpy](cp^Numpy) oder [Scipy](cp^Scipy) zu verheiraten. Ich bin schon ganz aufgeregt, ich muß das unbedingt baled testen.