#title "Pan Reading to a Woman"
#prevp "beardsley10"
#nextp "beardsley12"

<%= html.getLink(imageref("pan"), "https://fineartamerica.com/featured/cover-design-aubrey-vincent-beardsley.html") %>

*Aubrey Beardsley* Cover Design for The Yellow Book.