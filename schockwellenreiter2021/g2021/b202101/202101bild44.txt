#title "An analyst writes …"
#prevp "202101bild43"
#nextp "202101bild45"

<%= html.getLink(imageref("analystwrites"), "https://www.creativereview.co.uk/heath-robinsons-commercial-art/") %>

An analyst writes »I am pleased to say your inks withstood the most severe tests for permanency«. Werbezeichnung von [William Heath Robinson](https://www.creativereview.co.uk/heath-robinsons-commercial-art/) ca. 1924.