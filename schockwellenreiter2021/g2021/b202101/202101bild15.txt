#title "Der kleine Luftschiffer (The Air-Ship, 1922)"
#prevp "202101bild14"
#nextp "202101bild16"

<%= html.getLink(imageref("kleinerluftschiffer"), "https://www.royalacademy.org.uk/art-artists/work-of-art/the-air-ship") %>

Zeichnung von *[William Heath Robinson](https://www.royalacademy.org.uk/art-artists/name/william-heath-robinson)* (1872 - 1944).