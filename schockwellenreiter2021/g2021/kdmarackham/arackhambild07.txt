#title "Unterwasser-Nymphen"
#prevp "arackhambild06"
#nextp "arackhambild08"

<%= html.getLink(imageref("underwater-women"), "https://www.flickr.com/photos/schockwellenreiter/50914056773/") %>

Zeichnung von Arthur Rackham. *(Der Moderations-Bot* 🤖 *von* [[flickr]] *hat das Photo als »Eingeschränkt« eingestuft.)*