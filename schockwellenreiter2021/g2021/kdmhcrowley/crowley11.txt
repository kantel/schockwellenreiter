#title "Hot or Cold?"
#prevp "crowley10"
#nextp "crowley12"

<%= html.getLink(imageref("hotorcold"), "https://commons.wikimedia.org/wiki/File:Hot\_or\_Cold\_MET\_DP856181.jpg") %>

*Herbert Crowley* circa 1911-1924. *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Hot_or_Cold_MET_DP856181.jpg))*