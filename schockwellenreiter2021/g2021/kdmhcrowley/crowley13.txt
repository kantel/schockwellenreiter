#title "Kohl (Cabbages)"
#prevp "crowley12"
#nextp "crowley14"

<%= html.getLink(imageref("kohl"), "https://commons.wikimedia.org/wiki/File:Cabbages,\_a\_\%22Wiggle\_Much%22\_design\_MET\_DP856179.jpg") %>

*Cabbages*, a »Wiggle Much« design, circa 1910. *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Cabbages,_a_%22Wiggle_Much%22_design_MET_DP856179.jpg))*