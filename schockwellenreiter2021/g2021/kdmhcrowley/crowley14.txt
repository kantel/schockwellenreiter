#title "Coming Storm"
#prevp "crowley13"
#nextp "crowley15"

<%= html.getLink(imageref("comingstorm"), "https://commons.wikimedia.org/wiki/File:Coming\_Storm%E2%80%93Mother\_Smelling\_Thunder,\_possibly\_a\_%22Wiggle\_Much%22_\design\_MET\_DP856537.jpg") %>

Coming Storm – Mother Smelling Thunder, possibly a »Wiggle Much« design, circa 1910. *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Coming_Storm%E2%80%93Mother_Smelling_Thunder,_possibly_a_%22Wiggle_Much%22_design_MET_DP856537.jpg))*