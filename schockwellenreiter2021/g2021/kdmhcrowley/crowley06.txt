#title "Ein Lied ohne Worte"
#prevp "crowley05"
#nextp "crowley07"

<%= html.getLink(imageref("liedohneworte"), "https://commons.wikimedia.org/wiki/File:A\_Song\_Without\_Words,\_a\_%22Wiggle\_Much%22\_design\_MET\_DP856185.jpg") %>

A Song Without Words, a »Wiggle Much« design, circa 1910. *(Bildquelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:A_Song_Without_Words,_a_%22Wiggle_Much%22_design_MET_DP856185.jpg))*