#title "Peter Pan: Hausbau"
#prevp "hiroshige19"
#nextp "hassall02"

<%= html.getLink(imageref("peterpanhouse"), "https://es.paperblog.com/john-hassall-ilustraciones-para-peter-pan-493289/") %>

[Illustration zu Peter Pan](https://es.paperblog.com/john-hassall-ilustraciones-para-peter-pan-493289/) (1907) von [John Hassall](https://en.wikipedia.org/wiki/John_Hassall_(illustrator)) (1868–1948).