#title "Dover to Calais Tube"
#prevp "hassall02"
#nextp "hassall04"

<%= html.getLink(imageref("dovercalaistube"), "https://www.artfund.org/whats-on/exhibitions/2021/05/22/john-hassall-illustrator-and-poster-artist") %>

Dover to Calais Tube, Change at High Street, Kensington, [Poster von *John Hassall*](https://www.heathrobinsonmuseum.org/whats-on/john-hassall-illustrator-and-poster-artist/).