#title "Snow White (Schneewitchen)"
#prevp "hassall05"
#nextp "hassall07"

<%= html.getLink(imageref("snowhitehassall"), "https://www.flickr.com/photos/schockwellenreiter/51368692327/") %>

Illustration von *John Hassall* (1868-1948).