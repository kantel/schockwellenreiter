#title "Der Sturm (The Wind)"
#prevp "hassall01"
#nextp "hassall03"

<%= html.getLink(imageref("dersturm"), "https://rbkclocalstudies.files.wordpress.com/2015/06/the-wind.jpg") %>

[John Hassall: The Poster Man](https://rbkclocalstudies.wordpress.com/2015/06/11/john-hassall-the-poster-man/) by *Dave Walker* (The Library Time Machin vom 11. Juni 2015).