#title "Lotka-Volterra-Gleichung 1"
#prevp "202102bild24"
#nextp "202102bild26"

<%= html.getLink(imageref("lv01"), "https://www.flickr.com/photos/schockwellenreiter/50982959492/") %>

Numerische Lösung mit dem Euler-Verfahren. Screenshot, mehr [hier](Mathematik mit TigerJython: Lotka-Volterra - 20210227) …