#title "Harmonie der Flora"
#prevp "klee01"
#nextp "klee03"

<%= html.getLink(imageref("harmonieklee"), "https://www.deutschlandfunkkultur.de/paul-klee-in-der-fondation-beyeler-die-abstrakte-dimension-100.html") %>

*Paul Klee*, Harmonie der nördlichen Flora.