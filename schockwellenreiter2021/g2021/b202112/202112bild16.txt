#title "Christmas with the Cartoonists"
#prevp "202112bild15"
#nextp "202112bild17"

<%= html.getLink(imageref("christmascartoon"), "https://1.bp.blogspot.com/-6ciBLIb4Yxg/YbP1oyGd2cI/AAAAAAAAxLc/8CehFaHVM3sgt7VyqIBFNEtIr9frebGOACNcBGAsYHQ/s1499/YP-01-1909%2B12%2B20%2BA.jpg") %>

*Jimmy Swinnerton*, [Chicago Examiner, Dec 20, 1909](http://john-adcock.blogspot.com/2021/12/christmas-with-cartoonists.html).