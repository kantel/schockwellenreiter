#title "Der Spitz und der Frühling 2021"
#prevp "202104bild01"
#nextp "202104bild03"

<%= html.getLink(imageref("fruehlingsspitzterrasse"), "https://www.flickr.com/photos/schockwellenreiter/51086198106/") %>

Aufgenommen am 31. März 2021. *[[photojoerg]]*