#title "Grunewaldstraße (Westend)"
#prevp "wunderwald12"
#nextp "wunderwald14"

<%= html.getLink(imageref("berlinwestend"), "http://www.artnet.de/k%C3%BCnstler/gustav-wunderwald/stra%C3%9Fe-in-berlin-westend-grunewaldstra%C3%9Fe-dr-KVG5m96kKTJUawcRAYA2") %>

*Gustav Wunderwald*: Straße in Berlin-Westend (Grunewaldstraße), 1918 - 1920.