#title "Karolingerplatz 4"
#prevp "wunderwald13"
#nextp "wunderwald15"

<%= html.getLink(imageref("karolingerplatz4"), "http://forst-grunewald.de/wp-content/gallery/karolingerplatz/1926-gustav-wunderwald-karolingerplatz-4-1600.jpg") %>

*Gustav Wunderwald*, [Karolingerplatz 4](http://forst-grunewald.de/?page_id=11073), Villa an Brandwand, ca. 1926