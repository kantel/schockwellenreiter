#title "Viktoria-Ufer"
#prevp "wunderwald16"
#nextp "wunderwald18"

<%= html.getLink(imageref("viktoriaufer"), "https://sammlung-online.berlinischegalerie.de:443/eMP/eMuseumPlus?service=ExternalInterface&module=collection&objectId=142555&viewType=detailView") %>

*Gustav Wunderwald*, Viktoria-Ufer in Berlin-Spandau (1927).