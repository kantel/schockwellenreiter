#title "Titus und Jonny beim (Not-) Training"
#prevp "202109bild08"
#nextp "202109bild10"

<%= html.getLink(imageref("titusundjonnytraining"), "https://www.flickr.com/photos/schockwellenreiter/51430376081/") %>

Aufgenommen während des Bahnstreiks der GDL am 6. September 2021 auf dem Tempelhofer Feld. *[[photojoerg]]*