#title "Große Blätter im Herbst (mit Spitz)"
#prevp "202111bild10"
#nextp "202111bild12"

<%= html.getLink(imageref("blaetterimherbst"), "https://www.flickr.com/photos/schockwellenreiter/51689093585/") %>

Aufgenommen am 18. November 2021. *[[photojoerg]]*