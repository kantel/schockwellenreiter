#title "Picnic at Sherman's Point (1900)"
#prevp "202111bild08"
#nextp "202111bild10"

<%= html.getLink(imageref("babbprost"), "https://publicdomainreview.org/collection/babb-photographs") %>

Photo (Ausschnitt) von *[Theresa Babb](https://publicdomainreview.org/collection/babb-photographs)* (1868-1948).